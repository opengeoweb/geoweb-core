/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { LayerTree, recurseNodes } from './getCapabilities';

describe('utils/getCapabilities', () => {
  describe('recurseNodes', () => {
    it('should have hierarchical layer structure', () => {
      const webmapJSNodeListInput = {
        text: 'A',
        leaf: false,
        children: [
          {
            text: 'B',
            leaf: false,
            path: 'A',
            children: [
              {
                text: 'C',
                path: 'A/B',
                leaf: false,
                children: [
                  {
                    text: 'd',
                    name: 'd',
                    leaf: true,
                    path: 'A/B/C',
                    children: [],
                  },
                ],
              },
            ],
          },
        ],
      };
      const layerTree = {
        leaf: false,
        title: 'A',
        path: [],
        children: [
          {
            leaf: false,
            title: 'B',
            path: ['A'],
            children: [
              {
                leaf: false,
                title: 'C',
                path: ['A', 'B'],
                children: [
                  {
                    leaf: true,
                    name: 'd',
                    title: 'd',
                    path: ['A', 'B', 'C'],
                    children: [],
                  },
                ],
              },
            ],
          },
        ],
      };
      expect(recurseNodes(webmapJSNodeListInput as LayerTree)).toEqual(
        layerTree,
      );
    });
  });
});
