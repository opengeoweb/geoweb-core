/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { generateLayerId } from '..';
import { WMLayer } from '../geoweb-webmap/index';
import { LayerType } from '../store/mapStore/types';

export const defaultReduxLayerRadarColor = {
  service: 'https://testservice',
  name: 'RAD_NL25_PCP_CM',
  title: 'RAD_NL25_PCP_CM',
  format: 'image/png',
  style: 'rainbow/nearest',
  enabled: true,
  layerType: LayerType.mapLayer,
  dimensions: [],
  id: 'layerid_1',
};

export const defaultReduxLayerRadarKNMI = {
  service: 'https://testservice',
  name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
  title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
  format: 'image/png',
  style: 'knmiradar/nearest',
  enabled: true,
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T13:30:00Z',
    },
  ],
  id: 'layerid_2',
};

export const WmdefaultReduxLayerRadarKNMI = new WMLayer({
  service: 'https://testservice',
  name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
  title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
  format: 'image/png',
  style: 'knmiradar/nearest',
  enabled: true,
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T13:30:00Z',
    },
  ],
  id: 'layerid_2',
});

export const makeGeoservicesRadarLayer = (): WMLayer => {
  return new WMLayer({
    service:
      'https://adaguc-server-knmi-geoservices.pub.knmi.cloud/wms?DATASET=RADAR&',
    name: 'RAD_NL25_PCP_CM',
    title: 'RAD_NL25_PCP_CM',
    format: 'image/png',
    style: 'knmiradar/nearest',
    enabled: true,
    layerType: LayerType.mapLayer,
    dimensions: [
      {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-04-15T00:00:00Z',
      },
    ],
    id: generateLayerId(),
  });
};

export const defaultReduxServices = {
  'https://testservice': {
    layers: [
      {
        name: 'RAD_NL25_PCP_CM',
        text: 'RADAR NL COLOR',
        leaf: true,
        path: '',
      },
      {
        name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        text: 'RADAR NL KNMI',
        leaf: true,
        path: '',
      },
      {
        name: 'MULTI_DIMENSION_LAYER',
        text: 'Multi Dimension Layer',
        leaf: true,
        path: '',
      },
    ],
    layer: {
      RAD_NL25_PCP_CM: {
        styles: [
          {
            attr: {},
            Name: {
              attr: {},
              value: 'rainbow/nearest',
            },
            Title: {
              attr: {},
              value: 'rainbow/nearest',
            },
            LegendURL: {
              attr: {
                width: '300',
                height: '400',
              },
              Format: {
                attr: {},
                value: 'image/png',
              },
              OnlineResource: {
                attr: {
                  'xlink:type': 'simple',
                  'xlink:href':
                    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
                },
              },
            },
            index: 0,
            nrOfStyles: 1,
            title: 'rainbow/nearest',
            name: 'rainbow/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
            abstracttext: 'No abstract available',
          },
        ],
      },

      RADNL_OPER_R___25PCPRR_L3_KNMI: {
        styles: [
          {
            attr: {},
            Name: {
              attr: {},
              value: 'knmiradar/nearest',
            },
            Title: {
              attr: {},
              value: 'knmiradar/nearest',
            },
            LegendURL: {
              attr: {
                width: '300',
                height: '400',
              },
              Format: {
                attr: {},
                value: 'image/png',
              },
              OnlineResource: {
                attr: {
                  'xlink:type': 'simple',
                  'xlink:href':
                    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=knmiradar/nearest',
                },
              },
            },
            index: 0,
            nrOfStyles: 8,
            title: 'knmiradar/nearest',
            name: 'knmiradar/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=knmiradar/nearest',
            abstracttext: 'No abstract available',
          },
          {
            attr: {},
            Name: {
              attr: {},
              value: 'precip/nearest',
            },
            Title: {
              attr: {},
              value: 'precip/nearest',
            },
            LegendURL: {
              attr: {
                width: '300',
                height: '400',
              },
              Format: {
                attr: {},
                value: 'image/png',
              },
              OnlineResource: {
                attr: {
                  'xlink:type': 'simple',
                  'xlink:href':
                    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip/nearest',
                },
              },
            },
            index: 1,
            nrOfStyles: 8,
            title: 'precip/nearest',
            name: 'precip/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip/nearest',
            abstracttext: 'No abstract available',
          },
          {
            attr: {},
            Name: {
              attr: {},
              value: 'precip-transparent/nearest',
            },
            Title: {
              attr: {},
              value: 'precip-transparent/nearest',
            },
            LegendURL: {
              attr: {
                width: '300',
                height: '400',
              },
              Format: {
                attr: {},
                value: 'image/png',
              },
              OnlineResource: {
                attr: {
                  'xlink:type': 'simple',
                  'xlink:href':
                    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-transparent/nearest',
                },
              },
            },
            index: 2,
            nrOfStyles: 8,
            title: 'precip-transparent/nearest',
            name: 'precip-transparent/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-transparent/nearest',
            abstracttext: 'No abstract available',
          },
          {
            attr: {},
            Name: {
              attr: {},
              value: 'precip-gray/nearest',
            },
            Title: {
              attr: {},
              value: 'precip-gray/nearest',
            },
            LegendURL: {
              attr: {
                width: '300',
                height: '400',
              },
              Format: {
                attr: {},
                value: 'image/png',
              },
              OnlineResource: {
                attr: {
                  'xlink:type': 'simple',
                  'xlink:href':
                    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-gray/nearest',
                },
              },
            },
            index: 3,
            nrOfStyles: 8,
            title: 'precip-gray/nearest',
            name: 'precip-gray/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-gray/nearest',
            abstracttext: 'No abstract available',
          },
          {
            attr: {},
            Name: {
              attr: {},
              value: 'precip-rainbow/nearest',
            },
            Title: {
              attr: {},
              value: 'precip-rainbow/nearest',
            },
            LegendURL: {
              attr: {
                width: '300',
                height: '400',
              },
              Format: {
                attr: {},
                value: 'image/png',
              },
              OnlineResource: {
                attr: {
                  'xlink:type': 'simple',
                  'xlink:href':
                    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-rainbow/nearest',
                },
              },
            },
            index: 4,
            nrOfStyles: 8,
            title: 'precip-rainbow/nearest',
            name: 'precip-rainbow/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-rainbow/nearest',
            abstracttext: 'No abstract available',
          },
          {
            attr: {},
            Name: {
              attr: {},
              value: 'precip-blue/nearest',
            },
            Title: {
              attr: {},
              value: 'precip-blue/nearest',
            },
            LegendURL: {
              attr: {
                width: '300',
                height: '400',
              },
              Format: {
                attr: {},
                value: 'image/png',
              },
              OnlineResource: {
                attr: {
                  'xlink:type': 'simple',
                  'xlink:href':
                    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-blue/nearest',
                },
              },
            },
            index: 5,
            nrOfStyles: 8,
            title: 'precip-blue/nearest',
            name: 'precip-blue/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-blue/nearest',
            abstracttext: 'No abstract available',
          },
          {
            attr: {},
            Name: {
              attr: {},
              value: 'precip-blue-transparent/nearest',
            },
            Title: {
              attr: {},
              value: 'precip-blue-transparent/nearest',
            },
            LegendURL: {
              attr: {
                width: '300',
                height: '400',
              },
              Format: {
                attr: {},
                value: 'image/png',
              },
              OnlineResource: {
                attr: {
                  'xlink:type': 'simple',
                  'xlink:href':
                    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-blue-transparent/nearest',
                },
              },
            },
            index: 6,
            nrOfStyles: 8,
            title: 'precip-blue-transparent/nearest',
            name: 'precip-blue-transparent/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-blue-transparent/nearest',
            abstracttext: 'No abstract available',
          },
          {
            attr: {},
            Name: {
              attr: {},
              value: 'pseudoradar/nearest',
            },
            Title: {
              attr: {},
              value: 'pseudoradar/nearest',
            },
            LegendURL: {
              attr: {
                width: '300',
                height: '400',
              },
              Format: {
                attr: {},
                value: 'image/png',
              },
              OnlineResource: {
                attr: {
                  'xlink:type': 'simple',
                  'xlink:href':
                    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=pseudoradar/nearest',
                },
              },
            },
            index: 7,
            nrOfStyles: 8,
            title: 'pseudoradar/nearest',
            name: 'pseudoradar/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=pseudoradar/nearest',
            abstracttext: 'No abstract available',
          },
        ],
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: '2020-03-13T14:40:00Z',
          },
        ],
      },

      MULTI_DIMENSION_LAYER: {
        styles: [
          {
            attr: {},
            Name: {
              attr: {},
              value: 'rainbow/nearest',
            },
            Title: {
              attr: {},
              value: 'rainbow/nearest',
            },
            LegendURL: {
              attr: {
                width: '300',
                height: '400',
              },
              Format: {
                attr: {},
                value: 'image/png',
              },
              OnlineResource: {
                attr: {
                  'xlink:type': 'simple',
                  'xlink:href':
                    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
                },
              },
            },
            index: 0,
            nrOfStyles: 1,
            title: 'rainbow/nearest',
            name: 'rainbow/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
            abstracttext: 'No abstract available',
          },
        ],
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: '2020-03-13T14:40:00Z',
          },
          {
            name: 'flight level',
            units: 'hft',
            currentValue: '625',
            values: '25,325,625',
          },
          {
            name: 'elevation',
            units: 'meters',
            currentValue: '9000',
            values: '1000,5000,9000',
          },
        ],
      },

      LAYER_WITHOUT_TIME: {
        styles: [
          {
            attr: {},
            Name: {
              attr: {},
              value: 'rainbow/nearest',
            },
            Title: {
              attr: {},
              value: 'rainbow/nearest',
            },
            LegendURL: {
              attr: {
                width: '300',
                height: '400',
              },
              Format: {
                attr: {},
                value: 'image/png',
              },
              OnlineResource: {
                attr: {
                  'xlink:type': 'simple',
                  'xlink:href':
                    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
                },
              },
            },
            index: 0,
            nrOfStyles: 1,
            title: 'rainbow/nearest',
            name: 'rainbow/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
            abstracttext: 'No abstract available',
          },
        ],
        dimensions: [
          {
            name: 'flight level',
            units: 'hft',
            currentValue: '625',
            values: '25,325,625',
          },
        ],
      },
    },
  },
};

export const multiDimensionLayer = {
  service: 'https://testservice',
  id: 'multiDimensionLayerMock',
  name: 'MULTI_DIMENSION_LAYER',
  title: 'MULTI_DIMENSION_LAYER',
  layerType: LayerType.mapLayer,
  enabled: true,
  dimensions: [
    {
      name: 'flight level',
      units: 'hft',
      currentValue: '625',
      values: '25,325,625',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T14:40:00Z',
    },
  ],
};

export const flDimensionLayer = {
  service: 'https://testservice',
  id: 'multiDimensionLayerMock',
  name: 'MULTI_DIMENSION_LAYER',
  title: 'MULTI_DIMENSION_LAYER',
  layerType: LayerType.mapLayer,
  enabled: true,
  dimensions: [
    {
      name: 'flight level',
      units: 'hft',
      currentValue: '625',
      values: '25,325,625',
    },
    { name: 'time', units: 'ISO8601', currentValue: '2020-03-13T14:40:00Z' },
  ],
};

export const WmMultiDimensionLayer = new WMLayer({
  service: 'https://testservice',
  id: 'multiDimensionLayerMock',
  name: 'MULTI_DIMENSION_LAYER',
  title: 'MULTI_DIMENSION_LAYER',
  layerType: LayerType.mapLayer,
  enabled: true,
  dimensions: [
    {
      name: 'flight level',
      units: 'hft',
      currentValue: '625',
      values: '25,325,625',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T14:40:00Z',
      values: '2020-03-13T10:40:00Z/2020-03-13T14:40:00Z/PT5M',
    },
  ],
});

export const multiDimensionLayer2 = {
  service: 'https://testservicedimensions',
  id: 'multiDimensionLayerMock2',
  name: 'netcdf_5dims',
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'member',
      units: 'member number',
      currentValue: 'member1',
      values: 'member1,member2,member3,member4',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2017-01-01T00:25:00Z',
    },
  ],
};

export const WmMultiDimensionLayer2 = new WMLayer({
  service: 'https://testservicedimensions',
  id: 'multiDimensionLayerMock2',
  name: 'netcdf_5dims',
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'member',
      units: 'member number',
      currentValue: 'member1',
      values: 'member1,member2,member3,member4',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2017-01-01T00:25:00Z',
    },
  ],
});

export const WmMultiDimensionLayer3 = new WMLayer({
  service: 'https://testservicedimensions',
  id: 'multiDimensionLayerMock2',
  name: 'netcdf_5dims',
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'member',
      units: 'member number',
      currentValue: 'member1',
      values: 'member1,member2,member3,member4',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2017-01-01T00:25:00Z',
    },
  ],
});

export const WmMultiDimensionServices = {
  'https://testservicedimensions': {
    layers: [
      {
        name: 'netcdf_5dims',
        text: 'netcdf_5dims',
        leaf: true,
        path: '',
      },
    ],
    layer: {
      netcdf_5dims: {
        styles: [
          {
            attr: {},
            Name: {
              attr: {},
              value: 'rainbow/nearest',
            },
            Title: {
              attr: {},
              value: 'rainbow/nearest',
            },
            LegendURL: {
              attr: {
                width: '300',
                height: '400',
              },
              Format: {
                attr: {},
                value: 'image/png',
              },
              OnlineResource: {
                attr: {
                  'xlink:type': 'simple',
                  'xlink:href':
                    'https://geoservices.knmi.nl/cgi-bin/geoweb-demo.cgi?SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=netcdf_5dims&format=image/png&STYLE=rainbow/nearest',
                },
              },
            },
            index: 0,
            nrOfStyles: 1,
            title: 'rainbow/nearest',
            name: 'rainbow/nearest',
            legendURL:
              'https://geoservices.knmi.nl/cgi-bin/geoweb-demo.cgi?SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=netcdf_5dims&format=image/png&STYLE=rainbow/nearest',
            abstracttext: 'No abstract available',
          },
        ],
        dimensions: [
          {
            name: 'member',
            units: 'member number',
            currentValue: 'member1',
            values: 'member1,member2,member3,member4',
          },
          {
            name: 'elevation',
            units: 'meters',
            currentValue: '9000',
            values: '1000,5000,9000',
          },
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: '2017-01-01T00:25:00Z',
          },
        ],
      },
    },
  },
  'https://testservice': {
    layers: [
      {
        name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        text: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        leaf: true,
        path: '',
      },
      {
        name: 'LAYER_WITHOUT_TIME',
        text: 'LAYER_WITHOUT_TIME',
        leaf: true,
        path: '',
      },
    ],
    layer: {
      RADNL_OPER_R___25PCPRR_L3_KNMI: {
        styles: [],
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: '2020-03-13T14:40:00Z',
          },
        ],
      },

      LAYER_WITHOUT_TIME: {
        styles: [],
        dimensions: [
          {
            name: 'flight level',
            units: 'hft',
            currentValue: '625',
            values: '25,325,625',
          },
        ],
      },
    },
  },
};

export const layerWithoutTimeDimension = {
  service: 'https://testservice',
  id: 'layerWithoutTime',
  name: 'LAYER_WITHOUT_TIME',
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'flight level',
      units: 'hft',
      currentValue: '625',
      values: '25,325,625',
    },
  ],
};

export const WmLayerWithoutTimeDimension = new WMLayer(
  layerWithoutTimeDimension,
);
