/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { SetTimePayload, SetBboxPayload } from '../types';
import {
  SetTimeSync,
  SetBboxSync,
  SetBboxSyncPayload,
  SetTimeSyncPayload,
} from './types';
import { GENERIC_SETTIME, GENERIC_SETBBOX } from '../constants';
import { GENERIC_SYNC_SETTIME, GENERIC_SYNC_SETBBOX } from './constants';

/**
 * These actions are fired by the generic/saga.ts, based on generic actions and the synchronizationGroup state.
 *
 * These actions should not be used by components directly. Components should only use the generic actions.
 */

export const setTimeSync = (
  payload: SetTimePayload,
  targets: SetTimeSyncPayload[],
): SetTimeSync => {
  return {
    type: GENERIC_SYNC_SETTIME,
    payload: {
      source: {
        type: GENERIC_SETTIME,
        payload,
      },
      targets,
    },
  };
};

export const setBboxSync = (
  payload: SetBboxPayload,
  targets: SetBboxSyncPayload[],
): SetBboxSync => {
  return {
    type: GENERIC_SYNC_SETBBOX,
    payload: {
      source: {
        type: GENERIC_SETBBOX,
        payload,
      },
      targets,
    },
  };
};
