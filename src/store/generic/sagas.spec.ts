/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { put, select, takeLatest } from 'redux-saga/effects';
import { setTimeSaga, setBBoxSaga, rootSaga } from './sagas';
import * as genericConstants from './constants';
import * as genericActions from './actions';
import * as synchronizationGroupsSelector from './synchronizationGroups/selectors';

import {
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETTIME,
} from './synchronizationGroups/constants';
import { setBboxSync, setTimeSync } from './synchronizationActions/actions';

describe('store/generic/sagas', () => {
  describe('rootSaga', () => {
    it('should take actions and fire correct sagas', () => {
      const generator = rootSaga();

      expect(generator.next().value).toEqual(
        takeLatest(genericConstants.GENERIC_SETTIME, setTimeSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(genericConstants.GENERIC_SETBBOX, setBBoxSaga),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('setTimeSaga', () => {
    it('should handle setTime', () => {
      const payload = {
        sourceId: 'test-1',
        value: '2020-11-13T01:32:00Z',
      };
      const action = genericActions.setTime(payload);
      const generator = setTimeSaga(action);

      const targets = generator.next().value;
      expect(targets).toEqual(
        select(
          synchronizationGroupsSelector.getTargets,
          payload,
          SYNCGROUPS_TYPE_SETTIME,
        ),
      );
      expect(generator.next(targets).value).toEqual(
        put(setTimeSync(payload, targets)),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should handle setBBoxSaga', () => {
      const payload = {
        sourceId: 'test-1',
        bbox: {
          left: -450651.2255879827,
          bottom: 6490531.093143953,
          right: 1428345.8183648037,
          top: 7438773.776232235,
        },
        srs: 'EPSG:3857',
      };
      const action = genericActions.setBbox(payload);
      const generator = setBBoxSaga(action);

      const targets = generator.next().value;
      expect(targets).toEqual(
        select(
          synchronizationGroupsSelector.getTargets,
          payload,
          SYNCGROUPS_TYPE_SETBBOX,
        ),
      );
      expect(generator.next(targets).value).toEqual(
        put(setBboxSync(payload, targets)),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
});
