/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  SYNCGROUPS_ADD_SOURCE,
  SYNCGROUPS_REMOVE_SOURCE,
  SYNCGROUPS_TOGGLE_LINK_TARGET,
  SYNCGROUPS_ADD_TARGET,
  SYNCGROUPS_REMOVE_TARGET,
  SYNCGROUPS_ADD_GROUP,
  SYNCGROUPS_REMOVE_GROUP,
} from './constants';

import {
  SyncGroupAddGroupPayload,
  SyncGroupAddTargetPayload,
  SyncGroupLinkTargetPayload,
  SyncGroupRemoveGroupPayload,
  SyncGroupRemoveSourcePayload,
  SyncGroupRemoveTargetPayload,
  SyncGroupsAddSourcePayload,
  SynchronizationGroupActions,
} from './types';

/* Actions for sources */

/**
 * Add a source component to the sources list.
 *
 * @param payload: SyncGroupsAddSourcePayload;
 *   {
 *     id: string;                            // The id of the source ;
 *     type: SyncType[];                      // The types of this source
 *     defaultPayload?: GenericActionPayload; // Optional default value of this source
 *   }
 */
export const syncGroupAddSource = (
  payload: SyncGroupsAddSourcePayload,
): SynchronizationGroupActions => {
  return {
    type: SYNCGROUPS_ADD_SOURCE,
    payload,
  };
};

/**
 * Remove a source from the sources list. Besides removing it from the sources list, the reducer will also remove the source from the synchronization groups.
 *
 * @param payload  SyncGroupRemoveSourcePayload;
 *   {
 *     id: string; // The source id to remove
 *   }
 */
export const syncGroupRemoveSource = (
  payload: SyncGroupRemoveSourcePayload,
): SynchronizationGroupActions => ({
  type: SYNCGROUPS_REMOVE_SOURCE,
  payload,
});

/* Actions for groups */

/**
 * Add a target (which should be in the sources list) to this synchronization group.
 * The source will get the value set according to the other targets in the group. T
 * The value is set via a secondary action (via saga's) to allow the other reducers (not part of SyncGroups) to process the new value
 *
 * @param payload: SyncGroupAddTargetPayload;
 *   {
 *     id: string;        // SyncGroup Id to add the target to.
 *     targetId: string;  // TargetId of the target
 *     linked?: boolean;  // Whether it should be linked to the group or not, if not set it is always linked (true)
 *   }
 */
export const syncGroupAddTarget = (
  payload: SyncGroupAddTargetPayload,
): SynchronizationGroupActions => ({
  type: SYNCGROUPS_ADD_TARGET,
  payload,
});

/**
 * Remove a target from the group
 * @param payload: SyncGroupRemoveTargetPayload;
 *   {
 *     id: string;       // SyncGroup Id to remove target from;
 *     targetId: string; // Target id to remove
 *   }
 */
export const syncGroupRemoveTarget = (
  payload: SyncGroupRemoveTargetPayload,
): SynchronizationGroupActions => ({
  type: SYNCGROUPS_REMOVE_TARGET,
  payload,
});

/**
 * Toggle linking of a target in this group.
 * When linking is set to enabled, the value of the group is given to this target via an extra action (via saga's)
 *
 * @param payload: SyncGroupLinkTargetPayload;
 *   {
 *     linked: boolean;   // Target is linked or not in this group
 *     id: string;        // SyncGroup Id
 *     targetId: string;  // The targetId to set linked for
 *   }
 */
export const syncGroupLinkTarget = (
  payload: SyncGroupLinkTargetPayload,
): SynchronizationGroupActions => ({
  type: SYNCGROUPS_TOGGLE_LINK_TARGET,
  payload,
});

/**
 * Add a synchronization group
 *
 * @param payload: syncGroupAddGroupPayload;
 *   {
 *     id: string;        // Syncgroup id
 *     title: string;      // Title for sync group
 *     type: SyncType[];  // SyncGroup type
 *   }
 */
export const syncGroupAddGroup = (
  payload: SyncGroupAddGroupPayload,
): SynchronizationGroupActions => {
  return {
    type: SYNCGROUPS_ADD_GROUP,
    payload,
  };
};

/**
 * Remove a synchronization group
 *
 * @param payload: syncGroupRemoveGroupPayload;
 *   {
 *     id: string;        // Syncgroup id
 *   }
 */
export const syncGroupRemoveGroup = (
  payload: SyncGroupRemoveGroupPayload,
): SynchronizationGroupActions => {
  return {
    type: SYNCGROUPS_REMOVE_GROUP,
    payload,
  };
};
