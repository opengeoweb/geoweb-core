/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { SagaIterator } from 'redux-saga';
import { takeLatest, select, put, call } from 'redux-saga/effects';
import { setBboxSync, setTimeSync } from '../synchronizationActions/actions';

import * as synchronizationGroupsConstants from './constants';
import { SYNCGROUPS_TYPE_SETBBOX, SYNCGROUPS_TYPE_SETTIME } from './constants';
import * as synchronizationGroupsSelector from './selectors';
import { SyncGroupAddTarget, SyncGroupLinkTarget } from './types';

export function* updateSourceValueWhenLinkingComponentToGroupSaga(
  groupId: string,
  targetToUpdate: string,
): SagaIterator {
  const group = yield select(
    synchronizationGroupsSelector.getSynchronizationGroup,
    groupId,
  );

  if (!group) {
    // eslint-disable-next-line no-console
    console.warn(`group ${groupId} not found`);
    return;
  }

  const linkedTargetIds = yield select(
    synchronizationGroupsSelector.getLinkedGroupTargetsIds,
    groupId,
    targetToUpdate,
  );

  const firstLinkedTargetIdInGroup =
    linkedTargetIds.length > 0 && linkedTargetIds[0];

  const source = yield select(
    synchronizationGroupsSelector.getSynchronizationGroupSource,
    firstLinkedTargetIdInGroup,
  );

  if (!source || !source.payload) return;

  const targets = yield select(
    synchronizationGroupsSelector.getTargets,
    source.payload,
    group.type,
  );

  switch (group.type) {
    case SYNCGROUPS_TYPE_SETTIME:
      yield put(setTimeSync(source.payload, targets));
      break;
    case SYNCGROUPS_TYPE_SETBBOX:
      yield put(setBboxSync(source.payload, targets));
      break;
    default:
  }
}

export function* addGroupTargetSaga({
  payload,
}: SyncGroupAddTarget): SagaIterator {
  const { groupId, linked, targetId: targetToUpdate } = payload;

  if (!linked) {
    yield call(
      updateSourceValueWhenLinkingComponentToGroupSaga,
      groupId,
      targetToUpdate,
    );
  }
}

export function* linkGroupTargetSaga({
  payload,
}: SyncGroupLinkTarget): SagaIterator {
  const { groupId, linked, targetId: targetToUpdate } = payload;

  if (!linked) {
    yield call(
      updateSourceValueWhenLinkingComponentToGroupSaga,
      groupId,
      targetToUpdate,
    );
  }
}

function* rootSaga(): SagaIterator {
  yield takeLatest(
    synchronizationGroupsConstants.SYNCGROUPS_ADD_TARGET,
    addGroupTargetSaga,
  );
  yield takeLatest(
    synchronizationGroupsConstants.SYNCGROUPS_TOGGLE_LINK_TARGET,
    linkGroupTargetSaga,
  );
}

export default rootSaga;
