/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* Action constants for sources */
export const SYNCGROUPS_ADD_SOURCE = 'SYNCGROUPS_ADD_SOURCE';
export const SYNCGROUPS_REMOVE_SOURCE = 'SYNCGROUPS_REMOVE_SOURCE';

/* Action constants for groups */
export const SYNCGROUPS_TOGGLE_LINK_TARGET = 'SYNCGROUPS_TOGGLE_LINK_TARGET';
export const SYNCGROUPS_ADD_TARGET = 'SYNCGROUPS_ADD_TARGET';
export const SYNCGROUPS_REMOVE_TARGET = 'SYNCGROUPS_REMOVE_TARGET';
export const SYNCGROUPS_ADD_GROUP = 'SYNCGROUPS_ADD_GROUP';
export const SYNCGROUPS_REMOVE_GROUP = 'SYNCGROUPS_REMOVE_GROUP';

/* Type constants */
export const SYNCGROUPS_TYPE_SETTIME = 'SYNCGROUPS_TYPE_SETTIME';
export const SYNCGROUPS_TYPE_SETBBOX = 'SYNCGROUPS_TYPE_SETBBOX';
