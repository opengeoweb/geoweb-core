/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from 'reselect';
import { AppStore } from '../../../types/types';
import {
  GenericSyncActionPayload,
  SetBboxSyncPayload,
  SetTimeSyncPayload,
} from '../synchronizationActions/types';
import { GenericActionPayload } from '../types';
import {
  SynchronizationGroup,
  SynchronizationGroupState,
  SynchronizationSource,
  SyncType,
} from './types';

const syncGroupStore = (store: AppStore): SynchronizationGroupState =>
  store.syncronizationGroupStore || null;

/**
 * Gets synchronization group state
 *
 * Example: synchronizationGroupState = getSynchronizationGroupState(store)
 * @param {object} store store: object - Store object
 * @returns {object} returnType: SynchronizationGroupState
 */
export const getSynchronizationGroupState = createSelector(
  syncGroupStore,
  store => store || [],
);

export const getSynchronizationGroup = (
  state: AppStore,
  id: string,
): SynchronizationGroup => {
  return syncGroupStore(state).groups.byId[id];
};

export const getSynchronizationGroupSource = (
  state: AppStore,
  id: string,
): SynchronizationSource => {
  return syncGroupStore(state).sources.byId[id];
};

export const getLinkedGroupTargetsIds = (
  state: AppStore,
  id: string,
  targetId: string,
): string[] => {
  const group = syncGroupStore(state).groups.byId[id];
  const linkedTargetIds = group.targets.allIds.reduce((list, _targetId) => {
    if (_targetId !== targetId) {
      const target = group.targets.byId[_targetId];
      if (target.linked) {
        return list.concat(_targetId);
      }
    }
    return list;
  }, []);

  return linkedTargetIds;
};

export const getTargets = (
  state: AppStore,
  payload: GenericActionPayload,
  actionType: SyncType,
): SetBboxSyncPayload[] | SetTimeSyncPayload[] => {
  const actionPayloads = [];
  const targetsInActionPayload = {};
  const syncronizationGroupStore = syncGroupStore(state);
  if (syncronizationGroupStore && payload) {
    syncronizationGroupStore.groups.allIds.forEach(id => {
      const syncronizationGroup = syncronizationGroupStore.groups.byId[id];
      if (actionType === syncronizationGroup.type) {
        /* Check if the source is in the target list of the synchonizationGroup */
        const source = syncronizationGroup.targets.byId[payload.sourceId];
        /* If the source is part of the target list, and is linked, continue syncing the other targets */
        if (source && source.linked) {
          syncronizationGroup.targets.allIds.forEach(targetId => {
            const target = syncronizationGroup.targets.byId[targetId];
            if (target.linked && !targetsInActionPayload[targetId]) {
              /* Remember that we have already added this target in the action payloads, prevents adding it twice */
              targetsInActionPayload[targetId] = true;
              /* Compose the payload */
              const newPayload: GenericSyncActionPayload = {
                targetId,
                ...payload,
              };
              actionPayloads.push(newPayload);
            }
          });
        }
      }
    });
  }
  return actionPayloads;
};
