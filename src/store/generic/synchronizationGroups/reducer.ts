/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import produce from 'immer';
import {
  GENERIC_SYNC_SETTIME,
  GENERIC_SYNC_SETBBOX,
} from '../synchronizationActions/constants';

import {
  SynchronizationGroupActions,
  SynchronizationGroupState,
} from './types';
import {
  SYNCGROUPS_ADD_SOURCE,
  SYNCGROUPS_TOGGLE_LINK_TARGET,
  SYNCGROUPS_REMOVE_SOURCE,
  SYNCGROUPS_ADD_TARGET,
  SYNCGROUPS_REMOVE_TARGET,
  SYNCGROUPS_ADD_GROUP,
  SYNCGROUPS_REMOVE_GROUP,
} from './constants';
import { GenericSyncActions } from '../synchronizationActions/types';
import { removeInPlace } from '../utils';

export const initialState: SynchronizationGroupState = {
  sources: {
    byId: {},
    allIds: [],
  },
  groups: {
    byId: {},
    allIds: [],
  },
};

export const reducer = (
  state = initialState,
  action: SynchronizationGroupActions | GenericSyncActions,
): SynchronizationGroupState => {
  switch (action.type) {
    case SYNCGROUPS_ADD_SOURCE:
      return produce(state, draft => {
        const { id, type: payloadType, defaultPayload } = action.payload;
        if (!draft.sources.byId[id]) {
          draft.sources.byId[id] = {
            types: payloadType,
            payload: defaultPayload,
          };
          draft.sources.allIds.push(id);
        }
      });
    case SYNCGROUPS_REMOVE_SOURCE:
      return produce(state, draft => {
        const { id } = action.payload;
        if (draft.sources.byId[id]) {
          delete draft.sources.byId[id];
          removeInPlace(draft.sources.allIds, _id => _id === id);
          /* Also remove the source from all target lists */
          draft.groups.allIds.forEach(groupId => {
            delete draft.groups.byId[groupId].targets.byId[id];
            removeInPlace(
              draft.groups.byId[groupId].targets.allIds,
              _id => _id === id,
            );
          });
        }
      });
    case SYNCGROUPS_ADD_TARGET:
      return produce(state, draft => {
        const { groupId, targetId, linked } = action.payload;
        if (!draft.groups.byId[groupId]) {
          // eslint-disable-next-line no-console
          console.warn(
            `SYNCGROUPS_ADD_TARGET: Group with id ${groupId} not found`,
          );
          return;
        }
        if (draft.groups.byId[groupId].targets.allIds.includes(targetId)) {
          // eslint-disable-next-line no-console
          console.warn(
            `SYNCGROUPS_ADD_TARGET: Target with id ${targetId} already in group ${groupId}`,
          );
          return;
        }
        draft.groups.byId[groupId].targets.allIds.push(targetId);
        draft.groups.byId[groupId].targets.byId[targetId] = {
          linked: linked !== false,
        };
      });
    case SYNCGROUPS_REMOVE_TARGET:
      return produce(state, draft => {
        const { groupId, targetId } = action.payload;
        if (!draft.groups.byId[groupId]) return;
        if (!draft.groups.byId[groupId].targets.allIds.includes(targetId))
          return;
        delete draft.groups.byId[groupId].targets.byId[targetId];
        removeInPlace(
          draft.groups.byId[groupId].targets.allIds,
          _id => _id === targetId,
        );
      });
    case SYNCGROUPS_TOGGLE_LINK_TARGET:
      return produce(state, draft => {
        const { groupId, targetId, linked } = action.payload;
        if (
          !state.groups.byId[groupId] ||
          !state.groups.byId[groupId].targets.byId[targetId]
        ) {
          // eslint-disable-next-line no-console
          console.warn(
            `ignoring action SYNCGROUPS_TOGGLE_LINK_TARGET with payload ${JSON.stringify(
              action.payload,
            )}`,
          );
          return;
        }
        draft.groups.byId[groupId].targets.byId[targetId].linked = linked;
      });
    case SYNCGROUPS_ADD_GROUP:
      return produce(state, draft => {
        const { groupId, type, title } = action.payload;
        if (state.groups.byId[groupId]) {
          // eslint-disable-next-line no-console
          console.warn(
            `Group already exists: ignoring action SYNCGROUPS_ADD_GROUP with payload ${JSON.stringify(
              action.payload,
            )}`,
          );
          return;
        }
        draft.groups.byId[groupId] = {
          title,
          type,
          targets: {
            allIds: [],
            byId: {},
          },
        };
        draft.groups.allIds.push(groupId);
      });
    case SYNCGROUPS_REMOVE_GROUP:
      return produce(state, draft => {
        const { groupId } = action.payload;
        if (!state.groups.byId[groupId]) {
          // eslint-disable-next-line no-console
          console.warn(
            `Group does not exist: ignoring action SYNCGROUPS_REMOVE_GROUP with payload ${JSON.stringify(
              action.payload,
            )}`,
          );
          return;
        }
        delete draft.groups.byId[groupId];
        removeInPlace(draft.groups.allIds, _groupId => _groupId === groupId);
      });
    case GENERIC_SYNC_SETBBOX:
    case GENERIC_SYNC_SETTIME:
      return produce(state, draft => {
        const { targets, source } = action.payload;
        if (draft.sources.byId[source.payload.sourceId]) {
          draft.sources.byId[source.payload.sourceId].payload = source.payload;
        }
        targets.forEach(target => {
          if (draft.sources.byId[target.targetId]) {
            draft.sources.byId[target.targetId].payload = source.payload;
          }
        });
      });

    default:
      return state;
  }
};
