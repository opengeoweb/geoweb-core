/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import rootSaga, {
  addGroupTargetSaga,
  linkGroupTargetSaga,
  updateSourceValueWhenLinkingComponentToGroupSaga,
} from './sagas';
import * as synchronizationGroupsConstants from './constants';
import { syncGroupAddTarget, syncGroupLinkTarget } from './actions';

import * as synchronizationGroupsSelector from './selectors';
import { setBboxSync, setTimeSync } from '../synchronizationActions/actions';
import { AppStore } from '../../../types/types';
import { SetTimePayload, SetBboxPayload } from '../types';
import { SyncGroupAddTarget, SyncGroupLinkTarget } from './types';
import {
  SetBboxSyncPayload,
  SetTimeSyncPayload,
} from '../synchronizationActions/types';

const syncronizationGroupTestStore: AppStore = {
  syncronizationGroupStore: {
    sources: {
      byId: {
        mapA: {
          types: [
            synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETTIME,
            synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETBBOX,
          ],
          payload: {
            sourceId: 'mapA',
            value: '2020-11-13T01:32:00Z',
          },
        },
        mapB: {
          types: [
            synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETTIME,
            synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETBBOX,
          ],
          payload: {
            sourceId: 'mapA',
            value: '2020-11-13T01:32:00Z',
          },
        },
        mapC: {
          types: [
            synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETTIME,
            synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETBBOX,
          ],
          payload: {
            sourceId: 'mapA',
            value: '2020-11-13T01:32:00Z',
          },
        },
      },
      allIds: ['mapA', 'mapB'],
    },
    groups: {
      byId: {
        group1: {
          title: 'Group 1 for time',
          type: synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETTIME,
          targets: {
            allIds: ['mapB', 'mapA', 'mapC'],
            byId: {
              mapB: {
                linked: true,
              },
              mapA: {
                linked: true,
              },
              mapC: {
                linked: false,
              },
            },
          },
        },
        group2: {
          title: 'Group 2 for area',
          type: synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETBBOX,
          targets: {
            allIds: ['mapB', 'mapA', 'mapC'],
            byId: {
              mapB: {
                linked: true,
              },
              mapA: {
                linked: true,
              },
              mapC: {
                linked: false,
              },
            },
          },
        },
      },
      allIds: ['group1', 'group2'],
    },
  },
};

describe('store/generic/synchronizationGroups/sagas', () => {
  describe('updateSourceValueWhenLinkingComponentToGroupSaga', () => {
    it('should warn if groups doesn exist', () => {
      const generator = updateSourceValueWhenLinkingComponentToGroupSaga(
        'group7',
        'mapA',
      );
      expect(generator.next().value).toEqual(
        select(synchronizationGroupsSelector.getSynchronizationGroup, 'group7'),
      );
      const hasWarned = jest.spyOn(global.console, 'warn').mockImplementation();
      expect(hasWarned).toHaveBeenCalledTimes(0);
      generator.next();
      expect(hasWarned).toHaveBeenCalledTimes(1);
      expect(generator.next().done).toBeTruthy();
    });

    it('should fire first action in group list for time', () => {
      const groupId = 'group1';
      const linkedTarget = 'mapA';
      const generator = updateSourceValueWhenLinkingComponentToGroupSaga(
        groupId,
        linkedTarget,
      );

      const group = synchronizationGroupsSelector.getSynchronizationGroup(
        syncronizationGroupTestStore,
        groupId,
      );
      expect(generator.next(group).value).toEqual(
        select(synchronizationGroupsSelector.getSynchronizationGroup, groupId),
      );

      expect(generator.next(group).value).toEqual(
        select(
          synchronizationGroupsSelector.getLinkedGroupTargetsIds,
          groupId,
          linkedTarget,
        ),
      );

      expect(generator.next([linkedTarget]).value).toEqual(
        select(
          synchronizationGroupsSelector.getSynchronizationGroupSource,
          linkedTarget,
        ),
      );

      const source = synchronizationGroupsSelector.getSynchronizationGroupSource(
        syncronizationGroupTestStore,
        linkedTarget,
      );

      expect(generator.next(source).value).toEqual(
        select(
          synchronizationGroupsSelector.getTargets,
          source.payload,
          group.type,
        ),
      );

      const targets = synchronizationGroupsSelector.getTargets(
        syncronizationGroupTestStore,
        source.payload,
        group.type,
      );

      expect(generator.next(targets).value).toEqual(
        put(
          setTimeSync(
            source.payload as SetTimePayload,
            targets as SetTimeSyncPayload[],
          ),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
    it('should fire first action in group list for area', () => {
      const groupId = 'group2';
      const linkedTarget = 'mapA';
      const generator = updateSourceValueWhenLinkingComponentToGroupSaga(
        groupId,
        linkedTarget,
      );

      const group = synchronizationGroupsSelector.getSynchronizationGroup(
        syncronizationGroupTestStore,
        groupId,
      );
      expect(generator.next(group).value).toEqual(
        select(synchronizationGroupsSelector.getSynchronizationGroup, groupId),
      );

      expect(generator.next(group).value).toEqual(
        select(
          synchronizationGroupsSelector.getLinkedGroupTargetsIds,
          groupId,
          linkedTarget,
        ),
      );

      expect(generator.next([linkedTarget]).value).toEqual(
        select(
          synchronizationGroupsSelector.getSynchronizationGroupSource,
          linkedTarget,
        ),
      );

      const source = synchronizationGroupsSelector.getSynchronizationGroupSource(
        syncronizationGroupTestStore,
        linkedTarget,
      );

      expect(generator.next(source).value).toEqual(
        select(
          synchronizationGroupsSelector.getTargets,
          source.payload,
          group.type,
        ),
      );

      const targets = synchronizationGroupsSelector.getTargets(
        syncronizationGroupTestStore,
        source.payload,
        group.type,
      );

      expect(generator.next(targets).value).toEqual(
        put(
          setBboxSync(
            source.payload as SetBboxPayload,
            targets as SetBboxSyncPayload[],
          ),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should fire stop if source does not exist', () => {
      const groupId = 'group2';
      const linkedTarget = 'mapA';
      const generator = updateSourceValueWhenLinkingComponentToGroupSaga(
        groupId,
        linkedTarget,
      );

      const group = synchronizationGroupsSelector.getSynchronizationGroup(
        syncronizationGroupTestStore,
        groupId,
      );
      expect(generator.next(group).value).toEqual(
        select(synchronizationGroupsSelector.getSynchronizationGroup, groupId),
      );

      expect(generator.next(group).value).toEqual(
        select(
          synchronizationGroupsSelector.getLinkedGroupTargetsIds,
          groupId,
          linkedTarget,
        ),
      );

      expect(generator.next([linkedTarget]).value).toEqual(
        select(
          synchronizationGroupsSelector.getSynchronizationGroupSource,
          linkedTarget,
        ),
      );

      expect(generator.next(null).done).toBeTruthy();
    });
  });

  describe('addGroupTargetSaga', () => {
    it('should update if not linked', () => {
      const payload = {
        linked: false,
        groupId: 'test',
        targetId: 'test-2',
      };
      const action = syncGroupAddTarget(payload) as SyncGroupAddTarget;
      const generator = addGroupTargetSaga(action);

      expect(generator.next(action).value).toEqual(
        call(
          updateSourceValueWhenLinkingComponentToGroupSaga,
          payload.groupId,
          payload.targetId,
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  it('should not link if linked', () => {
    const payload = {
      linked: true,
      groupId: 'test',
      targetId: 'test-2',
    };
    const action = syncGroupAddTarget(payload) as SyncGroupAddTarget;
    const generator = addGroupTargetSaga(action);

    expect(generator.next().done).toBeTruthy();
  });

  describe('linkGroupTargetSaga', () => {
    it('should link if not linked', () => {
      const payload = {
        linked: false,
        groupId: 'test',
        targetId: 'test-2',
      };
      const action = syncGroupLinkTarget(payload) as SyncGroupLinkTarget;
      const generator = linkGroupTargetSaga(action);

      expect(generator.next(action).value).toEqual(
        call(
          updateSourceValueWhenLinkingComponentToGroupSaga,
          payload.groupId,
          payload.targetId,
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should not link if linked', () => {
      const payload = {
        linked: true,
        groupId: 'test',
        targetId: 'test-2',
      };
      const action = syncGroupLinkTarget(payload) as SyncGroupLinkTarget;
      const generator = linkGroupTargetSaga(action);

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('rootSaga', () => {
    it('should intercept actions and fire sagas', () => {
      const generator = rootSaga();

      expect(generator.next().value).toEqual(
        takeLatest(
          synchronizationGroupsConstants.SYNCGROUPS_ADD_TARGET,
          addGroupTargetSaga,
        ),
      );
      expect(generator.next().value).toEqual(
        takeLatest(
          synchronizationGroupsConstants.SYNCGROUPS_TOGGLE_LINK_TARGET,
          linkGroupTargetSaga,
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
});
