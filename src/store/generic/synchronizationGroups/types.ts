/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { Action } from 'redux';
import { GenericActionPayload } from '../types';
import {
  SYNCGROUPS_ADD_SOURCE,
  SYNCGROUPS_REMOVE_SOURCE,
  SYNCGROUPS_TOGGLE_LINK_TARGET,
  SYNCGROUPS_ADD_TARGET,
  SYNCGROUPS_REMOVE_TARGET,
  SYNCGROUPS_ADD_GROUP,
  SYNCGROUPS_REMOVE_GROUP,
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETTIME,
} from './constants';

// store
export interface SynchronizationGroup {
  type: SyncType;
  title?: string;
  targets: {
    byId: Record<string, { linked: boolean }>;
    allIds: string[];
  };
}

export interface SynchronizationSource {
  types: SyncType[];
  payload: GenericActionPayload;
}

export interface SynchronizationSources {
  byId: Record<string, SynchronizationSource>;
  allIds: string[];
}

export interface SynchronizationGroups {
  byId: Record<string, SynchronizationGroup>;
  allIds: string[];
}

export interface SynchronizationGroupState {
  sources: SynchronizationSources;
  groups: SynchronizationGroups;
}

export interface SynchronizationGroupModuleState {
  syncronizationGroupStore?: SynchronizationGroupState;
}

export type SyncType =
  | typeof SYNCGROUPS_TYPE_SETBBOX
  | typeof SYNCGROUPS_TYPE_SETTIME;

// action payloads
export interface SyncGroupsAddSourcePayload {
  id: string;
  type: SyncType[];
  defaultPayload?: GenericActionPayload;
}

export interface SyncGroupRemoveSourcePayload {
  id: string;
}

export interface SyncGroupAddTargetPayload {
  groupId: string;
  targetId: string;
  linked?: boolean;
}

export interface SyncGroupRemoveTargetPayload {
  groupId: string;
  targetId: string;
}

export interface SyncGroupLinkTargetPayload {
  linked: boolean;
  groupId: string;
  targetId: string;
}

export interface SyncGroupAddGroupPayload {
  groupId: string;
  title: string /* Title for sync group */;
  type: SyncType /* SyncGroup type */;
}

export interface SyncGroupRemoveGroupPayload {
  groupId: string;
}

// actions
// actions: Source action types
export interface SyncGroupsAddSource extends Action {
  type: typeof SYNCGROUPS_ADD_SOURCE;
  payload: SyncGroupsAddSourcePayload;
}

export interface SyncGroupsRemoveSource extends Action {
  type: typeof SYNCGROUPS_REMOVE_SOURCE;
  payload: SyncGroupRemoveSourcePayload;
}

// actions: Group action types
export interface SyncGroupAddTarget extends Action {
  type: typeof SYNCGROUPS_ADD_TARGET;
  payload: SyncGroupAddTargetPayload;
}

export interface SyncGroupRemoveTarget extends Action {
  type: typeof SYNCGROUPS_REMOVE_TARGET;
  payload: SyncGroupRemoveTargetPayload;
}

export interface SyncGroupLinkTarget extends Action {
  type: typeof SYNCGROUPS_TOGGLE_LINK_TARGET;
  payload: SyncGroupLinkTargetPayload;
}

export interface SyncGroupAddGroup extends Action {
  type: typeof SYNCGROUPS_ADD_GROUP;
  payload: SyncGroupAddGroupPayload;
}

export interface SyncGroupRemoveGroup extends Action {
  type: typeof SYNCGROUPS_REMOVE_GROUP;
  payload: SyncGroupRemoveGroupPayload;
}

export type SynchronizationGroupActions =
  | SyncGroupsAddSource
  | SyncGroupsRemoveSource
  | SyncGroupAddTarget
  | SyncGroupRemoveTarget
  | SyncGroupLinkTarget
  | SyncGroupAddGroup
  | SyncGroupRemoveGroup;
