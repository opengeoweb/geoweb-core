/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { takeLatest, select, put } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import * as genericConstants from './constants';
import * as synchronizationGroupsSelector from './synchronizationGroups/selectors';
import { SetBbox, SetTime } from './types';
import {
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETTIME,
} from './synchronizationGroups/constants';
import { setBboxSync, setTimeSync } from './synchronizationActions/actions';

export function* setTimeSaga({ payload }: SetTime): SagaIterator {
  const targets = yield select(
    synchronizationGroupsSelector.getTargets,
    payload,
    SYNCGROUPS_TYPE_SETTIME,
  );

  yield put(setTimeSync(payload, targets));
}

export function* setBBoxSaga({ payload }: SetBbox): SagaIterator {
  const targets = yield select(
    synchronizationGroupsSelector.getTargets,
    payload,
    SYNCGROUPS_TYPE_SETBBOX,
  );

  yield put(setBboxSync(payload, targets));
}

export function* rootSaga(): SagaIterator {
  yield takeLatest(genericConstants.GENERIC_SETTIME, setTimeSaga);
  yield takeLatest(genericConstants.GENERIC_SETBBOX, setBBoxSaga);
}

export default rootSaga;
