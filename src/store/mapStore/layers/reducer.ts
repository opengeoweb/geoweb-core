/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import produce from 'immer';

import * as MAP from '../map/constants';
import * as LAYERS from './constants';
import * as GENERICSYNC from '../../generic/synchronizationActions/constants';

import {
  LayerActions,
  LayerState,
  Layer,
  LayerType,
  LayerStatus,
} from './types';
import { MapActions, Dimension } from '../map/types';
import { checkValidLayersPayload } from '../map/utils';
import { produceDraftStateForAllLayersForDimensionWithinMap } from './utils';
import { getWMLayerById } from '../utils/helpers';
import WMJSDimension from '../../../geoweb-webmap/WMJSDimension';
import { GenericSyncActions } from '../../generic/synchronizationActions/types';

export const createLayer = ({
  id,
  opacity = 1,
  enabled = true,
  layerType = 'mapLayer' as LayerType,
  status = 'default' as LayerStatus,
  ...props
}: Layer): Layer => {
  const wmjsLayer = getWMLayerById(id);
  const dimensions =
    props.dimensions ||
    (wmjsLayer ? wmjsLayer.getDimensions() : []).map((dim: WMJSDimension) => ({
      name: dim.name,
      currentValue: dim.currentValue,
      units: dim.units,
    }));
  return {
    ...props,
    dimensions,
    id,
    opacity,
    enabled,
    layerType,
    status,
  };
};

export const initialState: LayerState = {
  byId: {},
  allIds: [],
  availableBaseLayers: { byId: {}, allIds: [] },
};

export const reducer = (
  state = initialState,
  action: LayerActions | MapActions | GenericSyncActions,
): LayerState => {
  switch (action.type) {
    case LAYERS.WEBMAP_ADD_LAYER: {
      const { layer, layerId, mapId } = action.payload;
      if (!checkValidLayersPayload([layer], mapId)) {
        // eslint-disable-next-line no-console
        console.warn('LAYERS.WEBMAP_ADD_LAYER invalid');
        return state;
      }

      return produce(state, draft => {
        if (!state.byId[layerId]) {
          // TODO: (Sander de Snaijer, 2020-03-19) remove layerIds from the layer utils
          draft.byId[layerId] = createLayer({ ...layer, id: layerId, mapId });
          draft.allIds.push(layerId);
        }
      });
    }

    case LAYERS.WEBMAP_LAYER_CHANGE_DIMENSION: {
      const { layerId: layerIdFromAction, dimension } = action.payload;
      const layerFromAction = state.byId[layerIdFromAction];
      if (!layerFromAction) return state;

      const { mapId } = layerFromAction;

      return produce(state, draft =>
        produceDraftStateForAllLayersForDimensionWithinMap(
          draft,
          dimension,
          mapId,
          layerIdFromAction,
        ),
      );
    }

    case GENERICSYNC.GENERIC_SYNC_SETTIME: {
      const { targets: targetsFromAction, source } = action.payload;
      /* Because we want backwards compatibility with the previous code, we also need to listen to the original source action */
      const targets = [
        {
          targetId: source.payload.sourceId,
          value: source.payload.value,
        },
      ];
      /* And then append the targets form the action */
      targets.push(...targetsFromAction);
      return produce(state, draft => {
        targets.forEach(payload => {
          const { targetId, value } = payload;
          const dimension: Dimension = {
            name: 'time',
            currentValue: value,
          };
          produceDraftStateForAllLayersForDimensionWithinMap(
            draft,
            dimension,
            targetId,
            null,
          );
        });
      });
    }

    case LAYERS.WEBMAP_LAYER_CHANGE_ENABLED: {
      const { layerId, enabled } = action.payload;
      return produce(state, draft => {
        if (state.byId[layerId]) {
          draft.byId[layerId].enabled = enabled;
        }
      });
    }

    case LAYERS.WEBMAP_LAYER_CHANGE_OPACITY: {
      const { layerId, opacity } = action.payload;
      return produce(state, draft => {
        if (state.byId[layerId]) {
          draft.byId[layerId].opacity = opacity;
        }
      });
    }

    case LAYERS.WEBMAP_LAYER_CHANGE_STYLE: {
      const { layerId, style } = action.payload;
      return produce(state, draft => {
        if (state.byId[layerId]) {
          draft.byId[layerId].style = style;
        }
      });
    }

    case LAYERS.WEBMAP_LAYER_CHANGE_NAME: {
      const { layerId, name } = action.payload;
      return produce(state, draft => {
        if (state.byId[layerId]) {
          draft.byId[layerId].name = name;
        }
      });
    }

    case LAYERS.WEBMAP_LAYER_DELETE: {
      const { layerId } = action.payload;
      return produce(state, draft => {
        if (draft.byId[layerId]) {
          draft.allIds = draft.allIds.filter(id => id !== layerId);
          delete draft.byId[layerId];
        }
      });
    }

    case LAYERS.WEBMAP_LAYER_ERROR: {
      const { layerId } = action.payload;
      return produce(state, draft => {
        if (draft.byId[layerId]) {
          draft.byId[layerId].status = LayerStatus.error;
        }
      });
    }

    case LAYERS.WEBMAP_BASELAYER_DELETE: {
      const { layerId } = action.payload;
      if (
        state.byId[layerId].layerType !== LayerType.baseLayer &&
        state.byId[layerId].layerType !== LayerType.overLayer
      )
        return state;

      return produce(state, draft => {
        if (draft.byId[layerId]) {
          draft.allIds = draft.allIds.filter(id => id !== layerId);
          delete draft.byId[layerId];
        }
      });
    }

    case LAYERS.WEBMAP_SET_LAYERS: {
      const { layers, mapId } = action.payload;
      if (!checkValidLayersPayload(layers, mapId)) {
        // eslint-disable-next-line no-console
        console.warn('LAYERS.WEBMAP_SET_LAYERS invalid');
        return state;
      }

      return produce(state, draft => {
        /* 
          All layer id's for the specified mapId should be removed. 
          This is done by clearing the allId object, and then adding back the layer ids for the other maps we want to keep.
          At the same time the byId object is synchronized. 
        */
        draft.allIds = [];
        state.allIds.forEach(layerId => {
          if (
            state.byId[layerId] &&
            state.byId[layerId].layerType !== LayerType.baseLayer &&
            state.byId[layerId].layerType !== LayerType.overLayer &&
            state.byId[layerId].mapId === mapId
          ) {
            delete draft.byId[layerId];
          } else {
            draft.allIds.push(layerId);
          }
        });

        /*
          Here we set the layers for the mapId from the action. byId and allIds is updated. 
        */
        layers.forEach(layer => {
          draft.byId[layer.id] = createLayer({ id: layer.id, mapId, ...layer });
          draft.allIds.push(layer.id);
        });
      });
    }

    case LAYERS.WEBMAP_SET_BASELAYERS: {
      const { layers, mapId } = action.payload;
      const filtererdBaseLayers = layers.filter(
        layer =>
          layer.layerType === LayerType.baseLayer ||
          layer.layerType === LayerType.overLayer,
      );

      return produce(state, draft => {
        // filter for unique layerTypes
        const layerTypes = [];
        filtererdBaseLayers.forEach(layer => {
          if (
            layer.layerType !== LayerType.baseLayer &&
            layer.layerType !== LayerType.overLayer
          )
            return;
          if (!layerTypes.includes(layer.layerType))
            layerTypes.push(layer.layerType);
        });

        // remove current layers with same type as one of the passed layers
        state.allIds.forEach(layerId => {
          if (
            state.byId[layerId] &&
            layerTypes.includes(state.byId[layerId].layerType) &&
            state.byId[layerId].mapId === mapId
          ) {
            delete draft.byId[layerId];
            const index = draft.allIds.indexOf(layerId);
            draft.allIds.splice(index, 1);
          }
        });
        // set over and base layers
        filtererdBaseLayers.forEach(layer => {
          draft.byId[layer.id] = createLayer({
            id: layer.id,
            layerType: layer.layerType,
            mapId,
            ...layer,
          });
          draft.allIds.push(layer.id);
        });
      });
    }

    case MAP.WEBMAP_MAP_CHANGE_DIMENSION: {
      const { mapId } = action.payload;
      return produce(state, draft => {
        produceDraftStateForAllLayersForDimensionWithinMap(
          draft,
          action.payload.dimension,
          mapId,
          null,
        );
      });
    }

    case LAYERS.WEBMAP_ADD_BASELAYER: {
      const { layer } = action.payload;
      if (
        layer.layerType !== LayerType.baseLayer &&
        layer.layerType !== LayerType.overLayer
      )
        return state;

      return produce(state, draft => {
        if (!state.byId[layer.id]) {
          draft.byId[layer.id] = createLayer({
            id: layer.id,
            layerType: layer.layerType,
            ...layer,
          });
          draft.allIds.push(layer.id);
        }
      });
    }

    case LAYERS.WEBMAP_LAYER_SET_DIMENSIONS: {
      const { dimensions, layerId } = action.payload;

      return produce(state, draft => {
        if (draft.byId[layerId]) {
          draft.byId[layerId].dimensions = dimensions;
        }
      });
    }

    case LAYERS.WEBMAP_ADD_AVAILABLE_BASELAYER: {
      const { layer } = action.payload;
      if (
        layer.layerType !== LayerType.baseLayer &&
        layer.layerType !== LayerType.overLayer
      )
        return state;

      return produce(state, draft => {
        if (!state.availableBaseLayers.byId[layer.id]) {
          draft.availableBaseLayers.byId[layer.id] = createLayer({
            id: layer.id,
            layerType: layer.layerType,
            ...layer,
          });
          draft.availableBaseLayers.allIds.push(layer.id);
        }
      });
    }

    case LAYERS.WEBMAP_ADD_AVAILABLE_BASELAYERS: {
      const { layers } = action.payload;

      return produce(state, draft => {
        // add new available baselayers
        layers.forEach(layer => {
          if (
            layer.layerType !== LayerType.baseLayer ||
            state.availableBaseLayers.byId[layer.id]
          )
            return;
          draft.availableBaseLayers.byId[layer.id] = createLayer({
            id: layer.id,
            layerType: layer.layerType,
            ...layer,
          });
          draft.availableBaseLayers.allIds.push(layer.id);
        });
      });
    }

    default:
      return state;
  }
};
