/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as layerSelectors from './selectors';
import { LayerType, LayerStatus } from './types';

const testState = {
  layers: {
    byId: {
      'test-1': {
        id: 'test-1',
        name: 'LAYER_NAME',
        dimensions: [
          { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
        ],
        layerType: LayerType.mapLayer,
        opacity: 0.5,
        enabled: true,
        service: 'test.service.com',
        status: LayerStatus.error,
      },
      'test-2': {
        id: 'test-2',
        layerType: LayerType.baseLayer,
      },
      'test-3': {
        id: 'test-3',
        layerType: LayerType.overLayer,
      },
    },
    allIds: ['test-1', 'test-2', 'test-3'],
    availableBaseLayers: {
      byId: {
        'base_layer-1': {
          id: 'base_layer-1',
          name: 'BASELAYER_NAME',
          layerType: LayerType.baseLayer,
          opacity: 1,
          enabled: true,
        },
        'base_layer-2': {
          id: 'base_layer-2',
          name: 'BASELAYER_NAME',
          layerType: LayerType.baseLayer,
          opacity: 1,
          enabled: true,
        },
      },
      allIds: ['base_layer-1', 'base_layer-2'],
    },
  },
};

describe('store/mapStore/layers/selectors', () => {
  describe('getLayerById', () => {
    it('should return the layer when it exists', () => {
      expect(layerSelectors.getLayerById(testState, 'test-1')).toEqual(
        testState.layers.byId['test-1'],
      );
    });
    it('should return null when layerId does not exist', () => {
      expect(layerSelectors.getLayerById(testState, 'testing')).toBeNull();
    });
  });

  describe('getLayersById', () => {
    it('should return the layers when they exists', () => {
      const result = layerSelectors.getLayersById(testState);
      expect(result).toEqual(testState.layers.byId);
      expect(Object.keys(result)).toHaveLength(3);
    });
    it('should return null when store does not exist', () => {
      expect(layerSelectors.getLayersById(null)).toBeNull();
    });
  });

  describe('getLayersIds', () => {
    it('should return array of layer ids when existing', () => {
      const result = layerSelectors.getLayersIds(testState);
      expect(result).toEqual(testState.layers.allIds);
      expect(result).toHaveLength(3);
    });
    it('should return empty list when store does not exist', () => {
      expect(layerSelectors.getLayersIds(null)).toHaveLength(0);
    });
  });

  describe('getAllLayers', () => {
    it('should return array of all layer objects including baselayers', () => {
      const result = layerSelectors.getAllLayers(testState);
      expect(result).toEqual(
        testState.layers.allIds.map(layerId => testState.layers.byId[layerId]),
      );
      expect(result).toHaveLength(3);
    });
    it('should return empty list when store does not exist', () => {
      expect(layerSelectors.getAllLayers(null)).toHaveLength(0);
    });
  });

  describe('getLayers', () => {
    it('should return array of layer objects that arent baselayers', () => {
      const result = layerSelectors.getLayers(testState);
      const nonBaselayerIds = testState.layers.allIds.filter(
        layerId =>
          testState.layers.byId[layerId].layerType !== LayerType.baseLayer &&
          testState.layers.byId[layerId].layerType !== LayerType.overLayer,
      );

      expect(result).toEqual(
        nonBaselayerIds.map(layerId => testState.layers.byId[layerId]),
      );
      expect(result).toHaveLength(1);
    });
    it('should return empty list when store does not exist', () => {
      expect(layerSelectors.getLayers(null)).toHaveLength(0);
    });
  });

  describe('getBaseLayers', () => {
    it('should return array of baselayer objects', () => {
      const result = layerSelectors.getBaseLayers(testState);
      const baseLayerIds = testState.layers.allIds.filter(
        layerId =>
          testState.layers.byId[layerId].layerType === LayerType.baseLayer,
      );
      expect(result).toEqual(
        baseLayerIds.map(layerId => testState.layers.byId[layerId]),
      );
      expect(result).toHaveLength(1);
    });
    it('should return empty list when store does not exist', () => {
      expect(layerSelectors.getBaseLayers(null)).toHaveLength(0);
    });
  });

  describe('getOverLayers', () => {
    it('should return array of overlayer objects', () => {
      const result = layerSelectors.getOverLayers(testState);
      const overLayerIds = testState.layers.allIds.filter(
        layerId =>
          testState.layers.byId[layerId].layerType === LayerType.overLayer,
      );
      expect(result).toEqual(
        overLayerIds.map(layerId => testState.layers.byId[layerId]),
      );
      expect(result).toHaveLength(1);
    });
    it('should return empty list when store does not exist', () => {
      expect(layerSelectors.getOverLayers(null)).toHaveLength(0);
    });
  });

  describe('getLayerDimensions', () => {
    it('should return array of layer dimensions', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerDimensions(testState, layerId);

      expect(result).toEqual(testState.layers.byId[layerId].dimensions);
    });
    it('should return empty list when layer does not exist', () => {
      expect(
        layerSelectors.getLayerDimensions(testState, 'testlayer'),
      ).toHaveLength(0);
    });
    it('should return empty list when dimensions does not exist', () => {
      expect(
        layerSelectors.getLayerDimensions(testState, 'test-2'),
      ).toHaveLength(0);
    });
  });

  describe('getLayerTimeDimension', () => {
    it('should return a time dimension', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerTimeDimension(testState, layerId);

      expect(result).toEqual(testState.layers.byId[layerId].dimensions[0]);
    });
    it('should return empty list when layer does not exist', () => {
      expect(
        layerSelectors.getLayerTimeDimension(testState, 'no-exist'),
      ).toHaveLength(0);
    });
    it('should return empty list when dimensions does not exist', () => {
      expect(
        layerSelectors.getLayerTimeDimension(testState, 'test-2'),
      ).toHaveLength(0);
    });
  });

  describe('getLayerOpacity', () => {
    it('should return the layer opacity', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerOpacity(testState, layerId);
      expect(result).toEqual(testState.layers.byId[layerId].opacity);
    });
    it('should return 0 when the layer has no opacity defined', () => {
      const layerId = 'test-2';
      const result = layerSelectors.getLayerOpacity(testState, layerId);
      expect(result).toEqual(0);
    });
    it('should return 0 when the layer does not exist', () => {
      const result = layerSelectors.getLayerOpacity(testState, 'fake-id');
      expect(result).toEqual(0);
    });
  });

  describe('getLayerEnabled', () => {
    it('should return the value of the layer enabled property', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerEnabled(testState, layerId);
      expect(result).toEqual(testState.layers.byId[layerId].enabled);
    });
    it('should return false when the layer has no enabled property', () => {
      const layerId = 'test-2';
      const result = layerSelectors.getLayerEnabled(testState, layerId);
      expect(result).toEqual(false);
    });
    it('should return null when the layer does not exist', () => {
      const result = layerSelectors.getLayerEnabled(testState, 'fake-id');
      expect(result).toEqual(false);
    });
  });

  describe('getLayerName', () => {
    it('should return the name of the layer', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerName(testState, layerId);
      expect(result).toEqual(testState.layers.byId[layerId].name);
    });
    it('should return an empty string when the layer has no name', () => {
      const layerId = 'test-2';
      const result = layerSelectors.getLayerName(testState, layerId);
      expect(result).toEqual('');
    });
    it('should return an empty string when the layer does not exist', () => {
      const result = layerSelectors.getLayerName(testState, 'fake-id');
      expect(result).toEqual('');
    });
  });

  describe('getLayerService', () => {
    it('should return the service name of the layer', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerService(testState, layerId);
      expect(result).toEqual(testState.layers.byId[layerId].service);
    });
    it('should return an empty string when the layer has no service', () => {
      const layerId = 'test-2';
      const result = layerSelectors.getLayerService(testState, layerId);
      expect(result).toEqual('');
    });
    it('should return an empty string when the layer does not exist', () => {
      const result = layerSelectors.getLayerService(testState, 'fake-id');
      expect(result).toEqual('');
    });
  });

  describe('getLayerStatus', () => {
    it('should return the status the layer', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerStatus(testState, layerId);
      expect(result).toEqual(testState.layers.byId[layerId].status);
    });
    it('should return a default status when the layer has no status specified', () => {
      const layerId = 'test-2';
      const result = layerSelectors.getLayerStatus(testState, layerId);
      expect(result).toEqual(LayerStatus.default);
    });
  });

  describe('getAvailableBaseLayers', () => {
    it('should get a list of the available base layers', () => {
      const result = layerSelectors.getAvailableBaseLayers(testState);
      expect(result).toEqual(
        Object.values(testState.layers.availableBaseLayers.byId),
      );
    });
    it('should return an empty array if no available base layers', () => {
      const emptyAvailBaseLayersState = {
        layers: {
          byId: {
            'test-1': {
              id: 'test-1',
              name: 'LAYER_NAME',
              dimensions: [
                { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
              ],
              layerType: LayerType.mapLayer,
              opacity: 0.5,
              enabled: true,
              service: 'test.service.com',
              status: LayerStatus.error,
            },
            'test-2': {
              id: 'test-2',
              layerType: LayerType.baseLayer,
            },
            'test-3': {
              id: 'test-3',
              layerType: LayerType.overLayer,
            },
          },
          allIds: ['test-1', 'test-2', 'test-3'],
          availableBaseLayers: {
            byId: {},
            allIds: [],
          },
        },
      };
      const result = layerSelectors.getAvailableBaseLayers(
        emptyAvailBaseLayersState,
      );
      expect(result).toEqual([]);
    });
  });
});
