/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  WEBMAP_LAYER_CHANGE_OPACITY,
  WEBMAP_LAYER_CHANGE_NAME,
  WEBMAP_LAYER_CHANGE_STYLE,
  WEBMAP_LAYER_CHANGE_DIMENSION,
  WEBMAP_LAYER_SET_DIMENSIONS,
  WEBMAP_LAYER_CHANGE_ENABLED,
  WEBMAP_LAYER_DELETE,
  WEBMAP_ADD_LAYER,
  WEBMAP_SET_LAYERS,
  WEBMAP_SET_BASELAYERS,
  WEBMAP_ADD_BASELAYER,
  WEBMAP_BASELAYER_DELETE,
  WEBMAP_LAYER_ERROR,
  WEBMAP_ADD_AVAILABLE_BASELAYER,
  WEBMAP_ADD_AVAILABLE_BASELAYERS,
} from './constants';

import {
  SetLayerOpacityPayload,
  SetLayerEnabledPayload,
  SetLayerDimensionPayload,
  SetLayerNamePayload,
  SetLayerStylePayload,
  LayerActions,
  AddLayerPayload,
  DeleteLayerPayload,
  SetLayersPayload,
  SetBaseLayersPayload,
  AddBaseLayerPayload,
  ErrorLayerPayload,
  AddAvailableBaseLayerPayload,
  AddAvailableBaseLayersPayload,
  SetLayerDimensionsPayload,
} from './types';

/**
 * Set the layers for a map. Erases all previous layers.
 * The following action types are triggered after the getCapabilities are parsed:
 * - serviceSetLayers
 * - layerSetStyles
 * - layerChangeStyle
 * - setLayerDimensionsForService
 * - layerChangeDimension
 *
 * Example: setLayers ({layers: someLayerObjectArray, mapId: 'mapId1'});
 * @param {object} payload Object containing the mapId: string and layers: Layer[] properties
 */
export const setLayers = (payload: SetLayersPayload): LayerActions => ({
  type: WEBMAP_SET_LAYERS,
  payload,
});

/**
 * Set the baselayers and overlayers for a map. Erases all previous baselayers and overlayers if one of these types is passed.
 * Ensure the passed layers have a layertype baseLayer or overLayer otherwise they'll be ignored
 * The following action types are triggered after the getCapabilities are parsed:
 * - serviceSetLayers
 * - layerSetStyles
 * - layerChangeStyle
 * - setLayerDimensionsForService
 * - layerChangeDimension
 *
 * Example: setBaseLayers({layers: someLayerObjectArray, mapId:'mapId1'})
 * @param {object} payload Object with layers: Layer[] and mapId: string.
 */
export const setBaseLayers = (payload: SetBaseLayersPayload): LayerActions => ({
  type: WEBMAP_SET_BASELAYERS,
  payload,
});

/**
 * Add a single baselayer or overlayer to a map. Keeps all previous baselayers or overlayers.
 * Ensure the passed layers have a layertype baseLayer or overLayer otherwise nothing will be added
 *
 * Example: setBaseLayers({layerId: 'layerId', layer: someLayerObject, mapId:'mapId1'})
 * @param {object} payload Object with a layer, layerId and mapId
 */
export const addBaseLayer = (payload: AddBaseLayerPayload): LayerActions => ({
  type: WEBMAP_ADD_BASELAYER,
  payload,
});

/**
 * Add a single layer to a map. Keeps all previous layers.
 * The following action types are triggered after the getCapabilities are parsed:
 * - serviceSetLayers
 * - layerSetStyles
 * - layerChangeStyle
 * - setLayerDimensionsForService
 * - layerChangeDimension
 *
 * Example: addLayer ({layerId: 'someLayerId', layer: someLayerObject, mapId:'mapId1'});
 * @param {object} payload Object with a layer, layerId and mapId
 */
export const addLayer = (payload: AddLayerPayload): LayerActions => ({
  type: WEBMAP_ADD_LAYER,
  payload,
});

/**
 * Sets opacity for a layer in the map
 *
 * Example: layerChangeOpacity ({layerId: 'layerid1', opacity: 0.5});
 * @param {object} payload Object with a layerId and opacity (between 0.0 and 1.0) property.
 */
export const layerChangeOpacity = (
  payload: SetLayerOpacityPayload,
): LayerActions => ({
  type: WEBMAP_LAYER_CHANGE_OPACITY,
  payload,
});

/**
 * Shows or hide a layer in the map.
 *
 * Example: layerChangeEnabled ({layerId: 'layerid1', enabled: false});
 * @param {object} payload Object with a layerId and enabled property.
 */
export const layerChangeEnabled = (
  payload: SetLayerEnabledPayload,
): LayerActions => ({
  type: WEBMAP_LAYER_CHANGE_ENABLED,
  payload,
});

/**
 * Changes the dimension of a layer. Can be used to change the time. The map will follow (if linking is set) accordingly.
 *
 * Example: layerChangeDimension( {layerId: 'layerId1', dimension: dimensionObject } )
 * @param {object} payload Object with  layerId: string, dimension: Dimension, origin?: string, service?: string
 */
export const layerChangeDimension = (
  payload: SetLayerDimensionPayload,
): LayerActions => ({
  type: WEBMAP_LAYER_CHANGE_DIMENSION,
  payload,
});

/**
 * Changes the dimensions of a layer. Can be used to change the time.
 *
 * Example: layerSetDimensions( {layerId: 'layerId1', dimensions: [dimensionObject] } )
 * @param {object} payload Object with  layerId: string, dimensions: [Dimension], origin?: string
 */
export const layerSetDimensions = (
  payload: SetLayerDimensionsPayload,
): LayerActions => ({
  type: WEBMAP_LAYER_SET_DIMENSIONS,
  payload,
});

/**
 * Changes the name of the layer
 *
 * Example: layerChangeName( { layerId: 'layerId1', name: 'NewLayerName' } )
 * @param {object} payload Object with a   layerId: string, name: string, origin?: string
 */
export const layerChangeName = (
  payload: SetLayerNamePayload,
): LayerActions => ({
  type: WEBMAP_LAYER_CHANGE_NAME,
  payload,
});

// TODO: Update this comment if SetLayerStylePayload is updated - Loes
/**
 * Changes the style of a layer
 *
 * Example: layerChangeStyle({ layerId: 'layerId1', style: 'SomeStyle' })
 * @param {object} payload Object with a layerId: string, style: string,  origin?: string, service?: string;
 */
export const layerChangeStyle = (
  payload: SetLayerStylePayload,
): LayerActions => ({
  type: WEBMAP_LAYER_CHANGE_STYLE,
  payload,
});

/**
 * Deletes a layer
 *
 * Example: layerDelete({ mapId: 'mapI1', layerId: 'layerId1' })
 * @param {object} payload Object with a mapId: string, layerId: string, origin?: string
 */
export const layerDelete = (payload: DeleteLayerPayload): LayerActions => ({
  type: WEBMAP_LAYER_DELETE,
  payload,
});

/**
 * Deletes a baselayer or an overlayer
 * Ensure the passed in layer type is an overLayer or a baseLayer otherwise nothing gets deleted
 *
 * Example: baseLayerDelete({ mapId: 'mapI1', layerId: 'layerId1' })
 * @param {object} payload Object with a mapId: string, layerId: string, origin?: string
 */
export const baseLayerDelete = (payload: DeleteLayerPayload): LayerActions => ({
  type: WEBMAP_BASELAYER_DELETE,
  payload,
});

/**
 * Handles layer error
 *
 * Example: layerError({ layerId: 'layer-1', error: new Error() })
 * @param {object} payload Object with a layerId: string, error: Error
 */
export const layerError = (payload: ErrorLayerPayload): LayerActions => ({
  type: WEBMAP_LAYER_ERROR,
  payload,
});

/**
 * Add a single baselayer available to be selected
 * Ensure the passed layers have a layertype baseLayer otherwise nothing will be added
 *
 * Example: addAvailableBaseLayer({layer: someLayerObject})
 * @param {object} payload Object with a layer
 */
export const addAvailableBaseLayer = (
  payload: AddAvailableBaseLayerPayload,
): LayerActions => ({
  type: WEBMAP_ADD_AVAILABLE_BASELAYER,
  payload,
});

/**
 * Add multiple baselayers available to be selected
 * Ensure the passed layers have a layertype baseLayer otherwise nothing will be added
 *
 * Example: addAvailableBaseLayers({layers: [someLayerObject, secondLayerObject]})
 * @param {object} payload Object with a layer
 */
export const addAvailableBaseLayers = (
  payload: AddAvailableBaseLayersPayload,
): LayerActions => ({
  type: WEBMAP_ADD_AVAILABLE_BASELAYERS,
  payload,
});
