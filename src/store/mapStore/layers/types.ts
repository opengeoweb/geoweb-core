/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { Action } from 'redux';
import {
  WEBMAP_LAYER_CHANGE_OPACITY,
  WEBMAP_LAYER_CHANGE_NAME,
  WEBMAP_LAYER_CHANGE_STYLE,
  WEBMAP_LAYER_CHANGE_DIMENSION,
  WEBMAP_LAYER_CHANGE_ENABLED,
  WEBMAP_LAYER_DELETE,
  WEBMAP_ADD_LAYER,
  WEBMAP_SET_LAYERS,
  WEBMAP_SET_BASELAYERS,
  WEBMAP_ADD_BASELAYER,
  WEBMAP_BASELAYER_DELETE,
  WEBMAP_LAYER_ERROR,
  WEBMAP_ADD_AVAILABLE_BASELAYER,
  WEBMAP_ADD_AVAILABLE_BASELAYERS,
  WEBMAP_LAYER_SET_DIMENSIONS,
} from './constants';

import { Dimension } from '../map/types';

export interface WMJSDimension {
  getFirstValue(): string;
  getLastValue(): string;
  getClosestValue(
    currentValue?: string,
    evenWhenOutsideRange?: boolean,
  ): string;
  setValue: (currentValue: string) => void;
  name: string;
  currentValue: string;
  getIndexForValue: (
    currentValue: string,
    outSideOfRangeFlag?: boolean,
  ) => number;
  getValueForIndex: (index: number) => string;
  size: () => number;
  linked: boolean;
  values?: string;
  units?: string;
  unitSymbol: string;
}

export enum LayerType {
  mapLayer = 'mapLayer',
  baseLayer = 'baseLayer',
  overLayer = 'overLayer',
}

export enum LayerStatus {
  default = 'default',
  error = 'error',
}

export interface Layer {
  id?: string;
  mapId?: string;
  service?: string;
  name?: string;
  format?: string;
  enabled?: boolean;
  style?: string;
  dimensions?: Dimension[];
  opacity?: number; // between 0.0 and 1.0
  headers?: Headers[];
  type?: string;
  layerType?: LayerType;
  geojson?: GeoJSON.FeatureCollection;
  status?: LayerStatus;
}

export interface AvailableBaseLayersType {
  byId: Record<string, Layer>;
  allIds: string[];
}

export interface LayerState {
  byId: Record<string, Layer>;
  allIds: string[];
  availableBaseLayers: AvailableBaseLayersType;
}

// actions
export interface LayerPayload {
  layerId: string;
  origin?: string;
}

export interface SetLayersPayload {
  mapId: string;
  layers: Layer[];
}

export interface SetLayers extends Action {
  type: typeof WEBMAP_SET_LAYERS;
  payload: SetLayersPayload;
}

export interface SetBaseLayersPayload {
  mapId: string;
  layers: Layer[];
}

export interface SetBaseLayers extends Action {
  type: typeof WEBMAP_SET_BASELAYERS;
  payload: SetBaseLayersPayload;
}

export interface SetLayerOpacityPayload extends LayerPayload {
  opacity: number; // between 0.0 and 1.0
}

export interface SetLayerOpacity extends Action {
  type: typeof WEBMAP_LAYER_CHANGE_OPACITY;
  payload: SetLayerOpacityPayload;
}

export interface SetLayerNamePayload extends LayerPayload {
  name: string;
}

export interface SetLayerName extends Action {
  type: typeof WEBMAP_LAYER_CHANGE_NAME;
  payload: SetLayerNamePayload;
}

export interface SetLayerEnabledPayload extends LayerPayload {
  enabled: boolean;
}

export interface SetLayerEnabled extends Action {
  type: typeof WEBMAP_LAYER_CHANGE_ENABLED;
  payload: SetLayerEnabledPayload;
}

export interface SetLayerDimensionPayload extends LayerPayload {
  origin: string;
  dimension: Dimension;
  service?: string;
}

export interface SetLayerDimensionsPayload extends LayerPayload {
  origin: string;
  dimensions: Dimension[];
}

export interface SetLayerDimension extends Action {
  type: typeof WEBMAP_LAYER_CHANGE_DIMENSION;
  payload: SetLayerDimensionPayload;
}

export interface SetLayerDimensions extends Action {
  type: typeof WEBMAP_LAYER_SET_DIMENSIONS;
  payload: SetLayerDimensionsPayload;
}

export interface SetLayerStylePayload extends LayerPayload {
  style: string; // TODO: (Sander de Snaijer, 2020-03-19) Change to name as well
}

export interface SetLayerStyle extends Action {
  type: typeof WEBMAP_LAYER_CHANGE_STYLE;
  payload: SetLayerStylePayload;
}

export interface DeleteLayerPayload extends LayerPayload {
  mapId: string;
}

export interface DeleteLayer extends Action {
  type: typeof WEBMAP_LAYER_DELETE;
  payload: DeleteLayerPayload;
}

export interface DeleteBaseLayer extends Action {
  type: typeof WEBMAP_BASELAYER_DELETE;
  payload: DeleteLayerPayload;
}

export interface AddLayerPayload {
  mapId: string;
  layerId: string;
  layer: Layer;
}

export interface AddLayer extends Action {
  type: typeof WEBMAP_ADD_LAYER;
  payload: AddLayerPayload;
}

export interface AddBaseLayer extends Action {
  type: typeof WEBMAP_ADD_BASELAYER;
  payload: AddBaseLayerPayload;
}

export interface AddBaseLayerPayload {
  mapId: string;
  layerId: string;
  layer: Layer;
}

export interface AddAvailableBaseLayer extends Action {
  type: typeof WEBMAP_ADD_AVAILABLE_BASELAYER;
  payload: AddAvailableBaseLayerPayload;
}

export interface AddAvailableBaseLayerPayload {
  layer: Layer;
}

export interface AddAvailableBaseLayers extends Action {
  type: typeof WEBMAP_ADD_AVAILABLE_BASELAYERS;
  payload: AddAvailableBaseLayersPayload;
}

export interface AddAvailableBaseLayersPayload {
  layers: Layer[];
}

export interface ErrorLayerPayload extends LayerPayload {
  error: Error;
}

export interface ErrorLayer extends Action {
  type: typeof WEBMAP_LAYER_ERROR;
  payload: ErrorLayerPayload;
}

export type LayerActions =
  | SetLayerOpacity
  | SetLayerEnabled
  | SetLayerDimension
  | SetLayerDimensions
  | SetLayerName
  | SetLayerStyle
  | DeleteLayer
  | DeleteBaseLayer
  | AddLayer
  | SetLayers
  | SetBaseLayers
  | AddBaseLayer
  | ErrorLayer
  | AddAvailableBaseLayer
  | AddAvailableBaseLayers;
