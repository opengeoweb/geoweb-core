/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { reducer as layerReducer, initialState, createLayer } from './reducer';
import * as layerActions from './actions';
import { LayerType, LayerStatus } from './types';
import { createLayersState } from '../../../utils/testUtils';

describe('store/mapStore/layers/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(layerReducer(undefined, {})).toEqual(initialState);
  });

  it('should add layer', () => {
    const layer = {
      name: 'test',
      layerType: LayerType.mapLayer,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addLayer({
        layerId,
        mapId: 'map-1',
        layer,
      }),
    );

    expect(result.byId[layerId]).toEqual(
      createLayer({ ...layer, id: layerId, mapId: 'map-1' }),
    );
    expect(result.allIds.includes(layerId)).toBeTruthy();
  });

  it('should add baselayer', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.baseLayer,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addBaseLayer({
        layerId,
        mapId: 'map-1',
        layer,
      }),
    );

    expect(result.byId[layerId]).toEqual({
      ...layer,
      id: layerId,
      dimensions: [],
      layerType: LayerType.baseLayer,
      enabled: true,
      opacity: 1,
      status: LayerStatus.default,
    });
    expect(result.allIds.includes(layerId)).toBeTruthy();
  });

  it('should add overlayer', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.overLayer,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addBaseLayer({
        layerId,
        mapId: 'map-1',
        layer,
      }),
    );

    expect(result.byId[layerId]).toEqual({
      ...layer,
      id: layerId,
      dimensions: [],
      layerType: LayerType.overLayer,
      enabled: true,
      opacity: 1,
      status: LayerStatus.default,
    });
    expect(result.allIds.includes(layerId)).toBeTruthy();
  });

  it('addBaseLayer should only accept over and base layers', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.mapLayer,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addBaseLayer({
        layerId,
        mapId: 'map-1',
        layer,
      }),
    );

    expect(result).toEqual(initialState);
  });

  it('should change dimensions', () => {
    const layerId = 'test-1';
    const initialDimension = { name: 'test-dim', currentValue: 'initialvalue' };
    const initialMockState = createLayersState(layerId, {
      mapId: 'mapid1',
      dimensions: [initialDimension],
    });

    // initial value
    expect(initialMockState.byId[layerId].dimensions).toEqual([
      initialDimension,
    ]);

    // change dimensions
    const newDimension = { name: 'test-dim', currentValue: 'newvalue' };
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: 'reducer.spec.ts',
        layerId,
        dimension: newDimension,
      }),
    );

    expect(result.byId[layerId].dimensions).toEqual([newDimension]);
  });

  it('should extend dimensions', () => {
    const layerId = 'test-1';
    const initialDimension = { name: 'test-start', currentValue: 'test-start' };
    const initialMockState = createLayersState(layerId, {
      mapId: 'mapid1',
      dimensions: [initialDimension],
    });

    // initial value
    expect(initialMockState.byId[layerId].dimensions).toEqual([
      initialDimension,
    ]);

    // change dimensions
    const newDimension = { name: 'test-end', currentValue: 'test-end' };
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: 'reducer.spec.ts',
        layerId,
        dimension: newDimension,
      }),
    );

    expect(result.byId[layerId].dimensions).toEqual([
      initialDimension,
      newDimension,
    ]);
  });

  // TODO: (Sander de Snaijer 2020-16-06): Find way to test this function, underneath it will use 'wmjsLayer.getDimensions', which will cause an error
  // it('should change multiple dimensions', () => {});

  it('should enable layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      enabled: false,
    });

    // initial value
    expect(initialMockState.byId[layerId].enabled).toBeFalsy();

    // enable layer
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeEnabled({
        layerId,
        enabled: true,
      }),
    );

    expect(result.byId[layerId].enabled).toBeTruthy();
  });

  it('should disable layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      enabled: true,
    });

    // initial value
    expect(initialMockState.byId[layerId].enabled).toBeTruthy();

    // disable layer
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeEnabled({
        layerId,
        enabled: false,
      }),
    );

    expect(result.byId[layerId].enabled).toBeFalsy();
  });

  it('should change opacity of a layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      opacity: 0.3,
    });

    // initial state
    expect(initialMockState.byId[layerId].opacity).toEqual(0.3);

    // change opacity
    const opacity = 0.5;
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeOpacity({
        layerId,
        opacity,
      }),
    );

    expect(result.byId[layerId].opacity).toEqual(opacity);
  });

  it('should change style of a layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      style: 'initial-style',
    });

    // initial state
    expect(initialMockState.byId[layerId].style).toEqual('initial-style');

    // change style
    const style = 'new-style';
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeStyle({
        layerId,
        style,
      }),
    );

    expect(result.byId[layerId].style).toEqual(style);
  });

  it('should change name of a layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      name: 'initial-name',
    });

    // initial state
    expect(initialMockState.byId[layerId].name).toEqual('initial-name');

    // change name
    const name = 'new-name';
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeName({
        layerId,
        name,
      }),
    );

    expect(result.byId[layerId].name).toEqual(name);
  });

  it('should set layers', () => {
    const initialMockState = {
      byId: {
        'layer-1-map-1': createLayer({ id: 'layer-1-map-1', mapId: 'map-1' }),
        'layer-2-map-2': createLayer({ id: 'layer-2-map-2', mapId: 'map-2' }),
        'base-layer-1-map-1': createLayer({
          id: 'base-layer-1-map-1',
          mapId: 'map-1',
          layerType: LayerType.baseLayer,
        }),
        'base-layer-2-map-1': createLayer({
          id: 'base-layer-2-map-1',
          mapId: 'map-1',
          layerType: LayerType.baseLayer,
        }),
        'base-layer-1-map-2': createLayer({
          id: 'base-layer-1',
          mapId: 'map-2',
          layerType: LayerType.baseLayer,
        }),
      },
      allIds: [
        'layer-1-map-1',
        'layer-2-map-2',
        'base-layer-1-map-1',
        'base-layer-2-map-1',
        'base-layer-1-map-2',
      ],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(5);
    expect(initialMockState.allIds.includes('layer-1-map-1')).toBeTruthy();
    expect(initialMockState.byId['layer-1-map-1']).toBeDefined();
    expect(initialMockState.allIds.includes('layer-2-map-2')).toBeTruthy();
    expect(initialMockState.byId['layer-2-map-2']).toBeDefined();
    expect(initialMockState.allIds.includes('base-layer-1-map-1')).toBeTruthy();
    expect(initialMockState.byId['base-layer-1-map-1']).toBeDefined();

    // set layers
    const layers = [{ id: 'layer-3-id', layerType: LayerType.mapLayer }];

    const result = layerReducer(
      initialMockState,
      layerActions.setLayers({ mapId: 'map-1', layers }),
    );

    expect(result.allIds).toHaveLength(5);
    expect(result.allIds.includes('layer-1')).toBeFalsy();
    expect(result.byId['layer-1']).toBeUndefined();

    layers.forEach(layer => {
      expect(result.allIds.includes(layer.id));
      expect(result.byId[layer.id]).toBeDefined();
    });

    // layers with different mapIds should be preserved
    expect(result.allIds.includes('layer-2-map-2')).toBeTruthy();
    expect(result.byId['layer-2-map-2']).toBeDefined();
    // baselayers should be preserved
    expect(result.allIds.includes('base-layer-1-map-1')).toBeTruthy();
    expect(result.byId['base-layer-1-map-1']).toBeDefined();
    expect(result.allIds.includes('base-layer-2-map-1')).toBeTruthy();
    expect(result.byId['base-layer-2-map-1']).toBeDefined();

    // Change multiple layers for map should preserve mapids
    const multipleLayers = [
      { id: 'layer-4-id', layerType: LayerType.mapLayer },
      { id: 'layer-5-id', layerType: LayerType.mapLayer },
      { id: 'layer-6-id', layerType: LayerType.mapLayer },
    ];

    const resultMultiple = layerReducer(
      result,
      layerActions.setLayers({ mapId: 'map-1', layers: multipleLayers }),
    );

    expect(resultMultiple.allIds).toHaveLength(7);
    expect(resultMultiple.allIds.includes('layer-3-id')).toBeFalsy();
    expect(resultMultiple.byId['layer-3-id']).toBeUndefined();

    multipleLayers.forEach(layer => {
      expect(resultMultiple.allIds.includes(layer.id));
      expect(resultMultiple.byId[layer.id]).toBeDefined();
    });

    // layers with different mapIds should be preserved
    expect(resultMultiple.allIds.includes('layer-2-map-2')).toBeTruthy();
    expect(resultMultiple.byId['layer-2-map-2']).toBeDefined();
    // baselayers should be preserved
    expect(resultMultiple.allIds.includes('base-layer-1-map-1')).toBeTruthy();
    expect(resultMultiple.byId['base-layer-1-map-1']).toBeDefined();

    // Change multiple layers to less for map should preserve mapids
    const multipleLayersLess = [
      { id: 'layer-6-id', layerType: LayerType.mapLayer },
    ];

    const resultMultipleLess = layerReducer(
      resultMultiple,
      layerActions.setLayers({ mapId: 'map-1', layers: multipleLayersLess }),
    );

    multipleLayersLess.forEach(layer => {
      expect(resultMultipleLess.allIds.includes(layer.id));
      expect(resultMultipleLess.byId[layer.id]).toBeDefined();
    });

    // Setting duplicate Id should do nothing
    const duplicateLayers = [
      { id: 'layer-5-id', layerType: LayerType.mapLayer },
      { id: 'layer-6-id', layerType: LayerType.mapLayer },
      { id: 'layer-5-id', layerType: LayerType.mapLayer },
    ];

    const resultduplicateLayers = layerReducer(
      initialMockState,
      layerActions.setLayers({ mapId: 'map-1', layers: duplicateLayers }),
    );
    duplicateLayers.forEach(layer => {
      expect(resultduplicateLayers.byId[layer.id]).toBeUndefined();
    });

    // Setting duplicate Id should do nothing
    const layerAlreadyUsed = [
      { id: 'layer-1-map-1', mapId: 'map-2', layerType: LayerType.mapLayer },
    ];

    layerAlreadyUsed.forEach(layer => {
      expect(initialMockState.byId[layer.id]).toBeDefined();
      expect(initialMockState.byId[layer.id].mapId).toBe('map-1');
    });

    const resultAlreadyUsedLayers = layerReducer(
      initialMockState,
      layerActions.setLayers({ mapId: 'map-1', layers: layerAlreadyUsed }),
    );
    layerAlreadyUsed.forEach(layer => {
      expect(resultAlreadyUsedLayers.byId[layer.id]).toBeDefined();
      expect(resultAlreadyUsedLayers.byId[layer.id].mapId).toBe('map-1');
    });
  });

  it('should set baselayers', () => {
    const initialMockState = {
      byId: {
        'base-layer-1': createLayer({
          id: 'layer-1',
          mapId: 'map-1',
          layerType: LayerType.baseLayer,
        }),
        'over-layer-1': createLayer({
          id: 'over-layer-1',
          mapId: 'map-1',
          layerType: LayerType.overLayer,
        }),
        'base-layer-2': createLayer({
          id: 'layer-2',
          mapId: 'map-2',
          layerType: LayerType.baseLayer,
        }),
        'over-layer-2': createLayer({
          id: 'over-layer-2',
          mapId: 'map-2',
          layerType: LayerType.overLayer,
        }),
        'layer-1': createLayer({
          id: 'layer-1',
          mapId: 'map-3',
        }),
      },
      allIds: [
        'base-layer-1',
        'over-layer-1',
        'base-layer-2',
        'over-layer-2',
        'layer-1',
      ],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(5);
    expect(initialMockState.allIds.includes('base-layer-1')).toBeTruthy();
    expect(initialMockState.byId['over-layer-1']).toBeDefined();
    expect(initialMockState.allIds.includes('over-layer-1')).toBeTruthy();
    expect(initialMockState.byId['base-layer-1']).toBeDefined();
    expect(initialMockState.allIds.includes('base-layer-2')).toBeTruthy();
    expect(initialMockState.byId['base-layer-2']).toBeDefined();
    expect(initialMockState.allIds.includes('over-layer-2')).toBeTruthy();
    expect(initialMockState.byId['over-layer-2']).toBeDefined();
    expect(initialMockState.allIds.includes('layer-1')).toBeTruthy();
    expect(initialMockState.byId['layer-1']).toBeDefined();

    // set baselayers
    const layers = [
      { id: 'base-layer-4', layerType: LayerType.baseLayer },
      { id: 'base-layer-3', layerType: LayerType.baseLayer },
      { id: 'over-layer-3', layerType: LayerType.overLayer },
    ];

    const result = layerReducer(
      initialMockState,
      layerActions.setBaseLayers({ mapId: 'map-1', layers }),
    );

    expect(result.allIds).toHaveLength(6);
    expect(result.allIds.includes('base-layer-1')).toBeFalsy();
    expect(result.byId['base-layer-1']).toBeUndefined();
    expect(result.allIds.includes('over-layer-1')).toBeFalsy();
    expect(result.byId['over-layer-1']).toBeUndefined();

    layers.forEach(layer => {
      expect(result.allIds.includes(layer.id));
      expect(result.byId[layer.id]).toBeDefined();
    });

    // baseLayers with different mapIds should be preserved
    expect(result.allIds.includes('base-layer-2')).toBeTruthy();
    expect(result.byId['base-layer-2']).toBeDefined();
    expect(result.allIds.includes('over-layer-2')).toBeTruthy();
    expect(result.byId['over-layer-2']).toBeDefined();
    expect(result.allIds.includes('base-layer-3')).toBeTruthy();
    expect(result.byId['base-layer-3']).toBeDefined();
    // layers should be preserved
    expect(result.allIds.includes('layer-1')).toBeTruthy();
    expect(result.byId['layer-1']).toBeDefined();

    // if wrong layerType is passed in, nothing should be done for that layer
    const result2 = layerReducer(
      initialMockState,
      layerActions.setBaseLayers({
        mapId: 'map-1',
        layers: [
          {
            id: 'faultyLayer1',
            name: 'faultyLayer1',
            layerType: LayerType.mapLayer,
          },
        ],
      }),
    );

    expect(result2.allIds).toHaveLength(5);
    expect(result2.allIds.includes('base-layer-1')).toBeTruthy();
    expect(result2.byId['over-layer-1']).toBeDefined();
    expect(result2.allIds.includes('over-layer-1')).toBeTruthy();
    expect(result2.byId['base-layer-1']).toBeDefined();
    expect(result2.allIds.includes('base-layer-2')).toBeTruthy();
    expect(result2.byId['base-layer-2']).toBeDefined();
    expect(result2.allIds.includes('over-layer-2')).toBeTruthy();
    expect(result2.byId['over-layer-2']).toBeDefined();
    expect(result2.allIds.includes('layer-1')).toBeTruthy();
    expect(result2.byId['layer-1']).toBeDefined();
  });

  it('should delete a layer', () => {
    const initialMockState = {
      byId: {
        'test-1': {
          id: 'test-1',
          layerType: LayerType.mapLayer,
        },
        'test-2': {
          id: 'test-2',
          layerType: LayerType.mapLayer,
        },
      },
      allIds: ['test-1', 'test-2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete layer
    const layerId = 'test-1';
    const result = layerReducer(
      initialMockState,
      layerActions.layerDelete({ layerId, mapId: 'test-1' }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-2')).toBeTruthy();
    expect(result.byId['test-2']).toBeDefined();
  });

  it('should delete a baselayer', () => {
    const initialMockState = {
      byId: {
        'test-BaseLayer1': {
          id: 'test-BaseLayer1',
          layerType: LayerType.baseLayer,
        },
        'test-BaseLayer2': {
          id: 'test-BaseLayer2',
          layerType: LayerType.baseLayer,
        },
      },
      allIds: ['test-BaseLayer1', 'test-BaseLayer2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete baselayer
    const layerId = 'test-BaseLayer1';
    const result = layerReducer(
      initialMockState,
      layerActions.baseLayerDelete({ layerId, mapId: 'test-BaseLayer1' }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-BaseLayer2')).toBeTruthy();
    expect(result.byId['test-BaseLayer2']).toBeDefined();
  });

  it('should error a layer', () => {
    const initialMockState = {
      byId: {
        'test-1': {
          id: 'test-1',
          layerType: LayerType.mapLayer,
          status: LayerStatus.default,
        },
      },
      allIds: ['test-1'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.byId['test-1'].status).toEqual(LayerStatus.default);

    const result = layerReducer(
      initialMockState,
      layerActions.layerError({ layerId: 'test-1', error: new Error('error') }),
    );

    expect(result.byId['test-1'].status).toEqual(LayerStatus.error);
  });

  it('should delete a layer', () => {
    const initialMockState = {
      byId: {
        'test-1': {
          id: 'test-1',
          layerType: LayerType.mapLayer,
        },
        'test-2': {
          id: 'test-2',
          layerType: LayerType.mapLayer,
        },
      },
      allIds: ['test-1', 'test-2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete layer
    const layerId = 'test-1';
    const result = layerReducer(
      initialMockState,
      layerActions.layerDelete({ layerId, mapId: 'test-1' }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-2')).toBeTruthy();
    expect(result.byId['test-2']).toBeDefined();
  });

  it('should delete a baselayer', () => {
    const initialMockState = {
      byId: {
        'test-BaseLayer1': {
          id: 'test-BaseLayer1',
          layerType: LayerType.baseLayer,
        },
        'test-BaseLayer2': {
          id: 'test-BaseLayer2',
          layerType: LayerType.baseLayer,
        },
      },
      allIds: ['test-BaseLayer1', 'test-BaseLayer2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete baselayer
    const layerId = 'test-BaseLayer1';
    const result = layerReducer(
      initialMockState,
      layerActions.baseLayerDelete({ layerId, mapId: 'test-BaseLayer1' }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-BaseLayer2')).toBeTruthy();
    expect(result.byId['test-BaseLayer2']).toBeDefined();
  });

  it('should delete an overlayer', () => {
    const initialMockState = {
      byId: {
        'test-overLayer1': {
          id: 'test-overLayer1',
          layerType: LayerType.overLayer,
        },
        'test-overLayer2': {
          id: 'test-overLayer2',
          layerType: LayerType.overLayer,
        },
      },
      allIds: ['test-overLayer1', 'test-overLayer2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete overlayer
    const layerId = 'test-overLayer1';
    const result = layerReducer(
      initialMockState,
      layerActions.baseLayerDelete({ layerId, mapId: 'test-map1' }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-overLayer2')).toBeTruthy();
    expect(result.byId['test-overLayer2']).toBeDefined();
  });

  it('baseLayerDelete should return state if no overlayer or baselayer is passed', () => {
    const initialMockState = {
      byId: {
        'test-overLayer1': {
          id: 'test-overLayer1',
          layerType: LayerType.overLayer,
        },
        'test-normalLayer2': {
          id: 'test-normalLayer2',
          layerType: LayerType.mapLayer,
        },
      },
      allIds: ['test-overLayer1', 'test-normalLayer2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete overlayer
    const layerId = 'test-normalLayer2';
    const result = layerReducer(
      initialMockState,
      layerActions.baseLayerDelete({ layerId, mapId: 'test-map1' }),
    );

    expect(result.allIds).toHaveLength(2);
    expect(result.allIds.includes('test-normalLayer2')).toBeTruthy();
    expect(result.byId['test-normalLayer2']).toBeDefined();

    expect(result.allIds.includes('test-overLayer1')).toBeTruthy();
    expect(result.byId['test-overLayer1']).toBeDefined();
  });

  it('should add an available baselayer', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.baseLayer,
    };
    const result = layerReducer(
      undefined,
      layerActions.addAvailableBaseLayer({
        layer,
      }),
    );

    expect(result.availableBaseLayers.byId[layer.id]).toEqual({
      ...layer,
      id: layer.id,
      dimensions: [],
      layerType: LayerType.baseLayer,
      enabled: true,
      opacity: 1,
      status: LayerStatus.default,
    });
    expect(result.availableBaseLayers.allIds.includes(layer.id)).toBeTruthy();

    // Adding the same base layer again should not result in any changes
    const result2 = layerReducer(
      result,
      layerActions.addAvailableBaseLayer({
        layer,
      }),
    );

    expect(result2.availableBaseLayers.allIds).toHaveLength(1);
    expect(result2.availableBaseLayers.byId[layer.id]).toEqual({
      ...layer,
      id: layer.id,
      dimensions: [],
      layerType: LayerType.baseLayer,
      enabled: true,
      opacity: 1,
      status: LayerStatus.default,
    });
    expect(result2.availableBaseLayers.allIds.includes(layer.id)).toBeTruthy();

    // Adding a new baselayer should not affect the exiting available base layers
    const layer3 = {
      name: 'test3',
      id: 'test-3',
      layerType: LayerType.baseLayer,
    };

    const result3 = layerReducer(
      result2,
      layerActions.addAvailableBaseLayer({
        layer: layer3,
      }),
    );

    expect(result3.availableBaseLayers.allIds).toHaveLength(2);
    expect(result3.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result3.availableBaseLayers.allIds.includes('test-3')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-3']).toBeDefined();
  });

  it('should add multiple available baselayers', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.baseLayer,
    };
    const layer2 = {
      name: 'test2',
      id: 'test-2',
      layerType: LayerType.baseLayer,
    };
    const result = layerReducer(
      undefined,
      layerActions.addAvailableBaseLayers({
        layers: [layer, layer2],
      }),
    );

    expect(result.availableBaseLayers.allIds).toHaveLength(2);
    expect(result.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result.availableBaseLayers.allIds.includes('test-2')).toBeTruthy();
    expect(result.availableBaseLayers.byId['test-2']).toBeDefined();

    // Adding the same baselayer should not result in any changes
    const result2 = layerReducer(
      result,
      layerActions.addAvailableBaseLayers({
        layers: [layer2],
      }),
    );

    expect(result2.availableBaseLayers.allIds).toHaveLength(2);
    expect(result2.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result2.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result2.availableBaseLayers.allIds.includes('test-2')).toBeTruthy();
    expect(result2.availableBaseLayers.byId['test-2']).toBeDefined();

    // Adding a new baselayer should not affect the exiting available base layers
    const layer3 = {
      name: 'test3',
      id: 'test-3',
      layerType: LayerType.baseLayer,
    };

    const result3 = layerReducer(
      result2,
      layerActions.addAvailableBaseLayers({
        layers: [layer3],
      }),
    );
    expect(result3.availableBaseLayers.allIds).toHaveLength(3);
    expect(result3.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result3.availableBaseLayers.allIds.includes('test-2')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-2']).toBeDefined();
    expect(result3.availableBaseLayers.allIds.includes('test-3')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-3']).toBeDefined();
  });
});
