/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

export const WEBMAP_LAYER_CHANGE_OPACITY = 'WEBMAP_LAYER_CHANGE_OPACITY';
export const WEBMAP_LAYER_CHANGE_NAME = 'WEBMAP_LAYER_CHANGE_NAME';
export const WEBMAP_LAYER_CHANGE_STYLE = 'WEBMAP_LAYER_CHANGE_STYLE';
export const WEBMAP_LAYER_CHANGE_DIMENSION = 'WEBMAP_LAYER_CHANGE_DIMENSION';
export const WEBMAP_LAYER_SET_DIMENSIONS = 'WEBMAP_LAYER_SET_DIMENSIONS';
export const WEBMAP_LAYER_CHANGE_ENABLED = 'WEBMAP_LAYER_CHANGE_ENABLED';
export const WEBMAP_LAYER_DELETE = 'WEBMAP_LAYER_DELETE';
export const WEBMAP_LAYER_ERROR = 'WEBMAP_LAYER_ERROR';
export const WEBMAP_BASELAYER_DELETE = 'WEBMAP_BASELAYER_DELETE';
export const WEBMAP_ADD_LAYER = 'WEBMAP_ADD_LAYER';
export const WEBMAP_SET_LAYERS = 'WEBMAP_SET_LAYERS';
export const WEBMAP_SET_BASELAYERS = 'WEBMAP_SET_BASELAYERS';
export const WEBMAP_ADD_BASELAYER = 'WEBMAP_ADD_BASELAYER';
export const WEBMAP_ADD_AVAILABLE_BASELAYER = 'WEBMAP_ADD_AVAILABLE_BASELAYER';
export const WEBMAP_ADD_AVAILABLE_BASELAYERS =
  'WEBMAP_ADD_AVAILABLE_BASELAYERS';
