/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as mapSelectors from './selectors';
import {
  defaultReduxServices,
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
  layerWithoutTimeDimension,
  multiDimensionLayer,
  multiDimensionLayer2,
} from '../../../utils/defaultTestSettings';
import {
  mockStateMapWithDimensions,
  mockStateMapWithMultipleLayers,
} from '../../../utils/testUtils';
import { createMap } from '../map/utils';

const fakeState = {
  mapServices: {
    services: defaultReduxServices,
  },
};

describe('store/mapStore/service/selectors', () => {
  describe('getServices', () => {
    it('should return services', () => {
      expect(mapSelectors.getServices(fakeState)).toEqual(
        fakeState.mapServices.services,
      );
      expect(mapSelectors.getServices({})).toEqual([]);
    });
  });

  describe('getServiceLayersByName', () => {
    it('should return services layers when service exists', () => {
      const serviceName = 'https://testservice';
      expect(
        mapSelectors.getServiceLayersByName(fakeState, serviceName),
      ).toEqual(fakeState.mapServices.services['https://testservice'].layers);
    });

    it('should return null when service does not exists', () => {
      expect(
        mapSelectors.getServiceLayersByName(fakeState, 'fake-service'),
      ).toBeNull();
    });
  });

  describe('getServiceLayerDimensions', () => {
    it('should return dimensions when layer has dimensions', () => {
      const mockState = mockStateMapWithDimensions(
        defaultReduxLayerRadarKNMI,
        'mapId-1',
      );

      expect(
        mapSelectors.getServiceLayerDimensions(
          mockState,
          defaultReduxLayerRadarKNMI.id,
        ),
      ).toEqual(
        mockState.mapServices.services[defaultReduxLayerRadarKNMI.service]
          .layer[defaultReduxLayerRadarKNMI.name].dimensions,
      );
    });

    it('should return an empty list when layer does not exists', () => {
      const mockState = mockStateMapWithDimensions(
        defaultReduxLayerRadarKNMI,
        'mapId-1',
      );
      expect(
        mapSelectors.getServiceLayerDimensions(mockState, 'mock-id'),
      ).toHaveLength(0);
    });

    it('should return an empty list when layer has no dimensions', () => {
      const mockState = mockStateMapWithDimensions(
        defaultReduxLayerRadarColor,
        'mapId-1',
      );
      expect(
        mapSelectors.getServiceLayerDimensions(
          mockState,
          defaultReduxLayerRadarColor.id,
        ),
      ).toHaveLength(0);
    });
  });

  describe('getServiceLayerNonTimeDimensions', () => {
    it('should return all dimensions for a layer without time', () => {
      const mockState = mockStateMapWithDimensions(
        layerWithoutTimeDimension,
        'mapId-1',
      );
      expect(
        mapSelectors.getServiceLayerNonTimeDimensions(
          mockState,
          layerWithoutTimeDimension.id,
        ),
      ).toEqual(
        mockState.mapServices.services[layerWithoutTimeDimension.service].layer[
          layerWithoutTimeDimension.name
        ].dimensions,
      );
    });

    it('should return only non-time dimensions for a multidimension layer', () => {
      const mockState = mockStateMapWithDimensions(
        multiDimensionLayer2,
        'mapId-1',
      );
      expect(
        mapSelectors.getServiceLayerNonTimeDimensions(
          mockState,
          multiDimensionLayer2.id,
        ),
      ).toEqual([
        {
          name: 'member',
          units: 'member number',
          currentValue: 'member1',
          values: 'member1,member2,member3,member4',
        },
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
        },
      ]);
    });

    it('should return an empty list when layer does not have dimensions other than time', () => {
      const mockState = mockStateMapWithDimensions(
        defaultReduxLayerRadarKNMI,
        'mapId-1',
      );
      expect(
        mapSelectors.getServiceLayerNonTimeDimensions(
          mockState,
          defaultReduxLayerRadarKNMI.id,
        ),
      ).toHaveLength(0);
    });
  });
});

describe('getServiceLayersByDimension', () => {
  it('should return an empty list when no layer has the specified dimension', () => {
    const mockState = mockStateMapWithDimensions(
      multiDimensionLayer2,
      'mapId-1',
    );
    expect(
      mapSelectors.getServiceLayersByDimension(mockState, 'nonexisting'),
    ).toHaveLength(0);
  });

  it('should return an empty list when there are no layers', () => {
    const layers = [];
    const mockState = mockStateMapWithMultipleLayers(layers, 'mapId-1');

    expect(
      mapSelectors.getServiceLayersByDimension(mockState, 'flight level'),
    ).toHaveLength(0);
  });

  it('should return layers containing the specified dimension', () => {
    const layers = [multiDimensionLayer, defaultReduxLayerRadarKNMI];
    const mockState = mockStateMapWithMultipleLayers(layers, 'mapId-1');

    expect(
      mapSelectors.getServiceLayersByDimension(mockState, 'flight level'),
    ).toHaveLength(1);

    expect(
      mapSelectors.getServiceLayersByDimension(mockState, 'time'),
    ).toHaveLength(2);
  });

  describe('getMapServiceLayersByDimension', () => {
    const mapId = 'mapId-1';
    it('should return an empty list when no layer has the specified dimension', () => {
      const mockState = mockStateMapWithDimensions(multiDimensionLayer2, mapId);
      expect(
        mapSelectors.getMapServiceLayersByDimension(
          mockState,
          mapId,
          'nonexisting',
        ),
      ).toHaveLength(0);
    });

    it('should return an empty list when there are no layers', () => {
      const layers = [];
      const mockState = mockStateMapWithMultipleLayers(layers, mapId);

      expect(
        mapSelectors.getMapServiceLayersByDimension(
          mockState,
          mapId,
          'flight level',
        ),
      ).toHaveLength(0);
    });

    it('should return layers containing the specified dimension', () => {
      const layers = [multiDimensionLayer, defaultReduxLayerRadarKNMI];
      const mockState = mockStateMapWithMultipleLayers(layers, mapId);
      expect(
        mapSelectors.getMapServiceLayersByDimension(
          mockState,
          mapId,
          'flight level',
        ),
      ).toHaveLength(1);

      expect(
        mapSelectors.getMapServiceLayersByDimension(mockState, mapId, 'time'),
      ).toHaveLength(2);
    });

    it('should ignore layers belonging to a different map and return an empty list when the specified map does not contain a layer with the dimension', () => {
      const mapId2 = 'mapId2';
      const webmap2 = createMap({ id: mapId2 });

      const layers = [multiDimensionLayer, defaultReduxLayerRadarKNMI];
      const mockState = mockStateMapWithMultipleLayers(layers, mapId);

      mockState.webmap.allIds.push(mapId2);
      mockState.webmap.byId[mapId2] = webmap2;
      expect(
        mapSelectors.getMapServiceLayersByDimension(mockState, mapId2, 'time'),
      ).toHaveLength(0);
    });
  });
});
