/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { reducer as serviceReducer, initialState } from './reducer';
import * as serviceActions from './actions';
import { ServiceState } from './types';

const layers = [
  {
    name: 'test',
    text: 'testtext',
    leaf: true,
    path: 'string',
  },
  {
    name: 'test2',
    text: 'testtext2',
    leaf: true,
    path: 'string',
  },
];

const styles = [
  {
    title: 'styleTitle',
    name: 'stylename',
    legendURL: 'legendURL',
    abstract: 'someabstract',
  },
];

const someDim = [
  { currentValue: 'someValue' },
  { currentValue: 'anotherValue' },
];

const createMockState = (): ServiceState => ({
  services: {
    someUrl: {
      layers,
      layer: { data: { styles, dimensions: someDim } },
    },
  },
});

describe('store/mapStore/service/reducer', () => {
  const service = 'someURL';
  const layerName = 'layerName';

  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(serviceReducer(undefined, {})).toEqual(initialState);
  });

  describe('serviceSetLayers', () => {
    it('should set layers if no state passed', () => {
      const result = serviceReducer(
        undefined,
        serviceActions.serviceSetLayers({
          service,
          layers,
        }),
      );

      expect(result.services[service]).toEqual({ layers });
    });

    it('should set layers if current state passed', () => {
      const newLayers = [
        {
          name: 'newLayerName',
          text: 'newTestText',
          leaf: true,
          path: 'somePath',
        },
      ];

      const result = serviceReducer(
        createMockState(),
        serviceActions.serviceSetLayers({
          service,
          layers: newLayers,
        }),
      );

      expect(result.services[service]).toEqual({ layers: newLayers });
    });
  });

  describe('setLayerStyles', () => {
    it('should set styles if no state passed', () => {
      const result = serviceReducer(
        undefined,
        serviceActions.layerSetStyles({
          service,
          name: layerName,
          styles,
        }),
      );

      expect(result.services[service].layer[layerName].styles).toEqual(styles);
    });

    it('should set styles if current state passed', () => {
      const newStyles = [
        {
          title: 'newStyleTitle',
          name: 'newStyleName',
          legendURL: 'legendURL_123',
          abstract: 'abstract_2',
        },
        {
          title: 'newStyleTitle2',
          name: 'newStyleName2',
          legendURL: 'legendURL_1',
          abstract: 'abstract_2',
        },
      ];
      const result = serviceReducer(
        createMockState(),
        serviceActions.layerSetStyles({
          service,
          name: layerName,
          styles: newStyles,
        }),
      );

      expect(result.services[service].layer[layerName].styles).toEqual(
        newStyles,
      );
    });
  });

  describe('setLayerDimensionsForService', () => {
    const serviceName = 'someService';
    it('should set dimensions if no state passed', () => {
      const result = serviceReducer(
        undefined,
        serviceActions.setLayerDimensionsForService({
          service: serviceName,
          name: layerName,
          dimensions: someDim,
        }),
      );

      expect(result.services[serviceName].layer[layerName].dimensions).toEqual(
        someDim,
      );
    });

    it('should set dimensions if current state passed', () => {
      const newDim = [
        { currentValue: 'someNewValue' },
        { units: 'someUnits', currentValue: 'anotherNewValue' },
      ];
      const result = serviceReducer(
        createMockState(),
        serviceActions.setLayerDimensionsForService({
          service: serviceName,
          name: layerName,
          dimensions: newDim,
        }),
      );

      expect(result.services[serviceName].layer[layerName].dimensions).toEqual(
        newDim,
      );
    });
  });
});
