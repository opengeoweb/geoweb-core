/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from 'reselect';
import { AppStore } from '../../../types/types';
import { ServiceState, ServiceLayer } from './types';
import { Dimension, Layer } from '../types';
import * as layerSelectors from '../layers/selectors';
import { filterNonTimeDimensions } from '../utils/helpers';
import * as mapSelectors from '../map/selectors';

const mapServicesStore = (store: AppStore): ServiceState =>
  store.mapServices ? store.mapServices : null;

/**
 * Gets map services
 *
 * Example: services = getServices(store)
 * @param {object} store store: object - Store object
 * @returns {object} returnType: ServiceState
 */
export const getServices = createSelector(mapServicesStore, store =>
  store ? store.services : [],
);

/**
 * Gets map layers from servicename
 *
 * Example: layers = getServices(store, 'serviceName')
 * @param {object} store store: object - Store object
 * @returns {array} returnType: ServiceLayer[]
 */
export const getServiceLayersByName = (
  store: AppStore,
  serviceName: string,
): ServiceLayer[] => {
  if (
    store &&
    store.mapServices &&
    store.mapServices.services &&
    store.mapServices.services[serviceName] &&
    store.mapServices.services[serviceName].layers
  ) {
    return store.mapServices.services[serviceName].layers;
  }
  return null;
};

/**
 * Gets all dimensions of a layer from a service
 *
 * Example: dimensions = getServiceLayerDimensions(store, 'layer-id')
 * @param {object} store store: object - Store object
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {array} returnType: Dimension[]
 */
export const getServiceLayerDimensions = (
  store: AppStore,
  layerId: string,
): Dimension[] => {
  const layerName = layerSelectors.getLayerName(store, layerId);
  const layerService = layerSelectors.getLayerService(store, layerId);

  if (
    layerName &&
    layerService &&
    getServices(store) &&
    getServices(store)[layerService] &&
    getServices(store)[layerService].layer &&
    getServices(store)[layerService].layer[layerName] &&
    getServices(store)[layerService].layer[layerName].dimensions
  ) {
    return getServices(store)[layerService].layer[layerName].dimensions;
  }
  return [];
};

/**
 * Gets all non time dimensions of a layer from a service
 *
 * Example: nonTimeDimensions = getServiceLayerNonTimeDimensions(store, 'layer-id')
 * @param {object} store store: object - Store object
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {array} returnType: Dimension[]
 */
export const getServiceLayerNonTimeDimensions = createSelector(
  getServiceLayerDimensions,
  dimensions => filterNonTimeDimensions(dimensions),
);

/**
 * Gets layers from store by dimension name
 *
 * Example: layer = getServiceLayersByDimension(store, 'dimName')
 * @param {object} store object from which the layer state will be extracted
 * @param {string} dimName dimension Name that the returned layers should contain
 * @returns {array} returnType:Layer[] array containing layers with desired dimension name
 */
export const getServiceLayersByDimension = (
  store: AppStore,
  dimName: string,
): Layer[] => {
  const allLayers = layerSelectors.getAllLayers(store);
  return allLayers.filter((layer: Layer) => {
    const serviceDimensions = getServiceLayerDimensions(store, layer.id);
    if (serviceDimensions.length)
      return serviceDimensions.some((dim: Dimension) => dim.name === dimName);
    return false;
  });
};

/**
 * Gets layers from store by dimension name for a certain map
 *
 * Example: layer = getMapServiceLayersByDimension(store, 'dimName')
 * @param {object} store object from which the layer state will be extracted
 * @param {string} mapId ]Id of the map
 * @param {string} dimName dimension Name that the returned layers should contain
 * @returns {array} returnType:Layer[] array containing layers containting requested dimension for specified map
 */
export const getMapServiceLayersByDimension = (
  store: AppStore,
  mapId: string,
  dimName: string,
): Layer[] => {
  const mapLayers = mapSelectors.getMapLayers(store, mapId);
  return mapLayers.filter((layer: Layer) => {
    const serviceDimensions = getServiceLayerDimensions(store, layer.id);
    return serviceDimensions.some((dim: Dimension) => dim.name === dimName);
  });
};
