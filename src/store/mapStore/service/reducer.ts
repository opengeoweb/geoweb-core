/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-console */
import produce from 'immer';
import {
  MAP_SERVICES_SET_LAYERS,
  MAP_SERVICES_LAYER_SET_STYLES,
  MAP_SERVICES_LAYER_SET_DIMENSIONS,
} from './constants';

import { ServiceActions } from '../types';
import { ServiceState } from './types';

export const initialState = {
  services: {},
};

export const WEBMAPJS_REDUCERNAME = 'react-webmapjs';

export const reducer = (
  state = initialState,
  action: ServiceActions,
): ServiceState => {
  switch (action.type) {
    case MAP_SERVICES_SET_LAYERS:
      return produce(state, draft => {
        if (!draft.services[action.payload.service])
          draft.services[action.payload.service] = {};
        draft.services[action.payload.service].layers = action.payload.layers;
      });

    case MAP_SERVICES_LAYER_SET_STYLES:
      return produce(state, draft => {
        if (!draft.services[action.payload.service])
          draft.services[action.payload.service] = {};
        if (!draft.services[action.payload.service].layer)
          draft.services[action.payload.service].layer = {};
        if (!draft.services[action.payload.service].layer[action.payload.name])
          draft.services[action.payload.service].layer[
            action.payload.name
          ] = {};
        draft.services[action.payload.service].layer[
          action.payload.name
        ].styles = action.payload.styles;
      });
    case MAP_SERVICES_LAYER_SET_DIMENSIONS:
      return produce(state, draft => {
        if (!draft.services[action.payload.service])
          draft.services[action.payload.service] = {};
        if (!draft.services[action.payload.service].layer)
          draft.services[action.payload.service].layer = {};
        if (!draft.services[action.payload.service].layer[action.payload.name])
          draft.services[action.payload.service].layer[
            action.payload.name
          ] = {};
        const dimensions = [];
        if (action.payload.dimensions) {
          for (let j = 0; j < action.payload.dimensions.length; j += 1) {
            dimensions.push({
              name: action.payload.dimensions[j].name,
              units: action.payload.dimensions[j].units,
              currentValue: action.payload.dimensions[j].currentValue,
            });
          }
        }
        draft.services[action.payload.service].layer[
          action.payload.name
        ].dimensions = dimensions;
      });

    default:
      return state;
  }
};
