/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  MAP_SERVICES_SET_LAYERS,
  MAP_SERVICES_LAYER_SET_STYLES,
  MAP_SERVICES_LAYER_SET_DIMENSIONS,
} from './constants';
import {
  ServiceActions,
  SetLayerDimensionsForServicePayload,
  SetLayersForServicePayload,
  SetLayerStylesForServicePayload,
} from './types';

/**
 * Action setLayerDimensionsForService is used automatically after calling the setLayers action.
 * It is populating the intial list of dimensions for the layer.
 * Contents is based on WMS GetCapabilities
 *
 * Example: setLayerDimensionsForService({origin: 'someOrigin', service: someService, name: 'layerName', dimensions: dimensionObjecttArray})
 * @param {object} payload consisting of service: string, name: string (layerName) and dimensions: Dimension[]
 */
export const setLayerDimensionsForService = (
  payload: SetLayerDimensionsForServicePayload,
): ServiceActions => ({
  type: MAP_SERVICES_LAYER_SET_DIMENSIONS,
  payload,
});

/**
 * Action serviceSetLayers is used automatically after calling the setLayers action. Contents is based on WMS GetCapabilities
 *
 * Example: serviceSetLayers({service: someService, layers: serviceLayerArray})
 * @param {object} payload consisting of  service: string, layers: ServiceLayer[]
 */
export const serviceSetLayers = (
  payload: SetLayersForServicePayload,
): ServiceActions => ({
  type: MAP_SERVICES_SET_LAYERS,
  payload,
});

/**
 * Action layerSetStyles is used automatically after calling the setLayers action. Contents is based on WMS GetCapabilities
 *
 * Example: layerSetStyles({service: someService,  name: 'layerName', styles: styleArray})
 * @param {object} payload consisting of service: string, name: string, styles: Style[]
 */
export const layerSetStyles = (
  payload: SetLayerStylesForServicePayload,
): ServiceActions => ({
  type: MAP_SERVICES_LAYER_SET_STYLES,
  payload,
});
