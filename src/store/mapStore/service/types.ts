/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { Action } from 'redux';

import { Dimension, Style } from '../map/types';

import {
  MAP_SERVICES_SET_LAYERS,
  MAP_SERVICES_LAYER_SET_STYLES,
  MAP_SERVICES_LAYER_SET_DIMENSIONS,
} from './constants';

// TODO: (Loes Cornelis, 2020-03-24) Need to further sepecify styles for this type
export interface ServicesLayerStyles {
  styles: Record<string, unknown>[];
  dimensions?: Dimension[];
}

export interface Services {
  [key: string]: Service;
}

export interface Service {
  layers?: ServiceLayer[];
  layer?: Record<string, ServicesLayerStyles>;
}

/**
 * Used in the services object of webmap to keep a list of available layers in the service
 */
export interface ServiceLayer {
  name: string;
  text: string;
  leaf: boolean;
  path: string;
}

export interface ServiceState {
  services: Services;
}

export interface SetLayerDimensionsForServicePayload {
  origin?: string;
  service: string /* The service base url */;
  name: string /* The name of the layer */;
  dimensions: Dimension[];
}

interface SetLayerDimensionsForService extends Action {
  type: typeof MAP_SERVICES_LAYER_SET_DIMENSIONS;
  payload: SetLayerDimensionsForServicePayload;
}

export interface SetLayersForServicePayload {
  service: string;
  layers: ServiceLayer[];
}

interface SetLayersForService extends Action {
  type: typeof MAP_SERVICES_SET_LAYERS;
  payload: SetLayersForServicePayload;
}

export interface SetLayerStylesForServicePayload {
  origin?: string;
  service: string /* The service base url */;
  name: string /* The name of the layer */;
  styles: Style[];
}

interface SetLayerStylesForService extends Action {
  type: typeof MAP_SERVICES_LAYER_SET_STYLES;
  payload: SetLayerStylesForServicePayload;
}

export type ServiceActions =
  | SetLayerDimensionsForService
  | SetLayersForService
  | SetLayerStylesForService;
