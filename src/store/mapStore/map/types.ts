/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { Moment } from 'moment';
import { Action } from 'redux';

import {
  WEBMAP_LAYER_MOVE,
  WEBMAP_SET_BBOX,
  WEBMAP_MAP_UPDATE_ALL_DIMENSIONS,
  WEBMAP_MAP_CHANGE_DIMENSION,
  WEBMAP_START_ANIMATION,
  WEBMAP_STOP_ANIMATION,
  WEBMAP_REGISTER_MAP,
  WEBMAP_UNREGISTER_MAP,
  WEBMAP_SET_ACTIVELAYERID,
  WEBMAP_TOGGLE_AUTO_UPDATE,
  WEBMAP_TOGGLE_LOOP,
  WEBMAP_SET_TIME_SLIDER_SCALE,
  WEBMAP_SET_TIME_STEP,
  WEBMAP_SET_ANIMATION_DELAY,
  WEBMAP_SET_ANIMATION_START_TIME,
  WEBMAP_SET_ANIMATION_END_TIME,
  WEBMAP_TOGGLE_TIMESTEP_AUTO,
  WEBMAP_TOGGLE_TIME_SLIDER_HOVER,
  WEBMAP_SET_TIME_SLIDER_CENTER_TIME,
  WEBMAP_SET_TIME_SLIDER_SECONDS_PER_PX,
} from './constants';
import WMJSDimension from '../../../geoweb-webmap/WMJSDimension';
import WMListener from '../../../geoweb-webmap/WMListener';

// Objects

/**
 * Dimension type
 * Dimension keeps the name of the dimension (time/elevation/treshold/member/ensemble/.../) and their unit.
 * The currentValue is the current value set for the dimension
 */
export interface Dimension {
  name?: string;
  units?: string;
  currentValue: string;
  maxValue?: string;
  minValue?: string;
  timeInterval?: Record<string, unknown>;
  allDates?: string[];
  synced?: boolean;
  values?: string;
}

export interface Style {
  title: string;
  name: string;
  legendURL: string;
  abstract: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface WMLayer {
  setDimension: (
    name: string,
    value: string,
    updateMapDimensions: boolean,
  ) => void;
  getDimension: (name: string) => WMJSDimension;
  dimensions?: WMJSDimension[];
  title?: string;
}

export interface WMJSMapPin {
  redrawMap: () => void;
  drawMarker: (ctx: CanvasRenderingContext2D) => void;
  repositionMapPin: (bbox) => void;
  setMapPin: (x: number, y: number, bbox: Bbox) => void;
  positionMapPinByLatLon: (coords: { x: number; y: number }) => void;
  showMapPin: () => void;
  hideMapPin: () => void;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface WMJSMap {
  xml2jsonrequest?: string;
  mapPin: WMJSMapPin;
  addListener: (
    name: string, // The name of the event to listen to
    callback: (any) => void, // The function to trigger when the event is triggered
    keepListener: boolean, // false means it will be triggered only once, otherwise it will always be triggerd
  ) => void;
  mapdimensions: Dimension[];
  setDimension: (
    name: string,
    value: string,
    triggerCallback?: boolean,
    updateLayers?: boolean,
  ) => void;
  redrawBuffer: () => void;
  deleteLayer: (layer: WMLayer) => void;
  zoomToLayer: (layer: WMLayer) => void;
  moveLayerUp: (layer: WMLayer) => void;
  moveLayerDown: (layer: WMLayer) => void;
  draw: (input?: string) => void;
  displayLayer: (layer: WMLayer, enabled: boolean) => void;
  getListener(): WMListener;
  getDimension(name): WMJSDimension;
}

export interface WebMap {
  id: string;
  isAnimating: boolean;

  animationStartTime?: string;
  animationEndTime?: string;
  isAutoUpdating: boolean;
  isLooping: boolean;
  srs: string;
  bbox: Bbox;
  mapLayers: string[];
  baseLayers: string[];
  overLayers: string[];
  featureLayers: string[];
  dimensions?: Dimension[];
  activeLayerId?: string;
  timeSliderScale?: number;
  timeStep?: number;
  animationDelay?: number;
  timeSliderCenterTime?: number;
  timeSliderSecondsPerPx?: number;
  isTimestepAuto?: boolean;
  isTimeSliderHoverOn?: boolean;
}

export interface WebMapState {
  byId: Record<string, WebMap>;
  allIds: string[];
}

export interface Bbox {
  left: number;
  right: number;
  top: number;
  bottom: number;
}

// Layer actions
export interface RegisterMap extends Action {
  type: typeof WEBMAP_REGISTER_MAP;
  payload: {
    mapId: string;
  };
}

export interface UnregisterMap extends Action {
  type: typeof WEBMAP_UNREGISTER_MAP;
  payload: {
    mapId: string;
  };
}

export interface MoveLayerPayload {
  mapId: string;
  oldIndex: number;
  newIndex: number;
}

export interface MoveLayer extends Action {
  type: typeof WEBMAP_LAYER_MOVE;
  payload: MoveLayerPayload;
}

export interface SetBboxPayload {
  mapId: string;
  bbox: Bbox;
  srs?: string;
}

export interface SetMapDimensionPayload {
  origin: string;
  mapId: string;
  dimension: Dimension;
}

export interface SetMapDimension extends Action {
  type: typeof WEBMAP_MAP_CHANGE_DIMENSION;
  payload: SetMapDimensionPayload;
}

export interface UpdateAllMapDimensionsPayload {
  origin: string;
  mapId: string;
  dimensions: Dimension[];
}

export interface UpdateAllMapDimensions extends Action {
  type: typeof WEBMAP_MAP_UPDATE_ALL_DIMENSIONS;
  payload: UpdateAllMapDimensionsPayload;
}

export type TimeListType = {
  name: string;
  value: string;
};

export interface SetMapAnimationStartPayload {
  mapId: string;
  start?: Moment;
  end?: Moment;
  interval?: number; // in seconds
  timeList?: TimeListType[];
}

export interface SetMapAnimationStart extends Action {
  type: typeof WEBMAP_START_ANIMATION;
  payload: SetMapAnimationStartPayload;
}

export interface SetMapAnimationStopPayload {
  mapId: string;
}

export interface SetMapAnimationStop extends Action {
  type: typeof WEBMAP_STOP_ANIMATION;
  payload: SetMapAnimationStopPayload;
}

export interface SetBbox extends Action {
  type: typeof WEBMAP_SET_BBOX;
  payload: SetBboxPayload;
}

export interface SetActiveLayerId extends Action {
  type: typeof WEBMAP_SET_ACTIVELAYERID;
  payload: SetActiveLayerIdPayload;
}
export interface SetActiveLayerIdPayload {
  mapId: string;
  layerId: string;
}

export interface ToggleAutoUpdatePayload {
  mapId: string;
  shouldAutoUpdate: boolean;
}
export interface ToggleLoopPayload {
  mapId: string;
  shouldLoop: boolean;
}
export interface ToggleAutoUpdate extends Action {
  type: typeof WEBMAP_TOGGLE_AUTO_UPDATE;
  payload: ToggleAutoUpdatePayload;
}

export interface ToggleLoop extends Action {
  type: typeof WEBMAP_TOGGLE_LOOP;
  payload: ToggleLoopPayload;
}

export interface SetTimeSliderScale extends Action {
  type: typeof WEBMAP_SET_TIME_SLIDER_SCALE;
  payload: SetTimeSliderScalePayload;
}

export interface SetTimeSliderScalePayload {
  mapId: string;
  timeSliderScale: number;
}
export interface SetTimeStep extends Action {
  type: typeof WEBMAP_SET_TIME_STEP;
  payload: SetTimeStepPayload;
}

export interface SetTimeStepPayload {
  mapId: string;
  timeStep: number;
}

export interface SetAnimationDelayPayload {
  mapId: string;
  animationDelay: number;
}

export interface SetAnimationDelay extends Action {
  type: typeof WEBMAP_SET_ANIMATION_DELAY;
  payload: SetAnimationDelayPayload;
}

export interface SetAnimationStartTime extends Action {
  type: typeof WEBMAP_SET_ANIMATION_START_TIME;
  payload: SetAnimationStartTimePayload;
}

export interface SetAnimationStartTimePayload {
  mapId: string;
  animationStartTime: Moment;
}

export interface SetAnimationEndTime extends Action {
  type: typeof WEBMAP_SET_ANIMATION_END_TIME;
  payload: SetAnimationEndTimePayload;
}

export interface SetAnimationEndTimePayload {
  mapId: string;
  animationEndTime: Moment;
}

export interface ToggleTimestepAutoPayload {
  mapId: string;
  timestepAuto: boolean;
}

export interface ToggleTimestepAuto extends Action {
  type: typeof WEBMAP_TOGGLE_TIMESTEP_AUTO;
  payload: ToggleTimestepAutoPayload;
}

export interface ToggleTimeSliderHoverPayload {
  mapId: string;
  isTimeSliderHoverOn: boolean;
}

export interface ToggleTimeSliderHover extends Action {
  type: typeof WEBMAP_TOGGLE_TIME_SLIDER_HOVER;
  payload: ToggleTimeSliderHoverPayload;
}

export interface SetTimeSliderCenterTime extends Action {
  type: typeof WEBMAP_SET_TIME_SLIDER_CENTER_TIME;
  payload: SetTimeSliderCenterTimePayload;
}

export interface SetTimeSliderCenterTimePayload {
  mapId: string;
  timeSliderCenterTime: number;
}

export interface SetTimeSliderSecondsPerPx extends Action {
  type: typeof WEBMAP_SET_TIME_SLIDER_SECONDS_PER_PX;
  payload: SetTimeSliderSecondsPerPxPayload;
}

export interface SetTimeSliderSecondsPerPxPayload {
  mapId: string;
  timeSliderSecondsPerPx: number;
}

export type MapActions =
  | RegisterMap
  | UnregisterMap
  | SetMapDimension
  | UpdateAllMapDimensions
  | SetMapAnimationStart
  | SetMapAnimationStop
  | SetBbox
  | MoveLayer
  | SetActiveLayerId
  | ToggleAutoUpdate
  | ToggleLoop
  | SetTimeStep
  | SetTimeSliderScale
  | SetAnimationDelay
  | SetAnimationStartTime
  | SetAnimationEndTime
  | ToggleTimestepAuto
  | ToggleTimeSliderHover
  | SetTimeSliderCenterTime
  | SetTimeSliderSecondsPerPx;
