/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

export const WEBMAP_REGISTER_MAP = 'WEBMAP_REGISTER_MAP';
export const WEBMAP_UNREGISTER_MAP = 'WEBMAP_UNREGISTER_MAP';
export const WEBMAP_LAYER_MOVE = 'WEBMAP_LAYER_MOVE';
export const WEBMAP_MAP_UPDATE_ALL_DIMENSIONS =
  'WEBMAP_MAP_UPDATE_ALL_DIMENSIONS';
export const WEBMAP_MAP_CHANGE_DIMENSION = 'WEBMAP_MAP_CHANGE_DIMENSION';
export const WEBMAP_START_ANIMATION = 'WEBMAP_START_ANIMATION';
export const WEBMAP_STOP_ANIMATION = 'WEBMAP_STOP_ANIMATION';
export const WEBMAP_SET_BBOX = 'WEBMAP_SET_BBOX';
export const WEBMAP_SET_ACTIVELAYERID = 'WEBMAP_SET_ACTIVELAYERID';
export const WEBMAP_TOGGLE_AUTO_UPDATE = 'WEBMAP_TOGGLE_AUTO_UPDATE';
export const WEBMAP_TOGGLE_LOOP = 'WEBMAP_TOGGLE_LOOP';
export const WEBMAP_SET_TIME_SLIDER_SCALE = 'WEBMAP_SET_TIME_SLIDER_SCALE';
export const WEBMAP_SET_TIME_STEP = 'WEBMAP_SET_TIME_STEP';
export const WEBMAP_SET_ANIMATION_DELAY = 'WEBMAP_SET_ANIMATION_DELAY';
export const WEBMAP_SET_ANIMATION_START_TIME =
  'WEBMAP_SET_ANIMATION_START_TIME';
export const WEBMAP_SET_ANIMATION_END_TIME = 'WEBMAP_SET_ANIMATION_END_TIME';
export const WEBMAP_TOGGLE_TIMESTEP_AUTO = 'WEBMAP_TOGGLE_TIMESTEP_AUTO';
export const WEBMAP_TOGGLE_TIME_SLIDER_HOVER =
  'WEBMAP_TOGGLE_TIME_SLIDER_HOVER';
export const WEBMAP_SET_TIME_SLIDER_CENTER_TIME =
  'WEBMAP_SET_TIME_SLIDER_CENTER_TIME';
export const WEBMAP_SET_TIME_SLIDER_SECONDS_PER_PX =
  'WEBMAP_SET_TIME_SLIDER_SECONDS_PER_PX';
