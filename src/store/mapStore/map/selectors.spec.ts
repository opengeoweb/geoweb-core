/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import * as mapSelectors from './selectors';
import { createMap } from './utils';
import { LayerType } from '../types';

const mapId = 'map-1';
const mockMap = createMap({ id: mapId });
const mockStoreDefaultMap = {
  webmap: {
    byId: {
      [mapId]: mockMap,
    },
    allIds: [mapId],
  },
};
const layerId1 = 'layer-1';
const layerId2 = 'layer-2';
const layerId3 = 'layer-3';
const mockStoreMapWithLayers = {
  webmap: {
    byId: {
      [mapId]: {
        ...mockMap,
        mapLayers: [layerId1],
        baseLayers: [layerId2],
        overLayers: [layerId3],
      },
    },
    allIds: [mapId],
  },
  layers: {
    byId: {
      [layerId1]: {
        mapId,
        service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
        name: 'RAD_NL25_PCP_CM',
        format: 'image/png',
        style: 'knmiradar/nearest',
        id: layerId1,
        opacity: 1,
        enabled: true,
        layerType: LayerType.mapLayer,
      },
      [layerId2]: {
        mapId,
        name: 'arcGisSat',
        title: 'arcGisSat',
        type: 'twms',
        id: layerId2,
        opacity: 1,
        enabled: true,
        layerType: LayerType.baseLayer,
      },
      [layerId3]: {
        mapId,
        name: 'someOverLayer',
        title: 'someOverLayer',
        type: 'twms',
        id: layerId3,
        opacity: 1,
        enabled: true,
        layerType: LayerType.overLayer,
      },
    },
    allIds: [layerId1, layerId2, layerId3],
    availableBaseLayers: { allIds: [], byId: {} },
  },
};

describe('store/mapStore/map/selectors', () => {
  describe('getMapById', () => {
    it('should return the map when it exists', () => {
      expect(mapSelectors.getMapById(mockStoreDefaultMap, mapId)).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId],
      );
    });
    it('should return null when the map does not exist', () => {
      expect(mapSelectors.getMapById(mockStoreDefaultMap, 'fake-id')).toEqual(
        null,
      );
    });
    it('should return null when the store does not exist', () => {
      expect(mapSelectors.getMapById(null, 'fake-id')).toEqual(null);
    });
  });

  describe('getLayerIds', () => {
    it('should return all layer ids for the map', () => {
      expect(mapSelectors.getLayerIds(mockStoreMapWithLayers, mapId)).toEqual(
        mockStoreMapWithLayers.webmap.byId[mapId].mapLayers,
      );
    });
    it('should return an empty list if the map has no layers', () => {
      expect(mapSelectors.getLayerIds(mockStoreDefaultMap, mapId)).toHaveLength(
        0,
      );
    });
    it('should return an empty list if the map does not exist', () => {
      expect(
        mapSelectors.getLayerIds(mockStoreDefaultMap, 'fake-id'),
      ).toHaveLength(0);
    });
  });

  describe('getLayers', () => {
    it('should return the list of layer objects for the map', () => {
      expect(mapSelectors.getMapLayers(mockStoreMapWithLayers, mapId)).toEqual([
        {
          mapId,
          service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
          name: 'RAD_NL25_PCP_CM',
          format: 'image/png',
          style: 'knmiradar/nearest',
          id: layerId1,
          opacity: 1,
          enabled: true,
          layerType: LayerType.mapLayer,
        },
      ]);
    });
    it('should return an empty list when the map has no layers', () => {
      expect(
        mapSelectors.getMapLayers(mockStoreDefaultMap, mapId),
      ).toHaveLength(0);
    });
  });

  describe('getMapBaseLayersIds', () => {
    it('should return a list of baselayer ids for the map', () => {
      const result = mapSelectors.getMapBaseLayersIds(
        mockStoreMapWithLayers,
        mapId,
      );
      expect(result).toHaveLength(1);
      expect(result).toEqual(
        mockStoreMapWithLayers.webmap.byId[mapId].baseLayers,
      );
    });
    it('should return an empty list when store does not exist', () => {
      const result = mapSelectors.getMapBaseLayersIds(null, mapId);
      expect(result).toHaveLength(0);
    });
  });

  describe('getMapBaseLayers', () => {
    it('should return a list of baseLayer objects for the map', () => {
      expect(
        mapSelectors.getMapBaseLayers(mockStoreMapWithLayers, mapId),
      ).toEqual([
        {
          mapId,
          name: 'arcGisSat',
          title: 'arcGisSat',
          type: 'twms',
          id: layerId2,
          layerType: LayerType.baseLayer,
          opacity: 1,
          enabled: true,
        },
      ]);
    });
    it('should return an empty list when the map has no baselayers', () => {
      expect(
        mapSelectors.getMapBaseLayers(mockStoreDefaultMap, mapId),
      ).toHaveLength(0);
    });
  });

  describe('getMapOverLayersIds', () => {
    it('should return a list of overlayer ids for the map', () => {
      const result = mapSelectors.getMapOverLayersIds(
        mockStoreMapWithLayers,
        mapId,
      );
      expect(result).toHaveLength(1);
      expect(result).toEqual(
        mockStoreMapWithLayers.webmap.byId[mapId].overLayers,
      );
    });
    it('should return an empty list when store does not exist', () => {
      const result = mapSelectors.getMapOverLayersIds(null, mapId);
      expect(result).toHaveLength(0);
    });
  });

  describe('getMapOverLayers', () => {
    it('should return a list of overLayer objects for the map', () => {
      expect(
        mapSelectors.getMapOverLayers(mockStoreMapWithLayers, mapId),
      ).toEqual([
        {
          mapId,
          name: 'someOverLayer',
          title: 'someOverLayer',
          type: 'twms',
          id: layerId3,
          opacity: 1,
          enabled: true,
          layerType: LayerType.overLayer,
        },
      ]);
    });
    it('should return an empty list when the map has no overLayers', () => {
      expect(
        mapSelectors.getMapOverLayers(mockStoreDefaultMap, mapId),
      ).toHaveLength(0);
    });
  });

  describe('getMapDimensions', () => {
    const mockStoreMapWithDimensions = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            dimensions: [
              {
                name: 'time',
                units: 'ISO8601',
                currentValue: '2020-03-13T13:30:00Z',
              },
            ],
            isAnimating: true,
          },
        },
        allIds: [mapId],
      },
    };

    it('should return the dimensions for the map', () => {
      expect(
        mapSelectors.getMapDimensions(mockStoreMapWithDimensions, mapId),
      ).toEqual(mockStoreMapWithDimensions.webmap.byId[mapId].dimensions);
    });
    it('should return an empty list when store does not exist', () => {
      expect(mapSelectors.getMapDimensions(null, mapId)).toHaveLength(0);
    });
  });

  describe('getSrs', () => {
    it('should return the Srs for the map', () => {
      expect(mapSelectors.getSrs(mockStoreDefaultMap, mapId)).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId].srs,
      );
    });
    it('should return an empty string when store does not exist', () => {
      expect(mapSelectors.getSrs(null, mapId)).toEqual('');
    });
  });

  describe('getBbox', () => {
    it('should return the Bbox for the map', () => {
      expect(mapSelectors.getBbox(mockStoreDefaultMap, mapId)).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId].bbox,
      );
    });
    it('should return an empty object when store does not exist', () => {
      expect(mapSelectors.getBbox(null, mapId)).toMatchObject({});
    });
  });

  describe('isAnimating', () => {
    it('should return isAnimating for the map', () => {
      expect(mapSelectors.isAnimating(mockStoreDefaultMap, mapId)).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId].isAnimating,
      );
    });
    it('should return false when store does not exist', () => {
      expect(mapSelectors.isAnimating(null, mapId)).toEqual(false);
    });
  });

  describe('getAnimationStartTime', () => {
    it('should return map animation start time', () => {
      const mockStoreMapAnimationTimes = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              animationStartTime: moment
                .utc()
                .subtract(6, 'h')
                .toISOString(),
              animationEndTime: moment
                .utc()
                .subtract(10, 'm')
                .toISOString(),
            },
          },
          allIds: [mapId],
        },
      };
      expect(
        mapSelectors
          .getAnimationStartTime(mockStoreMapAnimationTimes, mapId)
          .toISOString(),
      ).toBe(mockStoreMapAnimationTimes.webmap.byId[mapId].animationStartTime);
    });
    it('should return utc time - 6 hours when map does not exist', () => {
      expect(
        mapSelectors
          .getAnimationStartTime(null, mapId)
          .format('YYYY-MM-DD:hh:mm:dd'),
      ).toBe(
        moment
          .utc()
          .subtract(6, 'h')
          .format('YYYY-MM-DD:hh:mm:dd'),
      );
    });
  });

  describe('getAnimationEndTime', () => {
    it('should return map animation end time', () => {
      const mockStoreMapAnimationTimes = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              animationStartTime: moment
                .utc()
                .subtract(6, 'h')
                .toISOString(),
              animationEndTime: moment
                .utc()
                .subtract(10, 'm')
                .toISOString(),
            },
          },
          allIds: [mapId],
        },
      };
      expect(
        mapSelectors
          .getAnimationEndTime(mockStoreMapAnimationTimes, mapId)
          .toISOString(),
      ).toBe(mockStoreMapAnimationTimes.webmap.byId[mapId].animationEndTime);
    });
    it('should return utc time - 10 minutes when map does not exist', () => {
      expect(
        mapSelectors
          .getAnimationEndTime(null, mapId)
          .format('YYYY-MM-DD:hh:mm:dd'),
      ).toBe(
        moment
          .utc()
          .subtract(10, 'm')
          .format('YYYY-MM-DD:hh:mm:dd'),
      );
    });
  });

  describe('isAutoUpdating', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, isAutoUpdating: true },
        },
        allIds: [mapId],
      },
    };

    it('should return map is auto updating', () => {
      expect(
        mapSelectors.isAutoUpdating(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].isAutoUpdating);
    });
    it('should return an false when map does not exist', () => {
      expect(mapSelectors.isAutoUpdating(null, mapId)).toBeFalsy();
    });
  });

  describe('isLooping', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, isLooping: true },
        },
        allIds: [mapId],
      },
    };

    it('should return map looping', () => {
      expect(mapSelectors.isLooping(mockStoreMapActiveLayer, mapId)).toEqual(
        mockStoreMapActiveLayer.webmap.byId[mapId].isLooping,
      );
    });
    it('should return an false when map does not exist', () => {
      expect(mapSelectors.isLooping(null, mapId)).toBeFalsy();
    });
  });

  describe('getActiveLayerId', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, activeLayerId: layerId2 },
        },
        allIds: [mapId],
      },
    };

    it('should return the active layer id for the map', () => {
      expect(
        mapSelectors.getActiveLayerId(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].activeLayerId);
    });
    it('should return an empty string when store does not exist', () => {
      expect(mapSelectors.getActiveLayerId(null, mapId)).toEqual('');
    });
  });

  describe('getMaptimeSliderScale', () => {
    const mockStoreMapTimeSliderScale = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, timeSliderScale: 60 },
        },
        allIds: [mapId],
      },
    };

    it('should return map time step', () => {
      expect(
        mapSelectors.getMapTimeSliderScale(mockStoreMapTimeSliderScale, mapId),
      ).toEqual(mockStoreMapTimeSliderScale.webmap.byId[mapId].timeSliderScale);
    });
    it('should return 60 when map does not exist', () => {
      expect(mapSelectors.getMapTimeSliderScale(null, mapId)).toEqual(60);
    });
  });

  describe('getMaptimeStep', () => {
    const mockStoreMapTimeStep = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, timeStep: 10 },
        },
        allIds: [mapId],
      },
    };

    it('should return map time step', () => {
      expect(mapSelectors.getMapTimeStep(mockStoreMapTimeStep, mapId)).toEqual(
        mockStoreMapTimeStep.webmap.byId[mapId].timeStep,
      );
    });
    it('should return 5 when map does not exist', () => {
      expect(mapSelectors.getMapTimeStep(null, mapId)).toEqual(5);
    });
  });

  describe('getMapTimeSliderCenterTime', () => {
    const time = moment().unix();
    const storeWithCurrentTime = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            timeSliderCenterTime: time,
          },
        },
        allIds: [mapId],
      },
    };
    it('should return the current map time slider center time', () => {
      expect(
        mapSelectors.getMapTimeSliderCenterTime(storeWithCurrentTime, mapId),
      ).toEqual(time);
    });

    it('should return the current time when store does not exist', () => {
      const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
      jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
      expect(mapSelectors.getMapTimeSliderCenterTime(null, mapId)).toEqual(
        moment(now).unix(),
      );
    });
  });

  describe('getMapTimeSliderSecondsPerPx', () => {
    it('should return the map time slider secondsPerPx value', () => {
      expect(
        mapSelectors.getMapTimeSliderSecondsPerPx(mockStoreDefaultMap, mapId),
      ).toEqual(mockStoreDefaultMap.webmap.byId[mapId].timeSliderSecondsPerPx);
    });

    it('should return 60 when map does not exist', () => {
      expect(mapSelectors.getMapTimeSliderSecondsPerPx(null, mapId)).toEqual(
        60,
      );
    });
  });

  describe('isTimestepAuto', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, isTimestepAuto: true },
        },
        allIds: [mapId],
      },
    };
    it('should return map is timestep auto mode', () => {
      expect(
        mapSelectors.isTimestepAuto(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].isTimestepAuto);
    });
    it('should return an false when map does not exist', () => {
      expect(mapSelectors.isTimestepAuto(null, mapId)).toBeFalsy();
    });
  });

  describe('use time slider hover', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, isTimeSliderHoverOn: true },
        },
        allIds: [mapId],
      },
    };

    it('should return is map using time slider hover', () => {
      expect(
        mapSelectors.isTimeSliderHoverOn(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].isTimeSliderHoverOn);
    });
    it('should return an false when map does not exist', () => {
      expect(mapSelectors.isTimeSliderHoverOn(null, mapId)).toBeFalsy();
    });
  });
});
