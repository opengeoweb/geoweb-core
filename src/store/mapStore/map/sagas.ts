/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable require-yield */
import { takeLatest, select, put, call } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import moment, { Moment } from 'moment';

import * as mapConstants from './constants';
import * as mapSelectors from './selectors';
import * as mapActions from './actions';

import * as layerConstants from '../layers/constants';
import * as layerSelectors from '../layers/selectors';
import * as layerActions from '../layers/actions';

import { getWMJSMapById } from '../../..';
import {
  SetMapAnimationStop,
  SetMapAnimationStart,
  ToggleAutoUpdate,
} from './types';
import { DeleteLayer, SetLayerDimensions } from '../layers/types';
import { roundWithTimeStep } from '../../../components/TimeSlider/TimeSliderUtils';

export const generateTimeList = (
  start: Moment,
  end: Moment,
  interval: number,
): { name: string; value: string }[] => {
  const timeList = [];
  const unixStart = moment(start)
    .utc()
    .unix();
  const unixEnd = moment(end)
    .utc()
    .unix();
  for (let j = unixStart; j <= unixEnd; j += interval) {
    timeList.push({ name: 'time', value: moment.unix(j).toISOString() });
  }
  return timeList;
};

export function updateMapDraw(mapId: string, draw: unknown): void {
  const webMap = getWMJSMapById(mapId);
  if (webMap) {
    webMap.getListener().suspendEvents();
    webMap.stopAnimating();
    webMap.draw(draw);
    webMap.getListener().resumeEvents();
  }
}

export function* startAnimationSaga({
  payload,
}: SetMapAnimationStart): Generator {
  const { mapId, start, end, interval, timeList } = payload;
  const roundedStart = roundWithTimeStep(
    start && start.unix(),
    interval / 60,
    'ceil',
  );
  const roundedEnd = roundWithTimeStep(
    end && end.unix(),
    interval / 60,
    'floor',
  );

  const animationList = yield timeList ||
    generateTimeList(
      moment.utc(roundedStart * 1000),
      moment.utc(roundedEnd * 1000),
      interval,
    );

  yield updateMapDraw(mapId, animationList);
}

export function* stopAnimationSaga({
  payload,
}: SetMapAnimationStop): Generator {
  const { mapId } = payload;
  yield updateMapDraw(mapId, 'opengeoweb-core-map-reducer');
}

export function* deleteLayerSaga({ payload }: DeleteLayer): SagaIterator {
  const { mapId } = payload;
  const layers = yield select(mapSelectors.getMapLayers, mapId);
  if (!layers.length) {
    yield put(mapActions.mapStopAnimation({ mapId }));
  }
}

export function* updateAnimation(
  mapId: string,
  maxValue: string,
): SagaIterator {
  const animationStart = yield select(
    mapSelectors.getAnimationStartTime,
    mapId,
  );
  const animationEnd = yield select(mapSelectors.getAnimationEndTime, mapId);
  const t = moment.utc(maxValue).unix();
  const deltaT = t - animationEnd.unix();

  const start = moment.unix(animationStart.unix() + deltaT).utc();
  const end = moment.unix(animationEnd.unix() + deltaT).utc();
  yield put(mapActions.setAnimationEndTime({ mapId, animationEndTime: end }));
  yield put(
    mapActions.setAnimationStartTime({
      mapId,
      animationStartTime: start,
    }),
  );

  const isAnimating = yield select(mapSelectors.isAnimating, mapId);
  if (isAnimating) {
    // restart animation if animation was running
    const timeStep = yield select(mapSelectors.getMapTimeStep, mapId);
    yield put(
      mapActions.mapStartAnimation({
        mapId,
        start,
        end,
        interval: timeStep * 60,
      }),
    );
  }
}

let prevDimension;
export function* setLayerDimensionsSaga({
  payload,
}: SetLayerDimensions): SagaIterator {
  try {
    const { dimensions, layerId } = payload;
    const layer = yield select(layerSelectors.getLayerById, layerId);
    if (!layer) return;

    const newTimeDimension = dimensions.find(
      dimension => dimension.name === 'time',
    );

    const { mapId } = layer;
    const activeLayerId = yield select(mapSelectors.getActiveLayerId, mapId);
    const shouldAutoUpdate = yield select(mapSelectors.isAutoUpdating, mapId);

    if (
      layerId === activeLayerId && // only update the active layer
      shouldAutoUpdate &&
      newTimeDimension &&
      newTimeDimension.maxValue
    ) {
      if (prevDimension) {
        if (prevDimension !== newTimeDimension.maxValue) {
          yield put(
            layerActions.layerChangeDimension({
              layerId,
              origin: 'setLayerDimensionSaga',
              dimension: {
                name: 'time',
                currentValue: newTimeDimension.maxValue,
              },
            }),
          );

          yield call(updateAnimation, mapId, newTimeDimension.maxValue);
        }
      }
      prevDimension = newTimeDimension.maxValue;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.warn(error);
  }
}

export function* toggleAutoUpdateSaga({
  payload,
}: ToggleAutoUpdate): SagaIterator {
  try {
    const { shouldAutoUpdate, mapId } = payload;
    if (shouldAutoUpdate) {
      // go to end of active layer
      const layerId = yield select(mapSelectors.getActiveLayerId, mapId);
      const timeDimension = yield select(
        layerSelectors.getLayerTimeDimension,
        layerId,
      );
      if (timeDimension && timeDimension.maxValue) {
        yield put(
          layerActions.layerChangeDimension({
            layerId,
            origin: 'toggleAutoUpdateSaga',
            dimension: {
              name: 'time',
              currentValue: timeDimension.maxValue,
            },
          }),
        );

        yield call(updateAnimation, mapId, timeDimension.maxValue);
      }
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.warn(error);
  }
}

export function* rootSaga(): SagaIterator {
  yield takeLatest(mapConstants.WEBMAP_STOP_ANIMATION, stopAnimationSaga);
  yield takeLatest(mapConstants.WEBMAP_START_ANIMATION, startAnimationSaga);
  yield takeLatest(layerConstants.WEBMAP_LAYER_DELETE, deleteLayerSaga);
  yield takeLatest(
    layerConstants.WEBMAP_LAYER_SET_DIMENSIONS,
    setLayerDimensionsSaga,
  );
  yield takeLatest(
    mapConstants.WEBMAP_TOGGLE_AUTO_UPDATE,
    toggleAutoUpdateSaga,
  );
}

export default rootSaga;
