/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-console */
import { produce } from 'immer';

import {
  WEBMAP_LAYER_MOVE,
  WEBMAP_SET_BBOX,
  WEBMAP_MAP_CHANGE_DIMENSION,
  WEBMAP_MAP_UPDATE_ALL_DIMENSIONS,
  WEBMAP_START_ANIMATION,
  WEBMAP_STOP_ANIMATION,
  WEBMAP_REGISTER_MAP,
  WEBMAP_UNREGISTER_MAP,
  WEBMAP_SET_ACTIVELAYERID,
  WEBMAP_TOGGLE_AUTO_UPDATE,
  WEBMAP_TOGGLE_LOOP,
  WEBMAP_SET_TIME_SLIDER_SCALE,
  WEBMAP_SET_TIME_STEP,
  WEBMAP_SET_ANIMATION_DELAY,
  WEBMAP_SET_ANIMATION_START_TIME,
  WEBMAP_SET_ANIMATION_END_TIME,
  WEBMAP_TOGGLE_TIMESTEP_AUTO,
  WEBMAP_TOGGLE_TIME_SLIDER_HOVER,
  WEBMAP_SET_TIME_SLIDER_CENTER_TIME,
  WEBMAP_SET_TIME_SLIDER_SECONDS_PER_PX,
} from './constants';

import * as LAYERS from '../layers/constants';

import * as GENERICSYNC from '../../generic/synchronizationActions/constants';

import {
  WEBMAP_LAYER_DELETE,
  WEBMAP_ADD_LAYER,
  WEBMAP_SET_LAYERS,
  WEBMAP_SET_BASELAYERS,
  WEBMAP_ADD_BASELAYER,
  WEBMAP_BASELAYER_DELETE,
} from '../layers/constants';

import { generateLayerId, getWMJSMapById } from '../utils/helpers';
import { WebMapState, MapActions } from './types';
import { Layer, LayerActions, LayerType } from '../layers/types';
import {
  produceDraftStateSetWebMapDimension,
  produceDraftStateSetMapDimensionFromLayerChangeDimension,
  createMap,
} from './utils';
import { Dimension } from '../types';
import { GenericSyncActions } from '../../generic/synchronizationActions/types';

/**
 * Addes id's to the layer object. All layers need to have an id for referring.
 * @param layers
 */
const createLayersWithIds = (layers: Layer[]): Layer[] => {
  if (!layers) return layers;
  return produce(layers, draft => {
    for (let j = 0; j < draft.length; j += 1) {
      const layer = draft[j];
      if (!layer.id) {
        layer.id = generateLayerId();
      }
    }
  });
};

export const initialState: WebMapState = {
  byId: {},
  allIds: [],
};

export const reducer = (
  state = initialState,
  action: MapActions | LayerActions | GenericSyncActions,
): WebMapState => {
  switch (action.type) {
    // MAP actions
    case WEBMAP_REGISTER_MAP: {
      const { mapId } = action.payload;
      return produce(state, draft => {
        if (!draft.allIds.includes(mapId)) {
          draft.byId[mapId] = createMap({ id: mapId });
          draft.allIds.push(mapId);
        }
      });
    }
    case WEBMAP_UNREGISTER_MAP: {
      const { mapId } = action.payload;
      return produce(state, draft => {
        const mapIndex = draft.allIds.indexOf(mapId);
        if (mapIndex !== -1) {
          delete draft.byId[mapId];
          draft.allIds.splice(mapIndex, 1);
        }
      });
    }

    case WEBMAP_ADD_LAYER: {
      const { layerId, mapId } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      const layerIndex = state.byId[mapId].mapLayers.indexOf(layerId);

      if (layerIndex >= 0) {
        // Do nothing if layerId is already used
        return state;
      }
      return produce(state, draft => {
        if (layerIndex === -1) {
          if (!state.byId[mapId].mapLayers.length) {
            draft.byId[mapId].activeLayerId = layerId;
          }
          draft.byId[mapId].mapLayers.unshift(layerId);
        }
      });
    }

    case WEBMAP_SET_BBOX: {
      const { mapId, bbox } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      const srs = action.payload.srs || null;
      return produce(state, draft => {
        draft.byId[mapId].bbox = bbox;
        if (srs) draft.byId[mapId].srs = srs;
      });
    }

    case WEBMAP_MAP_UPDATE_ALL_DIMENSIONS: {
      const { mapId, dimensions } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        dimensions.forEach(dimension =>
          produceDraftStateSetWebMapDimension(draft, mapId, dimension, true),
        );
      });
    }

    case WEBMAP_MAP_CHANGE_DIMENSION: {
      const { mapId, dimension: dimensionFromAction } = action.payload;
      return produce(state, draft => {
        produceDraftStateSetWebMapDimension(
          draft,
          mapId,
          dimensionFromAction,
          true,
        );
      });
    }

    case WEBMAP_START_ANIMATION: {
      const { mapId } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].isAnimating = true;
      });
    }

    case WEBMAP_STOP_ANIMATION: {
      const { mapId } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].isAnimating = false;
      });
    }

    case WEBMAP_ADD_BASELAYER: {
      const { layer, layerId, mapId } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }

      if (layer.layerType === LayerType.baseLayer) {
        const layerIndex = state.byId[mapId].baseLayers.indexOf(layerId);
        if (layerIndex >= 0) {
          // Do nothing if layerId is already used
          return state;
        }
        return produce(state, draft => {
          if (layerIndex === -1) {
            draft.byId[mapId].baseLayers.unshift(layerId);
          }
        });
      }
      if (layer.layerType === LayerType.overLayer) {
        const layerIndex = state.byId[mapId].overLayers.indexOf(layerId);
        if (layerIndex >= 0) {
          // Do nothing if layerId is already used
          return state;
        }
        return produce(state, draft => {
          if (layerIndex === -1) {
            draft.byId[mapId].overLayers.unshift(layerId);
          }
        });
      }
      return state;
    }

    case WEBMAP_SET_TIME_SLIDER_SCALE: {
      const { mapId, timeSliderScale } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].timeSliderScale = timeSliderScale;
        draft.byId[mapId].timeSliderSecondsPerPx =
          timeSliderScale === 10080 ? 360 : timeSliderScale;
      });
    }

    case WEBMAP_SET_TIME_STEP: {
      const { mapId, timeStep } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].timeStep = timeStep;
      });
    }

    case WEBMAP_SET_ANIMATION_START_TIME: {
      const { mapId, animationStartTime } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].animationStartTime = animationStartTime.toISOString();
      });
    }

    case WEBMAP_SET_ANIMATION_END_TIME: {
      const { mapId, animationEndTime } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].animationEndTime = animationEndTime.toISOString();
      });
    }

    // LAYER actions
    case WEBMAP_SET_LAYERS: {
      const { mapId } = action.payload;
      const layersWithIds = createLayersWithIds(action.payload.layers);
      const layerIds = layersWithIds.map(({ id }) => id);
      const [activeLayerId] = layerIds;

      return produce(state, draft => {
        if (state.byId[mapId]) {
          draft.byId[mapId].mapLayers = layerIds;
          draft.byId[mapId].activeLayerId = activeLayerId;
          const webmapInstance = getWMJSMapById(mapId);
          if (webmapInstance) {
            const mapDims = webmapInstance.getDimensionList();
            if (mapDims) {
              draft.byId[mapId].dimensions = mapDims.map(dim => ({
                name: dim.name,
                currentValue: dim.currentValue,
                units: dim.units,
              }));
            }
          }
        }
      });
    }

    case WEBMAP_SET_BASELAYERS: {
      const { layers, mapId } = action.payload;
      // Split into base and overlayers
      const baseLayers = [];
      const overLayers = [];
      layers.forEach(layer => {
        if (layer.layerType === LayerType.overLayer) {
          overLayers.push(layer);
        } else if (layer.layerType === LayerType.baseLayer) {
          baseLayers.push(layer);
        }
      });

      const baseLayersWithIds = createLayersWithIds(baseLayers);
      const overLayersWithIds = createLayersWithIds(overLayers);

      const baseLayerIds = baseLayersWithIds.map(({ id }) => id);
      const overLayerIds = overLayersWithIds.map(({ id }) => id);

      return produce(state, draft => {
        if (state.byId[mapId]) {
          if (baseLayerIds.length !== 0)
            draft.byId[mapId].baseLayers = baseLayerIds;
          if (overLayerIds.length !== 0)
            draft.byId[mapId].overLayers = overLayerIds;
        }
      });
    }

    case WEBMAP_SET_ANIMATION_DELAY: {
      const { mapId, animationDelay } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].animationDelay = animationDelay;
      });
    }

    case WEBMAP_LAYER_DELETE: {
      const { mapId, layerId } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].mapLayers = draft.byId[mapId].mapLayers.filter(
          id => id !== layerId,
        );
        // if activeLayer is deleted, change activeLayerId
        if (
          draft.byId[mapId].activeLayerId === layerId &&
          draft.byId[mapId].mapLayers.length
        ) {
          const [activeLayerId] = draft.byId[mapId].mapLayers;
          draft.byId[mapId].activeLayerId = activeLayerId;
        }
        if (!draft.byId[mapId].mapLayers.length) {
          draft.byId[mapId].activeLayerId = '';
        }
      });
    }

    case WEBMAP_BASELAYER_DELETE: {
      const { mapId, layerId } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].baseLayers = draft.byId[mapId].baseLayers.filter(
          id => id !== layerId,
        );
        draft.byId[mapId].overLayers = draft.byId[mapId].overLayers.filter(
          id => id !== layerId,
        );
      });
    }

    case WEBMAP_LAYER_MOVE: {
      const { oldIndex, newIndex, mapId } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].mapLayers[oldIndex] =
          state.byId[mapId].mapLayers[newIndex];
        draft.byId[mapId].mapLayers[newIndex] =
          state.byId[mapId].mapLayers[oldIndex];
      });
    }

    case WEBMAP_SET_ACTIVELAYERID: {
      const { mapId, layerId } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].activeLayerId = layerId;
      });
    }

    case WEBMAP_SET_TIME_SLIDER_CENTER_TIME: {
      const { mapId, timeSliderCenterTime } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].timeSliderCenterTime = timeSliderCenterTime;
      });
    }

    case WEBMAP_SET_TIME_SLIDER_SECONDS_PER_PX: {
      const { mapId, timeSliderSecondsPerPx } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].timeSliderSecondsPerPx = timeSliderSecondsPerPx;
      });
    }

    case LAYERS.WEBMAP_LAYER_CHANGE_DIMENSION: {
      const { layerId, dimension } = action.payload;
      return produce(state, draft =>
        produceDraftStateSetMapDimensionFromLayerChangeDimension(
          draft,
          layerId,
          dimension,
        ),
      );
    }

    case GENERICSYNC.GENERIC_SYNC_SETTIME: {
      return produce(state, draft => {
        const { targets: targetsFromAction, source } = action.payload;
        /* Because we want backwards compatibility with the previous code, we also need to listen to the original source action */
        const targets = [
          {
            targetId: source.payload.sourceId,
            value: source.payload.value,
          },
        ];
        /* And then append the targets form the action */
        targets.push(...targetsFromAction);
        targets.forEach(target => {
          const { targetId, value } = target;
          if (draft.byId[targetId]) {
            const dimension: Dimension = {
              name: 'time',
              currentValue: value,
            };
            produceDraftStateSetWebMapDimension(
              draft,
              targetId,
              dimension,
              true,
            );
            produceDraftStateSetMapDimensionFromLayerChangeDimension(
              draft,
              targetId,
              dimension,
            );
          }
        });
      });
    }

    case GENERICSYNC.GENERIC_SYNC_SETBBOX: {
      return produce(state, draft => {
        action.payload.targets.forEach(payload => {
          const { targetId, bbox, srs } = payload;
          if (draft.byId[targetId]) {
            if (bbox) {
              draft.byId[targetId].bbox = bbox;
            }
            if (srs) draft.byId[targetId].srs = srs;
          }
        });
      });
    }

    case WEBMAP_TOGGLE_AUTO_UPDATE: {
      const { mapId, shouldAutoUpdate } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].isAutoUpdating = shouldAutoUpdate;
      });
    }

    case WEBMAP_TOGGLE_LOOP: {
      const { mapId, shouldLoop } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].isLooping = shouldLoop;
      });
    }

    case WEBMAP_TOGGLE_TIMESTEP_AUTO: {
      const { mapId, timestepAuto } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].isTimestepAuto = timestepAuto;
      });
    }

    case WEBMAP_TOGGLE_TIME_SLIDER_HOVER: {
      const { mapId, isTimeSliderHoverOn } = action.payload;
      if (!state.byId[mapId]) {
        return state;
      }
      return produce(state, draft => {
        draft.byId[mapId].isTimeSliderHoverOn = isTimeSliderHoverOn;
      });
    }

    default:
      return state;
  }
};
