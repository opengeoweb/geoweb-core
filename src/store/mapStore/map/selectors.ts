/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from 'reselect';
import moment from 'moment';
import { WebMap } from './types';
import { AppStore } from '../../../types/types';
import * as layerSelectors from '../layers/selectors';

/**
 * Gets the map state by mapId
 *
 * Example: getMapById(store, 'mapid_1')
 * @param {object} store store object from which the map state wll be extracted
 * @param {string} mapId Id of the map
 * @returns {object} object containing map state (isAnimating, bbox, baseLayers, layers etc.)
 */
export const getMapById = (store: AppStore, mapId: string): WebMap => {
  if (store && store.webmap && store.webmap.byId[mapId]) {
    return store.webmap.byId[mapId];
  }
  return null;
};

/**
 * Gets all layerIds for a map that aren't baselayers or overlayers
 *
 * Example: layerIds = getLayerIds(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array -  array containing all layerIds
 */
export const getLayerIds = createSelector(getMapById, store =>
  store ? store.mapLayers : [],
);

/**
 * Gets all layer states for a map
 *
 * Example: layers = getMapLayers(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array -  array containing all layer states for the map
 */
export const getMapLayers = createSelector(
  getLayerIds,
  layerSelectors.getLayersById,
  (layerIds, layers) => layerIds.map(layerId => layers[layerId]),
);

/**
 * Gets and array of baselayers ids for a map
 *
 * Example: baseLayersId = getMapBaseLayersIds(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array -  array containing an array of baselayers ids
 */
export const getMapBaseLayersIds = createSelector(getMapById, store =>
  store ? store.baseLayers : [],
);

/**
 * Gets all baselayers for a map
 *
 * Example: baseLayers = getMapBaseLayers(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array -  array containing all baselayers for the map
 */
export const getMapBaseLayers = createSelector(
  getMapBaseLayersIds,
  layerSelectors.getLayersById,
  (layerIds, layers) => layerIds.map(layerId => layers[layerId]),
);

/**
 * Gets and array of overLayers ids for a map
 *
 * Example: overLayersId = getMapOverLayersIds(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array -  array containing an array of overLayers ids
 */
export const getMapOverLayersIds = createSelector(getMapById, store =>
  store ? store.overLayers : [],
);

/**
 * Gets all overLayers for a map
 *
 * Example: overLayers = getMapOverLayers(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array -  array containing all overLayers for the map
 */
export const getMapOverLayers = createSelector(
  getMapOverLayersIds,
  layerSelectors.getLayersById,
  (layerIds, layers) => layerIds.map(layerId => layers[layerId]),
);

/**
 * Gets map dimensions
 *
 * Example: mapDimensions = getMapDimensions(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array - array containing map dimensions
 */
export const getMapDimensions = createSelector(getMapById, store =>
  store ? store.dimensions : [],
);

/**
 * Gets map srs
 *
 * Example: mapSrs = getSrs(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {string} returnType: string -  string containing srs
 */
export const getSrs = createSelector(getMapById, store =>
  store ? store.srs : '',
);

/**
 * Gets map bounding box
 *
 * Example: mapBbox = getBbox(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {object} returnType: object -  boundingbox object {left: number, bottom: number, right:number, top: number}
 */
export const getBbox = createSelector(getMapById, store =>
  store ? store.bbox : {},
);

/**
 * Gets if map is animating
 *
 * Example: mapIsAnimating = isAnimating(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const isAnimating = createSelector(getMapById, store =>
  store ? store.isAnimating : false,
);

/**
 * Gets start time of animation
 *
 * Example: endTimeOfAnimetion = getAnimationStartTime(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {moment} returnType: Moment
 */
export const getAnimationStartTime = createSelector(getMapById, store =>
  store ? moment(store.animationStartTime) : moment.utc().subtract(6, 'h'),
);

/**
 * Gets end time of animation
 *
 * Example: endTimeOfAnimetion = getAnimationEndTime(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {moment} returnType: Moment
 */
export const getAnimationEndTime = createSelector(getMapById, store =>
  store ? moment(store.animationEndTime) : moment.utc().subtract(10, 'm'),
);

/**
 * Returns map is auto updating
 *
 * Example: isAutoUpdating = isAutoUpdating(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const isAutoUpdating = createSelector(getMapById, store =>
  store ? store.isAutoUpdating : false,
);

/**
 * Tells whether the "loop" behavior of animation is on/off
 *
 * Example: isLooping = isLooping(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const isLooping = createSelector(getMapById, store =>
  store ? store.isLooping : false,
);

/**
 * Gets activeLayerId for map
 *
 * example: activeLayerId = getActiveLayerId(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 */
export const getActiveLayerId = createSelector(getMapById, store =>
  store ? store.activeLayerId : '',
);

/**
 * Gets scale of a time slider of a map
 *
 * Example: scale = getScale(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {object} returnType: number -  scale as a number
 */
export const getMapTimeSliderScale = createSelector(getMapById, store =>
  store ? store.timeSliderScale : 60,
);

/**
 * Gets time step of a map
 *
 * Example: timeStep = getTimeStep(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {object} returnType: number - time step as a number
 */
export const getMapTimeStep = createSelector(getMapById, store =>
  store ? store.timeStep : 5,
);

/**
 * Returns the speed of animation
 *
 * Example: speed = getSpeed(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {object} returnType: number - speed as a number
 */
export const getMapAnimationDelay = createSelector(getMapById, store =>
  store ? store.animationDelay : 1000,
);

/**
 * Returns the center time of time slider
 *
 * Example: centerTime = getCenterTime(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {object} returnType: number - center time as a unix timestamp
 */
export const getMapTimeSliderCenterTime = createSelector(getMapById, store =>
  store ? store.timeSliderCenterTime : moment().unix(),
);

/**
 * Returns the number of seconds per pixel on the time slider
 *
 * Example: secondsPerPx = getSecondsPerPx(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {object} returnType: number - the number of seconds per pixel
 */
export const getMapTimeSliderSecondsPerPx = createSelector(getMapById, store =>
  store ? store.timeSliderSecondsPerPx : 60,
);

/**
 * Returns map is timestep auto
 *
 * Example: isTimestepAuto = isTimestepAuto(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const isTimestepAuto = createSelector(getMapById, store =>
  store ? store.isTimestepAuto : false,
);

/**
 * Returns map is time slider hover
 *
 * Example: isTimeSliderHoverOn = isTimeSliderHoverOn(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const isTimeSliderHoverOn = createSelector(getMapById, store =>
  store ? store.isTimeSliderHoverOn : false,
);
