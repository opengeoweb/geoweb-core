/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  WEBMAP_REGISTER_MAP,
  WEBMAP_UNREGISTER_MAP,
  WEBMAP_LAYER_MOVE,
  WEBMAP_SET_BBOX,
  WEBMAP_MAP_CHANGE_DIMENSION,
  WEBMAP_MAP_UPDATE_ALL_DIMENSIONS,
  WEBMAP_START_ANIMATION,
  WEBMAP_STOP_ANIMATION,
  WEBMAP_SET_ACTIVELAYERID,
  WEBMAP_TOGGLE_AUTO_UPDATE,
  WEBMAP_TOGGLE_LOOP,
  WEBMAP_SET_TIME_SLIDER_SCALE,
  WEBMAP_SET_TIME_STEP,
  WEBMAP_SET_ANIMATION_DELAY,
  WEBMAP_SET_ANIMATION_START_TIME,
  WEBMAP_SET_ANIMATION_END_TIME,
  WEBMAP_TOGGLE_TIMESTEP_AUTO,
  WEBMAP_TOGGLE_TIME_SLIDER_HOVER,
  WEBMAP_SET_TIME_SLIDER_CENTER_TIME,
  WEBMAP_SET_TIME_SLIDER_SECONDS_PER_PX,
} from './constants';

import {
  MapActions,
  SetBboxPayload,
  MoveLayerPayload,
  SetMapDimensionPayload,
  UpdateAllMapDimensionsPayload,
  SetMapAnimationStartPayload,
  SetMapAnimationStopPayload,
  SetActiveLayerIdPayload,
  MoveLayer,
  SetActiveLayerId,
  ToggleAutoUpdatePayload,
  ToggleAutoUpdate,
  ToggleLoopPayload,
  ToggleLoop,
  SetTimeSliderScalePayload,
  SetTimeStepPayload,
  SetAnimationDelayPayload,
  SetAnimationStartTimePayload,
  SetAnimationEndTimePayload,
  ToggleTimestepAutoPayload,
  ToggleTimestepAuto,
  ToggleTimeSliderHoverPayload,
  ToggleTimeSliderHover,
  SetTimeSliderCenterTimePayload,
  SetTimeSliderSecondsPerPxPayload,
} from './types';

/**
 * Registers the map in the store and creates the map state
 *
 * Example: registerMap('mapId1')
 * @param {string} mapId the mapId to register in the store
 */
export const registerMap = (mapId: string): MapActions => ({
  type: WEBMAP_REGISTER_MAP,
  payload: {
    mapId,
  },
});

/**
 * Deletes the map from the store (from both allIds and byId)
 *
 * Example: unregisterMap('mapId1')
 * @param {string} mapId the mapId to delete from the store
 */
export const unregisterMap = (mapId: string): MapActions => ({
  type: WEBMAP_UNREGISTER_MAP,
  payload: {
    mapId,
  },
});

/**
 * Set the bbox for a map.
 * If srs passed in as well, this is also set
 *
 * Example: setBbox({mapId: 'mapid_1', bbox: someBboxObject, srs: initialSrs });
 * @param {object} payload Object containing the mapId: string, bbox: Bbox and srs?: string properties
 */
export const setBbox = (payload: SetBboxPayload): MapActions => ({
  type: WEBMAP_SET_BBOX,
  payload,
});

/**
 * Set the dimension for the map. Can be used to change the time. All layers in the map will follow (if linking is set) accordingly.
 *
 * Example: mapChangeDimension({mapId:'map_id1', dimension: someDimensionObject})
 * @param {object} payload Object with mapId: string and a dimension: Dimension (containing name and currentValue).
 */
export const mapChangeDimension = (
  payload: SetMapDimensionPayload,
): MapActions => ({
  type: WEBMAP_MAP_CHANGE_DIMENSION,
  payload,
});

/**
 * Sets all dimensions for the map
 *
 * Example: mapUpdateAllMapDimensions({mapId:'map_id1', dimensions: dimensionObjectsArray})
 * @param {object} payload Object with mapId: string and dimensions: Dimension[] (containing name and currentValue).
 */
export const mapUpdateAllMapDimensions = (
  payload: UpdateAllMapDimensionsPayload,
): MapActions => ({
  type: WEBMAP_MAP_UPDATE_ALL_DIMENSIONS,
  payload,
});

/**
 * Starts the animation for a map.
 *
 * Example: mapStartAnimation({mapId:'mapId1', start: 'someISOTime', end: 'someISOTime', interval: 1000})
 * @param {object} payload Object with mapId: string, start: Moment, end: Moment and interval: number (in seconds), timeList?: TimeListType[]
 */
export const mapStartAnimation = (
  payload: SetMapAnimationStartPayload,
): MapActions => ({
  type: WEBMAP_START_ANIMATION,
  payload,
});

/**
 * Stops the animation for a map.
 *
 * Example: mapStopAnimation({ mapId: 'mapId1' })
 * @param {object} payload object with mapId: string
 */
export const mapStopAnimation = (
  payload: SetMapAnimationStopPayload,
): MapActions => ({
  type: WEBMAP_STOP_ANIMATION,
  payload,
});

/**
 * Moves the layer (for example when sorting the stacking of the layers)
 *
 * Example: layerMoveLayer({ mapId: 'mapId1', oldIndex: 1, newIndex: 2 })
 * @param {object} payload object with mapId: string, oldIndex: number, newIndex: number
 */
export const layerMoveLayer = (payload: MoveLayerPayload): MoveLayer => ({
  type: WEBMAP_LAYER_MOVE,
  payload,
});

/**
 *Sets the active layer id for a map
 *
 * Example: setActiveLayerId({ mapId: 'mapId1', layerId: 'layerId1' })
 * @param {object} payload object with mapId: string, layerId: string
 */
export const setActiveLayerId = (
  payload: SetActiveLayerIdPayload,
): SetActiveLayerId => ({ type: WEBMAP_SET_ACTIVELAYERID, payload });

/**
 * Toggles auto update for map
 *
 * Example: toggleAutoUpdate({ mapId: 'mapId1', shouldAutoUpdate: false })
 * @param {object} payload object with mapId: string, shouldAutoUpdate: boolean
 */
export const toggleAutoUpdate = (
  payload: ToggleAutoUpdatePayload,
): ToggleAutoUpdate => ({
  type: WEBMAP_TOGGLE_AUTO_UPDATE,
  payload,
});

/**
 * Toggles loop state for map
 *
 * Example: toggleLoop({ mapId: 'mapId1', shouldLoop: true })
 * @param {object} payload object with mapId: string, shouldLoop: boolean
 */
export const toggleLoop = (payload: ToggleLoopPayload): ToggleLoop => ({
  type: WEBMAP_TOGGLE_LOOP,
  payload,
});

/**
 *Sets the scale of a time slider with id for a map
 *
 * Example: setTimeSliderScale({ mapId: 'mapId1' })
 * @param {object} payload object with  mapId: string, timeSliderScale: number
 */
export const setTimeSliderScale = (
  payload: SetTimeSliderScalePayload,
): MapActions => ({ type: WEBMAP_SET_TIME_SLIDER_SCALE, payload });

/**
 *Sets the time step with id for a map
 *
 * Example: setTimeStep({ mapId: 'mapId1'})
 * @param {object} payload object with  mapId: string, timeStep: number
 */
export const setTimeStep = (payload: SetTimeStepPayload): MapActions => ({
  type: WEBMAP_SET_TIME_STEP,
  payload,
});

/**
 *Sets the scale of a time slider with id for a map
 *
 * Example: setAnimationDelay({ mapId: 'mapId1', animationDelay: 100 })
 * @param {object} payload object with mapId: string, animationDelay: number
 */
export const setAnimationDelay = (
  payload: SetAnimationDelayPayload,
): MapActions => ({
  type: WEBMAP_SET_ANIMATION_DELAY,
  payload,
});

/**
 *Sets the animation start time with id for a map
 *
 * Example: setAnimationStartTime({ mapId: 'mapId1'})
 * @param {object} payload object with  mapId: string, animationStartTime: Moment
 */
export const setAnimationStartTime = (
  payload: SetAnimationStartTimePayload,
): MapActions => ({
  type: WEBMAP_SET_ANIMATION_START_TIME,
  payload,
});

/**
 *Sets the animation end time with id for a map
 *
 * Example: setAnimationEndTime({ mapId: 'mapId1'})
 * @param {object} payload object with  mapId: string, animationEndTime: Moment
 */
export const setAnimationEndTime = (
  payload: SetAnimationEndTimePayload,
): MapActions => ({
  type: WEBMAP_SET_ANIMATION_END_TIME,
  payload,
});

/**
 * Toggles timestep auto for map
 *
 * Example: toggleTimestepAuto({ mapId: 'mapId1', timestepAuto: false })
 * @param {object} payload object with mapId: string, timestepAuto: boolean
 */
export const toggleTimestepAuto = (
  payload: ToggleTimestepAutoPayload,
): ToggleTimestepAuto => ({
  type: WEBMAP_TOGGLE_TIMESTEP_AUTO,
  payload,
});

/**
 * Toggles time slider hover for map
 *
 * Example: toggleTimeSliderHover({ mapId: 'mapId1', isTimeSliderHoverOn: true })
 * @param {object} payload object with mapId: string, isTimeSliderHoverOn: boolean
 */
export const toggleTimeSliderHover = (
  payload: ToggleTimeSliderHoverPayload,
): ToggleTimeSliderHover => ({
  type: WEBMAP_TOGGLE_TIME_SLIDER_HOVER,
  payload,
});

/**
 *Sets the time slider center time with id for a map
 *
 * Example: setTimeSliderCenterTime({ mapId: 'mapId1'})
 * @param {object} payload object with  mapId: string, timeSliderCenterTime: number
 */
export const setTimeSliderCenterTime = (
  payload: SetTimeSliderCenterTimePayload,
): MapActions => ({
  type: WEBMAP_SET_TIME_SLIDER_CENTER_TIME,
  payload,
});

/**
 *Sets the time slider seconds per pixel with id for a map
 *
 * Example: setTimeSliderSecondsPerPx({ mapId: 'mapId1'})
 * @param {object} payload object with  mapId: string, timeSliderSecondsPerPx: number
 */
export const SetTimeSliderSecondsPerPx = (
  payload: SetTimeSliderSecondsPerPxPayload,
): MapActions => ({
  type: WEBMAP_SET_TIME_SLIDER_SECONDS_PER_PX,
  payload,
});
