/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { takeLatest, put, select } from 'redux-saga/effects';
import moment from 'moment';
import configureStore from 'redux-mock-store';
import createSagaMiddleware from 'redux-saga';

import * as mapConstants from './constants';
import * as mapActions from './actions';
import * as mapSelectors from './selectors';
import * as layerConstants from '../layers/constants';
import * as layerActions from '../layers/actions';
import * as layerSelectors from '../layers/selectors';
import {
  stopAnimationSaga,
  startAnimationSaga,
  rootSaga,
  updateMapDraw,
  generateTimeList,
  deleteLayerSaga,
  setLayerDimensionsSaga,
  toggleAutoUpdateSaga,
  updateAnimation,
} from './sagas';
import {
  SetMapAnimationStop,
  SetMapAnimationStart,
  TimeListType,
} from './types';
import { DeleteLayer, LayerType, SetLayerDimensions } from '../layers/types';
import { mockStateMapWithLayer } from '../../../utils/testUtils';

describe('store/mapStore/map/sagas', () => {
  it('should catch rootSaga actions and fire corresponding sagas', () => {
    const generator = rootSaga();
    expect(generator.next().value).toEqual(
      takeLatest(mapConstants.WEBMAP_STOP_ANIMATION, stopAnimationSaga),
    );
    expect(generator.next().value).toEqual(
      takeLatest(mapConstants.WEBMAP_START_ANIMATION, startAnimationSaga),
    );
    expect(generator.next().value).toEqual(
      takeLatest(layerConstants.WEBMAP_LAYER_DELETE, deleteLayerSaga),
    );
    expect(generator.next().value).toEqual(
      takeLatest(
        layerConstants.WEBMAP_LAYER_SET_DIMENSIONS,
        setLayerDimensionsSaga,
      ),
    );
    expect(generator.next().value).toEqual(
      takeLatest(mapConstants.WEBMAP_TOGGLE_AUTO_UPDATE, toggleAutoUpdateSaga),
    );
  });

  describe('stopAnimationSaga', () => {
    it('should stop animation', () => {
      const mapId = 'map-test';
      const action = mapActions.mapStopAnimation({
        mapId,
      }) as SetMapAnimationStop;
      const generator = stopAnimationSaga(action);

      expect(generator.next().value).toEqual(
        updateMapDraw(mapId, 'opengeoweb-core-map-reducer'),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('startAnimationSaga', () => {
    it('should start animation with timelist', () => {
      const mapId = 'map-test';
      const timeList = [
        { name: 'time', value: '2020-03-24T08:45:40.000Z' },
        { name: 'time', value: '2020-03-24T08:50:40.000Z' },
        { name: 'time', value: '2020-03-24T08:55:40.000Z' },
      ] as TimeListType[];

      const action = mapActions.mapStartAnimation({
        mapId,
        timeList,
      }) as SetMapAnimationStart;

      const generator = startAnimationSaga(action);
      const result = generator.next().value;
      expect(result).toEqual(timeList);
      expect(generator.next().value).toEqual(updateMapDraw(mapId, result));
    });

    it('should start animation and create timeList', () => {
      const mapId = 'map-test';

      const start = moment.utc('2021-01-01T11:59:30.000Z');
      const end = moment.utc('2021-01-01T12:15:30.000Z');
      const interval = 1 * 60;
      const action = mapActions.mapStartAnimation({
        mapId,
        start,
        end,
        interval,
      }) as SetMapAnimationStart;

      const generator = startAnimationSaga(action);
      const result = generator.next().value;
      expect(result).toEqual(
        generateTimeList(
          moment.utc('2021-01-01T12:00:00.000Z'),
          moment.utc('2021-01-01T12:15:00.000Z'),
          interval,
        ),
      );
      expect(generator.next().value).toEqual(updateMapDraw(mapId, result));
    });
  });

  describe('updateAnimation', () => {
    const mapId = 'map-test';
    const maxValue = '2021-01-01T12:00:00.000Z';
    const animationStart = moment.utc('2021-01-01T10:00:00.000Z');
    const animationEnd = moment.utc('2021-01-01T11:00:00.000Z');

    it('should set animation end to max value', () => {
      const generator = updateAnimation(mapId, maxValue);
      generator.next();
      generator.next(animationStart);

      expect(
        generator
          .next(animationEnd)
          .value.payload.action.payload.animationEndTime.unix(),
      ).toEqual(moment.utc(maxValue).unix());
    });

    it('should keep animation length fixed', () => {
      const generator = updateAnimation(mapId, maxValue);
      generator.next();
      generator.next(animationStart);
      generator.next(animationEnd);

      const length = animationEnd.unix() - animationStart.unix();
      const setAnimationStartAction = generator.next().value;
      const newAnimationStart =
        setAnimationStartAction.payload.action.payload.animationStartTime;
      expect(moment.utc(maxValue).unix() - newAnimationStart.unix()).toEqual(
        length,
      );
    });

    it('should restart the animation', () => {
      const generator = updateAnimation(mapId, maxValue);
      generator.next();
      generator.next(animationStart);
      generator.next(animationEnd);
      generator.next();
      generator.next();
      generator.next(true);
      expect(generator.next(20).value.payload.action.type).toEqual(
        put(mapActions.mapStartAnimation({ mapId })).payload.action.type,
      );
    });
  });

  describe('deleteLayerSaga', () => {
    it('should stop animation after deleting the last layer', () => {
      const mapId = 'map-test';
      const layerId = 'layer-1';
      const action = layerActions.layerDelete({
        mapId,
        layerId,
      }) as DeleteLayer;
      const generator = deleteLayerSaga(action);

      expect(generator.next().value).toEqual(
        select(mapSelectors.getMapLayers, mapId),
      );
      expect(generator.next([]).value).toEqual(
        put(mapActions.mapStopAnimation({ mapId })),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should not stop animation after deleting when there are still layers', () => {
      const mapId = 'map-test';
      const layerId = 'layer-1';
      const action = layerActions.layerDelete({
        mapId,
        layerId,
      }) as DeleteLayer;
      const generator = deleteLayerSaga(action);

      expect(generator.next().value).toEqual(
        select(mapSelectors.getMapLayers, mapId),
      );
      expect(generator.next(['layer-2']).done).toBeTruthy();
    });
  });

  describe('toggleAutoUpdateSaga', () => {
    it('should update active layer to latest timedimension', () => {
      const mapId = 'map-test';
      const layerId = 'test-layer';
      const timeDimension = {
        name: 'time',
        currentValue: 'xxx',
        maxValue: 'yyy',
      };

      const action = mapActions.toggleAutoUpdate({
        mapId,
        shouldAutoUpdate: true,
      });

      const generator = toggleAutoUpdateSaga(action);

      expect(generator.next().value).toEqual(
        select(mapSelectors.getActiveLayerId, mapId),
      );

      expect(generator.next(layerId).value).toEqual(
        select(layerSelectors.getLayerTimeDimension, layerId),
      );

      expect(generator.next(timeDimension).value).toEqual(
        put(
          layerActions.layerChangeDimension({
            layerId,
            origin: 'toggleAutoUpdateSaga',
            dimension: {
              name: 'time',
              currentValue: timeDimension.maxValue,
            },
          }),
        ),
      );

      generator.next();
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('setLayerDimensionsSaga', () => {
    it('should update the active layer to new time dimension if there is a previous dimension and autoupdating is true', done => {
      const mapId = 'map-test';
      const layer = {
        service: 'https://testservice',
        name: 'test-name',
        title: 'test-title',
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: '2020-03-13T13:30:00Z',
          },
        ],
        id: 'test-layer',
      };
      const saga = createSagaMiddleware();
      const mockState = mockStateMapWithLayer(layer, mapId);
      mockState.webmap.byId[mapId].isAutoUpdating = true;
      const mockStore = configureStore([saga]);
      const store = mockStore(mockState);

      const timeDimension = {
        name: 'time',
        currentValue: '2021-01-01T00:00:00.000Z',
        maxValue: '2021-01-01T03:00:00.000Z',
      };
      const timeDimension2 = {
        name: 'time',
        currentValue: '2021-01-01T03:00:00.000Z',
        maxValue: '2021-01-01T06:00:00.000Z',
      };

      const action = layerActions.layerSetDimensions({
        layerId: layer.id,
        dimensions: [timeDimension],
        origin: 'sagas.spec',
      }) as SetLayerDimensions;
      const action2 = layerActions.layerSetDimensions({
        layerId: layer.id,
        dimensions: [timeDimension2],
        origin: 'sagas.spec',
      }) as SetLayerDimensions;

      const actionNotToPut = {
        payload: {
          layerId: layer.id,
          origin: 'setLayerDimensionSaga',
          dimension: {
            name: 'time',
            currentValue: timeDimension.maxValue,
          },
        },
        type: 'WEBMAP_LAYER_CHANGE_DIMENSION',
      };

      const actionToPut = {
        payload: {
          layerId: layer.id,
          origin: 'setLayerDimensionSaga',
          dimension: {
            name: 'time',
            currentValue: timeDimension2.maxValue,
          },
        },
        type: 'WEBMAP_LAYER_CHANGE_DIMENSION',
      };

      store.subscribe(() => {
        const actions = store.getActions();
        if (actions.length >= 1) {
          expect(actions).not.toContainEqual(actionNotToPut);
          expect(actions).toContainEqual(actionToPut);
          done();
        }
      });

      saga.run(setLayerDimensionsSaga, action);
      saga.run(setLayerDimensionsSaga, action2);
    });
  });
});
