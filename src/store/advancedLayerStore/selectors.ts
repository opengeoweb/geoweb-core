/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from 'reselect';
import { AppStore } from '../../types/types';
import { AdvancedLayerStoreState } from './types';

const advancedLayerStore = (store: AppStore): AdvancedLayerStoreState =>
  store.advancedLayerStore ? store.advancedLayerStore : null;

/**
 * Extracts the state of the advancedLayerStore from the store
 *
 * Example: advancedLayerStore = getAdvancedLayerStore(store);
 * @param {object} store store: object - object from which the advancedLayerStore state will be extracted
 * @returns {object} returnType: object - advancedLayerStore state part of the store
 */
export const getAdvancedLayerStore = createSelector(
  advancedLayerStore,
  store => store,
);
