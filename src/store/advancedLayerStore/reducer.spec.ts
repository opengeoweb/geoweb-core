/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import { reducer as advancedLayerStoreReducer, initialState } from './reducer';
import * as advancedLayerStoreActions from './actions';

describe('store/advancedLayerStore/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(advancedLayerStoreReducer(undefined, {})).toEqual(initialState);
  });

  it('should set the time resolution', () => {
    const newTimeResolution = 30;
    const result = advancedLayerStoreReducer(
      undefined,
      advancedLayerStoreActions.setTimeResolution({
        timeResolution: newTimeResolution,
      }),
    );
    expect(result).toEqual({
      ...initialState,
      timeResolution: newTimeResolution,
    });
  });

  it('should set the timeStart, timeEnd and timeValue even if timeResolution is not given', () => {
    const newValues = {
      timeStart: moment('2020-01-25T23:14:30.000Z'),
      timeEnd: moment('2020-01-26T00:00:00.000Z'),
      timeValue: moment('2020-01-26T00:00:00.000Z'),
    };
    const result = advancedLayerStoreReducer(
      undefined,
      advancedLayerStoreActions.setTimeResolution({
        ...newValues,
      }),
    );
    expect(result).toEqual({
      ...newValues,
      timeResolution: initialState.timeResolution,
    });
  });

  it('should set the time value', () => {
    const newTimeValue = moment('2020-01-26T00:00:00.000Z');
    const result = advancedLayerStoreReducer(
      undefined,
      advancedLayerStoreActions.setTimeValue({
        timeValue: newTimeValue,
      }),
    );
    expect(result).toEqual({ ...initialState, timeValue: newTimeValue });
  });
});
