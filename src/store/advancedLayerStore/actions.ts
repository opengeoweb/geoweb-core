/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  ADVANCED_LAYER_STORE_SET_TIMERESOLUTION,
  ADVANCED_LAYER_STORE_SET_TIMEVALUE,
} from './constants';

import {
  AdvancedLayerStoreActions,
  SetTimeResolutionPayload,
  SetTimeValuePayload,
} from './types';

/**
 * Sets time start, end, value and resolution if passed in (timeResolution must be in seconds)
 *
 * Example: setTimeResolution ({ timeStart: momentValue, timeEnd: momentValue, timeValue: momentValue, timeResolution: 300 });
 * @param {object} payload object with: timeStart?: Moment, timeEnd?: Moment, timeValue?: Moment, timeResolution?: number->(in seconds);
 */
export const setTimeResolution = (
  payload: SetTimeResolutionPayload,
): AdvancedLayerStoreActions => ({
  type: ADVANCED_LAYER_STORE_SET_TIMERESOLUTION,
  payload,
});

/**
 * Sets time value
 *
 * Example: setTimeValue ({ timeValue: value });
 * @param {object} payload Object with timeValue: Moment;
 */
export const setTimeValue = (
  payload: SetTimeValuePayload,
): AdvancedLayerStoreActions => ({
  type: ADVANCED_LAYER_STORE_SET_TIMEVALUE,
  payload,
});
