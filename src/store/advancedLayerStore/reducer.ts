/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import produce from 'immer';
import {
  ADVANCED_LAYER_STORE_SET_TIMERESOLUTION,
  ADVANCED_LAYER_STORE_SET_TIMEVALUE,
} from './constants';

import { AdvancedLayerStoreState, AdvancedLayerStoreActions } from './types';
import { LayerActions, MapActions } from '../mapStore/types';

export const initialState = {
  timeResolution: 60,
  timeStart: null,
  timeEnd: null,
  timeValue: null,
};

export const reducer = (
  state = initialState,
  action: AdvancedLayerStoreActions | LayerActions | MapActions,
): AdvancedLayerStoreState => {
  switch (action.type) {
    case ADVANCED_LAYER_STORE_SET_TIMERESOLUTION:
      return produce(state, draft => {
        if (action.payload.timeResolution)
          draft.timeResolution = action.payload.timeResolution;

        if (action.payload.timeStart && action.payload.timeStart.isValid())
          draft.timeStart = action.payload.timeStart; // else draft.timeStart = newStart;
        if (action.payload.timeEnd && action.payload.timeEnd.isValid())
          draft.timeEnd = action.payload.timeEnd; // else draft.timeEnd = newEnd;
        if (action.payload.timeValue && action.payload.timeValue.isValid())
          draft.timeValue = action.payload.timeValue;
      });
    case ADVANCED_LAYER_STORE_SET_TIMEVALUE:
      return produce(state, draft => {
        draft.timeValue = action.payload.timeValue;
      });
    default:
      return state;
  }
};
