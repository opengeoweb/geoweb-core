/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { Moment } from 'moment';
import { Action } from 'redux';
import {
  ADVANCED_LAYER_STORE_SET_TIMERESOLUTION,
  ADVANCED_LAYER_STORE_SET_TIMEVALUE,
} from './constants';

// state
export interface AdvancedLayerStoreState {
  timeResolution: number; // Time resolution should be passed in in seconds
  timeStart: string;
  timeEnd: string;
  timeValue: string;
}

export interface AdvancedLayerStoreModuleState {
  advancedLayerStore?: AdvancedLayerStoreState;
}

// actions

export interface SetTimeResolutionPayload {
  timeStart?: Moment;
  timeEnd?: Moment;
  timeValue?: Moment;
  timeResolution?: number; // Time resolution should be passed in in seconds
}
export interface SetTimeResolution extends Action {
  type: typeof ADVANCED_LAYER_STORE_SET_TIMERESOLUTION;
  payload: SetTimeResolutionPayload;
}

// SetTimeValue
export interface SetTimeValuePayload {
  timeValue: Moment;
}

export interface SetTimeValue extends Action {
  type: typeof ADVANCED_LAYER_STORE_SET_TIMEVALUE;
  payload: SetTimeValuePayload;
}

export type AdvancedLayerStoreActions = SetTimeResolution | SetTimeValue;
