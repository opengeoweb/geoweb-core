/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import I18n from './I18n/lang.en';
import { WMSVersion, error, debug } from './WMConstants';
import { URLEncode, isDefined, toArray, isNull } from './WMJSTools';
import WMXMLParser from './WMXMLParser';

export interface GetCapabilitiesJson {
  WMS_Capabilities?;
  WMT_MS_Capabilities?;
  ServiceExceptionReport?;
}

export interface Capability {
  Layer;
  Request;
  Exception?;
}

export const loadGetCapabilitiesViaProxy = (
  url: string,
  succes: (param: string) => void,
  fail: (param: string) => void,
  xml2jsonrequestURL: string,
): void => {
  let getcapreq = `${xml2jsonrequestURL}request=`;

  getcapreq += URLEncode(url);

  try {
    fetch(getcapreq, {})
      .then(result => result.text())
      .then(result => {
        debug('loadGetCapabilitiesViaProxy succesfully finished');
        succes(result);
      })
      .catch(() => {
        fail(`Request failed for ${getcapreq}`);
      });
  } catch {
    fail(`Request failed for ${getcapreq}`);
  }
};

export const getWMSUrl = (service = '', additionalParams = ''): string =>
  `${service}&service=WMS&request=GetCapabilities${additionalParams}`;

/**
 * Global getcapabilities function
 */
export const WMJSGetCapabilities = (
  service: string,
  succes: (param) => void,
  fail: (param: string) => void,
  xml2jsonrequestURL: string,
  options: { headers? },
  disableCache = false,
): void => {
  /* Make the getCapabilitiesJSONrequest */
  let headers = [];
  if (options && options.headers) headers = options.headers;
  if (!isDefined(service)) {
    fail(I18n.no_service_defined.text);
    return;
  }
  if (service.length === 0) {
    error('Service is empty');
    fail(I18n.service_url_empty.text);
    return;
  }

  /* Allow relative URL's */
  if (service.startsWith('/') && !service.startsWith('//')) {
    const splittedHREF = window.location.href
      .split('/')
      .filter(e => e.length > 0);
    const hostName = `${splittedHREF[0]}//${splittedHREF[1]}/`;
    // eslint-disable-next-line no-param-reassign
    service = hostName + service;
  }

  if (
    !service.startsWith('http://') &&
    !service.startsWith('https:') &&
    !service.startsWith('//')
  ) {
    error('Service does not start with HTTPS');
    fail(I18n.service_url_empty.text);
    return;
  }

  if (service.indexOf('?') === -1) {
    // eslint-disable-next-line no-param-reassign
    service += '?';
  }
  debug('GetCapabilities:');

  const url = getWMSUrl(
    service,
    disableCache ? `&random=${Math.random()}` : '',
  );

  const newXml2jsonrequestURL = xml2jsonrequestURL;

  WMXMLParser(url, headers)
    .then(data => {
      try {
        succes(data);
      } catch (e) {
        console.error(e);
      }
    })
    .catch(() => {
      loadGetCapabilitiesViaProxy(
        url,
        succes,
        () => {
          fail(`Request failed for ${url}`);
        },
        newXml2jsonrequestURL,
      );
    });
};

interface NodeObject {
  text: string;
  leaf: boolean;
  name?: string;
  path?: string;
  children?: [];
}

export const sortByKey = (array: NodeObject[], key: string): NodeObject[] => {
  return array.sort((a, b) => {
    const x = a[key];
    const y = b[key];
    if (x < y) return -1;
    if (x > y) return 1;
    return 0;
  });
};

export const recursivelyFindLayer = (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  layers: any[],
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  rootNode: any[],
  path: string,
): void => {
  for (let j = 0; j < layers.length; j += 1) {
    let isleaf = false;
    if (layers[j].Name) isleaf = true;
    try {
      if (layers[j].Layer) isleaf = false;
    } catch (e) {
      // do nothing
    }
    let nodeObject: NodeObject;

    if (layers[j].Name) {
      nodeObject = {
        name: layers[j].Name.value,
        text: layers[j].Title.value,
        leaf: isleaf,
        path,
      };
    } else {
      const nodeText = isNull(layers[j].Title)
        ? 'Layer'
        : layers[j].Title.value;

      nodeObject = { text: nodeText, leaf: isleaf };
    }
    rootNode.push(nodeObject);
    if (layers[j].Layer) {
      nodeObject.children = [];
      recursivelyFindLayer(
        toArray(layers[j].Layer),
        nodeObject.children,
        path + layers[j].Title.value,
      );
    }
  }
  // Sort nodes alphabetically.
  sortByKey(rootNode, 'text');
};

/**
 * WMJSService Class
 *
 * options:
 *   service
 *   xml2jsonrequestURL
 *   title (optional)
 */

export class WMJSService {
  service: string = undefined;

  title: string;

  onlineresource: string;

  abstract: string;

  version: string;

  getcapabilitiesDoc: GetCapabilitiesJson;

  busy: boolean;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  _flatLayerObject: any[];

  xml2jsonrequestURL: string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  functionCallbackList: any[];

  _options;

  nodeCache;

  constructor(options: {
    service: string;
    xml2jsonrequestURL: string;
    title?: string;
  }) {
    this.service = undefined;
    this.title = undefined;
    this.onlineresource = undefined;
    this.abstract = undefined;
    this.version = WMSVersion.version111;
    this.getcapabilitiesDoc = undefined;
    this.busy = false;
    this._flatLayerObject = undefined;
    if (options) {
      this.service = options.service;
      this.title = options.title;
      this.xml2jsonrequestURL = options.xml2jsonrequestURL;
    }
    this.checkVersion111 = this.checkVersion111.bind(this);
    this.checkVersion130 = this.checkVersion130.bind(this);
    this.getCapabilityElement = this.getCapabilityElement.bind(this);
    this.checkVersion = this.checkVersion.bind(this);
    this.getCapabilities = this.getCapabilities.bind(this);
    this.checkException = this.checkException.bind(this);
    this.getNodes = this.getNodes.bind(this);
    this.getLayerObjectsFlat = this.getLayerObjectsFlat.bind(this);
    this.functionCallbackList = [];
  }

  checkVersion111(jsonData: GetCapabilitiesJson): void {
    try {
      const rootLayer = jsonData.WMT_MS_Capabilities.Capability.Layer;
      if (!rootLayer) throw new Error('No 111 layer');
    } catch (e) {
      const message = this.checkException(jsonData);
      if (message !== undefined) {
        throw new Error(message);
      }
      if (!jsonData.WMT_MS_Capabilities.Capability) {
        throw new Error(I18n.no_wms_capability_element_found.text);
      }
      if (!jsonData.WMT_MS_Capabilities.Capability.Layer) {
        throw new Error(I18n.no_wms_layer_element_found.text);
      }
    }
  }

  checkVersion130(jsonData: GetCapabilitiesJson): void {
    try {
      const rootLayer = jsonData.WMS_Capabilities.Capability.Layer;
      if (!rootLayer) throw new Error('No 130 layer');
    } catch (e) {
      const message = this.checkException(jsonData);
      if (message !== undefined) {
        throw new Error(message);
      }
      if (!jsonData.WMS_Capabilities.Capability) {
        throw new Error(I18n.no_wms_capability_element_found.text);
      }
      if (!jsonData.WMS_Capabilities.Capability.Layer) {
        throw new Error(I18n.no_wms_layer_element_found.text);
      }
    }
  }

  // eslint-disable-next-line class-methods-use-this
  getCapabilityElement(jsonData: GetCapabilitiesJson): Capability {
    let capabilityObject;
    try {
      capabilityObject = jsonData.WMT_MS_Capabilities.Capability;
    } catch (e) {
      try {
        capabilityObject = jsonData.WMS_Capabilities.Capability;
      } catch {
        throw new Error(I18n.no_capability_element_found.text);
      }
    }
    return capabilityObject;
  }

  checkVersion(jsonData: GetCapabilitiesJson): string {
    let version = null;
    try {
      if (WMSVersion.version100 === jsonData.WMT_MS_Capabilities.attr.version)
        version = WMSVersion.version100;
      if (WMSVersion.version111 === jsonData.WMT_MS_Capabilities.attr.version)
        version = WMSVersion.version111;
      if (WMSVersion.version130 === jsonData.WMT_MS_Capabilities.attr.version)
        version = WMSVersion.version130;
    } catch (e) {
      try {
        if (WMSVersion.version100 === jsonData.WMS_Capabilities.attr.version)
          version = WMSVersion.version100;
        if (WMSVersion.version111 === jsonData.WMS_Capabilities.attr.version)
          version = WMSVersion.version111;
        if (WMSVersion.version130 === jsonData.WMS_Capabilities.attr.version)
          version = WMSVersion.version130;
      } catch {
        const message = this.checkException(jsonData);
        if (message) {
          throw new Error(message);
        } else {
          throw new Error('Unable to determine WMS version');
        }
      }
    }
    if (version === WMSVersion.version111) {
      this.checkVersion111(jsonData);
      return version;
    }
    if (version === WMSVersion.version130) {
      this.checkVersion130(jsonData);
      return version;
    }
    return WMSVersion.version111;
  }

  /**
   * Does getcapabilities for a service.
   * When multple getCapabilities for the same service are made,
   * this method makes one get request and fires all callbacks with the same result.
   * @param succescallback Function called upon succes, cannot be left blank
   * @param failcallback Function called upon failure, cannot be left blank
   */
  getCapabilities(
    succescallback: (param: GetCapabilitiesJson) => void,
    failcallback: (param: unknown) => void,
    forceReload: boolean,
    xml2jsonrequestURL: string,
    options: unknown,
  ): void {
    if (options) {
      this._options = options;
    }
    if (this.busy) {
      const cf = { callback: succescallback, fail: failcallback };
      this.functionCallbackList.push(cf);
      return;
    }

    this._flatLayerObject = undefined;

    if (!this.getcapabilitiesDoc || forceReload === true) {
      this.busy = true;
      const cf = { callback: succescallback, fail: failcallback };
      this.functionCallbackList.push(cf);

      /* Fail functions */
      const fail = (jsonData): void => {
        this.busy = false;
        let current;
        while ((current = this.functionCallbackList.pop())) {
          current.fail(jsonData);
        }
      };
      /* Success functions */
      const succes = (jsonData): void => {
        this.busy = false;
        this.getcapabilitiesDoc = jsonData;

        try {
          this.version = this.checkVersion(jsonData);
        } catch (e) {
          fail(e.message);
          return;
        }

        let WMSCapabilities = jsonData.WMS_Capabilities;
        if (!WMSCapabilities) {
          WMSCapabilities = jsonData.WMT_MS_Capabilities;
        }

        // Get Abstract
        try {
          this.abstract = WMSCapabilities.Service.Abstract.value;
        } catch (e) {
          this.abstract = I18n.not_available_message.text;
        }

        // Get Title
        try {
          this.title = WMSCapabilities.Service.Title.value;
        } catch (e) {
          this.title = I18n.not_available_message.text;
        }

        // Get OnlineResource
        try {
          if (WMSCapabilities.Service.OnlineResource.value) {
            this.onlineresource = WMSCapabilities.Service.OnlineResource.value;
          } else {
            this.onlineresource =
              WMSCapabilities.Service.OnlineResource.attr['xlink:href'];
          }
        } catch (e) {
          this.onlineresource = I18n.not_available_message.text;
        }
        // Fill in flatlayer object
        this.getLayerObjectsFlat(
          () => null,
          () => null,
          false,
          xml2jsonrequestURL,
          options,
        );

        let current;
        while ((current = this.functionCallbackList.pop())) {
          current.callback(jsonData);
        }
      };

      WMJSGetCapabilities(
        this.service,
        succes,
        fail,
        xml2jsonrequestURL,
        options,
      );
    } else {
      /* The callbacks are not allowed to be called in the scope of this object. 
        By using window.setTimeOut they are externally called 
      */
      window.setTimeout(() => {
        succescallback(this.getcapabilitiesDoc);
      }, 1);
    }
  }

  // eslint-disable-next-line class-methods-use-this
  checkException(jsonData: GetCapabilitiesJson): string | undefined {
    try {
      if (jsonData.ServiceExceptionReport) {
        let code: string;
        let value: string;
        const se = jsonData.ServiceExceptionReport.ServiceException;
        if (se) {
          try {
            if (se.attr.code) code = se.attr.code;
          } catch (e) {
            // do nothing
          }
          if (se.value) {
            value = se.value;
            return `Exception: ${code}.\n${value}`;
          }
        }
        return I18n.wms_service_exception_code.text + code;
      }
    } catch (e) {
      // do nothing
    }
    return undefined;
  }

  /**
   * Calls succes with a hierarchical node structure
   * Calls failure with a string when someting goes wrong
   */
  getNodes(
    succes: (param: unknown) => void,
    failure: (param: unknown) => void,
    forceReload: boolean,
    xml2jsonrequestURL = this.xml2jsonrequestURL,
    options?: unknown,
  ): void {
    this.nodeCache = undefined;
    if (!failure) {
      // eslint-disable-next-line no-param-reassign
      failure = (msg: string): void => {
        error(msg);
      };
    }

    const parse = (jsonData: GetCapabilitiesJson): void => {
      const nodeStructure = {
        leaf: false,
        expanded: true,
        children: [],
        text: '',
      };
      const rootLayer = this.getCapabilityElement(jsonData).Layer;

      try {
        this.version = this.checkVersion(jsonData);
      } catch (e) {
        failure(e);
        return;
      }

      const WMSLayers = toArray(rootLayer.Layer);
      try {
        nodeStructure.text = rootLayer.Title.value;
      } catch (e) {
        nodeStructure.text = I18n.unnamed_service.text;
      }

      recursivelyFindLayer(WMSLayers, nodeStructure.children, '');
      succes(nodeStructure);
    };

    const callback = (jsonData: GetCapabilitiesJson): void => {
      parse(jsonData);
    };

    const fail = (data): void => {
      failure(data);
    };
    this.getCapabilities(
      callback,
      fail,
      forceReload,
      xml2jsonrequestURL,
      options,
    );
  }

  /** Calls succes with an array of all layerobjects
   * Calls failure when something goes wrong
   */
  getLayerObjectsFlat(
    succes: (param: unknown) => void,
    failure: (param: unknown) => void,
    forceReload: boolean,
    xml2jsonrequestURL = this.xml2jsonrequestURL,
    options = this._options,
  ): void {
    if (!xml2jsonrequestURL) {
      // eslint-disable-next-line no-param-reassign
      xml2jsonrequestURL = this.xml2jsonrequestURL;
    }
    if (isDefined(this._flatLayerObject) && forceReload !== true) {
      succes(this._flatLayerObject);
      return;
    }

    const callback = (data): void => {
      this._flatLayerObject = [];
      const getNames = (layers): void => {
        for (let j = 0; j < layers.length; j += 1) {
          if (layers[j].name) {
            this._flatLayerObject.push(layers[j]);
          }
          if (layers[j].children) {
            getNames(layers[j].children);
          }
        }
      };
      getNames(data.children);

      succes(this._flatLayerObject);
    };
    this.getNodes(callback, failure, forceReload, xml2jsonrequestURL, options);
  }
}
