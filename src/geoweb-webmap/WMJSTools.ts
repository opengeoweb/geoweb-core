/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  WMDateOutSideRange,
  WMDateTooEarlyString,
  WMDateTooLateString,
} from './WMConstants';

/**
 * Checks if variable is defined or not
 * @param variable The variable to check
 * @returns true if variable is indeed defined, otherwise false.
 */
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types,@typescript-eslint/no-explicit-any
export const isDefined = (variable: any): boolean => {
  if (variable === null) return false;
  if (typeof variable === 'undefined') {
    return false;
  }
  return true;
};

/**
 * Checks if a variable is null or not
 * @param variable The variable to check
 * @returns true if variable is indeed null, otherwise false.
 */
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types,@typescript-eslint/no-explicit-any
export const isNull = (variable: any): boolean => {
  if (variable === null) {
    return true;
  }
  return false;
};

/**
 * Converts a variable to an array. If the variable is not an array it will be pushed as the first entry in a new array. If the variable is already an array, nothing will  be done.
 * @param array The variable to convert
 * @returns Always an array
 */
export const toArray = (
  array: unknown | unknown[],
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): any[] => {
  if (array === null || typeof array === 'undefined') return [];
  if (array instanceof Array) {
    return array;
  }
  const newArray = [];
  newArray[0] = array;
  return newArray;
};

/**
 * Function which checks wether URL contains a ? token. If not, it is assumed that this token was not provided by the user,
 * manually add the token later.
 * @param url The URL to check
 * @return the fixed URL
 */
export const WMJScheckURL = (url: string): string => {
  if (!isDefined(url)) return '?';
  const trimmedUrl = url.trim();
  if (trimmedUrl.indexOf('?') === -1) {
    return `${trimmedUrl}?`;
  }
  return trimmedUrl;
};

export const preventdefaultEvent = (e: Event): void => {
  const event = e || window.event;
  if (event.preventDefault) {
    // Firefox
    event.preventDefault();
  } else {
    // IE
    event.returnValue = false;
  }
};

export const getMouseXCoordinate = (event: MouseEvent): number => {
  return (
    event.clientX +
    document.documentElement.scrollLeft +
    document.body.scrollLeft
  );
};

export const getMouseYCoordinate = (event: MouseEvent): number => {
  return (
    event.clientY + document.documentElement.scrollTop + document.body.scrollTop
  );
};

export const URLDecode = (encodedURL: string): string => {
  if (!isDefined(encodedURL)) return '';
  return encodedURL
    .replace('+', ' ')
    .replace('%2B', '+')
    .replace('%20', ' ')
    .replace('%5E', '^')
    .replace('%26', '&')
    .replace('%3F', '?')
    .replace('%3E', '>')
    .replace('%3C', '<')
    .replace('%5C', '\\')
    .replace('%2F', '/')
    .replace('%25', '%')
    .replace('%3A', ':')
    .replace('%27', "'")
    .replace('%24', '$');
};

// Encodes plain text to URL encoding
export const URLEncode = (plaintext: string): string => {
  if (!plaintext) return plaintext;
  if (plaintext === undefined) return plaintext;
  if (plaintext === '') return plaintext;
  const SAFECHARS =
    '0123456789' + // Numeric
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ' + // Alphabetic
    'abcdefghijklmnopqrstuvwxyz' +
    "%-_.!~*'()"; // RFC2396 Mark characters
  const HEX = '0123456789ABCDEF';

  const replacedText = plaintext
    .replace(/%/g, '%25')
    .replace(/\+/g, '%2B')
    .replace(/ /g, '%20')
    .replace(/\^/g, '%5E')
    .replace(/&/g, '%26')
    .replace(/\?/g, '%3F')
    .replace(/>/g, '%3E')
    .replace(/</g, '%3C')
    .replace(/\\/g, '%5C');

  let encoded = '';
  for (let i = 0; i < replacedText.length; i += 1) {
    const ch = replacedText.charAt(i);
    if (ch === ' ') {
      encoded += '%20'; // x-www-urlencoded, rather than %20
    } else if (SAFECHARS.indexOf(ch) !== -1) {
      encoded += ch;
    } else {
      const charCode = ch.charCodeAt(0);

      if (charCode > 255) {
        console.warn(
          `Unicode Character '${ch}' cannot be encoded using standard URL encoding.\n` +
            '(URL encoding only supports 8-bit characters.)\n' +
            'A space (+) will be substituted.',
        );
        encoded += '+';
      } else {
        encoded += '%';
        // eslint-disable-next-line no-bitwise
        encoded += HEX.charAt((charCode >> 4) & 0xf);
        // eslint-disable-next-line no-bitwise
        encoded += HEX.charAt(charCode & 0xf);
      }
    }
  }
  return encoded;
};

/* Adds DIM_ for certain dims */
export const getCorrectWMSDimName = (origDimName: string): string => {
  /* Adds DIM_ for dimensions other than height or time */
  if (origDimName.toUpperCase() === 'TIME') return origDimName;
  if (origDimName.toUpperCase() === 'ELEVATION') return origDimName;
  return `DIM_${origDimName}`;
};

/* Returns all dimensions with its current values as URL */
// TODO: fix `import/no-cycle` when adding type here https://gitlab.com/opengeoweb/geoweb-core/-/issues/339
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const getMapDimURL = (layer): string => {
  let request = '';
  for (let j = 0; j < layer.dimensions.length; j += 1) {
    const currentValue = layer.dimensions[j].getValue();
    request += `&${getCorrectWMSDimName(layer.dimensions[j].name)}`;
    request += `=${URLEncode(currentValue)}`;
    if (
      currentValue === WMDateOutSideRange ||
      currentValue === WMDateTooEarlyString ||
      currentValue === WMDateTooLateString
    ) {
      throw new Error(WMDateOutSideRange);
    }
  }
  return request;
};

/* debug helper */
export const enableConsoleDebugging = false;
export const debug = (message: string): void => {
  if (enableConsoleDebugging) {
    // eslint-disable-next-line no-console
    console.log(message);
  }
};
