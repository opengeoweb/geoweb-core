/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { isDefined } from './WMJSTools';

let numImagesLoading = 0;

export interface WMImageOptions {
  randomizer?: boolean;
  headers: Headers[];
}

/**
 * WMImage provides an API to the HTML image element. It is used for caching and easier access to images.
 */
export default class WMImage {
  imageLife: number;

  randomize: boolean;

  _srcLoaded: string;

  _isLoaded: boolean;

  _isLoading: boolean;

  _hasError: boolean;

  _stopLoading: boolean;

  srcToLoad: string;

  loadEventCallback: (image: WMImage) => void;

  el: HTMLImageElement;

  headers: Headers[];

  timeOutTimer: NodeJS.Timeout;

  timerIsRunning: boolean;

  /**
   *
   * @param src Optionally set the source URL to load
   * @param callback Optionally set the callback function to trigger once image has loaded
   * @param options Additional options to the image. You can set { randomizer: true } to force unique url's and prevent caching.
   */
  constructor(
    src: string,
    callback?: (image: WMImage) => void,
    options?: WMImageOptions,
  ) {
    this.randomize = false;
    this._srcLoaded = undefined;
    this._isLoaded = undefined;
    this._isLoading = undefined;
    this._hasError = undefined;
    this.srcToLoad = src;
    this.loadEventCallback = callback;
    this.el = new Image();
    this.headers = [];
    if (isDefined(options) && isDefined(options.randomizer)) {
      this.randomize = options.randomizer;
    }

    this.init = this.init.bind(this);
    this.isLoaded = this.isLoaded.bind(this);
    this.isLoading = this.isLoading.bind(this);
    this.checkIfThisSourceIsSet = this.checkIfThisSourceIsSet.bind(this);
    this.setSource = this.setSource.bind(this);
    this.clear = this.clear.bind(this);
    this.getSrc = this.getSrc.bind(this);
    this.hasError = this.hasError.bind(this);
    this.stopLoading = this.stopLoading.bind(this);
    this._load = this._load.bind(this);
    this.load = this.load.bind(this);
    this._loadEvent = this._loadEvent.bind(this);
    this.getWidth = this.getWidth.bind(this);
    this.getHeight = this.getHeight.bind(this);
    this.getElement = this.getElement.bind(this);
    this._getImageWithHeaders = this._getImageWithHeaders.bind(this);
    this.init();

    this.el.addEventListener('load', () => {
      this._loadEvent(this, false);
    });
    this.el.addEventListener('error', () => {
      this._loadEvent(this, true);
    });
    this.el.onselectstart = (): boolean => {
      return false;
    };
    this.el.ondrag = (): boolean => {
      return false;
    };
  }

  private init(): void {
    this._srcLoaded = 'undefined image';
    this._isLoaded = false;
    this._isLoading = false;
    this._hasError = false;
    this._stopLoading = false;
  }

  /**
   * Returns true if the image has been loaded
   */
  public isLoaded(): boolean {
    if (this._isLoading) return false;
    return this._isLoaded;
  }

  /**
   * Returns true if the image is still loading
   */
  public isLoading(): boolean {
    return this._isLoading;
  }

  /**
   * Function to check if this URL was given to the image
   * @param src URL to check if this was given to the WMImage
   */
  public checkIfThisSourceIsSet(src: string): boolean {
    if (this._srcLoaded === src || this.srcToLoad === src) {
      return true;
    }
    return false;
  }

  /**
   * Set source of image, but it will not load the image yet.
   * @param src URL of the image to load
   * @param options WMImageOptions to give to the image
   */
  public setSource(src: string, options: WMImageOptions): void {
    if (this._isLoading) {
      // eslint-disable-next-line no-console
      console.error(
        '-------------------------> Source set while still loading!!! ',
      );
      return;
    }
    this.srcToLoad = src;
    if (options && options.headers && options.headers.length > 0) {
      this.headers = options.headers;
    }
    if (this._srcLoaded === this.srcToLoad) {
      this._isLoaded = true;
      return;
    }

    this._isLoaded = false;
  }

  /**
   * Re-intialize the image
   */
  public clear(): void {
    this.init();
    this._stopLoading = true;
  }

  /**
   * Abort loading
   */
  public stopLoading(): void {
    this._stopLoading = true;
  }

  /**
   * Get the URL of the image
   */
  public getSrc(): string {
    return this.srcToLoad;
  }

  /**
   * Check if the image has an error
   */
  public hasError(): boolean {
    return this._hasError;
  }

  /**
   * Start loading the image
   */
  public load(): void {
    this._stopLoading = false;
    if (this.timeOutTimer) {
      clearTimeout(this.timeOutTimer);
    }
    this._load();
  }

  private _getImageWithHeaders(url, headers): void {
    const fetchHeaders = new Headers();
    if (headers && headers.length > 0) {
      for (let j = 0; j < headers.length; j += 1) {
        fetchHeaders.append(headers[j].name, headers[j].value);
      }
    }
    const options: RequestInit = {
      method: 'GET',
      headers: fetchHeaders,
      mode: 'cors',
      cache: 'default',
    };

    const request = new Request(url);

    const arrayBufferToBase64 = (buffer): string => {
      let binary = '';
      const bytes = [].slice.call(new Uint8Array(buffer));
      bytes.forEach(b => {
        binary += String.fromCharCode(b);
      });
      return window.btoa(binary);
    };

    fetch(request, options)
      .then(response => {
        response.arrayBuffer().then(buffer => {
          const base64Flag = 'data:image/png;base64,';
          const imageStr = arrayBufferToBase64(buffer);
          this.getElement().src = base64Flag + imageStr;
        });
      })
      .catch(() => {
        // eslint-disable-next-line no-console
        console.error(
          `Unable to fetch image ${url} with headers [${JSON.stringify(
            headers,
          )}]`,
        );
        if (url.startsWith('http://'))
          // eslint-disable-next-line no-console
          console.error(
            'Note that URL starts with http:// instead of https://',
          );
        this._loadEvent(this, true);
      });
  }

  private _load(): void {
    this._hasError = false;
    if (this._isLoaded === true) {
      this._loadEvent(this, false);
      return;
    }
    this._isLoading = true;
    if (!this.srcToLoad) {
      this._loadEvent(this, true);
      return;
    }

    /* Allow relative URL's */
    if (this.srcToLoad.startsWith('/') && !this.srcToLoad.startsWith('//')) {
      const splittedHREF = window.location.href
        .split('/')
        .filter(e => e.length > 0);
      const hostName = `${splittedHREF[0]}//${splittedHREF[1]}/`;
      this.srcToLoad = hostName + this.srcToLoad;
    }

    // if (this.srcToLoad.startsWith('http') === false && this.srcToLoad.startsWith('//') === false) {
    //   console.error('Source does not start with http');
    //   this.loadEvent(this, true);
    //   return;
    // }

    if (this.srcToLoad === this._srcLoaded) {
      this._loadEvent(this, false);
      return;
    }

    if (this.timerIsRunning === true) return;
    if (numImagesLoading >= 4) {
      if (this._stopLoading === false) {
        this.timerIsRunning = true;
        setTimeout(() => {
          this.timerIsRunning = false;
          this._load();
        }, 10);
      } else {
        /* Cancel loading */
        this.init();
      }
      return;
    }
    numImagesLoading += 1;

    if (this.headers && this.headers.length > 0) {
      /* Get an image which needs headers */
      this._getImageWithHeaders(this.srcToLoad, this.headers);
    } else if (this.randomize) {
      /* Do a standard img.src url request */
      let newSrc = this.srcToLoad;
      if (this.srcToLoad.indexOf('?') === -1) {
        newSrc += '?';
      }
      newSrc += `random=${Math.random()}`;

      this.getElement().src = newSrc;
    } else {
      this.getElement().src = this.srcToLoad;
    }

    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const image = this;
    this.timeOutTimer = setTimeout(() => {
      if (image._isLoading === true) {
        // eslint-disable-next-line no-console
        console.log('Slow service detected', image.srcToLoad);
        image._loadEvent(image, false);
      }
    }, 2500);
  }

  private _loadEvent(image: WMImage, hasError: boolean): void {
    numImagesLoading -= 1;
    this._hasError = hasError;
    this._isLoading = false;
    this._isLoaded = true;
    this._srcLoaded = this.srcToLoad;
    if (this.timeOutTimer) {
      clearTimeout(this.timeOutTimer);
    }

    if (isDefined(this.loadEventCallback)) {
      this.loadEventCallback(this);
    }
  }

  /**
   * Get the width of the current image
   */
  public getWidth(): number {
    return this.el.width;
  }

  /**
   * Get the height of the current image
   */
  public getHeight(): number {
    return this.el.height;
  }

  /**
   * Get the HTMLImageElement behind the WMImage
   */
  public getElement(): HTMLImageElement {
    return this.el;
  }
}
