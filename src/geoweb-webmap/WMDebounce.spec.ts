/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import WMDebouncer from './WMDebouncer';

describe('src/geoweb-webmap/WMDebouncer', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should create class with correct properties', () => {
    const wmDebouncer = new WMDebouncer();

    expect(wmDebouncer._isRunning).toBeFalsy();
    expect(wmDebouncer._milliseconds).toEqual(10);
    expect(wmDebouncer._stop).toBeFalsy();
  });

  it('should initialise with callback handler', () => {
    const wmDebouncer = new WMDebouncer();
    const callback = jest.fn();
    wmDebouncer.init(0, callback);

    expect(wmDebouncer._milliseconds).toEqual(50);
    expect(wmDebouncer._isRunning).toBeTruthy();

    jest.runAllTimers();

    expect(wmDebouncer._isRunning).toBeFalsy();
    expect(callback).toHaveBeenCalled();
  });

  it('should initialise with callback handler and supplied ms', () => {
    const wmDebouncer = new WMDebouncer();
    const callback = jest.fn();
    const spy = jest.spyOn(window, 'setTimeout');
    wmDebouncer.init(1000, callback);

    expect(wmDebouncer._milliseconds).toEqual(1000);
    expect(wmDebouncer._isRunning).toBeTruthy();

    jest.runAllTimers();

    expect(wmDebouncer._isRunning).toBeFalsy();
    expect(callback).toHaveBeenCalled();
    expect(spy).toHaveBeenCalled();
  });

  it('should stop', () => {
    const wmDebouncer = new WMDebouncer();
    const callback = jest.fn();
    wmDebouncer.init(1000, callback);
    wmDebouncer.stop();
    jest.runAllTimers();
    expect(wmDebouncer._stop).toBeTruthy();
    expect(callback).toHaveBeenCalledTimes(0);
  });
});
