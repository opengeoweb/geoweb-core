/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { WMServiceStore, WMServiceStoreXML2JSONRequest } from './WMGlobals';
import { WMJSService } from './WMJSService';

const WMGetServiceFromStore = (
  serviceName: string,
  xml2jsonrequestURL?: string,
): WMJSService => {
  for (let j = 0; j < WMServiceStore.length; j += 1) {
    if (WMServiceStore[j].service === serviceName) {
      return WMServiceStore[j];
    }
  }
  if (xml2jsonrequestURL) {
    WMServiceStoreXML2JSONRequest.proxy = xml2jsonrequestURL;
  }
  const service = new WMJSService({
    service: serviceName,
    xml2jsonrequestURL: WMServiceStoreXML2JSONRequest.proxy,
  });
  WMServiceStore.push(service);
  return service;
};
export default WMGetServiceFromStore;
