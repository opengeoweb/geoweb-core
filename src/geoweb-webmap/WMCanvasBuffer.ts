/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import WMImage from './WMImage';
import { error, debug } from './WMConstants';
import { isDefined } from './WMJSTools';
import WMImageStore from './WMImageStore';
import WMLayer from './WMLayer';
import WMBBOX from './WMBBOX';
import WMListener from './WMListener';
import { LinkedInfo, LinkedInfoContent, WMPosition } from './types';

type InternalCallback = {
  beforecanvasstartdraw?: (context: CanvasRenderingContext2D) => void;
  beforecanvasdisplay?: (context: CanvasRenderingContext2D) => void;
  canvasonerror?: (e: LinkedInfo[]) => void;
  aftercanvasdisplay?: (c: CanvasRenderingContext2D) => void;
};

export default class WMCanvasBuffer {
  canvas: HTMLCanvasElement;

  _ctx: CanvasRenderingContext2D;

  _imageStore: WMImageStore;

  ready: boolean;

  hidden: boolean;

  layerstodisplay: WMLayer[];

  layers: WMLayer[];

  _defaultImage: WMImage;

  _type: string;

  _currentbbox: WMBBOX;

  _currentnewbbox: WMBBOX;

  _width: number;

  _height: number;

  _webmapJSCallback: WMListener;

  nrLoading: number;

  onLoadReadyFunction: (param) => void;

  _internalCallbacks: InternalCallback;

  mapbbox: WMBBOX;

  srs: string;

  bbox: WMBBOX;

  constructor(
    webmapJSCallback: WMListener,
    _type: string,
    _imageStore: WMImageStore,
    w: number,
    h: number,
    internalCallbacks: InternalCallback,
  ) {
    this.canvas = document.createElement('canvas');
    this.canvas.width = w;
    this.canvas.height = h;
    this._ctx = this.canvas.getContext('2d');
    this._ctx.canvas.width = w;
    this._ctx.canvas.height = h;
    // TODO: Check if this really gives sharper instead of blurier images...
    // this._ctx.webkitImageSmoothingEnabled = true;
    // this._ctx.imageSmoothingQuality igh';
    // this._ctx.msImageSmoothingEnabled = true;
    // this._ctx.imageSmoothingEnabled = tr
    this._imageStore = _imageStore;
    this.ready = true;
    this.hidden = true;
    this.layerstodisplay = [];
    this.layers = [];
    this._defaultImage = new WMImage('webmapjs/img/stoploading.png', () => {
      this._statDivBufferImageLoaded();
    });
    this._currentbbox = undefined;
    this._currentnewbbox = undefined;

    this._width = w;
    this._height = h;
    this._type = _type;
    this._webmapJSCallback = webmapJSCallback;
    this._internalCallbacks = internalCallbacks;

    /* Bind */
    this.getCanvasContext = this.getCanvasContext.bind(this);
    this.imageLoadComplete = this.imageLoadComplete.bind(this);
    this._statDivBufferImageLoaded = this._statDivBufferImageLoaded.bind(this);
    this.hide = this.hide.bind(this);
    this.display = this.display.bind(this);
    this.finishedLoading = this.finishedLoading.bind(this);
    this.resize = this.resize.bind(this);
    this.load = this.load.bind(this);
    this.setSrc = this.setSrc.bind(this);
    this._getPixelCoordFromGeoCoord = this._getPixelCoordFromGeoCoord.bind(
      this,
    );
    this.setBBOX = this.setBBOX.bind(this);
    this.getBuffer = this.getBuffer.bind(this);
  }

  getCanvasContext(): CanvasRenderingContext2D {
    return this._ctx;
  }

  imageLoadComplete(image: WMImage): void {
    this._statDivBufferImageLoaded();
    this._webmapJSCallback.triggerEvent('onimageload', undefined);
    if (this._type === 'imagebuffer') {
      this._webmapJSCallback.triggerEvent('onimagebufferimageload', image);
    }
  }

  _statDivBufferImageLoaded(): void {
    for (let j = 0; j < this.layers.length; j += 1) {
      if (this.layers[j].image.isLoaded() === false) {
        return;
      }
    }
    this.finishedLoading();
  }

  hide(): void {
    this.hidden = true;
    this.canvas.style.display = 'none';
    this.layers.length = 0;
    this.layerstodisplay.length = 0;
  }

  display(newbbox?: WMBBOX, loadedbbox?: WMBBOX): void {
    const errorList = [];
    if (newbbox && !loadedbbox) {
      // eslint-disable-next-line no-console
      console.error(
        'skipping WMCanvasBuffer:display because newbbox is undefined',
      );
      return;
    }

    this.hidden = false;
    this._ctx.globalAlpha = 1;

    if (this._type === 'legendbuffer') {
      this._ctx.clearRect(0, 0, this._width, this._height);
    }
    if (this._type === 'imagebuffer') {
      this._ctx.beginPath();
      this._ctx.rect(0, 0, this._width, this._height);
      this._ctx.fillStyle = 'white';
      this._ctx.fill();
      this._webmapJSCallback.triggerEvent('beforecanvasstartdraw', this._ctx);
      if (
        this._internalCallbacks &&
        this._internalCallbacks.beforecanvasstartdraw
      ) {
        this._internalCallbacks.beforecanvasstartdraw(this._ctx);
      }
    }

    /* Calculcate new pos */
    let coord1;
    let coord2;
    if (newbbox) {
      const b1 = loadedbbox; // this.bbox;
      // if(newbbox == undefined)newbbox=b1;
      const b2 = newbbox;
      coord1 = this._getPixelCoordFromGeoCoord({ x: b1.left, y: b1.top }, b2);
      coord2 = this._getPixelCoordFromGeoCoord(
        { x: b1.right, y: b1.bottom },
        b2,
      );
    }

    let legendPosX = 0;
    for (let j = 0; j < this.layerstodisplay.length; j += 1) {
      if (this.layerstodisplay[j].image.hasError() === false) {
        // Draw
        const op = this.layerstodisplay[j].opacity;
        this._ctx.globalAlpha = op;
        const el = this.layerstodisplay[j].image.getElement();
        if (this._type === 'legendbuffer') {
          const legendW = Math.trunc(el.width) + 4;
          const legendH = Math.trunc(el.height) + 4;
          legendPosX += legendW + 4;
          const legendX = this._width - legendPosX + 2;
          const legendY = this._height - legendH - 2;
          this._ctx.beginPath();
          this._ctx.fillStyle = '#FFFFFF';
          this._ctx.lineWidth = 0.3;
          this._ctx.globalAlpha = 0.5;
          this._ctx.strokeStyle = '#000000';

          this._ctx.rect(legendX + 0.5, legendY + 0.5, legendW, legendH);
          this._ctx.fill();
          this._ctx.stroke();
          this._ctx.globalAlpha = 1.0;
          try {
            this._ctx.drawImage(el, legendX, legendY);
          } catch (e) {
            // eslint-disable-next-line no-console
            console.warn('146', e);
          }
        } else if (newbbox) {
          const imageX = coord1.x + 0.5;
          const imageY = coord1.y + 0.5;
          const imageW = coord2.x - coord1.x + 0.5;
          const imageH = coord2.y - coord1.y + 0.5;

          if (
            imageW === this._ctx.canvas.width &&
            imageH === this._ctx.canvas.height
          ) {
            try {
              this._ctx.drawImage(el, imageX, imageY);
            } catch (e) {
              // eslint-disable-next-line no-console
              console.warn('159', e);
            }
          } else {
            try {
              this._ctx.drawImage(el, imageX, imageY, imageW, imageH);
            } catch (e) {
              // eslint-disable-next-line no-console
              console.warn('165', e);
            }
          }
        } else {
          try {
            this._ctx.drawImage(el, 0, 0, this._width, this._height);
          } catch (e) {
            // eslint-disable-next-line no-console
            console.warn('172', e);
          }
        }
      } else {
        errorList.push(this.layerstodisplay[j]);
      }
    }
    this._ctx.globalAlpha = 1;

    /* Display errors */
    if (this._type === 'imagebuffer') {
      if (errorList.length > 0) {
        this._webmapJSCallback.triggerEvent('canvasonerror', errorList);
        if (this._internalCallbacks && this._internalCallbacks.canvasonerror) {
          this._internalCallbacks.canvasonerror(errorList);
        }
      }
    }
    if (this._type === 'imagebuffer') {
      this._webmapJSCallback.triggerEvent('beforecanvasdisplay', this._ctx);
      if (
        this._internalCallbacks &&
        this._internalCallbacks.beforecanvasdisplay
      ) {
        this._internalCallbacks.beforecanvasdisplay(this._ctx);
      }
    }

    this.canvas.style.display = 'inline-block';
    if (this._type === 'imagebuffer') {
      this._webmapJSCallback.triggerEvent('aftercanvasdisplay', this._ctx);
      if (
        this._internalCallbacks &&
        this._internalCallbacks.aftercanvasdisplay
      ) {
        this._internalCallbacks.aftercanvasdisplay(this._ctx);
      }
    }
  }

  finishedLoading(): void {
    if (this.ready) return;
    this.ready = true;
    this.layerstodisplay.length = 0;
    for (let j = 0; j < this.layers.length; j += 1) {
      this.layerstodisplay.push(this.layers[j]);
      if (this.layers[j].image.hasError()) {
        error(`image error: ${this.layerstodisplay[j].image.getSrc()}`);
      }
    }
    try {
      if (isDefined(this.onLoadReadyFunction)) {
        this.onLoadReadyFunction(this);
      }
    } catch (e) {
      error(`Exception in Divbuffer::finishedLoading: ${e}`);
    }
  }

  resize(w: string, h: string): void {
    const width = parseInt(w, 10);
    const height = parseInt(h, 10);
    if (this._width === width && this._height === height) return;
    this._width = width;
    this._height = height;
    this.canvas.width = width;
    this.canvas.height = height;
    this._ctx.canvas.height = height;
    this._ctx.canvas.width = width;
  }

  load(callback: () => void): void {
    if (this.ready === false) {
      return;
    }
    this.ready = false;
    this.layerstodisplay.length = 0;

    if (callback) {
      this.onLoadReadyFunction = callback;
    } else
      this.onLoadReadyFunction = (): void => {
        /* Do nothing */
      };
    this.nrLoading = 0;

    for (let j = 0; j < this.layers.length; j += 1) {
      this.layers[j].loadThisOne = false;

      if (this.layers[j].image.isLoaded() === false) {
        this.layers[j].loadThisOne = true;
        this.nrLoading += 1;
      }
    }
    if (this.nrLoading === 0) {
      this._statDivBufferImageLoaded();
    } else {
      if (this._type === 'imagebuffer') {
        debug('GetMap:');
      }
      if (this._type === 'legendbuffer') {
        debug('GetLegendGraphic:');
      }
      for (let j = 0; j < this.layers.length; j += 1) {
        if (this.layers[j].loadThisOne === true) {
          debug(
            `<a target='_blank' href='${this.layers[
              j
            ].image.getSrc()}'>${this.layers[j].image.getSrc()}</a>`,
          );
        }
      }

      for (let j = 0; j < this.layers.length; j += 1) {
        if (this.layers[j].loadThisOne === true) {
          this.layers[j].image.load();
        }
      }
    }
  }

  setSrc(
    layerIndex: number,
    imageSource: string,
    width: number,
    height: number,
    linkedInfo: LinkedInfoContent,
    opacity: number,
  ): void {
    if (!isDefined(imageSource)) {
      // eslint-disable-next-line no-console
      console.warn('undefined');
      return;
    }
    while (layerIndex >= this.layers.length) {
      const newLayer = new WMLayer();
      newLayer.image = this._defaultImage;
      newLayer.opacity = opacity;
      newLayer.loadThisOne = false;

      this.layers.push(newLayer);
    }
    const headers =
      linkedInfo && linkedInfo.layer && linkedInfo.layer.headers
        ? linkedInfo.layer.headers
        : [];
    const image = this._imageStore.getImage(imageSource, { headers });

    this.layers[layerIndex].image = image;
  }

  _getPixelCoordFromGeoCoord(coordinates: WMPosition, b: WMBBOX): WMPosition {
    const x = (this._width * (coordinates.x - b.left)) / (b.right - b.left);
    const y = (this._height * (coordinates.y - b.top)) / (b.bottom - b.top);
    return { x, y };
  }

  setBBOX(newbbox: WMBBOX, loadedbbox: WMBBOX): void {
    if (this._currentbbox === loadedbbox && this._currentnewbbox === newbbox)
      return;
    this._currentbbox = loadedbbox;
    this._currentnewbbox = newbbox;
    if (this.hidden === false) {
      this.display(newbbox, loadedbbox);
    }
  }

  getBuffer(): HTMLCanvasElement {
    return this.canvas;
  }
}
