/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { WMLayer, WMBBOX, WMJSMap } from '.';
import { defaultReduxLayerRadarKNMI } from '../utils/defaultTestSettings';
import { detectLeftButton, detectRightButton } from './WMJSMap';
import * as WMJSTools from './WMJSTools';

describe('src/geoweb-webmap/WMJSMap', () => {
  it('should initialise with correct elements', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);

    // load layer
    expect(
      map.loadingDiv.className.includes('WMJSDivBuffer-loading'),
    ).toBeTruthy();
    expect(baseElement.innerHTML).toContain('class="WMJSDivBuffer-loading"');
    // main div
    expect(baseElement.innerHTML).toContain(`id="${map.baseDiv.id}"`);
    // zoombox
    expect(baseElement.innerHTML).toContain('class="wmjs-zoombox"');
    // boundingbox
    expect(baseElement.innerHTML).toContain('class="wmjs-boundingbox"');
  });

  it('should attach events', () => {
    const mockBaseElement = document.createElement('div');
    mockBaseElement.addEventListener = jest.fn();
    mockBaseElement.appendChild = jest.fn();

    const map = new WMJSMap(mockBaseElement);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    map.baseDiv = mockBaseElement;

    map.attachEvents();

    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'mousedown',
      map.mouseDownEvent,
    );
    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'mouseup',
      map.mouseUpEvent,
    );
    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'mousemove',
      map.mouseMoveEvent,
    );
    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'contextmenu',
      map.onContextMenu,
    );
    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'wheel',
      map.mouseWheelEvent,
    );
  });

  it('should deattach events', () => {
    const mockBaseElement = document.createElement('div');
    mockBaseElement.removeEventListener = jest.fn();
    mockBaseElement.appendChild = jest.fn();

    const map = new WMJSMap(mockBaseElement);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    map.baseDiv = mockBaseElement;

    map.detachEvents();

    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'mousedown',
      map.mouseDownEvent,
    );
    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'mouseup',
      map.mouseUpEvent,
    );
    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'mousemove',
      map.mouseMoveEvent,
    );
    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'contextmenu',
      map.onContextMenu,
    );
    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'wheel',
      map.mouseWheelEvent,
    );
  });

  it('should set correct cursors when start panning map', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);

    map._mapPanStart(0, 0);

    expect(getComputedStyle(map.baseDiv).cursor).toEqual('move');
  });

  it('should set correct cursors when end panning map', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);

    map._mapPanEnd(0, 0);

    expect(getComputedStyle(map.baseDiv).cursor).toEqual('default');
  });

  it('should set correct cursors when start zooming map', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);
    map.mapZooming = 1;

    map._mapZoom();

    expect(getComputedStyle(map.baseDiv).cursor).toEqual('crosshair');

    // not allowed
    map.mouseX = 0;
    map.mouseDownX = 1;
    map.mouseY = 0;
    map.mouseDownY = 1;

    map._mapZoom();
    expect(getComputedStyle(map.baseDiv).cursor).toEqual('not-allowed');
  });

  it('should set correct cursors when end zooming map', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);
    map.mapZooming = 1;

    map._mapZoomEnd();

    expect(getComputedStyle(map.baseDiv).cursor).toEqual('default');
  });

  it('should get correct coordinates for document', () => {
    const baseElement = document.createElement('div');
    const parentElement = document.createElement('div');
    parentElement.appendChild(baseElement);
    const map = new WMJSMap(baseElement);

    expect(map.getMouseCoordinatesForDocument(new MouseEvent('click'))).toEqual(
      {
        x: 0,
        y: 0,
      },
    );

    expect(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      map.getMouseCoordinatesForDocument({ pageX: 10, pageY: 10 }),
    ).toEqual({
      x: 10,
      y: 10,
    });

    expect(
      map.getMouseCoordinatesForDocument(
        new TouchEvent('touchstart', {
          changedTouches: [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            {
              screenX: 0,
              screenY: 0,
            },
          ],
        }),
      ),
    ).toEqual({
      x: 0,
      y: 0,
    });

    expect(
      map.getMouseCoordinatesForDocument(
        new TouchEvent('touchstart', {
          changedTouches: [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            { screenX: 10, screenY: 10 },
          ],
        }),
      ),
    ).toEqual({
      x: 10,
      y: 10,
    });

    // mock offset
    Object.assign(parentElement, {
      ...parentElement,
      getBoundingClientRect: () => ({
        left: 100,
        top: 100,
      }),
    });

    expect(map.getMouseCoordinatesForDocument(new MouseEvent('click'))).toEqual(
      {
        x: -100,
        y: -100,
      },
    );

    const getMouseXCoordinateSpy = jest.spyOn(WMJSTools, 'getMouseXCoordinate');
    const getMouseYCoordinateSpy = jest.spyOn(WMJSTools, 'getMouseYCoordinate');
    const testEvent = new MouseEvent('click');
    map.getMouseCoordinatesForDocument(testEvent);
    expect(getMouseXCoordinateSpy).toHaveBeenCalledWith(testEvent);
    expect(getMouseYCoordinateSpy).toHaveBeenCalledWith(testEvent);
  });

  it('should get correct coordinates for element', () => {
    const baseElement = document.createElement('div');
    const parentElement = document.createElement('div');
    parentElement.appendChild(baseElement);

    const map = new WMJSMap(baseElement);

    expect(map.getMouseCoordinatesForElement(new MouseEvent('click'))).toEqual({
      x: 0,
      y: 0,
    });

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(map.getMouseCoordinatesForElement({ pageX: 10, pageY: 10 })).toEqual(
      {
        x: 10,
        y: 10,
      },
    );

    expect(
      map.getMouseCoordinatesForElement(
        new TouchEvent('touchstart', {
          changedTouches: [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            {
              screenX: 0,
              screenY: 0,
            },
          ],
        }),
      ),
    ).toEqual({
      x: 0,
      y: 0,
    });

    expect(
      map.getMouseCoordinatesForElement(
        new TouchEvent('touchstart', {
          changedTouches: [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            {
              screenX: 10,
              screenY: 10,
            },
          ],
        }),
      ),
    ).toEqual({
      x: 10,
      y: 10,
    });

    // mock offset
    Object.assign(parentElement, {
      ...parentElement,
      getBoundingClientRect: () => ({
        left: 100,
        top: 100,
      }),
    });
    expect(map.getMouseCoordinatesForElement(new MouseEvent('click'))).toEqual({
      x: -100,
      y: -100,
    });

    const getMouseXCoordinateSpy = jest.spyOn(WMJSTools, 'getMouseXCoordinate');
    const getMouseYCoordinateSpy = jest.spyOn(WMJSTools, 'getMouseYCoordinate');
    const testEvent = new MouseEvent('click');
    map.getMouseCoordinatesForElement(testEvent);
    expect(getMouseXCoordinateSpy).toHaveBeenCalledWith(testEvent);
    expect(getMouseYCoordinateSpy).toHaveBeenCalledWith(testEvent);
  });

  it('should handle layers', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);
    const testLayer1 = new WMLayer({
      ...defaultReduxLayerRadarKNMI,
      id: 'wmjsmap-testlayer-1',
      enabled: true,
    });

    const testLayer2 = new WMLayer({
      ...defaultReduxLayerRadarKNMI,
      id: 'wmjsmap-testlayer-2',
      enabled: true,
    });

    map.addLayer(testLayer1);
    map.addLayer(testLayer2);
    expect(map.getLayers()).toEqual([testLayer2, testLayer1]);

    map.swapLayers(testLayer1, testLayer2);
    expect(map.getLayers()).toEqual([testLayer1, testLayer2]);

    map.removeAllLayers();
    expect(map.getLayers()).toEqual([]);
  });

  it('should set xml2jsonrequestURL', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);
    const xml2jsonrequestURL = 'teststring';
    map.setXML2JSONURL(xml2jsonrequestURL);
    expect(map.xml2jsonrequest).toEqual(xml2jsonrequestURL);
  });

  it('should set projection', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);
    const srs = 'EPSG:4326';
    const bbox = { left: -180, bottom: -90, right: 180, top: 90 };
    const wmbbox = new WMBBOX(bbox.left, bbox.bottom, bbox.right, bbox.top);

    map.setProjection(srs, wmbbox);

    expect(map.getProjection()).toEqual({
      srs,
      bbox: expect.objectContaining(bbox),
    });
  });

  it('should set size when passing numbers', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);

    const width = 234;
    const height = 567;

    map.setSize(width, height);

    expect(map.getWidth()).toEqual(width);
    expect(map.getHeight()).toEqual(height);
  });

  it('should set size when passing strings', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);

    const width = 234;
    const height = 567;

    map.setSize(width, height);

    expect(map.getWidth()).toEqual(width);
    expect(map.getHeight()).toEqual(height);
  });

  it('should detect left and right button clicks', () => {
    const mockEventClickLeft = new MouseEvent('click', { button: 0 });
    const mockEventClickLeft2 = new MouseEvent('click', { buttons: 1 });
    const mockEventClickMiddle = new MouseEvent('click', { button: 1 });
    const mockEventClickMiddle2 = new MouseEvent('click', { buttons: 4 });
    const mockEventClickRight = new MouseEvent('click', { button: 2 });
    const mockEventClickRight2 = new MouseEvent('click', { buttons: 2 });

    expect(detectLeftButton(mockEventClickLeft)).toBeTruthy();
    expect(detectLeftButton(mockEventClickLeft2)).toBeTruthy();
    expect(detectLeftButton(mockEventClickMiddle)).toBeFalsy();
    expect(detectLeftButton(mockEventClickMiddle2)).toBeFalsy();
    expect(detectLeftButton(mockEventClickRight)).toBeFalsy();
    expect(detectLeftButton(mockEventClickRight2)).toBeFalsy();

    expect(detectRightButton(mockEventClickLeft)).toBeFalsy();
    expect(detectRightButton(mockEventClickLeft2)).toBeFalsy();
    expect(detectRightButton(mockEventClickMiddle)).toBeFalsy();
    expect(detectRightButton(mockEventClickMiddle2)).toBeFalsy();
    expect(detectRightButton(mockEventClickRight)).toBeTruthy();
    expect(detectRightButton(mockEventClickRight2)).toBeTruthy();
  });
});
