/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { ParseISOTimeRangeDuration } from '.';
import {
  CustomDate,
  DateInterval,
  getCurrentDateIso8601,
  getNumberOfTimeSteps,
  parseISO8601DateToDate,
  parseISO8601IntervalToDateInterval,
} from './WMTime';

describe('src/geoweb-webmap/WMTime', () => {
  describe('DateInterval', () => {
    it('should create class with default values', () => {
      const dateInterval = new DateInterval('0', '0', '0', '0', '0', '0');
      expect(dateInterval.year).toEqual(0);
      expect(dateInterval.month).toEqual(0);
      expect(dateInterval.day).toEqual(0);
      expect(dateInterval.hour).toEqual(0);
      expect(dateInterval.minute).toEqual(0);
      expect(dateInterval.second).toEqual(0);
      expect(dateInterval.isRegularInterval).toBeTruthy();
    });

    it('should return time', () => {
      expect(new DateInterval('0', '0', '1', '1', '1', '1').getTime()).toEqual(
        90061000,
      );
      expect(() =>
        new DateInterval('1', '0', '1', '1', '1', '1').getTime(),
      ).toThrowError(new Error('year !== 0'));
      expect(() =>
        new DateInterval('0', '1', '1', '1', '1', '1').getTime(),
      ).toThrowError(new Error('month !== 0'));
    });

    it('should return toISO8601', () => {
      expect(
        new DateInterval('0', '0', '0', '0', '0', '0').toISO8601(),
      ).toEqual('P');
      expect(
        new DateInterval('1', '0', '0', '0', '0', '0').toISO8601(),
      ).toEqual('P1Y');
      expect(
        new DateInterval('1', '1', '0', '0', '0', '0').toISO8601(),
      ).toEqual('P1Y1M');
      expect(
        new DateInterval('1', '1', '1', '0', '0', '0').toISO8601(),
      ).toEqual('P1Y1M1D');
      expect(
        new DateInterval('1', '1', '1', '1', '0', '0').toISO8601(),
      ).toEqual('P1Y1M1D1H');
      expect(
        new DateInterval('1', '1', '1', '1', '1', '0').toISO8601(),
      ).toEqual('P1Y1M1D1H1M');
      expect(
        new DateInterval('1', '1', '1', '1', '1', '1').toISO8601(),
      ).toEqual('P1Y1M1DT1H1M1S');
    });
  });

  describe('parseISO8601IntervalToDateInterval', () => {
    it('should parse isotime to date interval', () => {
      const isoTime = parseISO8601IntervalToDateInterval('P1Y1M1DT1H1M1S');
      expect(isoTime.year).toEqual(1);
      expect(isoTime.day).toEqual(1);
      expect(isoTime.month).toEqual(1);
      expect(isoTime.hour).toEqual(1);
      expect(isoTime.minute).toEqual(1);
      expect(isoTime.second).toEqual(1);
    });
    it('should parse isotime to date interval', () => {
      const isoTime = parseISO8601IntervalToDateInterval(
        'P10Y10M10DT10H10M10S',
      );
      expect(isoTime.year).toEqual(10);
      expect(isoTime.day).toEqual(10);
      expect(isoTime.month).toEqual(10);
      expect(isoTime.hour).toEqual(10);
      expect(isoTime.minute).toEqual(10);
      expect(isoTime.second).toEqual(10);
    });
    it('should return undefined if not valid', () => {
      expect(
        parseISO8601IntervalToDateInterval('10Y10M10DT10H10M10S'),
      ).toBeUndefined();
      expect(parseISO8601IntervalToDateInterval(null)).toBeUndefined();
    });
  });

  describe('ParseISOTimeRangeDuration', () => {
    it('should parse time duration', () => {
      const isoTimeDuration = '2021-03-23T00:00:00Z/2021-03-24T00:00:00Z/PT1H';
      const result = new ParseISOTimeRangeDuration(isoTimeDuration);

      expect(result.startTime).toEqual(
        parseISO8601DateToDate(isoTimeDuration.split('/')[0]),
      );
      expect(result.stopTime).toEqual(
        parseISO8601DateToDate(isoTimeDuration.split('/')[1]),
      );

      expect(result.timeSteps).toEqual(
        getNumberOfTimeSteps(
          result.startTime,
          result.stopTime,
          result.timeInterval,
        ),
      );
    });

    it('should return correct timesteps', () => {
      const isoTimeDuration = '2021-03-23T00:00:00Z/2021-03-24T00:00:00Z/PT1H';
      const result = new ParseISOTimeRangeDuration(isoTimeDuration);

      expect(result.getTimeSteps()).toEqual(result.timeSteps);

      expect(() =>
        result.getTimeStepFromDate(
          new Date('2020-03-23T00:00:00Z') as CustomDate,
          true,
        ),
      ).toThrow(new Error('0'));

      expect(result.getTimeStepFromDateWithinRange(result.startTime)).toEqual(
        0,
      );
      expect(result.getTimeStepFromDateWithinRange(result.stopTime)).toEqual(
        24,
      );

      expect(result.getTimeStepFromDateClipped(result.startTime)).toEqual(0);
      expect(result.getTimeStepFromDateClipped(result.stopTime)).toEqual(24);
    });
  });

  describe('parseISO8601DateToDate', () => {
    it('should return undefined if not valid', () => {
      expect(parseISO8601DateToDate(null)).toBeUndefined();
      expect(parseISO8601DateToDate('2021-04-18')).toBeUndefined();
    });
    it('should add the given dateinterval', () => {
      const isotime = '2021-04-18T22:12:54Z';
      const result = parseISO8601DateToDate(isotime);
      expect(result.toISO8601()).toEqual(isotime);
      result.add(null);
      expect(result.toISO8601()).toEqual(isotime);
      result.add(new DateInterval('0', '0', '0', '0', '0', '0'));
      expect(result.toISO8601()).toEqual(isotime);
      result.add(new DateInterval('1', '1', '1', '1', '1', '1'));
      expect(result.toISO8601()).toEqual('2022-05-19T23:13:55Z');
    });
    it('should substract the given dateinterval', () => {
      const isotime = '2021-04-18T22:12:54Z';
      const result = parseISO8601DateToDate(isotime);
      expect(result.toISO8601()).toEqual(isotime);
      result.substract(null);
      expect(result.toISO8601()).toEqual(isotime);
      result.substract(new DateInterval('0', '0', '0', '0', '0', '0'));
      expect(result.toISO8601()).toEqual(isotime);
      result.substract(new DateInterval('1', '1', '1', '1', '1', '1'));
      expect(result.toISO8601()).toEqual('2020-03-17T21:11:53Z');
    });
    it('should add the given dateinterval multiple times', () => {
      const isotime = '2021-04-18T22:12:54Z';
      const result = parseISO8601DateToDate(isotime);
      expect(result.toISO8601()).toEqual(isotime);
      result.addMultipleTimes(null, 1);
      expect(result.toISO8601()).toEqual(isotime);
      result.addMultipleTimes(
        new DateInterval('0', '0', '0', '0', '0', '0'),
        2,
      );
      expect(result.toISO8601()).toEqual(isotime);
      result.addMultipleTimes(
        new DateInterval('1', '1', '1', '1', '1', '1'),
        2,
      );
      expect(result.toISO8601()).toEqual('2023-06-21T00:14:56Z');
      result.addMultipleTimes(
        new DateInterval('0', '0', '0', '0', '0', '5'),
        5,
      );
      expect(result.toISO8601()).toEqual('2023-06-21T00:15:21Z');
    });
    it('should substract the given dateinterval multiple times', () => {
      const isotime = '2021-04-18T22:12:54Z';
      const result = parseISO8601DateToDate(isotime);
      expect(result.toISO8601()).toEqual(isotime);
      result.substractMultipleTimes(null, 1);
      expect(result.toISO8601()).toEqual(isotime);
      result.substractMultipleTimes(
        new DateInterval('0', '0', '0', '0', '0', '0'),
        2,
      );
      expect(result.toISO8601()).toEqual(isotime);
      result.substractMultipleTimes(
        new DateInterval('1', '1', '1', '1', '1', '1'),
        2,
      );
      expect(result.toISO8601()).toEqual('2019-02-16T20:10:52Z');
      result.substractMultipleTimes(
        new DateInterval('0', '0', '0', '0', '0', '5'),
        5,
      );
      expect(result.toISO8601()).toEqual('2019-02-16T20:10:27Z');
    });
  });

  describe('getNumberOfTimeSteps', () => {
    it('should return undefined if not valid', () => {
      const dummytime = new Date('2020-03-23T00:00:00Z') as CustomDate;
      const dummyinterval = new DateInterval('0', '0', '0', '0', '0', '0');
      expect(getNumberOfTimeSteps(null, null, null)).toBeUndefined();
      expect(getNumberOfTimeSteps(dummytime, null, null)).toBeUndefined();
      expect(
        getNumberOfTimeSteps(dummytime, null, dummyinterval),
      ).toBeUndefined();
    });
    it('should return unequally distributed time steps', () => {
      const starttime = parseISO8601DateToDate('2020-03-23T00:00:00Z');
      const endtime = parseISO8601DateToDate('2021-03-24T00:00:00Z');
      const interval = new DateInterval('0', '2', '0', '0', '0', '0');
      expect(getNumberOfTimeSteps(starttime, endtime, interval)).toEqual(8);
    });
    it('should return equally distributed time steps', () => {
      const starttime = parseISO8601DateToDate('2020-03-23T00:00:00Z');
      const endtime = parseISO8601DateToDate('2021-03-24T00:00:00Z');
      const interval = new DateInterval('0', '0', '1', '0', '0', '0');
      expect(getNumberOfTimeSteps(starttime, endtime, interval)).toEqual(367);
    });
  });
  describe('getCurrentDateIso8601', () => {
    it('should return current date with milliseconds set to zero', () => {
      const now = new Date();
      jest.spyOn(Date, 'now').mockReturnValue(now.valueOf());
      now.setMilliseconds(0);
      expect(getCurrentDateIso8601()).toEqual(now);
    });
  });
});
