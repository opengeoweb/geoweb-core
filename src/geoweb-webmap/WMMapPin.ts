/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { debug } from './WMJSTools';
import WMDrawMarker from './WMDrawMarker';
import WMBBOX from './WMBBOX';

class MapPin {
  private _map = null;

  private x: number = null;

  private y: number = null;

  private exactX: number = null;

  private exactY: number = null;

  private geoPosX: number = null;

  private geoPosY: number = null;

  public displayMapPin = true;

  // TODO: find way without circular dependency https://gitlab.com/opengeoweb/geoweb-core/-/issues/352
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  constructor(_map) {
    this._map = _map;
  }

  public redrawMap = (): void => {
    this._map.draw();
  };

  public drawMarker(ctx: CanvasRenderingContext2D): void {
    WMDrawMarker(ctx, this.x, this.y, '#9090FF', '#000');
  }

  public repositionMapPin = (_bbox?: WMBBOX): void => {
    if (!_bbox) return;
    const newpos = this._map.getPixelCoordFromGeoCoord(
      { x: this.geoPosX, y: this.geoPosY },
      _bbox,
    );
    this.setMapPin(newpos.x, newpos.y, _bbox);
  };

  public setMapPin = (_x: number, _y: number, _bbox?: WMBBOX): void => {
    const x = (_x as unknown) as string;
    const y = (_y as unknown) as string;

    if (!x || !y) return;
    this.x = parseInt(x, 10);
    this.y = parseInt(y, 10);
    this.exactX = parseFloat(x);
    this.exactY = parseFloat(y);

    const geopos = this._map.getGeoCoordFromPixelCoord(
      { x: this.exactX, y: this.exactY },
      _bbox,
    );

    this.geoPosX = geopos.x;
    this.geoPosY = geopos.y;
    this.redrawMap();
  };

  public positionMapPinByLatLon = (coord: { x: number; y: number }): void => {
    debug(`positionMapPinByLatLon at ${coord.x},${coord.y}`);
    const newpos = this._map.getPixelCoordFromLatLong(coord);
    this.setMapPin(newpos.x, newpos.y);
  };

  public showMapPin = (): void => {
    this.displayMapPin = true;
    this.redrawMap();
  };

  public hideMapPin = (): void => {
    this.displayMapPin = false;
    this.redrawMap();
  };
}

export default MapPin;
