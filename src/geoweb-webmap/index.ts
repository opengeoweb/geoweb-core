/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import WMJSMap from './WMJSMap';
import {
  isDefined,
  WMJScheckURL,
  URLDecode,
  URLEncode,
  toArray,
} from './WMJSTools';
import WMLayer from './WMLayer';
import WMTimer from './WMTimer';
import WMGetServiceFromStore from './WMGetServiceFromStore';
import {
  WMDateOutSideRange,
  WMDateTooEarlyString,
  WMDateTooLateString,
  WMEmptyLayerName,
  WMEmptyLayerTitle,
} from './WMConstants';
import {
  parseISO8601DateToDate,
  DateInterval,
  parseISO8601IntervalToDateInterval,
  ParseISOTimeRangeDuration,
} from './WMTime';
import WMBBOX from './WMBBOX';
import I18n from './I18n/lang.en';
import WMImage from './WMImage';

import { getLegendGraphicURLForLayer, legendImageStore } from './WMLegend';

export {
  WMJSMap,
  DateInterval,
  parseISO8601IntervalToDateInterval,
  ParseISOTimeRangeDuration,
  isDefined,
  WMLayer,
  WMTimer,
  WMGetServiceFromStore,
  WMJScheckURL,
  URLEncode,
  URLDecode,
  WMDateOutSideRange,
  WMDateTooEarlyString,
  WMDateTooLateString,
  WMEmptyLayerName,
  WMEmptyLayerTitle,
  parseISO8601DateToDate,
  I18n,
  WMBBOX,
  toArray,
  getLegendGraphicURLForLayer,
  legendImageStore,
  WMImage,
};
