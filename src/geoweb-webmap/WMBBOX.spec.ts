/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import WMBBOX from './WMBBOX';

// Mock data
const left = -20;
const bottom = -120;
const right = 20;
const top = 80;

describe('src/geoweb-webmap/WMBBOX', () => {
  it('should create a BBOX with the provided data', () => {
    const box1 = new WMBBOX(left, bottom, right, top);
    expect(box1.left).toEqual(left);
    expect(box1.bottom).toEqual(bottom);
    expect(box1.right).toEqual(right);
    expect(box1.top).toEqual(top);

    const box2 = new WMBBOX(box1, undefined, undefined, undefined);
    expect(box2.left).toEqual(box1.left);
    expect(box2.bottom).toEqual(box1.bottom);
    expect(box2.right).toEqual(box1.right);
    expect(box2.top).toEqual(box1.top);

    const box3 = new WMBBOX('100,50,200,140', undefined, undefined, undefined);
    expect(box3.left).toEqual(100);
    expect(box3.bottom).toEqual(50);
    expect(box3.right).toEqual(200);
    expect(box3.top).toEqual(140);
  });
  it('should create a BBOX with default values if faulty params provided', () => {
    const box1 = new WMBBOX(left, bottom, undefined, top);
    expect(box1.left).toEqual(left);
    expect(box1.bottom).toEqual(bottom);
    expect(box1.right).toEqual(180);
    expect(box1.top).toEqual(top);
    const box2 = new WMBBOX(undefined, bottom, right, top);
    expect(box2.left).toEqual(-180);
    expect(box2.bottom).toEqual(bottom);
    expect(box2.right).toEqual(right);
    expect(box2.top).toEqual(top);
    // Also add params out of bounds
  });
  it('should clone a BBOX', () => {
    const box1 = new WMBBOX(left, bottom, right, top);
    const box2 = box1.clone();
    expect(box2.left).toEqual(box1.left);
    expect(box2.bottom).toEqual(box1.bottom);
    expect(box2.right).toEqual(box1.right);
    expect(box2.top).toEqual(box1.top);
  });
  it('should compare two BBOXes', () => {
    const box1 = new WMBBOX(left, bottom, right, top);
    expect(box1.equals(left, bottom, right, top)).toBeTruthy();
    expect(box1.equals(left + 1, bottom, right, top)).toBeFalsy();
  });
});
