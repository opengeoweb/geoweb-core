/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import I18n from './I18n/lang.en';
import { isDefined, isNull, toArray, WMJScheckURL } from './WMJSTools';
import { WMEmptyLayerTitle, WMSVersion, error, debug } from './WMConstants';
import WMJSDimension from './WMJSDimension';
import WMProjection from './WMProjection';
import WMBBOX from './WMBBOX';
import WMGetServiceFromStore from './WMGetServiceFromStore';
import WMImage from './WMImage';
import { LayerType } from '../store/mapStore/layers/types';
import { Capability, GetCapabilitiesJson, WMJSService } from './WMJSService';
import { WMJSMap, Style, Dimension } from '../store/mapStore/map/types';

interface WMLayerv111 {
  Dimension: WMJSDimension;
  Extent: Record<string, unknown>[];
}

interface LegendResource {
  OnlineResource: { attr: { [name: string]: string } };
}

interface ExtendedStyle extends Style {
  index: number;
  nrOfStyles: number;
  Name: { value: string };
  Title: { value: string };
  Abstract?: { value: string };
  LegendURL: LegendResource;
}

interface LayerOptions {
  id: string;
  name?: string;
  layerType: LayerType;
  service: string;
  active?: boolean;
  format?: string;
  getgraphinfoURL?: string;
  style?: string;
  currentStyle?: string;
  sldURL?: string;
  opacity?: number;
  title?: string;
  enabled?: boolean;
  keepOnTop?: boolean;
  transparent?: boolean;
  onReady?: (param) => void;
  type?: string;
  parentMaps?: WMJSMap[];
  dimensions?: Dimension[];
  headers?: Headers[];
  failure?: (layer: WMLayer, message: string) => void;
}

export default class WMLayer {
  id: string;

  name?: string;

  ReactWMJSLayerId?: string;

  reactWebMapJSLayer?: React.ReactElement;

  hasError?: boolean;

  public enabled?: boolean;

  lastError?: string;

  legendGraphic?: string;

  title?: string;

  public opacity?: number;

  service: string;

  layerType: LayerType;

  currentStyle?: string;

  image?: WMImage;

  loadThisOne?: boolean;

  linkedInfo: { layer: string; message: string };

  headers: Headers[];

  autoupdate: boolean;

  timer: NodeJS.Timeout;

  WMJSService: WMJSService;

  getmapURL: string;

  getfeatureinfoURL: string;

  getlegendgraphicURL: string;

  keepOnTop: boolean;

  transparent: boolean;

  legendIsDimensionDependent: boolean;

  wms130bboxcompatibilitymode: boolean;

  version: string;

  path: string;

  type: string;

  objectpath: { Dimension: WMJSDimension }[];

  jsonlayer_v1_1_1: WMLayerv111;

  abstract: string;

  dimensions: WMJSDimension[];

  projectionProperties: WMProjection[];

  queryable: boolean;

  styles: ExtendedStyle[];

  getCapabilitiesDoc: GetCapabilitiesJson;

  serviceTitle: string;

  parentMaps: WMJSMap[];

  sldURL: string;

  active: boolean;

  getgraphinfoURL: string;

  format: string;

  _options: LayerOptions;

  onReady: (param) => void;

  optimalFormat: string;

  wmsextensions: { url: string; colorscalerange: Record<string, unknown>[] };

  jsonlayer: this;

  isConfigured: boolean;

  init(): void {
    this.autoupdate = false;
    this.timer = undefined;
    this.service = undefined; // URL of the WMS Service
    this.WMJSService = undefined; // Corresponding WMJSService
    this.getmapURL = undefined;
    this.getfeatureinfoURL = undefined;
    this.getlegendgraphicURL = undefined;
    this.keepOnTop = false;
    this.transparent = true;
    this.hasError = false;
    this.legendIsDimensionDependent = true;
    this.wms130bboxcompatibilitymode = false;
    this.version = WMSVersion.version111;
    this.path = '';
    this.type = 'wms';
    this.objectpath = [];
    this.wmsextensions = { url: '', colorscalerange: [] };
    this.jsonlayer_v1_1_1 = undefined; // JSON data for this layer from getcapabilities XML file.
    this.name = undefined;
    this.title = WMEmptyLayerTitle;
    this.abstract = undefined;
    this.dimensions = []; // Array of Dimension
    this.legendGraphic = '';
    this.projectionProperties = []; // Array of WMProjections
    this.queryable = false;
    this.enabled = true;
    this.styles = [];
    this.currentStyle = '';
    this.id = '-1';
    this.opacity = 1.0; // Ranges from 0.0-1.0
    this.getCapabilitiesDoc = undefined;
    this.serviceTitle = 'not defined';
    this.parentMaps = [];
    this.sldURL = null;
    this.isConfigured = false;
  }

  constructor(options?: LayerOptions) {
    this.init = this.init.bind(this);
    this.getLayerName = this.getLayerName.bind(this);
    this.toggleAutoUpdate = this.toggleAutoUpdate.bind(this);
    this.setAutoUpdate = this.setAutoUpdate.bind(this);
    this.setOpacity = this.setOpacity.bind(this);
    this.getOpacity = this.getOpacity.bind(this);
    this.remove = this.remove.bind(this);
    this.moveUp = this.moveUp.bind(this);
    this.moveDown = this.moveDown.bind(this);
    this.zoomToLayer = this.zoomToLayer.bind(this);
    this.draw = this.draw.bind(this);
    this.handleReferenceTime = this.handleReferenceTime.bind(this);
    this.setDimension = this.setDimension.bind(this);
    this.configureDimensions = this.configureDimensions.bind(this);
    this.parseLayer = this.parseLayer.bind(this);
    this.cloneLayer = this.cloneLayer.bind(this);
    this.setName = this.setName.bind(this);
    this.getLayerRelative = this.getLayerRelative.bind(this);
    this.autoSelectLayer = this.autoSelectLayer.bind(this);
    this.getNextLayer = this.getNextLayer.bind(this);
    this.getPreviousLayer = this.getPreviousLayer.bind(this);
    this.setStyle = this.setStyle.bind(this);
    this.getStyles = this.getStyles.bind(this);
    this.getStyleObject = this.getStyleObject.bind(this);
    this.getStyle = this.getStyle.bind(this);
    this.getDimension = this.getDimension.bind(this);
    this.getProjection = this.getProjection.bind(this);
    this.setSLDURL = this.setSLDURL.bind(this);
    this.display = this.display.bind(this);
    this.getDimensions = this.getDimensions.bind(this);
    this.init();
    this._options = options;
    this.sldURL = null;
    this.headers = [];
    if (options) {
      this.service = options.service;
      this.getmapURL = options.service;
      this.getfeatureinfoURL = options.service;
      this.getlegendgraphicURL = options.service;
      if (options.active === true) this.active = true;
      else this.active = false;
      this.name = options.name;
      if (options.getgraphinfoURL)
        this.getgraphinfoURL = options.getgraphinfoURL;
      if (options.style) {
        this.currentStyle = options.style;
      }
      if (options.currentStyle) {
        this.currentStyle = options.currentStyle;
      }
      if (options.sldURL) {
        this.sldURL = options.sldURL;
      }
      if (options.id) {
        this.id = options.id;
      }
      if (options.format) this.format = options.format;
      else this.format = 'image/png';
      if (isDefined(options.opacity)) {
        this.opacity = options.opacity;
      }
      if (options.title) this.title = options.title;
      this.abstract = I18n.not_available_message.text;

      if (options.enabled === false) this.enabled = false;

      if (options.keepOnTop === true) this.keepOnTop = true;

      if (options.transparent === false) {
        this.transparent = false;
      }
      if (isDefined(options.onReady)) {
        this.onReady = options.onReady;
        this.parseLayer(undefined, undefined, undefined);
      }
      if (isDefined(options.type)) {
        this.type = options.type;
      }
      if (options.parentMaps) {
        this.parentMaps = options.parentMaps;
      }
      if (options.dimensions && options.dimensions.length) {
        for (let d = 0; d < options.dimensions.length; d += 1) {
          this.dimensions.push(new WMJSDimension(options.dimensions[d]));
        }
      }
      if (options.headers) {
        this.headers = options.headers;
      }
    }
  }

  getLayerName(): string {
    return this.name;
  }

  toggleAutoUpdate(): void {
    this.autoupdate = !this.autoupdate;
    if (this.autoupdate) {
      const numDeltaMS = 60000;
      this.timer = setInterval(() => {
        this.parseLayer(undefined, true, undefined);
      }, numDeltaMS);
    } else {
      clearInterval(this.timer);
    }
  }

  setAutoUpdate(val: boolean, interval?: number, callback?: () => void): void {
    if (val !== this.autoupdate) {
      this.autoupdate = val;
      if (!val) {
        clearInterval(this.timer);
      } else {
        this.timer = setInterval(() => {
          this.parseLayer(callback, true, undefined);
        }, interval);
      }
    }
  }

  setOpacity(opacityValue: string): void {
    this.opacity = parseFloat(opacityValue);
    if (this.parentMaps.length === 0) {
      // eslint-disable-next-line no-console
      console.warn('Layer has no parent maps');
    }
    for (let j = 0; j < this.parentMaps.length; j += 1) {
      this.parentMaps[j].redrawBuffer();
    }
  }

  getOpacity(): number {
    return this.opacity;
  }

  remove(): void {
    for (let j = 0; j < this.parentMaps.length; j += 1) {
      this.parentMaps[j].deleteLayer(this);
      this.parentMaps[j].draw('WMLayer::remove');
    }
    clearInterval(this.timer);
  }

  moveUp(): void {
    for (let j = 0; j < this.parentMaps.length; j += 1) {
      this.parentMaps[j].moveLayerUp(this);
      this.parentMaps[j].draw('WMLayer::moveUp');
    }
  }

  moveDown(): void {
    for (let j = 0; j < this.parentMaps.length; j += 1) {
      this.parentMaps[j].moveLayerDown(this);
      this.parentMaps[j].draw('WMLayer::moveDown');
    }
  }

  zoomToLayer(): void {
    for (let j = 0; j < this.parentMaps.length; j += 1) {
      this.parentMaps[j].zoomToLayer(this);
    }
  }

  draw(e: string): void {
    for (let j = 0; j < this.parentMaps.length; j += 1) {
      this.parentMaps[j].draw(`WMLayer::draw::${e}`);
    }
  }

  handleReferenceTime(
    name: string,
    value: string,
    updateMapDimensions = true,
  ): void {
    if (name === 'reference_time') {
      const timeDim = this.getDimension('time');
      const referenceTimeDim = this.getDimension(name);
      if (timeDim) {
        timeDim.setTimeValuesForReferenceTime(value, referenceTimeDim);
        if (updateMapDimensions) {
          if (this.parentMaps && this.parentMaps.length > 0) {
            this.parentMaps[0]
              .getListener()
              .triggerEvent('ondimchange', 'time');
          }
        }
      }
    }
  }

  getDimensions(): WMJSDimension[] {
    return this.dimensions;
  }

  setDimension(name: string, value: string, updateMapDimensions = true): void {
    if (!isDefined(value)) return;

    const dimIndex = this.dimensions.findIndex(dim => dim.name === name);
    if (dimIndex < 0) return;

    const dim = this.dimensions[dimIndex];

    dim.setValue(value);

    this.handleReferenceTime(name, value, updateMapDimensions);
    if (updateMapDimensions) {
      if (dim.linked === true) {
        for (let j = 0; j < this.parentMaps.length; j += 1) {
          this.parentMaps[j].setDimension(name, dim.getValue());
        }
      }
    }
  }

  configureDimensions(): void {
    const currentLayer = this.cloneLayer();
    const jsonlayer = this.jsonlayer_v1_1_1;
    if (!jsonlayer) return;
    // Fill in dimensions
    const dimensions = toArray(jsonlayer.Dimension);

    // Add dims from parentlayers
    for (let j = this.objectpath.length - 1; j >= 0; j -= 1) {
      const parentDims = [this.objectpath[j].Dimension];
      if (!isNull(parentDims) && isDefined(parentDims)) {
        for (let d = 0; d < parentDims.length; d += 1) {
          const parentDim = parentDims[d];
          if (!isNull(parentDim) && isDefined(parentDim)) {
            let foundDim = false;
            for (let i = 0; i < dimensions.length; i += 1) {
              if (
                parentDim.name.toLowerCase() ===
                dimensions[j].attr.name.toLowerCase()
              ) {
                foundDim = true;
                break;
              }
            }
            if (!foundDim) {
              dimensions.push(parentDim);
            }
          }
        }
      }
    }

    const extents = toArray(jsonlayer.Extent);
    const layerDimsToRemove = [];
    /* Remember which dims we currently have, later on we see which ones are still present and which ones need to be removed */
    for (let i = 0; i < this.dimensions.length; i += 1) {
      layerDimsToRemove.push(this.dimensions[i].name);
    }
    let hasRefTimeDimension = false;

    for (let j = 0; j < dimensions.length; j += 1) {
      let dim;
      if (dimensions[j].attr.name.toLowerCase() === 'reference_time') {
        hasRefTimeDimension = true;
        dim = new WMJSDimension({ linked: false });
      } else {
        dim = new WMJSDimension({});
      }
      dim.name = dimensions[j].attr.name.toLowerCase();
      dim.units = dimensions[j].attr.units;
      dim.unitSymbol = dimensions[j].attr.unitSymbol;
      // WMS 1.1.1 Mode:
      for (let i = 0; i < extents.length; i += 1) {
        if (extents[i].attr.name.toLowerCase() === dim.name) {
          // Check if a value is given:
          if (!extents[i].value) {
            dim.values = '';
          } else {
            dim.values = extents[i].value.trim();
          }
          // Check for default
          if (extents[i].attr.default) {
            dim.defaultValue = extents[i].attr.default.trim();
          } else {
            const s = dim.values.split('/');
            let defaultIndex = 0;
            if (s.length > 1) defaultIndex = 1;
            else if (s.length > 0) defaultIndex = 0;
            dim.defaultValue = s[defaultIndex];
          }
          // If no values are given, provide the defaults.
          if (!extents[i].value) {
            error(
              `No extent defined for dim ${dim.name} in layer ${this.title}`,
            );
            error(`Using default value ${dim.defaultValue}`);
            dim.values = dim.defaultValue;
          }
        }
      }

      // WMS 1.3.0 Mode:
      if (this.version === WMSVersion.version130) {
        dim.values = dimensions[j].value;
        dim.defaultValue = dimensions[j].attr.default;
      }

      let { defaultValue } = dim;

      if (this.parentMaps.length > 0) {
        const mapDim = this.parentMaps[0].getDimension(dim.name);
        if (isDefined(mapDim) && mapDim.linked) {
          if (isDefined(mapDim.currentValue)) {
            defaultValue = dim.getClosestValue(mapDim.currentValue);
            debug(
              `WMLayer::configureDimensions Dimension ${dim.name} default value [${defaultValue}] is based on map value [${mapDim.currentValue}]`,
            );
          } else {
            debug(
              `WMLayer::configureDimensions Map dimension currentValue for ${dim.name} does not exist.`,
            );
          }
        }
      } else {
        debug('WMLayer::configureDimensions Layer has no parentMaps');
      }
      if (
        currentLayer.dimensions.filter(d => d.name === dim.name).length === 1
      ) {
        const oldDim = currentLayer.dimensions.filter(
          d => d.name === dim.name,
        )[0];
        if (isDefined(oldDim.currentValue)) {
          dim.setValue(oldDim.currentValue);
        } else {
          dim.setValue(defaultValue);
        }
      } else {
        dim.setValue(defaultValue);
      }

      dim.parentLayer = this;
      if (isDefined(dim.values)) {
        const i = this.dimensions.findIndex(d => d.name === dim.name);
        /* Found dim, remove it from the list with dims to remove */
        const k = layerDimsToRemove.findIndex(d => d === dim.name);
        if (k !== -1) layerDimsToRemove.splice(k, 1);
        if (i === -1) {
          this.dimensions.push(dim);
        } else {
          const { currentValue } = this.dimensions[i];
          this.dimensions[i] = dim;
          this.dimensions[i].currentValue = currentValue;
        }
      } else {
        error(`Skipping dimension ${dim.name}`);
      }
    }

    /* Check which dims are still present and should be removed */
    for (let d = 0; d < layerDimsToRemove.length; d += 1) {
      const i = this.dimensions.findIndex(
        dim => dim.name === layerDimsToRemove[d],
      );
      if (i !== -1) this.dimensions.splice(i, 1);
    }
    if (hasRefTimeDimension) {
      const refTimeDimension = this.getDimension('reference_time');
      this.handleReferenceTime('reference_time', refTimeDimension.getValue());
    }
  }

  __parseGetCapForLayer(
    layer: WMLayer,
    getcapabilitiesjson: GetCapabilitiesJson,
    layerDoneCallback: (layer: WMLayer) => void,
    fail: (layer: WMLayer, message: string) => void,
  ): void {
    const jsondata = getcapabilitiesjson;
    if (!jsondata) {
      this.title = I18n.service_has_error.text;
      this.abstract = I18n.not_available_message.text;
      fail(layer, I18n.unable_to_connect_server.text);
      return;
    }

    let j = 0;

    // Get the capability object
    let capabilityObject: Capability;
    try {
      capabilityObject = layer.WMJSService.getCapabilityElement(
        getcapabilitiesjson,
      );
    } catch {
      fail(layer, 'No capability element in service');
      return;
    }

    this.version = layer.WMJSService.version;

    // Get the rootLayer
    const rootLayer = capabilityObject.Layer;
    if (!isDefined(rootLayer)) {
      fail(layer, 'No Layer element in service');
      return;
    }

    try {
      this.serviceTitle = rootLayer.Title.value;
    } catch (e) {
      this.serviceTitle = 'Unnamed service';
    }

    this.optimalFormat = 'image/png';
    // Get the optimal image format for this layer
    try {
      const serverFormats = capabilityObject.Request.GetMap.Format;
      for (let f = 0; f < serverFormats.length; f += 1) {
        if (serverFormats[f].value.indexOf('24') > 0)
          this.optimalFormat = serverFormats[f].value;
        if (serverFormats[f].value.indexOf('32') > 0)
          this.optimalFormat = serverFormats[f].value;
      }
    } catch (e) {
      error('This WMS service has no getmap formats listed: using image/png');
    }

    if (this.name === undefined || this.name.length < 1) {
      this.title = WMEmptyLayerTitle;
      this.abstract = I18n.not_available_message.text;
      layerDoneCallback(this);
      return;
    }

    let foundLayer = 0;
    // Function will be called when the layer with the right name is found in the getcap doc
    const foundLayerFunction = (jsonlayer, path, objectpath): void => {
      this.jsonlayer_v1_1_1 = jsonlayer;

      this.getmapURL = undefined;
      try {
        this.getmapURL =
          capabilityObject.Request.GetMap.DCPType.HTTP.Get.OnlineResource.attr[
            'xlink:href'
          ];
      } catch (e) {
        /* Do nothing */
      }
      if (!isDefined(this.getmapURL)) {
        this.getmapURL = this.service;
        error('GetMap OnlineResource is not specified. Using default.');
      }

      this.getfeatureinfoURL = undefined;
      try {
        this.getfeatureinfoURL =
          capabilityObject.Request.GetFeatureInfo.DCPType.HTTP.Get.OnlineResource.attr[
            'xlink:href'
          ];
      } catch (e) {
        /* Do nothing */
      }
      if (!isDefined(this.getfeatureinfoURL)) {
        this.getfeatureinfoURL = this.service;
        error('GetFeatureInfo OnlineResource is not specified. Using default.');
      }

      this.getlegendgraphicURL = undefined;
      try {
        this.getlegendgraphicURL =
          capabilityObject.Request.GetLegendGraphic.DCPType.HTTP.Get.OnlineResource.attr[
            'xlink:href'
          ];
      } catch (e) {
        /* Do nothing */
      }

      if (!isDefined(this.getlegendgraphicURL)) {
        this.getlegendgraphicURL = this.service;
      }

      // TODO Should be arranged also for the other services:
      this.getmapURL = WMJScheckURL(layer.getmapURL);
      this.getfeatureinfoURL = WMJScheckURL(layer.getfeatureinfoURL);
      this.getlegendgraphicURL = WMJScheckURL(layer.getlegendgraphicURL);

      this.getCapabilitiesDoc = jsondata;
      this.title = jsonlayer.Title.value;
      try {
        this.abstract = jsonlayer.Abstract.value;
      } catch (e) {
        this.abstract = I18n.not_available_message.text;
      }
      this.path = path;
      this.objectpath = objectpath;

      this.styles = [];
      this.jsonlayer = this;
      try {
        let layerStyles = [];
        if (jsonlayer.Style) {
          layerStyles = toArray(jsonlayer.Style);
        }
        for (let k = 0; k < layerStyles.length; k += 1) {
          this.styles.push({ ...layerStyles[k] });
        }

        // parse styles

        for (let k = 0; k < this.styles.length; k += 1) {
          const style = this.styles[j];
          style.index = k;
          style.nrOfStyles = this.styles.length;
          style.title = 'default';
          style.name = 'default';
          style.legendURL = '';
          style.abstract = 'No abstract available';

          try {
            style.title = style.Title.value;
          } catch (e) {
            /* Do nothing */
          }
          try {
            style.name = style.Name.value;
          } catch (e) {
            /* Do nothing */
          }
          try {
            style.legendURL = style.LegendURL.OnlineResource.attr['xlink:href'];
          } catch (e) {
            /* Do nothing */
          }
          try {
            style.abstract = style.Abstract.value;
          } catch (e) {
            /* Do nothing */
          }
        }

        if (this.currentStyle === '') {
          this.currentStyle = this.styles[0].Name.value;
        }

        this.setStyle(this.currentStyle);
      } catch (e) {
        this.currentStyle = '';
        this.styles.length = 0;
        error(`No styles found for layer ${layer.title}`);
      }
      this.configureDimensions();
      let gp = toArray(jsonlayer.SRS);

      if (isDefined(jsonlayer.CRS)) {
        gp = toArray(jsonlayer.CRS);
      }

      this.projectionProperties = [];

      let tempSRS = [];

      const getgpbbox = (data): void => {
        if (isDefined(data.BoundingBox)) {
          // Fill in SRS and BBOX on basis of BoundingBox attribute
          const gpbbox = toArray(data.BoundingBox);
          for (j = 0; j < gpbbox.length; j += 1) {
            let srs;
            srs = gpbbox[j].attr.SRS;

            if (isDefined(gpbbox[j].attr.CRS)) {
              srs = gpbbox[j].attr.CRS;
            }
            if (srs) {
              if (srs.length > 0) {
                srs = decodeURIComponent(srs);
              }
            }
            let alreadyAdded = false;
            for (let i = 0; i < this.projectionProperties.length; i += 1) {
              if (srs === this.projectionProperties[i].srs) {
                alreadyAdded = true;
                break;
              }
            }

            if (alreadyAdded === false) {
              const geoProperty = new WMProjection();

              geoProperty.srs = srs;
              let swapBBOX = false;
              if (layer.version === WMSVersion.version130) {
                if (
                  geoProperty.srs === 'EPSG:4326' &&
                  layer.wms130bboxcompatibilitymode === false
                ) {
                  swapBBOX = true;
                }
              }
              if (swapBBOX === false) {
                geoProperty.bbox.left = parseFloat(gpbbox[j].attr.minx);
                geoProperty.bbox.bottom = parseFloat(gpbbox[j].attr.miny);
                geoProperty.bbox.right = parseFloat(gpbbox[j].attr.maxx);
                geoProperty.bbox.top = parseFloat(gpbbox[j].attr.maxy);
              } else {
                geoProperty.bbox.left = parseFloat(gpbbox[j].attr.miny);
                geoProperty.bbox.bottom = parseFloat(gpbbox[j].attr.minx);
                geoProperty.bbox.right = parseFloat(gpbbox[j].attr.maxy);
                geoProperty.bbox.top = parseFloat(gpbbox[j].attr.maxx);
              }

              this.projectionProperties.push(geoProperty);
              tempSRS.push(geoProperty.srs);
            }
          }
        }
      };

      getgpbbox(jsonlayer);
      getgpbbox(rootLayer);

      // Fill in SRS  on basis of SRS attribute
      for (j = 0; j < gp.length; j += 1) {
        if (tempSRS.indexOf(gp[j].value) === -1) {
          const geoProperty = new WMProjection();
          error(`Warning: BoundingBOX missing for SRS ${gp[j].value}`);
          geoProperty.bbox.left = -180;
          geoProperty.bbox.bottom = -90;
          geoProperty.bbox.right = 180;
          geoProperty.bbox.top = 90;
          geoProperty.srs = gp[j].value;
          layer.projectionProperties.push(geoProperty);
        }
      }
      tempSRS = [];
      /* Check if layer is queryable */
      this.queryable = false;
      try {
        if (parseInt(jsonlayer.attr.queryable, 10) === 1) this.queryable = true;
        else this.queryable = false;
      } catch (e) {
        error(
          `Unable to detect whether this layer is queryable (for layer ${layer.title})`,
        );
      }
      foundLayer = 1;
    };

    function recursivelyFindLayer(JSONLayers, path, _objectpath): WMLayer {
      const objectpath = [];
      for (let i = 0; i < _objectpath.length; i += 1) {
        objectpath.push(_objectpath[i]);
      }
      objectpath.push(JSONLayers);

      for (let k = 0; k < JSONLayers.length; k += 1) {
        if (JSONLayers[k].Layer) {
          let pathnew = path;

          try {
            pathnew += `${JSONLayers[k].Title.value}/`;
          } catch (e) {
            /* Do nothing */
          }

          recursivelyFindLayer(
            toArray(JSONLayers[k].Layer),
            pathnew,
            objectpath,
          );
        } else if (JSONLayers[k].Name) {
          if (JSONLayers[k].Name.value === layer.name) {
            foundLayerFunction(JSONLayers[k], path, objectpath);
            return;
          }
        }
      }
    }
    // Try to recursively find the name in the getcap doc
    const JSONLayers = toArray(rootLayer.Layer);
    const path = '';
    const objectpath = [];
    objectpath.push(rootLayer);
    recursivelyFindLayer(JSONLayers, path, objectpath);

    if (foundLayer === 0) {
      // Layer was not found...
      let message = '';
      if (layer.name) {
        message = `Unable to find layer '${this.name}' in service '${this.service}'`;
      } else {
        message = `Unable to find layer '${this.title}' in service '${this.service}'`;
      }
      this.title = '--- layer not found in service ---';
      this.abstract = I18n.not_available_message.text;
      fail(this, message);
      return;
    }
    /* Layer was found */
    if (this.onReady) {
      this.onReady(this);
    }
    this.isConfigured = true;
    layerDoneCallback(this);
  }

  /**
   * Calls success with a configured layer object
   * Calls options.failure with error message.
   * Throws string exceptions when someting goes wrong
   */
  parseLayer(
    _layerDoneCallback: (layer: WMLayer) => void,
    forceReload: boolean,
    xml2jsonrequest?: string,
  ): void {
    this.hasError = false;
    const layerDoneCallback = (layer): void => {
      if (isDefined(_layerDoneCallback)) {
        try {
          _layerDoneCallback(layer);
        } catch (e) {
          // eslint-disable-next-line no-console
          console.error(e);
        }
      }
    };
    const fail = (layer: WMLayer, message: string): void => {
      this.hasError = true;
      this.lastError = message;
      this.title = I18n.service_has_error.text;
      error(message);
      layerDoneCallback(layer);
      if (isDefined(this._options.failure)) {
        this._options.failure(layer, message);
      }
    };

    const callback = (data: GetCapabilitiesJson): void => {
      this.__parseGetCapForLayer(this, data, layerDoneCallback, fail);
    };

    const requestfail = (): void => {
      fail(this, I18n.no_capability_element_found.text);
    };

    let newXml2jsonrequest = xml2jsonrequest;
    if (!xml2jsonrequest) {
      newXml2jsonrequest =
        this.parentMaps && this.parentMaps.length > 0
          ? this.parentMaps[0].xml2jsonrequest
          : undefined;
    }

    this.WMJSService = WMGetServiceFromStore(this.service, newXml2jsonrequest);

    const options = { headers: {} };
    if (this.headers && this.headers.length > 0) {
      options.headers = this.headers;
    }

    if (this.WMJSService.service !== undefined) {
      this.WMJSService.getCapabilities(
        data => {
          callback(data);
        },
        requestfail,
        forceReload,
        xml2jsonrequest,
        options,
      );
    }
  }

  cloneLayer(): WMLayer {
    return new WMLayer(this);
  }

  setName(name: string): void {
    this.name = name;
    this.parseLayer(undefined, undefined, undefined);
  }

  getLayerRelative(
    success: (layer: WMLayer, index: number, length: number) => void,
    failure: (param: string) => void,
    prevNext: number,
  ): void {
    let localPrevNext = prevNext;
    if (!isDefined(prevNext)) {
      localPrevNext = 0;
    }
    const getLayerObjectsFinished = (layerObjects: WMLayer[]): void => {
      let currentLayerIndex = -1;
      for (let j = 0; j < layerObjects.length; j += 1) {
        if (layerObjects[j].name === this.name) {
          currentLayerIndex = j;
          break;
        }
      }
      if (currentLayerIndex === -1) {
        failure(`Current layer [${this.name}] not in this service`);
        return;
      }

      if (localPrevNext === -1) currentLayerIndex -= 1;
      if (localPrevNext === 1) currentLayerIndex += 1;
      if (currentLayerIndex > layerObjects.length - 1) currentLayerIndex = 0;
      if (currentLayerIndex < 0) currentLayerIndex = layerObjects.length - 1;
      success(
        layerObjects[currentLayerIndex],
        currentLayerIndex,
        layerObjects.length,
      );
    };
    this.WMJSService.getLayerObjectsFlat(
      getLayerObjectsFinished,
      failure,
      undefined,
    );
  }

  autoSelectLayer(
    success: (layer: WMLayer) => void,
    failure: (param: string) => void,
  ): void {
    const getLayerObjectsFinished = (layerObjects: WMLayer[]): void => {
      for (let j = 0; j < layerObjects.length; j += 1) {
        if (isDefined(layerObjects[j].name)) {
          if (layerObjects[j].name.indexOf('baselayer') === -1) {
            if (layerObjects[j].path.indexOf('baselayer') === -1) {
              success(layerObjects[j]);
              return;
            }
          }
        }
      }
    };
    this.WMJSService.getLayerObjectsFlat(
      getLayerObjectsFinished,
      failure,
      undefined,
    );
  }

  getNextLayer(
    success: (layer: WMLayer, index: number, length: number) => void,
    failure: (param: string) => void,
  ): void {
    this.getLayerRelative(success, failure, 1);
  }

  getPreviousLayer(
    success: (layer: WMLayer, index: number, length: number) => void,
    failure: (param: string) => void,
  ): void {
    this.getLayerRelative(success, failure, -1);
  }

  /**
   * Sets the style by its name
   * @param style: The name of the style (not the object)
   */
  setStyle(styleName: string): void {
    debug(`WMLayer::setStyle: ${styleName}`);

    if (!this.styles || this.styles.length === 0) {
      this.currentStyle = '';
      this.legendGraphic = '';
      debug('Layer has no styles.');
      return;
    }

    for (let j = 0; j < this.styles.length; j += 1) {
      if (this.styles[j].Name.value === styleName) {
        this.legendGraphic = this.styles[j].LegendURL.OnlineResource.attr[
          'xlink:href'
        ];
        this.currentStyle = this.styles[j].Name.value;
        return;
      }
    }
    debug(
      `WMLayer::setStyle: Style ${styleName} not found, setting style ${this.styles[0].name}`,
    );
    this.currentStyle = this.styles[0].name;
    this.legendGraphic = this.styles[0].legendURL;
  }

  getStyles(): Style[] {
    if (this.styles) {
      return this.styles;
    }
    return [];
  }

  /**
   * Get the styleobject by name
   * @param styleName The name of the style
   * @param nextPrev, can be -1 or +1 to get the next or previous style object in circular manner.
   */
  getStyleObject(styleName: string, nextPrev: number): ExtendedStyle {
    if (isDefined(this.styles) === false) {
      return undefined;
    }
    for (let j = 0; j < this.styles.length; j += 1) {
      if (this.styles[j].name === styleName) {
        if (nextPrev === -1) j -= 1;
        if (nextPrev === 1) j += 1;
        if (j < 0) j = this.styles.length - 1;
        if (j > this.styles.length - 1) j = 0;
        this.styles[j].nrOfStyles = this.styles.length;
        this.styles[j].index = j;
        return this.styles[j];
      }
    }
    return undefined;
  }

  /*
   *Get the current stylename as used in the getmap request
   */
  getStyle(): string {
    return this.currentStyle;
  }

  getDimension(name: string): WMJSDimension {
    const dimIndex = this.dimensions.findIndex(dim => dim.name === name);
    if (dimIndex < 0) return undefined;
    return this.dimensions[dimIndex];
  }

  getProjection(srsName: string): WMProjection {
    for (let j = 0; j < this.projectionProperties.length; j += 1) {
      if (this.projectionProperties[j].srs === srsName) {
        const returnSRS = {
          srs: '',
          bbox: new WMBBOX(undefined, undefined, undefined, undefined),
        };
        returnSRS.srs = `${this.projectionProperties[j].srs}`;
        returnSRS.bbox = new WMBBOX(
          this.projectionProperties[j].bbox.left,
          this.projectionProperties[j].bbox.bottom,
          this.projectionProperties[j].bbox.right,
          this.projectionProperties[j].bbox.top,
        );
        return returnSRS;
      }
    }
    return undefined;
  }

  setSLDURL(url: string): void {
    this.sldURL = url;
  }

  display(displayornot: boolean): void {
    this.enabled = displayornot;
    for (let j = 0; j < this.parentMaps.length; j += 1) {
      this.parentMaps[j].displayLayer(this, this.enabled);
    }
  }
}
