/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

export default class WMDebouncer {
  _isRunning: boolean;

  _milliseconds: number;

  _stop: boolean;

  constructor() {
    this._isRunning = false;
    this._milliseconds = 10;
    this._stop = false;
    this.init = this.init.bind(this);
    this.stop = this.stop.bind(this);
  }

  init(ms: number, functionhandler: () => void): void {
    this._stop = false;
    this._milliseconds = ms;
    if (this._milliseconds < 10) this._milliseconds = 50;
    if (this._isRunning === false) {
      window.self.setTimeout(() => {
        this._isRunning = false;
        if (this._stop === false) {
          functionhandler();
        }
      }, this._milliseconds);
      this._isRunning = true;
    }
  }

  stop(): void {
    this._stop = true;
  }
}
