/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import WMImage from './WMImage';

const mockRandom = 123;

describe('src/geoweb-webmap/WMImage', () => {
  beforeEach(() => {
    jest.spyOn(global.Math, 'random').mockReturnValue(mockRandom);
  });
  afterEach(() => {
    jest.spyOn(global.Math, 'random').mockRestore();
  });

  it('should initialise with correct params', () => {
    const url = 'http://test/';
    const fn = jest.fn();
    const image = new WMImage(url, fn);

    expect(image.randomize).toBeFalsy();
    expect(image.getElement().src).toEqual('');

    // trigger load
    image.load();

    expect(image.srcToLoad).toEqual(url);
    expect(image.getElement().src).toEqual(url);
  });

  it('should initialise with correct params and disabled cache as options', () => {
    const url = 'http://test/';
    const fn = jest.fn();
    const options = {
      randomizer: true,
      headers: [],
    };
    const image = new WMImage(url, fn, options);

    expect(image.randomize).toBeTruthy();
    expect(image.getElement().src).toEqual('');

    // trigger load
    image.load();
    expect(image.srcToLoad).toEqual(url);
    expect(image.getElement().src).toEqual(`${url}?random=${mockRandom}`);
  });
});
