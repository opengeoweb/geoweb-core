/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { defaultReduxLayerRadarKNMI } from '../utils/defaultTestSettings';
import WMLayer from './WMLayer';

describe('src/geoweb-webmap/WMLayer', () => {
  it('should initialise a new layer with default values', () => {
    const layer = new WMLayer();

    expect(layer.id).toEqual('-1');
    expect(layer.hasError).toBeFalsy();
    expect(layer.enabled).toBeTruthy();
    expect(layer.opacity).toEqual(1.0);
    expect(layer.type).toEqual('wms');
    expect(layer.keepOnTop).toBeFalsy();
    expect(layer.transparent).toBeTruthy();
    expect(layer.autoupdate).toBeFalsy();
  });
  it('should initialise a new layer with given options', () => {
    const testLayer1 = new WMLayer({
      ...defaultReduxLayerRadarKNMI,
      id: 'wmlayer-testlayer-1',
      enabled: false,
      opacity: 0.6,
      keepOnTop: true,
      getgraphinfoURL: 'graphinfotest',
      sldURL: 'sldURLtest',
      transparent: false,
      onReady: jest.fn(),
    });
    const layer = new WMLayer(testLayer1);

    expect(layer.id).toEqual(testLayer1.id);
    expect(layer.hasError).toBeFalsy();
    expect(layer.enabled).toEqual(testLayer1.enabled);
    expect(layer.opacity).toEqual(testLayer1.opacity);
    expect(layer.type).toEqual('wms');
    expect(layer.keepOnTop).toEqual(testLayer1.keepOnTop);
    expect(layer.getgraphinfoURL).toEqual(testLayer1.getgraphinfoURL);
    expect(layer.sldURL).toEqual(testLayer1.sldURL);
    expect(layer.transparent).toBeFalsy();
    expect(layer.getLayerName()).toEqual(testLayer1.name);
  });
  it('should toggle autoupdate', async () => {
    jest.useFakeTimers();
    const layer = new WMLayer();
    expect(layer.autoupdate).toBeFalsy();
    expect(setInterval).toHaveBeenCalledTimes(0);
    expect(clearInterval).toHaveBeenCalledTimes(0);

    layer.toggleAutoUpdate();

    expect(setInterval).toHaveBeenCalledTimes(1);
    expect(setInterval).toHaveBeenCalledWith(expect.any(Function), 60000);
    expect(layer.autoupdate).toBeTruthy();

    layer.setAutoUpdate(false);
    expect(layer.autoupdate).toBeFalsy();
    expect(clearInterval).toHaveBeenCalledTimes(1);

    layer.toggleAutoUpdate();
    expect(layer.autoupdate).toBeTruthy();
    expect(setInterval).toHaveBeenCalledTimes(2);

    layer.toggleAutoUpdate();
    expect(layer.autoupdate).toBeFalsy();
    expect(clearInterval).toHaveBeenCalledTimes(2);

    jest.clearAllTimers();
    jest.useRealTimers();
  });
  it('should set opacity', () => {
    const layer = new WMLayer();
    layer.setOpacity('0.7');
    expect(layer.getOpacity()).toEqual(0.7);
  });
  it('should set dimension', () => {
    const testLayer2 = new WMLayer({
      ...defaultReduxLayerRadarKNMI,
      id: 'wmlayer-testlayer-1',
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
        },
      ],
    });
    const layer = new WMLayer(testLayer2);
    expect(layer.getDimension('elevation').currentValue).toEqual('9000');
    layer.setDimension('elevation', '1000');
    expect(layer.getDimension('elevation').currentValue).toEqual('1000');
    expect(layer.getDimensions()).toEqual([
      expect.objectContaining({ currentValue: '1000', name: 'elevation' }),
    ]);
  });
});
