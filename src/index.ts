/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as mapActions from './store/mapStore/actions';
import * as mapSelectors from './store/mapStore/selectors';
import * as mapTypes from './store/mapStore/types';
import * as mapConstants from './store/mapStore/constants';
import { AppStore } from './types/types';

export { MapViewConnect, MapViewLayer, MapView } from './components/MapView';

export { ReactMapView, ReactMapViewLayer } from './components/ReactMapView';

export {
  VerticalDimensionSelectConnect,
  VerticalSelectDialog,
  VerticalDimensionSelect,
} from './components/VerticalDimensionSelect';

export { MultiDimensionSelectConnect } from './components/MultiDimensionSelect';

export { LegendConnect, Legend } from './components/Legend';

export { GeoWebLayerSelectConnect } from './components/GeoWebLayerSelect';

export { TimeSliderConnect, LayerManagerButton } from './components/TimeSlider';

export { mapActions, mapSelectors, mapTypes, mapConstants, AppStore };

export * from './store/mapStore/utils/helpers';

export { store } from './store';
