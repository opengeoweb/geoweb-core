/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { findByRole, fireEvent, render } from '@testing-library/react';
import ResetButton, { defaultTextAnnotation } from './ResetButton';

describe('src/components/TimeSlider/TimeSliderButtons/ResetButton', () => {
  it('should display a proper default Svg path for icon', () => {
    const { getByTestId } = render(<ResetButton />);

    // Should display "reset" Svg path by default
    const resetButton = getByTestId('reset-svg-path');
    expect(resetButton).toBeTruthy();
  });

  it('should show a title text "Reset" in tooltip when hovering', async () => {
    const { getByTestId, container } = render(<ResetButton />);

    const resetButton = getByTestId('reset-svg-path');
    expect(resetButton).toBeTruthy();

    fireEvent.mouseOver(resetButton);
    // Wait until tooltip appears
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain('Reset');
  });

  it('should render a reset button as enabled by default', () => {
    const { getByTestId } = render(<ResetButton />);
    fireEvent.click(getByTestId('resetButton'));
    expect(getByTestId('resetButton')).toBeTruthy();

    expect(
      getByTestId('resetButton').classList.contains('Mui-disabled'),
    ).toBeFalsy();
  });

  it('should render button as enabled when it is not disabled in props', () => {
    const props = {
      isDisabled: false,
      onToggleReset: jest.fn(),
    };

    const { getByTestId } = render(<ResetButton {...props} />);

    const resetButton = getByTestId('resetButton');
    expect(resetButton).toBeTruthy();

    // An enabled reset button should show up
    expect(resetButton.classList.contains('Mui-disabled')).toBeFalsy();
  });

  it('should render button as disabled when it is disabled in props', async () => {
    const props = {
      isDisabled: true,
      onToggleReset: jest.fn(),
    };

    const { getByTestId } = render(<ResetButton {...props} />);

    // Should display "reset" icon (default)
    const button = getByTestId('resetButton');
    expect(button).toBeTruthy();

    // A disabled reset button should show up
    expect(button.classList.contains('Mui-disabled')).toBeTruthy();
  });

  it('should render button as enabled when it is not disabled in props', async () => {
    const props = {
      isDisabled: false,
      onToggleReset: jest.fn(),
    };

    const { getByTestId } = render(<ResetButton {...props} />);

    // Should display "reset" icon (default)
    const button = getByTestId('resetButton');
    expect(button).toBeTruthy();

    // A non-disabled reset button should show up
    expect(button.classList.contains('Mui-disabled')).toBeFalsy();
  });

  it('should render button with default (reset) text annotation "RESET"', async () => {
    const { getByTestId, container } = render(<ResetButton />);

    // Should display "reset" icon
    const button = getByTestId('reset-svg-text');

    expect(button).toBeTruthy();
    expect(defaultTextAnnotation).toBe('RESET');
    expect(container.parentElement.querySelector('tspan').innerHTML).toEqual(
      defaultTextAnnotation,
    );
  });

  it('should render button with custom (reset) text annotation in props', async () => {
    const props = {
      isDisabled: false,
      textAnnotation: 'TEST',
      onToggleReset: jest.fn(),
    };

    const { getByTestId, container } = render(<ResetButton {...props} />);

    // Should display "reset" icon
    const button = getByTestId('reset-svg-text');

    expect(button).toBeTruthy();
    expect(container.parentElement.querySelector('tspan').innerHTML).toEqual(
      props.textAnnotation,
    );
  });

  it('should call function in props when clicked', () => {
    const props = {
      onToggleReset: jest.fn(),
    };

    const { getByTestId } = render(<ResetButton {...props} />);

    // Should display Svg path
    const resetButton = getByTestId('resetButton');
    expect(resetButton).toBeTruthy();

    fireEvent.click(resetButton);
    expect(props.onToggleReset).toBeCalled();
    expect(props.onToggleReset).toBeCalled();
  });

  it('should call function in props for each click of reset button', async () => {
    const props = {
      isDisabled: false,
      onToggleReset: jest.fn(),
    };

    const { getByTestId } = render(<ResetButton {...props} />);

    // Function in props should have been called once after first click
    const button = getByTestId('resetButton');
    expect(button).toBeTruthy();

    fireEvent.click(button);
    expect(props.onToggleReset).toHaveBeenCalledTimes(1);

    //  Function in props should have been called once again after second click, making up a total of two calls
    fireEvent.click(button);
    expect(props.onToggleReset).toHaveBeenCalledTimes(2);
  });
});
