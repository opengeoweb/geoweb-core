/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { IconButton, SvgIcon, makeStyles, Tooltip } from '@material-ui/core';

const colors = {
  DARK_GREY: '#484848',
  LIGHT_GREY: '#c3c3c3',
  BLUE: '#0075a9',
  LIGHT_BLUE: '#d9eaf2',
  BLACK: '#000',
  WHITE: '#ffffff',
};

const useStyles = makeStyles({
  resetButton: {
    height: '24px',
    width: '24px',
    backgroundColor: colors.WHITE,
    borderRadius: '4.5px',
    '&&:hover': {
      background: colors.LIGHT_BLUE,
    },
  },
});

const svgResetPath = {
  title: 'Reset',
  src:
    'M17.95 5.05c2.733 2.734 2.733 7.166 0 9.9-2.734 2.733-7.166 2.733-9.9 0-2.733-2.734-2.733-7.166 0-9.9 2.734-2.733 7.166-2.733 9.9 0zM9.092 6.092c-2.158 2.158-2.158 5.658 0 7.816 2.158 2.158 5.658 2.158 7.816 0 2.158-2.158 2.158-5.658 0-7.816-2.158-2.158-5.658-2.158-7.816 0zm4.752.027l1.357.576-1.44 3.389 1.649 2.187-1.177.887-1.409-1.87-.995-.421 2.015-4.748z',
};

export interface ResetButtonProps {
  isDisabled?: boolean;
  textAnnotation?: string;
  onToggleReset?: () => void;
}

export const defaultTextAnnotation = 'RESET';

const ResetButton: React.FC<ResetButtonProps> = ({
  isDisabled = false,
  textAnnotation = defaultTextAnnotation,
  onToggleReset = (): void => null,
}: ResetButtonProps) => {
  const classes = useStyles();

  return (
    <Tooltip title={svgResetPath.title}>
      <span>
        <IconButton
          onClick={(): void => onToggleReset()}
          disabled={isDisabled}
          className={classes.resetButton}
          data-testid="resetButton"
          disableRipple
        >
          <SvgIcon>
            <rect width={24} height={24} fill="transparent" />
            <path
              data-testid="reset-svg-path"
              d={svgResetPath.src}
              fill={colors.BLUE}
            />
            <text
              data-testid="reset-svg-text"
              fill="#0075A9"
              fontFamily="Roboto-Black, Roboto"
              fontSize="6"
              fontWeight="800"
            >
              <tspan x="5" y="23">
                {textAnnotation}
              </tspan>
            </text>
          </SvgIcon>
        </IconButton>
      </span>
    </Tooltip>
  );
};

export default ResetButton;
