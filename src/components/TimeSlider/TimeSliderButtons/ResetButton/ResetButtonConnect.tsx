/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { connect } from 'react-redux';
import React from 'react';
import moment from 'moment';
import { mapActions, mapSelectors } from '../../../..';
import { AppStore } from '../../../../types/types';
import { Dimension } from '../../../../store/mapStore/types';
import ResetButton from './ResetButton';

export const defaultAnimationDuration = 6; // [h]
export interface ResetButtonConnectProps {
  mapId: string;
  isDisabled?: boolean;
  textAnnotation?: string;
  dimensions?: Dimension[];
  mapStopAnimation?: typeof mapActions.mapStopAnimation;
  setAnimationStartTime?: typeof mapActions.setAnimationStartTime;
  setAnimationEndTime?: typeof mapActions.setAnimationEndTime;
}

const connectRedux = connect(
  (store: AppStore, props: ResetButtonConnectProps) => ({
    dimensions: mapSelectors.getMapDimensions(store, props.mapId),
  }),
  {
    mapStopAnimation: mapActions.mapStopAnimation,
    setAnimationStartTime: mapActions.setAnimationStartTime,
    setAnimationEndTime: mapActions.setAnimationEndTime,
  },
);

const ResetButtonConnect: React.FC<ResetButtonConnectProps> = connectRedux(
  ({
    mapId,
    isDisabled,
    textAnnotation,
    dimensions,
    mapStopAnimation,
    setAnimationStartTime,
    setAnimationEndTime,
  }: ResetButtonConnectProps) => {
    const onMapToggleReset = (): void => {
      const timeDim = dimensions.find(dim => dim.name === 'time');
      const curSelectedTime = timeDim
        ? timeDim.currentValue
        : moment.utc().toISOString();

      const start = moment
        .utc(curSelectedTime)
        .subtract(defaultAnimationDuration, 'h');
      const end = moment.utc(curSelectedTime);

      mapStopAnimation({ mapId });

      setAnimationStartTime({ mapId, animationStartTime: start });
      setAnimationEndTime({ mapId, animationEndTime: end });
    };

    return (
      <ResetButton
        isDisabled={isDisabled}
        textAnnotation={textAnnotation}
        onToggleReset={(): void => {
          onMapToggleReset();
        }}
      />
    );
  },
);

export default ResetButtonConnect;
