/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import moment from 'moment';
import * as mapActionTypes from '../../../../store/mapStore/map/constants';
import ResetButtonConnect from './ResetButtonConnect';

const currentTime = moment.utc();
const startValue = moment.utc().subtract(6, 'h');
const endValue = currentTime;

describe('src/components/TimeSlider/TimeSliderButtons/ResetButton/ResetButtonConnect', () => {
  const dimensions = [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: currentTime,
    },
  ];

  const mapId = 'map-1';

  it('should reset animation with the correct default start and end times in store when clicked', () => {
    const props = {
      mapId,
    };

    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            animationStartTime: startValue,
            animationEndTime: endValue,
            dimensions,
          },
        },
      },
    });

    const { container } = render(
      <Provider store={store}>
        <ResetButtonConnect {...props} />
      </Provider>,
    );

    const button = getByRole(container, 'button');
    expect(button).toBeTruthy();

    fireEvent.click(button);

    const expectedActionForStopAnimation = {
      type: mapActionTypes.WEBMAP_STOP_ANIMATION,
      payload: {
        mapId,
      },
    };

    const expectedActionForStartTime = {
      type: mapActionTypes.WEBMAP_SET_ANIMATION_START_TIME,
      payload: {
        mapId,
        animationStartTime: startValue,
      },
    };

    const expectedActionForEndTime = {
      type: mapActionTypes.WEBMAP_SET_ANIMATION_END_TIME,
      payload: {
        mapId,
        animationEndTime: endValue,
      },
    };

    const numberOfDecimals = 1;

    // Compare action for stopping animation.
    expect(store.getActions()[0]).toEqual(expectedActionForStopAnimation);

    // Compare action for setting animation start time. Allows 'toBeCloseTo' when comparing 'animationStartTime'.
    expect(store.getActions()[1].type).toEqual(expectedActionForStartTime.type);
    expect(store.getActions()[1].payload.mapId).toEqual(
      expectedActionForStartTime.payload.mapId,
    );
    const receivededStartTimeInSeconds =
      store.getActions()[1].payload.animationStartTime.unix() / 1000;
    const expectedTimeInSeconds =
      expectedActionForStartTime.payload.animationStartTime.unix() / 1000;
    expect(receivededStartTimeInSeconds).toBeCloseTo(
      expectedTimeInSeconds,
      numberOfDecimals,
    );

    // Compare action for setting animation end time. Allows 'toBeCloseTo' when comparing 'animationEndTime'.
    expect(store.getActions()[2].type).toEqual(expectedActionForEndTime.type);
    expect(store.getActions()[2].payload.mapId).toEqual(
      expectedActionForEndTime.payload.mapId,
    );
    const receivededEndTimeInSeconds =
      store.getActions()[2].payload.animationEndTime.unix() / 1000;
    const expectedEndTimeInSeconds =
      expectedActionForEndTime.payload.animationEndTime.unix() / 1000;
    expect(receivededEndTimeInSeconds).toBeCloseTo(
      expectedEndTimeInSeconds,
      numberOfDecimals,
    );
  });
});
