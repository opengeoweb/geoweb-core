/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';

import TimeStepButton, {
  getTimeStepValue,
  obtainButtonValues,
} from './TimeStepButton';

describe('src/components/TimeSlider/TimeStepButton', () => {
  const props = {
    timeStep: 5,
    onChangeTimeStep: jest.fn(),
  };
  it('should render the component with text', () => {
    const { container } = render(<TimeStepButton {...props} />);

    const componentText = container.querySelector('div').textContent;
    expect(componentText).toContain('5m');
  });
  it('should toggle a slider on click', () => {
    const { queryByRole } = render(<TimeStepButton {...props} />);
    expect(queryByRole('slider')).toBeFalsy();
    fireEvent.click(queryByRole('button'));
    expect(queryByRole('slider')).toBeTruthy();
    fireEvent.click(queryByRole('button'));
    expect(queryByRole('slider')).toBeFalsy();
  });
  it('should render the slider correctly with 8 marks and first one active', () => {
    const { container, queryByRole } = render(<TimeStepButton {...props} />);

    const buttonText = queryByRole('button').querySelector('span').textContent;
    expect(buttonText).toContain('5m');

    fireEvent.click(queryByRole('button'));
    const activeMark = container.getElementsByClassName('MuiSlider-markActive');
    const marks = container.getElementsByClassName('MuiSlider-mark');
    expect(marks.length === 21).toBeTruthy();
    expect(activeMark.length === 3).toBeTruthy();
    // Confirms that the active mark is the first one
    expect(marks[0].classList[2]).toContain('MuiSlider-markActive');
  });
  it('should render a slider as enabled', () => {
    const { queryByRole } = render(<TimeStepButton {...props} />);
    fireEvent.click(queryByRole('button'));
    expect(queryByRole('slider')).toBeTruthy();

    // slider should be enabled
    expect(
      queryByRole('slider').classList.contains('Mui-disabled'),
    ).toBeFalsy();
  });
  it('should have focus when slider is first rendered', () => {
    const { queryByRole, getByTestId } = render(<TimeStepButton {...props} />);
    fireEvent.click(queryByRole('button'));
    expect(queryByRole('slider')).toBeTruthy();

    expect(
      queryByRole('slider').classList.contains('Mui-disabled'),
    ).toBeFalsy();
    // slider should have focus
    expect(
      getByTestId('timeStepButtonSliderThumb').classList.contains(
        'Mui-focusVisible',
      ),
    ).toBeTruthy();
  });
  it('should render a slider as disabled if passed in as props', () => {
    const props2 = { disabled: true, ...props };
    const { queryByRole, getByTestId } = render(<TimeStepButton {...props2} />);

    // button should be disabled
    expect(
      queryByRole('button').classList.contains('Mui-disabled'),
    ).toBeTruthy();
    expect(getByTestId('timeStepButtonText').getAttribute('fill')).toEqual(
      '#c3c3c3',
    );
  });
  it('timeStepButton slider can be opened and closed and handleChange is called correctly', async () => {
    const { getByTestId, queryByRole } = render(<TimeStepButton {...props} />);
    fireEvent.click(queryByRole('button'));
    const slider = getByTestId('timeStepButtonSlider');
    expect(slider).toBeTruthy();
    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientY: 1 });
    fireEvent.mouseUp(slider);
    expect(props.onChangeTimeStep).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeStep).toHaveBeenCalledWith(5256000);

    fireEvent.mouseDown(slider, { clientY: 0 });
    fireEvent.mouseUp(slider);
    expect(props.onChangeTimeStep).toHaveBeenCalledTimes(2);
    expect(props.onChangeTimeStep).toHaveBeenLastCalledWith(1);
  });
  it('timeStepButton slider can be correctly called with other timeStep than 5', async () => {
    const props2 = {
      mapId: 'map_id',
      timeStep: 2880,
      onChangeTimeStep: jest.fn(),
    };
    const { getByTestId, queryByRole } = render(<TimeStepButton {...props2} />);
    fireEvent.click(queryByRole('button'));
    const slider = getByTestId('timeStepButtonSlider');
    expect(slider).toBeTruthy();

    const activeMark = slider.getElementsByClassName('MuiSlider-markActive');
    const marks = slider.getElementsByClassName('MuiSlider-mark');
    expect(activeMark.length === 13).toBeTruthy();
    // Confirms that the value 7 (48h) is marked active
    expect(marks[12].classList[2]).toContain('MuiSlider-markActive');

    const componentText = getByTestId('timeStepButtonText').textContent;
    expect(componentText).toContain('48h');
  });
  it('timeStepButton slider value can be changed with an arrow up and down key when a ctrl key is pressed', async () => {
    const { getByTestId, queryByRole } = render(<TimeStepButton {...props} />);
    fireEvent.click(queryByRole('button'));
    const slider = getByTestId('timeStepButtonSlider');
    expect(slider).toBeTruthy();
    const thumb = getByTestId('timeStepButtonSliderThumb');
    expect(thumb).toBeTruthy();
    thumb.focus();
    fireEvent.keyDown(thumb, {
      ctrlKey: true,
      key: 'ArrowUp',
      code: 'ArrowUp',
    });
    fireEvent.keyUp(thumb, { key: 'ArrowUp', code: 'ArrowUp' });
    expect(props.onChangeTimeStep).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeStep).toHaveBeenCalledWith(10);
    fireEvent.keyDown(thumb, {
      ctrlKey: true,
      key: 'ArrowDown',
      code: 'ArrowDown',
    });
    fireEvent.keyUp(thumb, { key: 'ArrowDown', code: 'ArrowDown' });
    expect(props.onChangeTimeStep).toHaveBeenCalledTimes(2);
    expect(props.onChangeTimeStep).toHaveBeenCalledWith(2);
  });

  describe('obtainButtonValues', () => {
    it('should return the right value and texts for the passed value', () => {
      expect(obtainButtonValues(1)).toEqual({
        value: 1,
        label: '1 min',
        text: '1m',
      });
      expect(obtainButtonValues(2)).toEqual({
        value: 2,
        label: '2 min',
        text: '2m',
      });
      expect(obtainButtonValues(5)).toEqual({
        value: 3,
        label: '5 min',
        text: '5m',
      });
      expect(obtainButtonValues(10)).toEqual({
        value: 4,
        label: '10 min',
        text: '10m',
      });
      expect(obtainButtonValues(15)).toEqual({
        value: 5,
        label: '15 min',
        text: '15m',
      });
      expect(obtainButtonValues(30)).toEqual({
        value: 6,
        label: '30 min',
        text: '30m',
      });
      expect(obtainButtonValues(60)).toEqual({
        value: 7,
        label: '1 h',
        text: '1h',
      });
      expect(obtainButtonValues(120)).toEqual({
        value: 8,
        label: '2 h',
        text: '2h',
      });
      expect(obtainButtonValues(180)).toEqual({
        value: 9,
        label: '3 h',
        text: '3h',
      });
      expect(obtainButtonValues(360)).toEqual({
        value: 10,
        label: '6 h',
        text: '6h',
      });
      expect(obtainButtonValues(720)).toEqual({
        value: 11,
        label: '12 h',
        text: '12h',
      });
      expect(obtainButtonValues(1440)).toEqual({
        value: 12,
        label: '24 h',
        text: '24h',
      });
      expect(obtainButtonValues(2880)).toEqual({
        value: 13,
        label: '48 h',
        text: '48h',
      });
      expect(obtainButtonValues(4320)).toEqual({
        value: 14,
        label: '72 h',
        text: '72h',
      });
      expect(obtainButtonValues(10080)).toEqual({
        value: 15,
        label: '1 week',
        text: '1w',
      });
      expect(obtainButtonValues(20160)).toEqual({
        value: 16,
        label: '2 weeks',
        text: '2w',
      });
      expect(obtainButtonValues(40320)).toEqual({
        value: 17,
        label: '1 month',
        text: '4w',
      });
      expect(obtainButtonValues(80640)).toEqual({
        value: 18,
        label: '2 months',
        text: '8w',
      });
      expect(obtainButtonValues(241920)).toEqual({
        value: 19,
        label: '6 months',
        text: '24w',
      });
      expect(obtainButtonValues(525600)).toEqual({
        value: 20,
        label: '1 year',
        text: '1y',
      });
      expect(obtainButtonValues(5256000)).toEqual({
        value: 21,
        label: '10 years',
        text: '10y',
      });
    });
  });
  describe('getTimeStepValue', () => {
    it('should return the right value and texts for the passed value', () => {
      expect(getTimeStepValue(1)).toEqual(1);
      expect(getTimeStepValue(2)).toEqual(2);
      expect(getTimeStepValue(3)).toEqual(5);
      expect(getTimeStepValue(4)).toEqual(10);
      expect(getTimeStepValue(5)).toEqual(15);
      expect(getTimeStepValue(6)).toEqual(30);
      expect(getTimeStepValue(7)).toEqual(60);
      expect(getTimeStepValue(8)).toEqual(120);
      expect(getTimeStepValue(9)).toEqual(180);
      expect(getTimeStepValue(10)).toEqual(360);
      expect(getTimeStepValue(11)).toEqual(720);
      expect(getTimeStepValue(12)).toEqual(1440);
      expect(getTimeStepValue(13)).toEqual(2880);
      expect(getTimeStepValue(14)).toEqual(4320);
      expect(getTimeStepValue(15)).toEqual(10080);
      expect(getTimeStepValue(16)).toEqual(20160);
      expect(getTimeStepValue(17)).toEqual(40320);
      expect(getTimeStepValue(18)).toEqual(80640);
      expect(getTimeStepValue(19)).toEqual(241920);
      expect(getTimeStepValue(20)).toEqual(525600);
      expect(getTimeStepValue(21)).toEqual(5256000);
    });
  });
});
