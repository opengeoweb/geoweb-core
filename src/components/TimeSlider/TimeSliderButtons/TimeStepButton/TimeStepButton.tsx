/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  IconButton,
  SvgIcon,
  makeStyles,
  withStyles,
  Slider,
  Tooltip,
  Switch,
  FormControlLabel,
} from '@material-ui/core';
import { getValueFromEvent } from '../SpeedButton/SpeedButton';

import { Layer } from '../../../../store/mapStore/types';
import { getMainLayer } from '../../TimeSliderUtils';

const colors = {
  DARK_GREY: '#484848',
  TIME_STEP_DARK_GRAY: '#404040',
  TIME_STEP_DISABLED_GRAY: '#909090',
  TIME_STEP_DARKER_GRAY: '#767676',
  LIGHT_GRAY: '#c3c3c3',
  TIME_STEP_HEADER_GREY: '#aaaaaa',
  RAIL_LIGHT_BLUE: '#90d0ef',
  BLUE: '#0075a9',
  LIGHT_BLUE: '#d9eaf2',
  MIDDLE_BLUE: '#90d0ef',
  BLACK: '#000',
  TIMESTEP_LIGHT_GREY: '#f8f9f9',
  WHITE: '#ffffff',
};

const useStyles = makeStyles({
  timeStepDiv: {
    width: '58px',
    height: '24px',
  },
  timeStepButton: {
    width: '58px',
    height: '24px',
    color: colors.BLUE,
    backgroundColor: colors.WHITE,
    padding: '1px 0 0 0',
    borderRadius: '4.5px',
    '&&:hover': {
      background: colors.LIGHT_BLUE,
    },
    '&&:disabled': {
      background: colors.WHITE,
    },
  },
  SliderBackground: {
    backgroundColor: colors.TIME_STEP_DARK_GRAY,
    width: '110px',
    height: '350px',
    padding: '0 0 83px 0',
    position: 'relative',
    top: '-500px',
  },
  timestepAutoText: {
    fontFamily: 'Roboto-Black, Roboto',
    fontSize: 14,
    color: colors.TIME_STEP_HEADER_GREY,
    paddingLeft: '3px',
  },
  timestepAutoTextOff: {
    fontFamily: 'Roboto-Black, Roboto',
    fontSize: 14,
    color: colors.WHITE,
    paddingLeft: '3px',
  },
  chosenMarkLabelStyle: {
    fontWeight: 'bold',
    '$disabled &': {
      color: colors.TIME_STEP_DISABLED_GRAY,
    },
  },
  sliderHeader: {
    fontFamily: 'Roboto-Black, Roboto',
    fontSize: 14,
    color: colors.TIME_STEP_HEADER_GREY,
    padding: '4px 0 18px 27px',
  },
  disabled: {},
});

const buttonValues = (
  index,
): { value: number; label: string; text: string } => {
  const markObject = marks.find(markObject0 => markObject0.value === index + 1);
  const buttonValue = {
    value: markObject.value,
    label: markObject.label,
    text: longTexts[markObject.value - 1].text,
  };
  return buttonValue;
};

const longTexts = [
  { text: '1m' },
  { text: '2m' },
  { text: '5m' },
  { text: '10m' },
  { text: '15m' },
  { text: '30m' },
  { text: '1h' },
  { text: '2h' },
  { text: '3h' },
  { text: '6h' },
  { text: '12h' },
  { text: '24h' },
  { text: '48h' },
  { text: '72h' },
  { text: '1w' },
  { text: '2w' },
  { text: '4w' },
  { text: '8w' },
  { text: '24w' },
  { text: '1y' },
  { text: '10y' },
];

const timeStepValues = [
  1,
  2,
  5,
  10,
  15,
  30,
  60,
  120,
  180,
  360,
  720,
  1440,
  2880,
  4320,
  10080,
  20160,
  40320,
  80640,
  241920,
  525600,
  5256000,
];

const marks = [
  {
    value: 1,
    label: '1 min',
  },
  {
    value: 2,
    label: '2 min',
  },
  {
    value: 3,
    label: '5 min',
  },
  {
    value: 4,
    label: '10 min',
  },
  {
    value: 5,
    label: '15 min',
  },
  {
    value: 6,
    label: '30 min',
  },
  {
    value: 7,
    label: '1 h',
  },
  {
    value: 8,
    label: '2 h',
  },
  {
    value: 9,
    label: '3 h',
  },
  {
    value: 10,
    label: '6 h',
  },
  {
    value: 11,
    label: '12 h',
  },
  {
    value: 12,
    label: '24 h',
  },
  {
    value: 13,
    label: '48 h',
  },
  {
    value: 14,
    label: '72 h',
  },
  {
    value: 15,
    label: '1 week',
  },
  {
    value: 16,
    label: '2 weeks',
  },
  {
    value: 17,
    label: '1 month',
  },
  {
    value: 18,
    label: '2 months',
  },
  {
    value: 19,
    label: '6 months',
  },
  {
    value: 20,
    label: '1 year',
  },
  {
    value: 21,
    label: '10 years',
  },
];

const ThumbComponent: React.ElementType<React.HTMLAttributes<
  HTMLSpanElement
>> = props => {
  const ref = React.useRef(null);

  React.useEffect(() => {
    if (ref.current) {
      ref.current.focus();
    }
  }, []);
  return (
    <span
      {...props}
      ref={ref}
      data-testid="timeStepButtonSliderThumb"
      id="thumbTimeStep"
    />
  );
};

const TimeStepSwitch = withStyles({
  root: {
    marginLeft: '18px',
  },
  thumb: {
    width: 20,
    height: 20,
    margin: '0 0 0 0',
  },
  switchBase: {
    '&$checked': {
      color: colors.TIMESTEP_LIGHT_GREY,
    },
    '&$checked + $track': {
      backgroundColor: colors.BLUE,
    },
  },
  input: {
    color: colors.TIMESTEP_LIGHT_GREY,
  },
  checked: {},
  track: {
    backgroundColor: colors.TIME_STEP_DARKER_GRAY,
  },
})(Switch);

const CustomSlider = withStyles({
  root: {
    margin: '0 0 0 13px',
  },
  thumb: {
    width: 20,
    height: 20,
    marginLeft: '-8px !important',
    marginBottom: '-10px !important',
    backgroundColor: colors.BLUE,
    '$disabled &': {
      visibility: 'hidden',
    },
  },
  mark: {
    color: colors.BLUE,
    height: '2px',
    width: '2px',
    marginLeft: '2px',
    '$disabled &': {
      color: colors.TIME_STEP_DISABLED_GRAY,
    },
  },
  markActive: {
    color: colors.MIDDLE_BLUE,
    '$disabled &': {
      color: colors.TIME_STEP_DISABLED_GRAY,
    },
  },
  markLabel: {
    fontFamily: 'Roboto-Black, Roboto',
    fontSize: 9,
    color: colors.WHITE,
    '$disabled &': {
      color: colors.TIME_STEP_DISABLED_GRAY,
    },
  },
  track: {
    borderRadius: '0 0 4.5px 4.5px',
    width: '6px !important',
    backgroundColor: colors.BLUE,
    '$disabled &': {
      backgroundColor: colors.TIME_STEP_DISABLED_GRAY,
    },
  },
  rail: {
    width: '4px !important',
    borderRadius: 4.5,
    backgroundColor: colors.RAIL_LIGHT_BLUE,
    opacity: 0.8,
    '$disabled &': {
      backgroundColor: colors.TIME_STEP_DISABLED_GRAY,
      opacity: 1,
    },
    marginLeft: '1px', // ?
  },
  disabled: {
    // this has to exist, because otherwise '$disabled &' rules above has no effect ???
  },
})(Slider);

export const obtainButtonValues = (
  timeStep: number,
): { value: number; label: string; text: string } => {
  const buttonValuesIndex = timeStepValues.findIndex(
    value0 =>
      timeStepValues.reduce((prev, curr) =>
        Math.abs(curr - timeStep) < Math.abs(prev - timeStep) ? curr : prev,
      ) === value0,
  );
  return buttonValues(buttonValuesIndex);
};

export const getTimeStepValue = (value: number): number => {
  return timeStepValues[Math.round(value - 1)];
};

interface TimeStepProps {
  layers?: Layer[];
  timeStep: number;
  disabled?: boolean;
  isTimestepAuto?: boolean;
  onChangeTimeStep: (scale: number) => void;
  onToggleTimestepAuto?: () => void;
}

const getTimeStepFromDataInterval = (timeInterval): number => {
  switch (timeInterval.isRegularInterval) {
    case false:
      switch (timeInterval.year) {
        case 0: // month
          return 30 * 24 * 60;
        default:
          return timeInterval.year * 365 * 24 * 60;
      }
    case true:
      if (timeInterval.day !== 0) {
        return timeInterval.day * 24 * 60;
      }
      if (timeInterval.hour !== 0) {
        return timeInterval.hour * 60;
      }
      if (timeInterval.minute !== 0) {
        return timeInterval.minute;
      }
      if (timeInterval.second !== 0) {
        return timeInterval.second / 60.0;
      }
      return 5;
    default:
      return 5;
  }
};

const timestepAutoSwitchActive = {
  title: 'Timestep Auto on',
};

const timestepAutoSwitchInactive = {
  title: 'Timestep Auto off',
};

const TimeStepButton: React.FC<TimeStepProps> = ({
  layers,
  timeStep,
  disabled = false,
  isTimestepAuto = false,
  onChangeTimeStep,
  onToggleTimestepAuto = (): void => null,
}: TimeStepProps) => {
  const classes = useStyles();

  const timestepAutoSwitch = !isTimestepAuto
    ? timestepAutoSwitchInactive
    : timestepAutoSwitchActive;

  const timestepAutoTextStyle = !isTimestepAuto
    ? classes.timestepAutoText
    : classes.timestepAutoTextOff;

  const [open, setOpen] = React.useState(false);
  const onClickButton = (): void => {
    setOpen(!open);
  };
  const { value, text } = !isTimestepAuto
    ? obtainButtonValues(timeStep)
    : { value: 0, text: '' };

  const onChangeSlider = (event, newValue): void => {
    getValueFromEvent(event, newValue, getTimeStepValue, onChangeTimeStep);
  };

  const onKeyDown = (event): void => {
    if (event.key === 'Tab') {
      setOpen(!open);
    }
  };

  React.useEffect(() => {
    // to make label of a chosen value of a slider with a bold style
    const activeMarkLabelElements = document.getElementsByClassName(
      'MuiSlider-markLabelActive',
    );
    const chosenMarkLabelElement =
      activeMarkLabelElements[activeMarkLabelElements.length - 1];
    for (
      let index1 = 0;
      index1 < activeMarkLabelElements.length - 1;
      index1 += 1
    ) {
      const { classList: classList0 } = activeMarkLabelElements[index1];
      const removeClassList = [];
      classList0.forEach(function fillRemoveClassList(value0) {
        const result = /([^ ]*)(chosenMarkLabelStyle)([^ ]*)/g.test(value0);
        if (result) {
          removeClassList.push(value0);
        }
      });
      removeClassList.forEach(function removeClass(value1) {
        activeMarkLabelElements[index1].classList.remove(value1);
      });
    }

    if (chosenMarkLabelElement !== undefined) {
      chosenMarkLabelElement.className += ` ${classes.chosenMarkLabelStyle}`;
    }
  });
  const dim =
    isTimestepAuto &&
    getMainLayer(layers) &&
    getMainLayer(layers).dimensions !== undefined
      ? getMainLayer(layers).dimensions[
          getMainLayer(layers).dimensions.findIndex(
            value0 => value0.name === 'time',
          )
        ]
      : undefined;

  const dimTimeInterval =
    dim !== undefined && dim.timeInterval !== undefined
      ? dim.timeInterval
      : null;
  const disabledSlider = disabled || isTimestepAuto;

  React.useEffect(() => {
    if (isTimestepAuto && dim !== undefined) {
      if (dim.timeInterval != null) {
        const newTimeStep = getTimeStepFromDataInterval(dim.timeInterval);
        if (timeStep !== newTimeStep) {
          onChangeTimeStep(newTimeStep);
        }
      } else if (dim.allDates != null && dim.allDates.length > 0) {
        // this part will be specified later
      }
    }
  }, [isTimestepAuto, dimTimeInterval, dim, timeStep, onChangeTimeStep]);
  return (
    <div className={classes.timeStepDiv}>
      <Tooltip title="Time step">
        <span>
          <IconButton
            onClick={onClickButton}
            className={classes.timeStepButton}
            id="timeStepButton"
            data-testid="timeStepButton"
            disableRipple
            disabled={disabled}
          >
            <SvgIcon>
              <path
                fill={disabled ? colors.LIGHT_GRAY : colors.BLUE}
                id="lvnl598lea"
                d="M12.1 5.86c.247 0 .5.089.745.267l6.008 4.316c.368.265.58.65.58 1.057 0 .408-.211.793-.581 1.058l-6.005 4.316c-.248.177-.5.267-.747.267-.466 0-.967-.36-.967-1.147v-3.016l-5.419 3.896c-.207.148-.417.234-.624.26l-.124.007c-.465 0-.966-.36-.966-1.147V7.006c0-.787.5-1.147.966-1.147.247 0 .5.09.746.268l5.421 3.894V7.006c0-.735.436-1.097.873-1.142z"
              />
            </SvgIcon>
            <SvgIcon>
              <text
                data-testid="timeStepButtonText"
                fill={disabled ? colors.LIGHT_GRAY : colors.BLUE}
                fontFamily="Roboto-Black, Roboto"
                fontSize={12}
              >
                {text.length === 3 ? (
                  <tspan x={0} y={16}>
                    {text}
                  </tspan>
                ) : (
                  <tspan x={6} y={16}>
                    {text}
                  </tspan>
                )}
              </text>
            </SvgIcon>
          </IconButton>
        </span>
      </Tooltip>

      {open ? (
        <div className={classes.SliderBackground}>
          <div className={classes.sliderHeader}>Time step</div>
          <CustomSlider
            data-testid="timeStepButtonSlider"
            orientation="vertical"
            value={value}
            step={null}
            min={0}
            max={22}
            marks={marks}
            disabled={disabledSlider}
            ThumbComponent={ThumbComponent}
            onChange={onChangeSlider}
            onKeyDown={onKeyDown}
          />
          <Tooltip title={timestepAutoSwitch.title}>
            <span data-testid="timestepAutoSwitchTooltip">
              <FormControlLabel
                control={
                  <TimeStepSwitch
                    checked={isTimestepAuto}
                    onChange={(): void => onToggleTimestepAuto()}
                    disabled={disabled}
                    id="timestepAutoSwitch"
                    disableRipple
                  />
                }
                label="Auto"
                labelPlacement="end"
                classes={{
                  label: timestepAutoTextStyle,
                }}
              />
            </span>
          </Tooltip>
        </div>
      ) : null}
    </div>
  );
};

export default TimeStepButton;
