/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import * as mapActions from '../../../../store/mapStore/actions';
import TimeStepButton from './TimeStepButton';
import { Layer } from '../../../../store/mapStore/types';
import { AppStore } from '../../../../types/types';
import * as mapSelectors from '../../../../store/mapStore/selectors';

interface ConnectedTimeStepProps {
  timeStep?: number;
  mapId: string;
  layers: Layer[];
  isAnimating: boolean;
  isTimestepAuto: boolean;
  setMapTimeStep?: typeof mapActions.setTimeStep;
  toggleTimestepAuto: typeof mapActions.toggleTimestepAuto;
}

const connectReduxTimeStep = connect(
  (store: AppStore, props: ConnectedTimeStepProps) => ({
    layers: mapSelectors.getMapLayers(store, props.mapId),
    timeStep: mapSelectors.getMapTimeStep(store, props.mapId),
    isAnimating: mapSelectors.isAnimating(store, props.mapId),
    isTimestepAuto: mapSelectors.isTimestepAuto(store, props.mapId),
  }),
  {
    setMapTimeStep: mapActions.setTimeStep,
    toggleTimestepAuto: mapActions.toggleTimestepAuto,
  },
);

const TimeStepButtonComponent: React.FC<ConnectedTimeStepProps> = ({
  timeStep,
  mapId,
  layers,
  isAnimating,
  isTimestepAuto,
  setMapTimeStep,
  toggleTimestepAuto,
}: ConnectedTimeStepProps) => {
  const onToggleTimestepAuto = (): void => {
    toggleTimestepAuto({
      mapId,
      timestepAuto: !isTimestepAuto,
    });
  };
  const onSetTimeStep = (_timestep: number): void => {
    setMapTimeStep({
      mapId,
      timeStep: _timestep,
    });
  };
  return (
    <TimeStepButton
      layers={layers}
      timeStep={timeStep}
      onChangeTimeStep={onSetTimeStep}
      disabled={isAnimating}
      isTimestepAuto={isTimestepAuto}
      onToggleTimestepAuto={onToggleTimestepAuto}
    />
  );
};

/**
 * TimeStepButton component connected to the store displaying a time step of the time slider
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <TimeStepButtonConnect mapId={mapId} />```
 */

const TimeStepButtonConnect = connectReduxTimeStep(TimeStepButtonComponent);
export default TimeStepButtonConnect;
