/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import TimeStepButtonConnect from './TimeStepButtonConnect';

import { mockStateMapWithTimeStepWithoutLayers } from '../../../../utils/testUtils';
import * as mapActionTypes from '../../../../store/mapStore/map/constants';

describe('src/components/TimeSlider/TimeSliderButtons/TimeStepButton/TimeStepButtonConnect', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
  };
  it('should render an enabled time step slider and show a correct time step text', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithTimeStepWithoutLayers(mapId, 5);
    const store = mockStore(mockState);

    const { container, getByTestId } = render(
      <Provider store={store}>
        <TimeStepButtonConnect {...props} />
      </Provider>,
    );
    const timeStepButton = getByRole(container, 'button');
    expect(timeStepButton).toBeTruthy();

    expect(
      getByTestId('timeStepButton').classList.contains('Mui-disabled'),
    ).toBeFalsy();

    expect(
      getByTestId('timeStepButton').querySelector('span').textContent,
    ).toEqual('5m');
  });

  it('timeStepslider renders, when clicked open and should change time step in store', async () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithTimeStepWithoutLayers(mapId, 60);
    const store = mockStore(mockState);
    const { getByTestId } = render(
      <Provider store={store}>
        <TimeStepButtonConnect {...props} />
      </Provider>,
    );
    const button = getByTestId('timeStepButton');
    fireEvent.click(button);
    const slider = getByTestId('timeStepButtonSlider');
    expect(slider).toBeTruthy();
    // a line below moves timeStep value to max = 5256000
    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientY: 1 });
    fireEvent.mouseUp(slider);

    const expectedAction = {
      type: mapActionTypes.WEBMAP_SET_TIME_STEP,
      payload: {
        mapId,
        timeStep: 5256000,
      },
    };

    expect(store.getActions()).toEqual([expectedAction]);

    // a line below moves timeStep value to min = 1
    fireEvent.mouseDown(slider, { clientY: 0 });
    fireEvent.mouseUp(slider);

    const expectedActions2 = [
      {
        type: mapActionTypes.WEBMAP_SET_TIME_STEP,
        payload: {
          mapId,
          timeStep: 5256000,
        },
      },
      {
        type: mapActionTypes.WEBMAP_SET_TIME_STEP,
        payload: {
          mapId,
          timeStep: 1,
        },
      },
    ];

    expect(store.getActions()).toEqual(expectedActions2);
  });
});
