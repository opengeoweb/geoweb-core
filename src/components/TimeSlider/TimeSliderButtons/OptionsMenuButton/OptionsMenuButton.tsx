/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  IconButton,
  makeStyles,
  withStyles,
  Popover,
  Grid,
  Tooltip,
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import AutoUpdateButton from '../AutoUpdateButton/AutoUpdateButton';
import LoopButton from '../LoopButton/LoopButton';
import SpeedButton from '../SpeedButton/SpeedButton';
import TimeStepButton from '../TimeStepButton/TimeStepButton';

const colors = {
  DARK_GREY: '#484848',
  LIGHT_GREY: '#c3c3c3',
  BLUE: '#0075a9',
  LIGHT_BLUE: '#d9eaf2',
  BLACK: '#000',
  WHITE: '#ffffff',
};

const useStyles = makeStyles({
  optionsMenuButton: {
    height: '24px',
    width: '24px',
    color: colors.BLUE,
    backgroundColor: colors.WHITE,
    borderRadius: '4.5px',
    '&&:hover': {
      background: colors.LIGHT_BLUE,
    },
  },
});

const OptionsPopOver = withStyles({
  paper: {
    height: '24px',
    padding: '4px',
    borderRadius: '0',
    overflow: 'visible',
  },
})(Popover);

interface OptionsMenuButtonProps {
  autoUpdateBtn?: React.ReactChild;
  loopBtn?: React.ReactChild;
  speedBtn?: React.ReactChild;
  timeStepBtn?: React.ReactChild;
}

const OptionsMenuButton: React.FC<OptionsMenuButtonProps> = ({
  autoUpdateBtn,
  loopBtn,
  speedBtn,
  timeStepBtn,
}: OptionsMenuButtonProps) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const onClickButton = (event): void => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <div>
      <Tooltip title="Options">
        <IconButton
          onClick={onClickButton}
          className={classes.optionsMenuButton}
          data-testid="optionsMenuButton"
          disableRipple
        >
          <MoreVertIcon data-testid="moreVertIcon" fontSize="small" />
        </IconButton>
      </Tooltip>
      <OptionsPopOver
        id={id}
        open={open}
        data-testid="optionsMenuPopOver"
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <Grid container spacing={1}>
          <Grid item xs="auto">
            {timeStepBtn || (
              <TimeStepButton
                timeStep={0}
                onChangeTimeStep={(): void => null}
              />
            )}
          </Grid>
          <Grid item xs="auto">
            {speedBtn || (
              <SpeedButton
                animationDelay={1000}
                setMapAnimationDelay={(): void => null}
              />
            )}
          </Grid>
          <Grid item xs="auto">
            {loopBtn || <LoopButton />}
          </Grid>
          <Grid item xs="auto">
            {autoUpdateBtn || <AutoUpdateButton />}
          </Grid>
        </Grid>
      </OptionsPopOver>
    </div>
  );
};

export default OptionsMenuButton;
