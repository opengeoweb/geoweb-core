/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, findByRole } from '@testing-library/react';

import OptionsMenuButton from './OptionsMenuButton';

describe('src/components/TimeSlider/OptionsMenuButton', () => {
  it('should render the component with Options Menu Icon', () => {
    const props = {};
    const { queryByTestId } = render(<OptionsMenuButton {...props} />);

    expect(queryByTestId('moreVertIcon')).toBeTruthy();
  });

  it('should open menu when button clicked', () => {
    const props = {};
    const { queryByTestId } = render(<OptionsMenuButton {...props} />);
    expect(queryByTestId('optionsMenuPopOver')).toBeFalsy();
    fireEvent.click(queryByTestId('optionsMenuButton'));
    expect(queryByTestId('optionsMenuPopOver')).toBeTruthy();
  });

  it('should contain correct 4 buttons', () => {
    const props = {};
    const { queryByTestId } = render(<OptionsMenuButton {...props} />);

    expect(queryByTestId('speedButton')).toBeFalsy();
    expect(queryByTestId('timeStepButton')).toBeFalsy();
    expect(queryByTestId('loopButton')).toBeFalsy();
    expect(queryByTestId('autoUpdateButton')).toBeFalsy();
    fireEvent.click(queryByTestId('optionsMenuButton'));
    const popover = queryByTestId('optionsMenuPopOver');
    expect(popover.children.length === 4);
    expect(queryByTestId('speedButton')).toBeTruthy();
    expect(queryByTestId('timeStepButton')).toBeTruthy();
    expect(queryByTestId('loopButton')).toBeTruthy();
    expect(queryByTestId('autoUpdateButton')).toBeTruthy();
  });

  it('should show a proper title in tooltip when hovering', async () => {
    const { getByTestId, container } = render(<OptionsMenuButton />);

    fireEvent.mouseOver(getByTestId('optionsMenuButton'));
    // Wait until tooltip appears
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain('Options');
  });
});
