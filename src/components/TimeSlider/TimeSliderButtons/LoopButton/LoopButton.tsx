/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { IconButton, SvgIcon, makeStyles, Tooltip } from '@material-ui/core';

const colors = {
  DARK_GREY: '#484848',
  LIGHT_GREY: '#c3c3c3',
  BLUE: '#0075a9',
  LIGHT_BLUE: '#d9eaf2',
  BLACK: '#000',
  WHITE: '#ffffff',
};

const useStyles = makeStyles({
  loopButton: {
    height: '24px',
    width: '24px',
    backgroundColor: colors.WHITE,
    borderRadius: '4.5px',
    '&&:hover': {
      background: colors.LIGHT_BLUE,
    },
  },
});

const svgLoopPath = {
  title: 'Loop',
  src:
    'M20.029 10.371c.24-.013.487.078.67.246.176.162.278.38.288.615.001 0 .22 1.941-1.458 3.79-1.124 1.224-2.718 1.844-4.74 1.844H6.96v1.288c0 .43-.284.654-.564.654-.14 0-.277-.05-.41-.148l-2.671-1.948c-.2-.147-.314-.36-.314-.587 0-.229.114-.443.315-.588l2.67-1.95c.134-.096.272-.145.41-.145.28 0 .563.224.563.653v1.116h7.832c1.417 0 2.59-.465 3.393-1.342 1.025-1.13 1.011-2.28.998-2.5-.023-.508.355-.954.842-.998zM17.605 5c.14 0 .277.05.41.148l2.671 1.947c.2.147.314.36.314.587 0 .23-.114.444-.315.589l-2.67 1.949c-.134.096-.272.146-.41.146-.28 0-.563-.224-.563-.654V8.597H9.21c-1.416 0-2.59.465-3.393 1.342-1.025 1.13-1.011 2.329-.999 2.558.024.508-.354.954-.841.998l-.05.002c-.228 0-.454-.09-.626-.248-.176-.163-.278-.38-.288-.614-.002 0-.227-1.993 1.458-3.849 1.124-1.224 2.718-1.845 4.74-1.845h7.83V5.654c0-.429.284-.654.564-.654z',
};

const svgLoopOffPath = {
  title: 'Loop off',
  src:
    'M15.154 3.26l1.827.814-1.278 2.867h1.339V5.654c0-.429.283-.654.563-.654.14 0 .277.05.41.148l2.671 1.947c.2.147.314.36.314.587 0 .23-.114.444-.315.589l-2.67 1.949c-.134.096-.272.146-.41.146-.28 0-.563-.224-.563-.654V8.597h-2.076l-2.944 6.613h2.768c1.328 0 2.442-.408 3.238-1.182l.155-.16c1.025-1.129 1.011-2.279.998-2.499-.023-.508.355-.954.842-.998.247-.013.493.078.676.246.176.162.278.38.288.615.001 0 .22 1.941-1.458 3.79-1.124 1.224-2.718 1.844-4.74 1.844h-3.504l-2.439 5.478-1.827-.813 2.077-4.666H6.958v1.29c0 .429-.283.653-.563.653-.14 0-.277-.05-.41-.148l-2.671-1.948c-.2-.147-.314-.36-.314-.587 0-.229.114-.443.315-.588l2.67-1.95c.134-.096.272-.145.41-.145.28 0 .563.224.563.653v1.116h2.875l2.944-6.614H9.21c-1.322 0-2.432.405-3.228 1.172l-.165.17c-1.025 1.13-1.011 2.329-.999 2.558.024.508-.354.954-.841.998l-.05.002c-.228 0-.454-.09-.626-.248-.176-.163-.278-.38-.288-.614-.002 0-.227-1.993 1.458-3.849 1.124-1.224 2.718-1.845 4.74-1.845h4.303l1.64-3.68z',
};

export interface LoopButtonProps {
  isLooping?: boolean;
  isDisabled?: boolean;
  onToggleLoop?: () => void;
}

const LoopButton: React.FC<LoopButtonProps> = ({
  isLooping = true,
  isDisabled = false,
  onToggleLoop = (): void => null,
}: LoopButtonProps) => {
  const classes = useStyles();
  const loopButtonPath = isLooping ? svgLoopOffPath : svgLoopPath;

  return (
    <Tooltip title={loopButtonPath.title}>
      <span>
        <IconButton
          onClick={(): void => onToggleLoop()}
          disabled={isDisabled}
          className={classes.loopButton}
          data-testid="loopButton"
          disableRipple
        >
          <SvgIcon>
            <rect width={24} height={24} fill="transparent" />
            <path
              data-testid={isLooping ? 'loop-off-svg-path' : 'loop-svg-path'}
              d={loopButtonPath.src}
              fill={colors.BLUE}
            />
          </SvgIcon>
        </IconButton>
      </span>
    </Tooltip>
  );
};

export default LoopButton;
