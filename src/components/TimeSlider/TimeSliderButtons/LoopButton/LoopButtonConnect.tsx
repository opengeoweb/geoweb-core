/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { connect } from 'react-redux';
import React from 'react';
import LoopButton from './LoopButton';
import * as mapActions from '../../../../store/mapStore/actions';
import * as mapSelectors from '../../../../store/mapStore/selectors';
import { AppStore } from '../../../../types/types';

export interface LoopButtonConnectProps {
  mapId: string;
  isLooping?: boolean;
  isDisabled?: boolean;
  mapToggleLoop?: typeof mapActions.toggleLoop;
}

const connectRedux = connect(
  (store: AppStore, props: LoopButtonConnectProps) => ({
    isLooping: mapSelectors.isLooping(store, props.mapId),
  }),
  {
    mapToggleLoop: mapActions.toggleLoop,
  },
);

const LoopButtonConnect: React.FC<LoopButtonConnectProps> = connectRedux(
  ({
    mapId,
    isLooping = true,
    isDisabled = false,
    mapToggleLoop,
  }: LoopButtonConnectProps) => {
    const onMapToggleLoop = (): void => {
      mapToggleLoop({
        mapId,
        shouldLoop: !isLooping,
      });
    };

    return (
      <LoopButton
        isLooping={isLooping}
        isDisabled={isDisabled}
        onToggleLoop={(): void => {
          onMapToggleLoop();
        }}
      />
    );
  },
);

export default LoopButtonConnect;
