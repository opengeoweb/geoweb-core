/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import * as mapActionTypes from '../../../../store/mapStore/map/constants';
import LoopButtonConnect from './LoopButtonConnect';
import { mockStateMapWithTimeStepWithoutLayers } from '../../../../utils/testUtils';

const props = {
  mapId: 'mapid_1',
};

it('should render an enabled loop button', async () => {
  const mockStore = configureStore();
  const mockState = mockStateMapWithTimeStepWithoutLayers(props.mapId, 5);
  const store = mockStore(mockState);

  const { container, getByTestId } = render(
    <Provider store={store}>
      <LoopButtonConnect {...props} />
    </Provider>,
  );
  const loopButton = getByRole(container, 'button');
  expect(loopButton).toBeTruthy();
  expect(
    getByTestId('loopButton').classList.contains('Mui-disabled'),
  ).toBeFalsy();
});

it('should change the loop state from true to false in the store when loop button is clicked,', async () => {
  const mockStore = configureStore();
  const store = mockStore({
    webmap: {
      byId: {
        [props.mapId]: {
          shouldLoop: true,
        },
      },
    },
  });

  const { container } = render(
    <Provider store={store}>
      <LoopButtonConnect {...props} />
    </Provider>,
  );
  const button = getByRole(container, 'button');
  const expectedAction = [
    {
      type: mapActionTypes.WEBMAP_TOGGLE_LOOP,
      payload: {
        mapId: 'mapid_1',
        shouldLoop: false,
      },
    },
  ];
  await fireEvent.click(button);
  expect(store.getActions()).toEqual(expectedAction);
});
