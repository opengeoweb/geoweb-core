/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { findByRole, fireEvent, render } from '@testing-library/react';
import LoopButton from './LoopButton';

describe('src/components/TimeSlider/TimeSliderButtons/LoopButton', () => {
  it('should display a proper default Svg path for icon', () => {
    const { getByTestId } = render(<LoopButton />);

    // Should display "loop off" Svg path by default
    const loopButton = getByTestId('loop-off-svg-path');
    expect(loopButton).toBeTruthy();
  });

  it('should display a "loop" Svg path for icon while not looping', () => {
    const props = {
      isLooping: false,
    };

    const { getByTestId } = render(<LoopButton {...props} />);
    const loopOffButton = getByTestId('loop-svg-path');
    expect(loopOffButton).toBeTruthy();
  });

  it('should show a title text "Loop" in tooltip when hovering while not looping', async () => {
    const props = {
      isLooping: false,
    };
    const { getByTestId, container } = render(<LoopButton {...props} />);

    const loopButton = getByTestId('loop-svg-path');
    expect(loopButton).toBeTruthy();

    fireEvent.mouseOver(loopButton);
    // Wait until tooltip appears
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain('Loop');
  });

  it('should show a title text "Loop off" in tooltip when hovering while looping', async () => {
    const props = {
      isLooping: true,
      onToggleLoop: jest.fn(),
    };
    const { getByTestId, container } = render(<LoopButton {...props} />);

    const loopOffButton = getByTestId('loop-off-svg-path');
    expect(loopOffButton).toBeTruthy();

    fireEvent.mouseOver(loopOffButton);
    // Wait until tooltip appears
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain('Loop off');
  });

  it('should render a loop button as enabled by default', () => {
    const { getByTestId } = render(<LoopButton />);
    fireEvent.click(getByTestId('loopButton'));
    expect(getByTestId('loopButton')).toBeTruthy();

    expect(
      getByTestId('loopButton').classList.contains('Mui-disabled'),
    ).toBeFalsy();
  });

  it('should render button as enabled when it is neither looping nor disabled in props', () => {
    const props = {
      isLooping: false,
      isDisabled: false,
      onToggleLoop: jest.fn(),
    };

    const { getByTestId } = render(<LoopButton {...props} />);

    // Should display a proper Svg path for "loop" icon (default)
    const loopButton = getByTestId('loop-svg-path');
    expect(loopButton).toBeTruthy();

    // An enabled loop button should show up
    expect(loopButton.classList.contains('Mui-disabled')).toBeFalsy();
  });

  it('should render button as disabled when it is not looping and is disabled in props', async () => {
    const props = {
      isLooping: false,
      isDisabled: true,
      onToggleLoop: jest.fn(),
    };

    const { getByTestId } = render(<LoopButton {...props} />);

    // Should display "loop" icon (default)
    const button = getByTestId('loopButton');
    expect(button).toBeTruthy();

    // A disabled loop button should show up
    expect(button.classList.contains('Mui-disabled')).toBeTruthy();
  });

  it('should render button as disabled when it is looping and is disabled in props', async () => {
    const props = {
      isLooping: true,
      isDisabled: true,
      onToggleLoop: jest.fn(),
    };

    const { getByTestId } = render(<LoopButton {...props} />);

    // Should display "loop" icon (default)
    const button = getByTestId('loopButton');
    expect(button).toBeTruthy();

    // A disabled loop button should show up
    expect(button.classList.contains('Mui-disabled')).toBeTruthy();
  });

  it('should render button as enabled when it is looping and not disabled in props', async () => {
    const props = {
      isLooping: true,
      isDisabled: false,
      onToggleLoop: jest.fn(),
    };

    const { getByTestId } = render(<LoopButton {...props} />);

    // Should display "loop" icon (default)
    const button = getByTestId('loopButton');
    expect(button).toBeTruthy();

    // A disabled loop button should show up
    expect(button.classList.contains('Mui-disabled')).toBeFalsy();
  });

  it('should call function in props when clicked while looping', () => {
    const props = {
      isLooping: true,
      onToggleLoop: jest.fn(),
    };

    const { getByTestId } = render(<LoopButton {...props} />);

    // Should display "loop off" Svg path
    const loopOffButton = getByTestId('loop-off-svg-path');
    expect(loopOffButton).toBeTruthy();

    fireEvent.click(loopOffButton);
    expect(props.onToggleLoop).toBeCalled();
  });

  it('should call function in props when clicked while not looping', () => {
    const props = {
      isLooping: false,
      onToggleLoop: jest.fn(),
    };

    const { getByTestId } = render(<LoopButton {...props} />);

    // Should display "loop off" Svg path
    const loopButton = getByTestId('loop-svg-path');
    expect(loopButton).toBeTruthy();

    fireEvent.click(loopButton);
    expect(props.onToggleLoop).toBeCalled();
    expect(props.onToggleLoop).toBeCalled();
  });

  it('should call function in props for each click of loop or loop off button', async () => {
    const props = {
      isLooping: false,
      isDisabled: false,
      onToggleLoop: jest.fn(),
    };

    const { getByTestId } = render(<LoopButton {...props} />);

    // Function in props should have been called once after first click
    const button = getByTestId('loopButton');
    expect(button).toBeTruthy();

    fireEvent.click(button);
    expect(props.onToggleLoop).toHaveBeenCalledTimes(1);

    //  Function in props should have been called once again after second click, making up a total of two calls
    fireEvent.click(button);
    expect(props.onToggleLoop).toHaveBeenCalledTimes(2);
  });
});
