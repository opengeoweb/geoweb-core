/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { IconButton, SvgIcon, makeStyles, Tooltip } from '@material-ui/core';

const colors = {
  DARK_GREY: '#484848',
  LIGHT_GREY: '#c3c3c3',
  BLUE: '#0075a9',
  LIGHT_BLUE: '#d9eaf2',
  BLACK: '#000',
  WHITE: '#ffffff',
};

const useStyles = makeStyles({
  autoUpdateButton: {
    height: '24px',
    width: '24px',
    backgroundColor: colors.WHITE,
    padding: '4px 0 0 1px',
    borderRadius: '4.5px',
    '&&:hover': {
      background: colors.LIGHT_BLUE,
    },
  },
  autoUpdateText: {
    fill: colors.BLUE,
    fontFamily: 'Roboto-Black, Roboto',
    fontSize: '6px',
  },
});

const autoUpdateButtonActive = {
  tranform: 'translate(0.2,1.2) rotate(30 13.454 13.295)',
  title: 'Auto update',
  src:
    'M12.385 3.295l5 3-5 3V7.723l-.085.014c-3.08.548-5.418 3.24-5.418 6.479 0 3.634 2.945 6.58 6.577 6.58 2.914 0 5.385-1.896 6.248-4.523l2.815-1.626c-.225 4.814-4.197 8.648-9.063 8.648-5.012 0-9.074-4.065-9.074-9.08 0-4.65 3.495-8.484 8-9.015V3.295z',
};

const autoUpdateButtonInactive = {
  tranform: 'rotate(30 13.454 13.295)',
  title: 'Auto update off',
  src:
    'M15.172 3.109l.214 2.455 2.792 1.675-2.514 1.508 1.095 12.499c1.763-.729 3.141-2.202 3.742-4.028l2.815-1.626c-.182 3.872-2.787 7.11-6.332 8.228l.193 2.201-1.992.175-.174-1.988c-.25.02-.503.031-.759.031-5.011 0-9.074-4.065-9.074-9.079 0-4.65 3.495-8.484 8-9.016V4.24l.09.054-.088-1.01 1.992-.174zm-1.995 5.559l.149-.023c-3.193.45-5.65 3.196-5.65 6.515 0 3.634 2.944 6.58 6.576 6.58.182 0 .363-.007.54-.021L13.758 9.89l-.579.348V8.668z',
};

interface AutoUpdateButtonProps {
  toggleAutoUpdate?: () => void;
  disabled?: boolean;
  isAutoUpdating?: boolean;
}

const AutoUpdateButton: React.FC<AutoUpdateButtonProps> = ({
  toggleAutoUpdate = (): void => null,
  isAutoUpdating = false,
  disabled = false,
}: AutoUpdateButtonProps) => {
  const classes = useStyles();
  const autoUpdateButton = isAutoUpdating
    ? autoUpdateButtonInactive
    : autoUpdateButtonActive;

  return (
    <Tooltip title={autoUpdateButton.title}>
      <span data-testid="autoUpdateButtonTooltip">
        <IconButton
          data-testid="autoUpdateButton"
          onClick={(): void => toggleAutoUpdate()}
          disabled={disabled}
          className={classes.autoUpdateButton}
          id="autoUpdateButton"
          disableRipple
        >
          <div>
            <SvgIcon>
              <g transform="translate(-843 -496) translate(88 327) translate(397 146) translate(196) translate(32) translate(0 21) translate(127)">
                <rect width={24} height={24} fill="transparent" />
                <path
                  data-testid={
                    isAutoUpdating
                      ? 'auto-update-svg-on'
                      : 'auto-update-svg-off'
                  }
                  d={autoUpdateButton.src}
                  transform={autoUpdateButton.tranform}
                />
                <text
                  fontFamily="Roboto-Black, Roboto"
                  fontWeight={700}
                  fontSize={6}
                >
                  <tspan x={9.2} y={17.3}>
                    AUTO
                  </tspan>
                </text>
              </g>
            </SvgIcon>
          </div>
        </IconButton>
      </span>
    </Tooltip>
  );
};

export default AutoUpdateButton;
