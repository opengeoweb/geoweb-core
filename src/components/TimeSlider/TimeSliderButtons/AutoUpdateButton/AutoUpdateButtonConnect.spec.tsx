/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import AutoUpdateButtonConnect from './AutoUpdateButtonConnect';

import { mockStateMapWithTimeStepWithoutLayers } from '../../../../utils/testUtils';
import * as mapActionTypes from '../../../../store/mapStore/map/constants';

describe('src/components/TimeSlider/TimeSliderButtons/AutoUpdateButton/AutoUpdateButtonConnect.tsx', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
  };
  it('should render an enabled autoupdate button', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithTimeStepWithoutLayers(mapId, 5);
    const store = mockStore(mockState);

    const { container, getByTestId } = render(
      <Provider store={store}>
        <AutoUpdateButtonConnect {...props} />
      </Provider>,
    );
    const autoUpdateButton = getByRole(container, 'button');
    expect(autoUpdateButton).toBeTruthy();
    expect(
      getByTestId('autoUpdateButton').querySelector('span').textContent,
    ).toEqual('AUTO');
    expect(
      getByTestId('autoUpdateButton').classList.contains('Mui-disabled'),
    ).toBeFalsy();
    expect(getByTestId('autoUpdateButtonTooltip').title).toEqual('Auto update');
  });

  it('autoupdate button is clicked, the autoupdate state should change in the store', async () => {
    const mockStore = configureStore();

    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            shouldAutoUpdate: false,
          },
        },
      },
    });

    const { container } = render(
      <Provider store={store}>
        <AutoUpdateButtonConnect {...props} />
      </Provider>,
    );
    const button = getByRole(container, 'button');
    const expectedAction1 = [
      {
        type: mapActionTypes.WEBMAP_TOGGLE_AUTO_UPDATE,
        payload: {
          mapId: 'mapid_1',
          shouldAutoUpdate: true,
        },
      },
    ];
    await fireEvent.click(button);
    expect(store.getActions()).toEqual(expectedAction1);
  });
});
