/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  IconButton,
  SvgIcon,
  makeStyles,
  withStyles,
  Slider,
  Tooltip,
} from '@material-ui/core';

const colors = {
  DARK_GREY: '#484848',
  LIGHT_GREY: '#c3c3c3',
  BLUE: '#0075a9',
  LIGHT_BLUE: '#d9eaf2',
  MIDDLE_BLUE: '#90d0ef',
  BLACK: '#000',
  WHITE: '#ffffff',
};

const useStyles = makeStyles({
  speedButtonDiv: {
    height: '24px',
    width: '24px',
  },
  speedButton: {
    height: '24px',
    width: '24px',
    backgroundColor: colors.WHITE,
    borderRadius: '4.5px',
    '&&:hover': {
      background: colors.LIGHT_BLUE,
    },
  },
  sliderBackground: {
    backgroundColor: 'transparent',
    width: '24px',
    height: '150px',
    position: 'relative',
    top: '-182px',
  },
});

const CustomSlider = withStyles({
  root: {
    margin: '0 0 0 -4px',
  },
  thumb: {
    width: 20,
    height: 20,
    marginLeft: '-7px !important',
    marginBottom: '-10px !important',
    backgroundColor: colors.BLUE,
    '&:hover': {
      boxShadow: '0 0 0 21px rgba(127,186,212,0.1)',
    },
    '&:focus': {
      boxShadow: '0 0 0 21px rgba(127,186,212,0.3)',
    },
  },
  mark: {
    height: '2px',
    width: '2px',
    marginLeft: '2px',
    color: colors.BLUE,
  },
  markActive: {
    color: colors.MIDDLE_BLUE,
  },
  track: {
    borderRadius: '0 0 4.5px 4.5px',
    width: '6px !important',
    backgroundColor: colors.BLUE,
  },
  rail: {
    width: '4px !important',
    borderRadius: 4.5,
    backgroundColor: colors.MIDDLE_BLUE,
    marginLeft: '1px',
    opacity: 1,
  },
})(Slider);

const SliderTooltip = withStyles({
  arrow: {
    color: colors.DARK_GREY,
  },
})(Tooltip);

interface ValueLabelProps {
  children: React.ReactElement;
  value: string;
}

const ValueLabelComponent: React.FC<unknown> = (props: ValueLabelProps) => {
  const { children, value } = props;
  const ref = React.useRef(null);
  React.useEffect(() => {
    if (ref.current) {
      ref.current.focus();
    }
  }, []);
  return (
    <SliderTooltip
      ref={ref}
      placement="right"
      title={value}
      data-testid="speedSliderThumb"
      arrow
    >
      {children}
    </SliderTooltip>
  );
};

export const getValueFromEvent = (
  event: React.KeyboardEvent<HTMLInputElement>,
  newValue: string,
  sliderChangeFunction: (number) => number,
  sliderSetFunction: (number) => void,
): void => {
  if (
    event.type === 'touchstart' ||
    event.type === 'touchmove' ||
    event.type === 'mousedown' ||
    event.type === 'mousemove'
  ) {
    sliderSetFunction(sliderChangeFunction(newValue));
  }
  if (event.ctrlKey || event.metaKey) {
    switch (event.key) {
      case 'ArrowDown':
        sliderSetFunction(sliderChangeFunction(newValue));
        break;
      case 'ArrowUp':
        sliderSetFunction(sliderChangeFunction(newValue));
        break;
      default:
    }
  }
};

export const defaultDelay = 1000; // [ms]
export const warpSpeedFactors = [0.1, 0.2, 0.5, 1, 2, 4, 8, 16]; // Declares available animation speed multipliers for default delay
const markMinPos = 85;
const markMaxPos = 1020;
const markRailBorderRadius = 4.5;
interface SpeedInfoType {
  delay: number;
  markPos: number;
  text: string;
}

const createSpeedInfo = (
  defaultAnimationSpeedDelay: number,
  speedFactors: number[],
  markPixMin: number,
  markPixMax: number,
  markRadius: number,
): SpeedInfoType[] => {
  const markDiam = 2 * markRadius;
  const markPosVerticalSpacing =
    (markPixMax - markPixMin) / (speedFactors.length - 1) - markRadius;

  return speedFactors.map((_, index) => ({
    delay: defaultAnimationSpeedDelay / speedFactors[index],
    markPos: markPixMin + index * markPosVerticalSpacing + markDiam,
    text: `${speedFactors[index]}x`,
  }));
};
interface SpeedButtonProps {
  animationDelay: number;
  setMapAnimationDelay: (delay: number) => void;
}

const SpeedButton: React.FC<SpeedButtonProps> = ({
  animationDelay,
  setMapAnimationDelay,
}: SpeedButtonProps) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const speedInfo = createSpeedInfo(
    defaultDelay,
    warpSpeedFactors,
    markMinPos,
    markMaxPos,
    markRailBorderRadius,
  );

  const obtainButtonValues = (delay: number): SpeedInfoType => {
    return (
      speedInfo.find(elem => delay >= elem.delay) ||
      speedInfo[speedInfo.length - 1]
    );
  };

  const getAnimationDelayValue = (markPos: number): number => {
    return (
      speedInfo.find(elem => markPos <= elem.markPos).delay ||
      speedInfo[0].delay
    );
  };

  const valueLabelFormat = (value): string => {
    const delay = getAnimationDelayValue(value);
    return obtainButtonValues(delay).text;
  };

  const onClickButton = (): void => {
    setOpen(!open);
  };

  const onChangeSlider = (event, newValue): void => {
    getValueFromEvent(
      event,
      newValue,
      getAnimationDelayValue,
      setMapAnimationDelay,
    );
  };

  const onKeyDown = (event): void => {
    if (event.key === 'Tab') {
      setOpen(!open);
    }
  };

  const { markPos, text } = obtainButtonValues(animationDelay);

  const markPositions = speedInfo.map(elem => {
    return { value: elem ? elem.markPos : null };
  });

  return (
    <div className={classes.speedButtonDiv}>
      <Tooltip title="Speed">
        <IconButton
          onClick={onClickButton}
          className={classes.speedButton}
          id="speedButton"
          data-testid="speedButton"
          disableRipple
        >
          <SvgIcon>
            <rect width={24} height={24} fill="transparent" />
            <text
              fill={colors.BLUE}
              fontFamily="Roboto-Black, Roboto"
              fontSize={12}
              fontWeight={400}
            >
              <tspan x={text.length > 2 ? 1 : 5} y={17}>
                {text}
              </tspan>
            </text>
          </SvgIcon>
        </IconButton>
      </Tooltip>
      {open ? (
        <div className={classes.sliderBackground}>
          <CustomSlider
            data-testid="speedSlider"
            orientation="vertical"
            value={markPos}
            step={null}
            min={markMinPos}
            max={markMaxPos}
            marks={markPositions}
            onChange={onChangeSlider}
            onKeyDown={onKeyDown}
            valueLabelDisplay="auto"
            valueLabelFormat={valueLabelFormat}
            ValueLabelComponent={ValueLabelComponent}
          />
        </div>
      ) : null}
    </div>
  );
};

export default SpeedButton;
