/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';

import SpeedButton, { defaultDelay, warpSpeedFactors } from './SpeedButton';

describe('src/components/TimeSlider/SpeedButton', () => {
  const props = {
    animationDelay: defaultDelay,
    setMapAnimationDelay: jest.fn(),
  };
  it('should render the button with text', () => {
    const { getByTestId } = render(<SpeedButton {...props} />);

    const buttonText = getByTestId('speedButton').textContent;
    expect(buttonText).toEqual('1x');
  });

  it('should toggle a slider on click', () => {
    const { getByTestId, queryByRole } = render(<SpeedButton {...props} />);

    expect(queryByRole('slider')).toBeFalsy();
    fireEvent.click(getByTestId('speedButton'));
    expect(queryByRole('slider')).toBeTruthy();
    fireEvent.click(getByTestId('speedButton'));
    expect(queryByRole('slider')).toBeFalsy();
  });

  it('should render the slider correctly with 8 marks and first one active', () => {
    const { container, getByTestId } = render(<SpeedButton {...props} />);

    const buttonText = getByTestId('speedButton').textContent;
    expect(buttonText).toEqual('1x');

    fireEvent.click(getByTestId('speedButton'));
    const marks = container.getElementsByClassName('MuiSlider-mark');
    expect(marks.length === 8).toBeTruthy();
    const activeMarks = container.getElementsByClassName(
      'MuiSlider-markActive',
    );
    expect(activeMarks.length > 0).toBeTruthy();
    // Confirms that the active mark is the first one
    expect(marks[0].classList[2]).toContain('MuiSlider-markActive');
  });

  it('should render a slider as enabled', () => {
    const { getByTestId } = render(<SpeedButton {...props} />);
    fireEvent.click(getByTestId('speedButton'));
    expect(getByTestId('speedSlider')).toBeTruthy();

    // slider should be enabled
    expect(
      getByTestId('speedSlider').classList.contains('Mui-disabled'),
    ).toBeFalsy();
  });

  it('speedButton slider can be opened and closed and handleChange is called correctly', async () => {
    const { getByTestId } = render(<SpeedButton {...props} />);
    fireEvent.click(getByTestId('speedButton'));
    const slider = getByTestId('speedSlider');
    expect(slider).toBeTruthy();
    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientY: 1 });
    fireEvent.mouseUp(slider);
    expect(props.setMapAnimationDelay).toHaveBeenCalledTimes(1);
    expect(props.setMapAnimationDelay).toHaveBeenCalledWith(
      defaultDelay / warpSpeedFactors[warpSpeedFactors.length - 1],
    );

    fireEvent.mouseDown(slider, { clientY: 0 });
    fireEvent.mouseUp(slider);
    expect(props.setMapAnimationDelay).toHaveBeenCalledTimes(2);
    expect(props.setMapAnimationDelay).toHaveBeenLastCalledWith(
      defaultDelay / warpSpeedFactors[0],
    );
  });

  it('speedButton slider can be correctly called with other delay than default delay', async () => {
    const props2 = {
      animationDelay: 250,
      setMapAnimationDelay: jest.fn(),
    };
    const { getByTestId } = render(<SpeedButton {...props2} />);
    fireEvent.click(getByTestId('speedButton'));
    const slider = getByTestId('speedSlider');
    expect(slider).toBeTruthy();

    const activeMarks = slider.getElementsByClassName('MuiSlider-markActive');
    const marks = slider.getElementsByClassName('MuiSlider-mark');
    expect(activeMarks.length > 0).toBeTruthy();
    // Confirms that the value 5 (4x) is marked active
    expect(marks[5].classList[2]).toContain('MuiSlider-markActive');

    const buttonText = getByTestId('speedButton').textContent;
    expect(buttonText).toEqual('4x');
  });
  it('speed slider value can be changed with an arrow up and down key when a ctrl key is pressed', async () => {
    const { getByTestId } = render(<SpeedButton {...props} />);
    fireEvent.click(getByTestId('speedButton'));
    const slider = getByTestId('speedSlider');

    expect(slider).toBeTruthy();
    const thumb = getByTestId('speedSliderThumb');
    expect(thumb).toBeTruthy();
    thumb.focus();
    fireEvent.keyDown(thumb, {
      ctrlKey: true,
      key: 'ArrowUp',
      code: 'ArrowUp',
    });
    fireEvent.keyUp(thumb, { key: 'ArrowUp', code: 'ArrowUp' });
    expect(props.setMapAnimationDelay).toHaveBeenCalledTimes(1);
    expect(props.setMapAnimationDelay).toHaveBeenCalledWith(500);
    fireEvent.keyDown(thumb, {
      ctrlKey: true,
      key: 'ArrowDown',
      code: 'ArrowDown',
    });
    fireEvent.keyUp(thumb, { key: 'ArrowDown', code: 'ArrowDown' });
    expect(props.setMapAnimationDelay).toHaveBeenCalledTimes(2);
    expect(props.setMapAnimationDelay).toHaveBeenCalledWith(500);
    expect(props.setMapAnimationDelay).toHaveBeenCalledWith(2000);
  });
});
