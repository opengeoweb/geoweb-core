/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { connect } from 'react-redux';
import React from 'react';
import { Moment } from 'moment';
import { mapActions, mapSelectors } from '../../../..';
import { AppStore } from '../../../../types/types';
import PlayButton from './PlayButton';

export interface PlayButtonConnectProps {
  mapId: string;
  isAnimating?: boolean;
  animationStartTime?: Moment;
  animationEndTime?: Moment;
  isDisabled?: boolean;
  mapStartAnimation?: typeof mapActions.mapStartAnimation;
  mapStopAnimation?: typeof mapActions.mapStopAnimation;
  timeStep?: number;
}

const connectRedux = connect(
  (store: AppStore, props: PlayButtonConnectProps) => ({
    isAnimating: mapSelectors.isAnimating(store, props.mapId),
    animationStartTime: mapSelectors.getAnimationStartTime(store, props.mapId),
    animationEndTime: mapSelectors.getAnimationEndTime(store, props.mapId),
    timeStep: mapSelectors.getMapTimeStep(store, props.mapId),
  }),
  {
    mapStartAnimation: mapActions.mapStartAnimation,
    mapStopAnimation: mapActions.mapStopAnimation,
  },
);

const PlayButtonConnect: React.FC<PlayButtonConnectProps> = connectRedux(
  ({
    mapId,
    isAnimating,
    animationStartTime,
    animationEndTime,
    isDisabled,
    timeStep,
    mapStartAnimation,
    mapStopAnimation,
  }: PlayButtonConnectProps) => {
    const animationIntervalDefault = 300;
    const animationInterval = timeStep
      ? timeStep * 60
      : animationIntervalDefault;

    const onTogglePlay = (): void => {
      if (isAnimating) {
        mapStopAnimation({ mapId });
      } else {
        mapStartAnimation({
          mapId,
          start: animationStartTime,
          end: animationEndTime,
          interval: animationInterval,
        });
      }
    };

    return (
      <PlayButton
        isAnimating={isAnimating}
        isDisabled={isDisabled}
        onTogglePlayButton={(): void => {
          onTogglePlay();
        }}
      />
    );
  },
);

export default PlayButtonConnect;
