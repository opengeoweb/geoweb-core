/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { IconButton, SvgIcon, makeStyles, Tooltip } from '@material-ui/core';

const colors = {
  DARK_GREY: '#484848',
  LIGHT_GREY: '#c3c3c3',
  BLUE: '#0075a9',
  LIGHT_BLUE: '#d9eaf2',
  BLACK: '#000',
  WHITE: '#ffffff',
};

const useStyles = makeStyles({
  playButton: {
    height: '24px',
    width: '24px',
    backgroundColor: colors.WHITE,
    borderRadius: '4.5px',
    '&&:hover': {
      background: colors.LIGHT_BLUE,
    },
  },
});

const playPath = {
  title: 'Play',
  src:
    'M15.18 12.688l-3.909 2.81c-.161.115-.325.175-.486.175-.303 0-.629-.234-.629-.748v-5.85c0-.514.325-.747.63-.747.16 0 .324.059.485.174l3.91 2.81c.24.172.378.423.378.688 0 .265-.137.517-.379.688M12 4c-4.418 0-8 3.582-8 8s3.582 8 8 8 8-3.582 8-8-3.582-8-8-8',
};

const pausePath = {
  title: 'Pause',
  src:
    'M8.579 4c.865 0 1.565.7 1.565 1.564v12.87c0 .866-.7 1.566-1.565 1.566-.863 0-1.565-.7-1.565-1.565v-4.87H7v-3.13h.014V5.564C7.014 4.7 7.716 4 8.579 4zm6.915 0c.865 0 1.565.7 1.565 1.564v12.871c0 .865-.7 1.565-1.565 1.565-.863 0-1.565-.7-1.565-1.565v-4.87h-.014v-3.13h.014v-4.87C13.93 4.7 14.631 4 15.494 4z',
};

export interface PlayButtonProps {
  isAnimating?: boolean;
  isDisabled?: boolean;
  onTogglePlayButton?: () => void;
}

const PlayButton: React.FC<PlayButtonProps> = ({
  isAnimating,
  isDisabled,
  onTogglePlayButton = (): void => null,
}: PlayButtonProps) => {
  const classes = useStyles();

  const onTogglePlay = (): void => {
    onTogglePlayButton();
  };

  const playButtonPath = isAnimating ? pausePath : playPath;

  return (
    <Tooltip title={playButtonPath.title}>
      <span>
        <IconButton
          onClick={onTogglePlay}
          disabled={isDisabled}
          className={classes.playButton}
          id="playButton"
          data-testid="playButton"
          disableRipple
        >
          <SvgIcon>
            <rect width={24} height={24} fill="transparent" />
            <path
              d={playButtonPath.src}
              fill={colors.BLUE}
              fillRule="evenodd"
              data-testid={isAnimating ? 'pause-svg-path' : 'play-svg-path'}
            />
          </SvgIcon>
        </IconButton>
      </span>
    </Tooltip>
  );
};

export default PlayButton;
