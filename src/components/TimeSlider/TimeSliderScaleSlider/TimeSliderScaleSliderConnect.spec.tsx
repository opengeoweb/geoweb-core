/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, getByRole } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import TimeSliderScaleSliderConnect from './TimeSliderScaleSliderConnect';
import { mockStateMapWithTimeSliderScaleWithoutLayers } from '../../../utils/testUtils';

import * as mapActionTypes from '../../../store/mapStore/map/constants';

describe('src/components/TimeSlider/TimeSliderScaleSlider/TimeSliderScaleSliderConnect', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
  };

  it('should render an enabled slider and show a correct scale text', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithTimeSliderScaleWithoutLayers(mapId, 60);
    const store = mockStore(mockState);

    const { container, getByTestId } = render(
      <Provider store={store}>
        <TimeSliderScaleSliderConnect {...props} />
      </Provider>,
    );
    const slider = getByRole(container, 'slider');
    expect(slider).toBeTruthy();

    expect(
      getByTestId('scaleSlider').classList.contains('Mui-disabled'),
    ).toBeFalsy();

    expect(getByTestId('scaleSlider').querySelector('div').textContent).toEqual(
      '1 h',
    );
  });

  it('timeSliderScaleSlider renders and should change slider scale in store', async () => {
    const mockStore = configureStore();

    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            timeSliderScale: 60,
          },
        },
      },
    });

    const { getByTestId } = render(
      <Provider store={store}>
        <TimeSliderScaleSliderConnect {...props} />
      </Provider>,
    );

    const slider = getByTestId('scaleSliderSlider');
    expect(slider).toBeTruthy();
    // a line below moves a scale slider value to max = 10080
    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientX: 1 });
    fireEvent.mouseUp(slider);

    const expectedAction = {
      type: mapActionTypes.WEBMAP_SET_TIME_SLIDER_SCALE,
      payload: {
        mapId,
        timeSliderScale: 10080,
      },
    };

    expect(store.getActions()).toEqual([expectedAction]);

    // a line below moves a scale value to min = 1
    fireEvent.mouseDown(slider, { clientX: 0 });
    fireEvent.mouseUp(slider);

    const expectedActions2 = [
      {
        type: mapActionTypes.WEBMAP_SET_TIME_SLIDER_SCALE,
        payload: {
          mapId,
          timeSliderScale: 10080,
        },
      },
      {
        type: mapActionTypes.WEBMAP_SET_TIME_SLIDER_SCALE,
        payload: {
          mapId,
          timeSliderScale: 1,
        },
      },
    ];

    expect(store.getActions()).toEqual(expectedActions2);
  });
});
