/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  withStyles,
  makeStyles,
  Slider,
  SvgIcon,
  useMediaQuery,
} from '@material-ui/core';

const scaleColors = {
  DARK_GREY: '#484848',
  MIDDLE_GREY: '#bfbfbf',
  LIGHT_GREY: '#c3c3c3',
  NEEDLE_GREY: '#9d9d9d',
  BLACK: '#000',
  WHITE: '#ffffff',
};

const scaleSliderOtherSettings = {
  LEFT_PADDING: '15%',
  LEFT_PADDING_SHORT: '6px',
};

const useStyles = makeStyles({
  Background: {
    backgroundColor: scaleColors.DARK_GREY,
    width: '210px',
    height: '40px',
    paddingTop: '10px',
  },
  BackgroundShort: {
    backgroundColor: scaleColors.DARK_GREY,
    width: '100%',
    height: '40px',
    paddingTop: '10px',
  },
  ScaleText: {
    width: '62px',
    height: '30px',
    padding: '7px 10px 0 8px',
    color: scaleColors.LIGHT_GREY,
    backgroundColor: scaleColors.DARK_GREY,
    float: 'right',
    font: '12px Roboto',
  },
  ScaleTextShort: {
    width: '30px',
    height: '30px',
    padding: '7px 10px 0 4px',
    color: scaleColors.LIGHT_GREY,
    backgroundColor: scaleColors.DARK_GREY,
    float: 'right',
    font: '12px Roboto',
  },
});

const thumbComponent: React.FC<unknown> = props => {
  return (
    // eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex
    <span {...props} data-testid="scaleSliderSliderThumb" tabIndex={0}>
      <SvgIcon>
        <polygon
          points="6,0 11,5 11,12 0,12 0,5"
          fill={scaleColors.NEEDLE_GREY}
        />
      </SvgIcon>
    </span>
  );
};

const marks = [
  {
    value: 0.2,
  },
  {
    value: 0.5,
  },
  {
    value: 1,
  },
  {
    value: 2,
  },
  {
    value: 3,
  },
];

const valueLabelFormat = (value: number): string => {
  const valueNew = value.toString();
  return valueNew;
};

const GeoWebScaleSlider = withStyles({
  root: {
    height: 4,
    width: 88,
    margin: `0 0 18px ${scaleSliderOtherSettings.LEFT_PADDING}`,
  },
  thumb: {
    height: 0,
    width: 0,
    marginTop: 12,
    marginLeft: 7,
  },
  active: {},
  valueLabel: {
    left: '20px',
  },
  track: {
    backgroundColor: 'rgba(0, 0, 0, 0)',
    height: 4,
    borderRadius: 4.5,
  },
  rail: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    height: 4,
    width: 88,
    borderRadius: 4.5,
  },
  mark: {
    backgroundColor: scaleColors.NEEDLE_GREY,
    height: 6,
    width: 2,
    marginTop: -9,
  },
  '@media (max-width: 456px)': {
    root: {
      width: 58,
      margin: `0 0 18px ${scaleSliderOtherSettings.LEFT_PADDING_SHORT}`,
    },
    rail: {
      width: 58,
    },
  },
})(Slider);

const obtainScaleValues = (
  timeSliderScale,
): { value: number; text: string; shortText: string } => {
  if (timeSliderScale === 1) {
    return { value: 0.2, text: '1 min scale', shortText: '1 min' };
  }
  if (timeSliderScale === 5) {
    return { value: 0.5, text: '5 min scale', shortText: '5 min' };
  }
  if (timeSliderScale === 60) {
    return { value: 1, text: '1 h scale', shortText: '1 h' };
  }
  if (timeSliderScale === 180) {
    return { value: 2, text: '3 h scale', shortText: '3 h' };
  }
  if (timeSliderScale === 10080) {
    return { value: 3, text: '1 w scale', shortText: '1 w' };
  }
  return { value: 1, text: '1 h scale', shortText: '1 h' };
};

const getScaleSliderValue = (value): number => {
  if (Math.abs(value - 0.2) < 0.0001) {
    return 1;
  }
  if (Math.abs(value - 0.5) < 0.0001) {
    return 5;
  }
  if (Math.abs(value - 1) < 0.0001) {
    return 60;
  }
  if (Math.abs(value - 2) < 0.0001) {
    return 180;
  }
  return 10080;
};

interface TimeSliderScaleSliderProps {
  timeSliderScale: number;
  onChangeTimeSliderScale: (scale: number) => void;
}

const TimeSliderScaleSlider: React.FC<TimeSliderScaleSliderProps> = ({
  timeSliderScale,
  onChangeTimeSliderScale,
}: TimeSliderScaleSliderProps) => {
  const classes = useStyles();
  const matches = useMediaQuery('(min-width:600px)');
  const classNameBackground = matches
    ? classes.Background
    : classes.BackgroundShort;

  const handleChange = (event, newValue): void => {
    onChangeTimeSliderScale(getScaleSliderValue(newValue));
  };

  const { value, text, shortText } = obtainScaleValues(timeSliderScale);
  return (
    <div className={classNameBackground} data-testid="scaleSlider">
      <GeoWebScaleSlider
        data-testid="scaleSliderSlider"
        defaultValue={1}
        value={value}
        min={0}
        step={null}
        max={3}
        getAriaValueText={valueLabelFormat}
        onChange={handleChange}
        valueLabelDisplay="auto"
        aria-labelledby="scale-slider"
        ThumbComponent={thumbComponent}
        marks={marks}
      />
      {matches ? (
        <div data-testid="scaleSliderText" className={classes.ScaleText}>
          {text}
        </div>
      ) : (
        <div data-testid="scaleSliderText" className={classes.ScaleTextShort}>
          {shortText}
        </div>
      )}
    </div>
  );
};

export default TimeSliderScaleSlider;
