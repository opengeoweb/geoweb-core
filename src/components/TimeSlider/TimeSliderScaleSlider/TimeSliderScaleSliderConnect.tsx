/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import * as mapActions from '../../../store/mapStore/actions';
import TimeSliderScaleSlider from './TimeSliderScaleSlider';
import { AppStore } from '../../../types/types';
import * as mapSelectors from '../../../store/mapStore/selectors';

interface ConnectedScaleSliderProps {
  timeSliderScale?: number;
  mapId: string;
  mapSetTimeSliderScale?: typeof mapActions.setTimeSliderScale;
}

const connectReduxTimeSliderScaleSlider = connect(
  (store: AppStore, props: ConnectedScaleSliderProps) => ({
    timeSliderScale: mapSelectors.getMapTimeSliderScale(store, props.mapId),
  }),
  {
    mapSetTimeSliderScale: mapActions.setTimeSliderScale,
  },
);

const TimeSliderScaleSliderComponent: React.FC<ConnectedScaleSliderProps> = ({
  timeSliderScale,
  mapId,
  mapSetTimeSliderScale,
}: ConnectedScaleSliderProps) => {
  const onSetTimeSliderScale = (_scale: number): void => {
    mapSetTimeSliderScale({
      mapId,
      timeSliderScale: _scale,
    });
  };

  return (
    <TimeSliderScaleSlider
      timeSliderScale={timeSliderScale}
      onChangeTimeSliderScale={onSetTimeSliderScale}
    />
  );
};

/**
 * TimeSliderScaleSlider component connected to the store displaying a scale slider of the time slider
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <TimeSliderScaleSliderConnect mapId={mapId} />```
 */

const TimeSliderScaleSliderConnect = connectReduxTimeSliderScaleSlider(
  TimeSliderScaleSliderComponent,
);

export default TimeSliderScaleSliderConnect;
