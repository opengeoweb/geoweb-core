/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';

import matchMediaPolyfill from 'mq-polyfill';
import TimeSliderScaleSlider from './TimeSliderScaleSlider';

describe('src/components/TimeSlider/TimeSliderScaleSlider', () => {
  const props = {
    timeSliderScale: 5,
    onChangeTimeSliderScale: jest.fn(),
  };

  it('should at start in tests render the component with a short scale text', () => {
    const { container } = render(<TimeSliderScaleSlider {...props} />);

    const componentText = container.querySelector('div').textContent;
    expect(componentText).toContain('5 min');
  });

  it('should render the component with a long scale text, when a width is 600', () => {
    matchMediaPolyfill(window);
    window.resizeTo = function resizeTo(width, height): void {
      Object.assign(this, {
        innerWidth: width,
        innerHeight: height,
        outerWidth: width,
        outerHeight: height,
      }).dispatchEvent(new this.Event('resize'));
    };
    window.resizeTo(600, 50);
    const { container } = render(<TimeSliderScaleSlider {...props} />);

    const componentText = container.querySelector('div').textContent;
    expect(componentText).toContain('5 min scale');
  });
  it('should render the component with a short scale text, when a width is 599', () => {
    matchMediaPolyfill(window);
    window.resizeTo = function resizeTo(width, height): void {
      Object.assign(this, {
        innerWidth: width,
        innerHeight: height,
        outerWidth: width,
        outerHeight: height,
      }).dispatchEvent(new this.Event('resize'));
    };
    window.resizeTo(599, 50);
    const { container } = render(<TimeSliderScaleSlider {...props} />);

    const componentText = container.querySelector('div').textContent;
    expect(componentText).toContain('5 min');
  });

  it('should render a slider as enabled', () => {
    const { getByRole, getByTestId } = render(
      <TimeSliderScaleSlider {...props} />,
    );
    expect(getByRole('slider')).toBeTruthy();

    // slider should be enabled
    expect(
      getByTestId('scaleSlider').classList.contains('Mui-disabled'),
    ).toBeFalsy();
  });

  it('should render the slider correctly with 5 marks and the second value also as active', () => {
    const { container } = render(<TimeSliderScaleSlider {...props} />);

    const activeMark = container.getElementsByClassName('MuiSlider-markActive');
    const marks = container.getElementsByClassName('MuiSlider-mark');
    expect(marks.length === 5).toBeTruthy();
    expect(activeMark.length === 2).toBeTruthy();
    // Confirms that the value 0.5 (5 min) is marked active
    expect(marks[1].classList[2]).toContain('MuiSlider-markActive');
  });

  it('handleChange is called correctly', async () => {
    const { getByTestId } = render(<TimeSliderScaleSlider {...props} />);
    const slider = getByTestId('scaleSliderSlider');
    expect(slider).toBeTruthy();
    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientX: 1 });
    fireEvent.mouseUp(slider);
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledWith(10080);

    fireEvent.mouseDown(slider, { clientX: 0 });
    fireEvent.mouseUp(slider);
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledTimes(2);
    expect(props.onChangeTimeSliderScale).toHaveBeenLastCalledWith(1);
  });

  it('scale slider can be correctly called with other scale value than 5', async () => {
    const props2 = {
      mapId: 'map_id',
      timeSliderScale: 10080,
      onChangeTimeSliderScale: jest.fn(),
    };
    const { getByTestId } = render(<TimeSliderScaleSlider {...props2} />);
    const slider = getByTestId('scaleSliderSlider');
    expect(slider).toBeTruthy();

    const activeMark = slider.getElementsByClassName('MuiSlider-markActive');
    const marks = slider.getElementsByClassName('MuiSlider-mark');
    expect(activeMark.length === 5).toBeTruthy();
    // Confirms that the value 4 (1 w) is marked active
    expect(marks[4].classList[2]).toContain('MuiSlider-markActive');

    const componentText = getByTestId('scaleSliderText').textContent;
    expect(componentText).toContain('1 w');
  });
  it('scale slider value can be changed with an arrow right key', async () => {
    const { getByTestId } = render(<TimeSliderScaleSlider {...props} />);
    const slider = getByTestId('scaleSliderSlider');

    expect(slider).toBeTruthy();
    const thumb = getByTestId('scaleSliderSliderThumb');
    expect(thumb).toBeTruthy();
    thumb.focus();
    fireEvent.keyDown(thumb, { key: 'ArrowRight', code: 'ArrowRight' });
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledWith(60);
  });
  it('scale slider value can be changed with an arrow left key', async () => {
    const { getByTestId } = render(<TimeSliderScaleSlider {...props} />);
    const slider = getByTestId('scaleSliderSlider');

    expect(slider).toBeTruthy();
    const thumb = getByTestId('scaleSliderSliderThumb');
    expect(thumb).toBeTruthy();
    thumb.focus();
    fireEvent.keyDown(thumb, { key: 'ArrowLeft', code: 'ArrowLeft' });
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledWith(1);
  });
});
