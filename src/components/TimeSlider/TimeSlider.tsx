/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import moment from 'moment';
import { makeStyles, Grid } from '@material-ui/core';

import TimeSliderButtons from './TimeSliderButtons/TimeSliderButtons';
import TimeSliderLegend from './TimeSliderLegend/TimeSliderLegend';
import TimeSliderScaleSlider from './TimeSliderScaleSlider/TimeSliderScaleSlider';
import TimeSliderRail from './TimeSliderRail/TimeSliderRail';

const useStyles = makeStyles({
  container: {
    display: 'grid',
    gridTemplateColumns: 'minmax(100px, 210px) minmax(66%, 1fr)',
    gridTemplateRows: '50px',
  },
  scaleSlider: {
    height: '50px',
  },
});

const useTimeSlider = (): boolean => {
  const [openTimeSlider, setOpenTimeSlider] = React.useState(true);

  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent): void => {
      const target = event.target as Element;
      if (target.tagName !== 'INPUT' && event.key === 't') {
        setOpenTimeSlider(!openTimeSlider);
      }
    };

    document.addEventListener('keydown', handleKeyDown);
    return (): void => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [openTimeSlider]);

  return openTimeSlider;
};

interface TimeSliderProps {
  buttons?: React.ReactChild;
  rail?: React.ReactChild;
  scaleSlider?: React.ReactChild;
  legend?: React.ReactChild;
}

const defaultProps = {
  centerTime: moment
    .utc()
    .startOf('day')
    .hours(12)
    .unix(),
  secondsPerPx: 5,
  selectedTime: moment
    .utc()
    .startOf('day')
    .hours(12),
};

const TimeSlider: React.FC<TimeSliderProps> = ({
  buttons,
  rail,
  scaleSlider,
  legend,
}: TimeSliderProps) => {
  const styles = useStyles();
  const openTimeSlider = useTimeSlider();
  return openTimeSlider ? (
    <Grid container className={styles.container} data-testid="timeSlider">
      <Grid item> {buttons || <TimeSliderButtons />} </Grid>
      <Grid item> {rail || <TimeSliderRail {...defaultProps} />} </Grid>
      <Grid item className={styles.scaleSlider}>
        {scaleSlider || (
          <TimeSliderScaleSlider
            timeSliderScale={0}
            onChangeTimeSliderScale={(): void => null}
          />
        )}
      </Grid>
      <Grid item>
        {legend || (
          <TimeSliderLegend
            {...defaultProps}
            selectedTime={defaultProps.selectedTime.unix()}
          />
        )}{' '}
      </Grid>
    </Grid>
  ) : null;
};

export default TimeSlider;
