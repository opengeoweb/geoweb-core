/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import moment from 'moment';
import { Dimension, Layer } from '../../../store/mapStore/types';
import { AppStore } from '../../../types/types';
import * as mapSelectors from '../../../store/mapStore/selectors';
import * as mapActions from '../../../store/mapStore/actions';

import TimeSliderLegend from './TimeSliderLegend';
import {
  getDataLimitsFromLayers,
  getTimeBounds,
  TimeBounds,
} from '../TimeSliderUtils';

interface TimeSliderLegendConnectProps {
  mapId: string;
  dimensions?: Dimension[];
  scale?: number;
  centerTime?: number;
  secondsPerPx?: number;
  layers?: Layer[];
  isAnimating: boolean;
  timeStep?: number;
  isTimeSliderHoverOn: boolean;
  stopMapAnimation: typeof mapActions.mapStopAnimation;
  mapChangeDimension: typeof mapActions.mapChangeDimension;
  mapSetTimeSliderCenterTime?: typeof mapActions.setTimeSliderCenterTime;
}

const connectRedux = connect(
  (store: AppStore, props: TimeSliderLegendConnectProps) => ({
    layers: mapSelectors.getMapLayers(store, props.mapId),
    dimensions: mapSelectors.getMapDimensions(store, props.mapId),
    scale: mapSelectors.getMapTimeSliderScale(store, props.mapId),
    centerTime: mapSelectors.getMapTimeSliderCenterTime(store, props.mapId),
    secondsPerPx: mapSelectors.getMapTimeSliderSecondsPerPx(store, props.mapId),
    isAnimating: mapSelectors.isAnimating(store, props.mapId),
    timeStep: mapSelectors.getMapTimeStep(store, props.mapId),
    isTimeSliderHoverOn: mapSelectors.isTimeSliderHoverOn(store, props.mapId),
  }),
  {
    stopMapAnimation: mapActions.mapStopAnimation,
    mapChangeDimension: mapActions.mapChangeDimension,
    mapSetTimeSliderCenterTime: mapActions.setTimeSliderCenterTime,
  },
);

const TimeSliderLegendConnect: React.FC<TimeSliderLegendConnectProps> = ({
  mapId,
  dimensions,
  scale,
  centerTime,
  secondsPerPx,
  layers,
  isAnimating,
  timeStep,
  isTimeSliderHoverOn,
  stopMapAnimation,
  mapChangeDimension,
  mapSetTimeSliderCenterTime,
}: TimeSliderLegendConnectProps) => {
  const { selectedTime }: TimeBounds = getTimeBounds(dimensions);
  const currentTime = moment.utc().unix();
  const [dataStartTime, dataEndTime] = getDataLimitsFromLayers(layers);

  return (
    <TimeSliderLegend
      centerTime={centerTime}
      secondsPerPx={secondsPerPx}
      selectedTime={selectedTime}
      scale={scale}
      currentTime={currentTime}
      isTimeSliderHoverOn={isTimeSliderHoverOn}
      dataStartTime={dataStartTime}
      dataEndTime={dataEndTime}
      timeStep={timeStep}
      onSetNewDate={(newDate): void => {
        if (isAnimating) {
          stopMapAnimation({ mapId });
        }
        mapChangeDimension({
          origin,
          mapId,
          dimension: { name: 'time', currentValue: newDate },
        });
      }}
      onSetCenterTime={(newTime: number): void => {
        mapSetTimeSliderCenterTime({ mapId, timeSliderCenterTime: newTime });
      }}
    />
  );
};

/**
 * TimeSliderLegend component connected to the store displaying a legend of the time slider
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <TimeSliderLegendConnect mapId={mapId} />```
 */

export default connectRedux(TimeSliderLegendConnect);
