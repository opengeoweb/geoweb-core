/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import { pixelToTimestamp, timestampToPixelEdges } from '../TimeSliderUtils';

const legendColors = {
  LIGHT_GREY: '#484848',
  WHITE: '#ffffff',
  YELLOW: '#ffa800',
};

const drawNeedle = (
  context: CanvasRenderingContext2D,
  width: number,
  height: number,
  startTime: number,
  endTime: number,
  selectedTime: number,
): void => {
  const ctx = context;
  const selected = selectedTime;
  const selectedPx = selected
    ? ((selected - startTime) * width) / (endTime - startTime)
    : 0;

  // Draw a high vertical line (needle) indicating the current or selected time
  ctx.lineWidth = 1;
  ctx.strokeStyle = legendColors.WHITE;
  ctx.beginPath();
  ctx.moveTo(selectedPx, height - 50);
  ctx.lineTo(selectedPx, height);
  ctx.stroke();
};

const drawCurrentTime = (
  context: CanvasRenderingContext2D,
  width: number,
  height: number,
  startTime: number,
  endTime: number,
  currentTime: number,
): void => {
  const ctx = context;
  const current = currentTime;
  const currentPx = current
    ? timestampToPixelEdges(current, startTime, endTime, width)
    : 0;

  // Draw a high vertical line (needle) indicating the current or selected time
  ctx.lineWidth = 1;
  ctx.strokeStyle = legendColors.YELLOW;
  ctx.beginPath();
  ctx.moveTo(currentPx, height - 50);
  ctx.lineTo(currentPx, height);
  ctx.stroke();
};

const drawTimeScale = (
  context: CanvasRenderingContext2D,
  newStartTime: number,
  newEndTime: number,
  stepLength: number,
  canvasWidth: number,
  height: number,
  scale: number,
): void => {
  const roundedStart =
    Math.ceil(newStartTime / (60 * stepLength)) * 60 * stepLength;
  const ctx = context;
  const start = roundedStart / 60 / stepLength;
  const end = newEndTime / 60 / stepLength;
  const timeAtTimeStep = moment.utc(roundedStart * 1000);
  for (let i = start; i < end; i += 1) {
    // Draw a short vertical line indicating the start of the time step
    const stepPx = timestampToPixelEdges(
      i * 60 * stepLength,
      newStartTime,
      newEndTime,
      canvasWidth,
    );
    ctx.strokeStyle = legendColors.WHITE;
    ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.moveTo(stepPx, height - 4);
    ctx.lineTo(stepPx, height);
    ctx.stroke();

    // Draw the time step time as text
    const timeFormat = scale > 5 ? 'H' : 'HH:mm';
    const timeText = timeAtTimeStep
      .utc()
      .format(timeFormat)
      .toString();
    ctx.fillStyle = legendColors.WHITE;
    ctx.font = '14px Roboto';
    ctx.textAlign = 'center';
    ctx.fillText(timeText, stepPx, height - 6);

    const isDateChanged = timeText === '0' || timeText === '00:00';
    const isFirstTimeStep = i === start;
    if (isFirstTimeStep || isDateChanged) {
      // Draw the day of the week and the day of the month as text
      const dateFormat = 'dddd D';
      const newDate = timeAtTimeStep
        .utc()
        .format(dateFormat)
        .toString();
      ctx.textAlign = 'left';
      ctx.fillText(`${newDate}`, isFirstTimeStep ? 0 : stepPx + 6, height - 30);
    }
    if (isDateChanged) {
      // Draw high vertical line indicating a date change
      ctx.strokeStyle = legendColors.WHITE;
      ctx.lineWidth = 1;
      ctx.beginPath();
      ctx.moveTo(stepPx, height - 40);
      ctx.lineTo(stepPx, height);
      ctx.stroke();
    }
    timeAtTimeStep.add(stepLength, 'm');
  }
};

const drawLegend = (
  context: CanvasRenderingContext2D,
  canvasWidth: number,
  height: number,
  centerTime: number,
  secondsPerPx: number,
  selectedTimeUnix: number,
  scale: number,
  currentTimeUnix: number,
): void => {
  const ctx = context;
  const stepLength = scale === 10080 ? 180 : scale;

  const [newStartTime, newEndTime] = [0, canvasWidth].map(px =>
    pixelToTimestamp(px, centerTime, canvasWidth, secondsPerPx),
  );

  drawTimeScale(
    ctx,
    newStartTime,
    newEndTime,
    stepLength,
    canvasWidth,
    height,
    scale,
  );
  drawNeedle(
    ctx,
    canvasWidth,
    height,
    newStartTime,
    newEndTime,
    selectedTimeUnix,
  );
  drawCurrentTime(
    ctx,
    canvasWidth,
    height,
    newStartTime,
    newEndTime,
    currentTimeUnix,
  );
};

export const renderTimeSliderLegend = (
  context: CanvasRenderingContext2D,
  width: number,
  height: number,
  centerTime: number,
  secondsPerPx: number,
  selectedTime: number,
  scale: number,
  currentTime: number,
): void => {
  const ctx = context;
  // Set the background color of the canvas
  ctx.fillStyle = legendColors.LIGHT_GREY;
  ctx.fillRect(0, 0, width, height);
  drawLegend(
    ctx,
    width,
    height,
    centerTime,
    secondsPerPx,
    selectedTime,
    scale,
    currentTime,
  );
};
