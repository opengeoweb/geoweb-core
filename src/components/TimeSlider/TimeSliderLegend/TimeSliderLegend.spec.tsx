/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import moment from 'moment';

import TimeSliderLegend from './TimeSliderLegend';

describe('src/components/TimeSlider/TimeSliderLegend', () => {
  it('should render the component with canvas', () => {
    const date = moment.utc('20200101', 'YYYYMMDD');
    const props = {
      scale: 5,
      timeStep: 5,
      centerTime: moment
        .utc(date)
        .startOf('day')
        .hours(12)
        .unix(),
      secondsPerPx: 5,
    };
    const { container } = render(<TimeSliderLegend {...props} />);

    expect(container.querySelector('canvas')).toBeTruthy();
  });
});
