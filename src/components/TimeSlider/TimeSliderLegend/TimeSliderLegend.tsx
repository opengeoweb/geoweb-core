/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { makeStyles } from '@material-ui/core';

import { renderTimeSliderLegend } from './TimeSliderLegendRenderFunctions';
import CanvasComponent from '../../CanvasComponent/CanvasComponent';
import { setNewRoundedTime } from '../TimeSliderUtils';

const useStyles = makeStyles({
  timeSliderLegend: {
    height: '50px',
  },
});

interface TimeSliderLegendProps {
  centerTime: number;
  secondsPerPx: number;
  selectedTime?: number;
  scale?: number;
  currentTime?: number;
  dataStartTime?: number;
  dataEndTime?: number;
  isTimeSliderHoverOn?: boolean;
  timeStep?: number;
  onSetNewDate?: (newDate: string) => void;
  onSetCenterTime?: (newTime: number) => void;
}

const TimeSliderLegend: React.FC<TimeSliderLegendProps> = ({
  centerTime,
  secondsPerPx,
  selectedTime,
  scale = 5,
  currentTime,
  dataStartTime,
  dataEndTime,
  isTimeSliderHoverOn,
  timeStep,
  onSetNewDate,
  onSetCenterTime,
}: TimeSliderLegendProps) => {
  const classes = useStyles();

  return (
    <div className={classes.timeSliderLegend} data-testid="timeSliderLegend">
      <CanvasComponent
        onMouseMove={(
          x: number,
          y: number,
          event: MouseEvent,
          width: number,
        ): void => {
          if (event.buttons === 1) {
            onSetCenterTime &&
              onSetCenterTime(centerTime - event.movementX * secondsPerPx);
            return;
          }
          if (isTimeSliderHoverOn && dataStartTime && dataEndTime) {
            setNewRoundedTime(
              x,
              centerTime,
              width,
              secondsPerPx,
              timeStep,
              dataStartTime,
              dataEndTime,
              onSetNewDate,
            );
          }
        }}
        onRenderCanvas={(
          ctx: CanvasRenderingContext2D,
          width: number,
          height: number,
        ): void => {
          renderTimeSliderLegend(
            ctx,
            width,
            height,
            centerTime,
            secondsPerPx,
            selectedTime,
            scale,
            currentTime,
          );
        }}
      />
    </div>
  );
};

export default TimeSliderLegend;
