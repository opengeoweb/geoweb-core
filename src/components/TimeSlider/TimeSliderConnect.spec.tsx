/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import TimeSliderConnect from './TimeSliderConnect';
import { mockStateMapWithLayer } from '../../utils/testUtils';
import {
  defaultReduxLayerRadarKNMI,
  layerWithoutTimeDimension,
} from '../../utils/defaultTestSettings';
import * as mapActionTypes from '../../store/mapStore/map/constants';

describe('src/components/TimeSlider/TimeSliderConnect', () => {
  it('should render the component when the map has a layer with time dimension', () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { getByTestId } = render(
      <Provider store={store}>
        <TimeSliderConnect mapId={mapId} />
      </Provider>,
    );

    expect(getByTestId('timeSliderButtons')).toBeTruthy();
    expect(getByTestId('timeSliderRail')).toBeTruthy();
    expect(getByTestId('scaleSlider')).toBeTruthy();
    expect(getByTestId('timeSliderLegend')).toBeTruthy();
  });

  it('should render the component when the layer on the map does not have a time dimension', () => {
    const mapId = 'mapid_2';
    const layer = layerWithoutTimeDimension;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { getByTestId } = render(
      <Provider store={store}>
        <TimeSliderConnect mapId={mapId} />
      </Provider>,
    );

    expect(getByTestId('timeSliderButtons')).toBeTruthy();
    expect(getByTestId('timeSliderRail')).toBeTruthy();
    expect(getByTestId('scaleSlider')).toBeTruthy();
    expect(getByTestId('timeSliderLegend')).toBeTruthy();
  });

  it('should toggle time slider hover when ctrl + alt + h is pressed', async () => {
    const mapId = 'mapid_3';
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { getByTestId } = render(
      <Provider store={store}>
        <TimeSliderConnect mapId={mapId} />
      </Provider>,
    );
    const timeSlider = getByTestId('timeSlider');
    expect(timeSlider).toBeTruthy();
    timeSlider.focus();

    fireEvent.keyDown(timeSlider, {
      ctrlKey: true,
      altKey: true,
      key: 'h',
      code: 'KeyH',
    });

    const expectedActions = {
      payload: { mapId, isTimeSliderHoverOn: true },
      type: mapActionTypes.WEBMAP_TOGGLE_TIME_SLIDER_HOVER,
    };
    expect(store.getActions()).toContainEqual(expectedActions);
  });
});
