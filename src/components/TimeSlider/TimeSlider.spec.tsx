/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';

import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import TimeSlider from './TimeSlider';
import LayerManagerButton from './LayerManager/LayerManager';

import { mockStateMapWithAnimationDelayWithoutLayers } from '../../utils/testUtils';

describe('src/components/TimeSlider/TimeSlider', () => {
  it('should render the wrapped components', () => {
    const props = {};
    const { getByTestId } = render(<TimeSlider {...props} />);
    expect(getByTestId('timeSliderButtons')).toBeTruthy();
    expect(getByTestId('timeSliderRail')).toBeTruthy();
    expect(getByTestId('scaleSlider')).toBeTruthy();
    expect(getByTestId('timeSliderLegend')).toBeTruthy();
  });
  it('timeslider can be removed and fetched back with t key', async () => {
    const props = {};
    const { container, queryByTestId } = render(<TimeSlider {...props} />);
    expect(queryByTestId('timeSlider')).toBeTruthy();
    fireEvent.keyDown(container, {
      key: 't',
      code: 'KeyT',
    });
    expect(queryByTestId('timeSlider')).toBeNull();

    fireEvent.keyDown(container, {
      key: 't',
      code: 'KeyT',
    });
    expect(queryByTestId('timeSlider')).toBeTruthy();
  });
  it(
    'timeslider cannot be removed and fetched back with t key in an input field,' +
      'but can be done elsewhere',
    async () => {
      const props = {
        mapId: 'mapId_1',
      };
      const props2 = {};
      const mockStore = configureStore();
      const mockState = mockStateMapWithAnimationDelayWithoutLayers(
        props.mapId,
      );
      const store = mockStore(mockState);
      const { queryByTestId, getAllByTestId } = render(
        <Provider store={store}>
          <LayerManagerButton {...props} />
          <TimeSlider {...props2} />
        </Provider>,
      );
      expect(queryByTestId('timeSlider')).toBeTruthy();
      expect(queryByTestId('layerManagerPopOver')).toBeFalsy();
      fireEvent.click(queryByTestId('layerManagerButton'));
      expect(queryByTestId('layerManagerPopOver')).toBeTruthy();
      const popOver = queryByTestId('layerManagerPopOver');
      await waitFor(() => expect(popOver).toBeTruthy());
      const popOverButtons = getAllByTestId('layerManagerPopOverIconButton');
      fireEvent.click(popOverButtons[0]);
      await waitFor(() => {
        const inputField = queryByTestId('wmsLoaderTextField')
          .children.item(1)
          .children.item(0);
        expect(inputField).toBeTruthy();
        fireEvent.keyDown(inputField, {
          key: 't',
          code: 'KeyT',
        });
      });
      expect(queryByTestId('timeSlider')).toBeTruthy();
      const wholeDialogContent = queryByTestId('wmsLoaderDialogContent');
      fireEvent.keyDown(wholeDialogContent, {
        key: 't',
        code: 'KeyT',
      });

      expect(queryByTestId('timeSlider')).toBeNull();

      fireEvent.keyDown(wholeDialogContent, {
        key: 't',
        code: 'KeyT',
      });

      expect(queryByTestId('timeSlider')).toBeTruthy();
    },
  );
});
