/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import {
  getTimeBounds,
  pixelToTimestamp,
  scalingCoefficient,
  timestampToPixel,
  timestampToPixelEdges,
} from './TimeSliderUtils';

describe('src/components/TimeSlider/TimeSliderUtils', () => {
  describe('getTimeBounds', () => {
    it('should return the proper time bounds', () => {
      const dimensions = [
        {
          name: 'time',
          currentValue: '2020-07-01T10:00:00Z',
          maxValue: '2020-07-01T08:00:00Z',
          minValue: '2020-07-02T10:00:00Z',
        },
        {
          name: 'someOtherDim',
          currentValue: '12',
          maxValue: 'gigantisch',
          minValue: 'anders',
        },
      ];
      expect(getTimeBounds(dimensions)).toStrictEqual({
        selectedTime: moment.utc(dimensions[0].currentValue).unix(),
        startTime: moment.utc(dimensions[0].minValue).unix(),
        endTime: moment.utc(dimensions[0].maxValue).unix(),
      });
    });
    it('should return the default time bounds if no time dimension passed', () => {
      const currentTime = moment
        .utc()
        .set({ second: 0, millisecond: 0 })
        .unix();
      const dimensions = [
        {
          name: 'someOtherDim',
          currentValue: '12',
          maxValue: 'gigantisch',
          minValue: 'anders',
        },
      ];
      expect(getTimeBounds(dimensions)).toStrictEqual({
        selectedTime: moment.utc().unix(),
        startTime: currentTime,
        endTime: currentTime,
      });
    });
  });

  describe('scalingCoefficient', () => {
    const timeStart = moment.utc('2020-07-01T10:00:00Z').unix();
    const timeEnd = moment.utc('2020-07-01T10:10:00Z').unix();
    it('should return the proper time bounds', () => {
      expect(scalingCoefficient(timeEnd, timeStart, 600)).toBe(1);
      expect(scalingCoefficient(timeEnd, timeStart, 30)).toBe(0.05);
    });
  });
  describe('timestampToPixelEdges', () => {
    const timeStamp = moment.utc('2020-07-01T10:05:00Z').unix();
    const timeStart = moment.utc('2020-07-01T10:00:00Z').unix();
    const timeEnd = moment.utc('2020-07-01T10:10:00Z').unix();
    it('should return the proper time bounds', () => {
      expect(timestampToPixelEdges(timeStamp, timeStart, timeEnd, 600)).toBe(
        300,
      );
      expect(
        timestampToPixelEdges(
          moment.utc('2020-07-01T10:02:00Z').unix(),
          timeStart,
          timeEnd,
          30,
        ),
      ).toBe(6);
    });
  });

  describe('timestampToPixel', () => {
    const centerTime = moment.utc('2020-07-01T10:00:00Z').unix();
    it('should return the proper pixel position', () => {
      expect(
        timestampToPixel(
          moment.utc('2020-07-01T09:50:00Z').unix(),
          centerTime,
          1000,
          10,
        ),
      ).toBe(440);
      expect(
        timestampToPixel(
          moment.utc('2020-07-01T10:15:00Z').unix(),
          centerTime,
          1000,
          10,
        ),
      ).toBe(590);
    });
  });

  describe('pixelToTimestamp', () => {
    const centerTime = moment.utc('2020-07-01T10:00:00Z').unix();
    it('should return the proper timestamp', () => {
      expect(pixelToTimestamp(620, centerTime, 1000, 10)).toBe(
        moment.utc('2020-07-01T10:20:00Z').unix(),
      );
      expect(pixelToTimestamp(560, centerTime, 1000, 10)).toBe(
        moment.utc('2020-07-01T10:10:00Z').unix(),
      );
    });
  });

  // Test inverse with 10 random numbers
  const centerTime = moment.utc('2020-07-01T10:00:00Z').unix();
  const testPixelWidths = new Array(10)
    .fill(0)
    // eslint-disable-next-line no-unused-vars
    .map(_ => Math.random() * 1000);
  describe.each(testPixelWidths)(
    'timestampToPixel(pixelToTimestamp(%d, ...), ...)',
    t => {
      it(`should return ${t}`, () => {
        expect(
          timestampToPixel(
            pixelToTimestamp(t, centerTime, 1000, 10),
            centerTime,
            1000,
            10,
          ),
        ).toBeCloseTo(t);
      });
    },
  );
});
