/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import moment from 'moment';

import TimeSliderRail from './TimeSliderRail';

const date = moment.utc('20200101', 'YYYYMMDD');
const props = {
  scale: 5,
  timeStep: 5,
  dataStartTime: moment.utc('20100101', 'YYYYMMDD').unix(),
  dataEndTime: moment.utc('20300101', 'YYYYMMDD').unix(),
  centerTime: moment
    .utc(date)
    .startOf('day')
    .hours(12)
    .unix(),
  secondsPerPx: 50,
  onSetNewDate: jest.fn(),
  onSetNewScale: jest.fn(),
  onSetAnimationStartTime: jest.fn(),
  onSetAnimationEndTime: jest.fn(),
};
const startTime = moment
  .utc(date)
  .startOf('day')
  .hours(9)
  .minutes(58);
const endTime = moment
  .utc(date)
  .startOf('day')
  .hours(14)
  .minutes(2);

describe('src/components/TimeSlider/TimeSliderRail', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should render a canvas with start, end and selected time props', () => {
    const { container } = render(
      <TimeSliderRail
        {...props}
        animationStartTime={startTime}
        animationEndTime={endTime}
        selectedTime={moment
          .utc(date)
          .startOf('day')
          .hours(12)}
      />,
    );
    expect(container.querySelector('canvas')).toBeTruthy();
  });

  it('should render a canvas with animation start and end times added to props', () => {
    const { container } = render(
      <TimeSliderRail
        {...props}
        animationStartTime={moment
          .utc(date)
          .startOf('day')
          .hours(10)}
        animationEndTime={moment
          .utc(date)
          .startOf('day')
          .hours(14)}
        selectedTime={moment
          .utc(date)
          .startOf('day')
          .hours(12)}
      />,
    );
    expect(container.querySelector('canvas')).toBeTruthy();
  });
  it(
    'rail slider value can be changed with a scroll with deltaY > 0 ' +
      'and change scale with shift down and change animation interval with alt key',
    async () => {
      const date1 = moment.utc().subtract(6, 'h');
      const wheeledDate = date1
        .clone()
        .subtract(5, 'm')
        .toISOString();

      const { container } = render(
        <TimeSliderRail
          {...props}
          selectedTime={moment.utc(date1)}
          animationStartTime={startTime}
          animationEndTime={endTime}
        />,
      );
      expect(container).toBeTruthy();
      const canvas = container.querySelector('canvas');
      canvas.focus();
      fireEvent.wheel(canvas, { deltaY: 1 });
      // run timers set by wheel to avoid side effects to the next test
      jest.runOnlyPendingTimers();

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props.onSetNewDate).toHaveBeenCalledWith(wheeledDate);

      fireEvent.wheel(canvas, { shiftKey: true, deltaY: 1 });
      expect(props.onSetNewScale).toHaveBeenCalledTimes(1);
      expect(props.onSetNewScale).toHaveBeenCalledWith(1);
      fireEvent.wheel(canvas, {
        altKey: true,
        deltaY: 1,
      });
      expect(props.onSetAnimationStartTime).toHaveBeenCalledTimes(1);
      expect(props.onSetAnimationEndTime).toHaveBeenCalledTimes(1);
    },
  );
  it(
    'railslider value can be changed with a scroll and with deltaY < 0  ' +
      'and change scale with shift down and change animation interval with alt key',
    async () => {
      const date2 = moment.utc();
      const wheeledDate = date2
        .clone()
        .add(5, 'm')
        .toISOString();

      const { container } = render(
        <TimeSliderRail
          {...props}
          selectedTime={moment.utc(date2)}
          animationStartTime={startTime}
          animationEndTime={endTime}
        />,
      );
      expect(container).toBeTruthy();
      const canvas = container.querySelector('canvas');
      canvas.focus();

      fireEvent.wheel(canvas, { deltaY: -1 });

      // run timers set by wheel to avoid side effects to the next test
      jest.runOnlyPendingTimers();

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props.onSetNewDate).toHaveBeenCalledWith(wheeledDate);

      fireEvent.wheel(canvas, { shiftKey: true, deltaY: -1 });
      expect(props.onSetNewScale).toHaveBeenCalledTimes(1);
      expect(props.onSetNewScale).toHaveBeenCalledWith(60);
      fireEvent.wheel(canvas, { altKey: true, deltaY: -1 });
      expect(props.onSetAnimationStartTime).toHaveBeenCalledTimes(1);
      expect(props.onSetAnimationEndTime).toHaveBeenCalledTimes(1);
    },
  );
  it('rail slider value can be changed with an right arrow key and a ctrl key', async () => {
    const date3 = moment.utc().subtract(6, 'h');
    const wheeledDate = date3
      .clone()
      .add(5, 'm')
      .toISOString();

    const { container, queryByRole } = render(
      <TimeSliderRail
        {...props}
        selectedTime={moment.utc(date3)}
        animationStartTime={moment
          .utc(date3)
          .startOf('day')
          .hours(10)}
        animationEndTime={moment
          .utc(date3)
          .startOf('day')
          .hours(14)}
      />,
    );
    expect(container).toBeTruthy();
    const button = queryByRole('button');
    button.focus();
    const canvas = container.querySelector('canvas');
    canvas.focus();
    fireEvent.keyDown(canvas, {
      ctrlKey: true,
      key: 'ArrowRight',
      code: 'ArrowRight',
      keyCode: 39,
    });
    jest.runOnlyPendingTimers();
    expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
    expect(props.onSetNewDate).toHaveBeenCalledWith(wheeledDate);
  });
  it('rail slider value can be changed with an left arrow key and a ctrl key', async () => {
    const date4 = moment.utc().subtract(6, 'h');
    const wheeledDate = date4
      .clone()
      .subtract(5, 'm')
      .toISOString();

    const { container, queryByRole } = render(
      <TimeSliderRail
        {...props}
        selectedTime={moment.utc(date4)}
        animationStartTime={moment
          .utc(date4)
          .startOf('day')
          .hours(10)}
        animationEndTime={moment
          .utc(date4)
          .startOf('day')
          .hours(14)}
      />,
    );
    expect(container).toBeTruthy();
    const button = queryByRole('button');
    button.focus();
    const canvas = container.querySelector('canvas');
    canvas.focus();
    fireEvent.keyDown(canvas, {
      ctrlKey: true,
      key: 'ArrowLeft',
      code: 'ArrowLeft',
      keyCode: 37,
    });
    jest.runOnlyPendingTimers();
    expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
    expect(props.onSetNewDate).toHaveBeenCalledWith(wheeledDate);
  });
});
