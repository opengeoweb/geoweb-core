/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import moment from 'moment';

import { timestampToPixel } from '../TimeSliderUtils';

/* eslint-disable no-param-reassign */

const needleGeom = {
  width: 60,
  height: 24,
  cornerRadius: 5,
};

const needleFont = {
  fontName: 'Roboto',
  fontSize: '14',
};

const needleStyles = {
  lineWidth: 1,
  fillColor: '#0075a9',
  strokeColor: '#0075a9',
  textColor: 'white',
  font: `${needleFont.fontSize}px ${needleFont.fontName}`,
};

const drawRoundedRectangle = (
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  width: number,
  height: number,
  cornerRadius: number,
): void => {
  ctx.save();
  ctx.translate(x, y);

  ctx.beginPath();
  ctx.strokeStyle = needleStyles.strokeColor;
  ctx.fillStyle = needleStyles.fillColor;
  ctx.lineWidth = needleStyles.lineWidth;
  const halfLineWidth = needleStyles.lineWidth / 2;

  // Top left corner
  ctx.moveTo(halfLineWidth, height / 2);
  ctx.arcTo(
    halfLineWidth,
    halfLineWidth,
    width / 2,
    halfLineWidth,
    cornerRadius,
  );

  // Top right corner
  ctx.arcTo(
    width - halfLineWidth,
    halfLineWidth,
    width - halfLineWidth,
    height / 2,
    cornerRadius,
  );

  // Bottom right corner
  ctx.arcTo(
    width - halfLineWidth,
    height - halfLineWidth,
    width / 2,
    height - halfLineWidth,
    cornerRadius,
  );

  // Arrow down
  ctx.lineTo(width / 2 + 5, height);
  ctx.lineTo(width / 2, height + 5);
  ctx.lineTo(width / 2 - 5, height);

  // Bottom left corner
  ctx.arcTo(
    halfLineWidth,
    height - halfLineWidth,
    halfLineWidth,
    height / 2,
    cornerRadius,
  );

  ctx.lineTo(halfLineWidth, height / 2);
  ctx.fill();
  ctx.stroke();

  ctx.restore();
};

const drawTextAnnotation = (
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  text: string,
): void => {
  ctx.save();
  ctx.translate(x, y);

  ctx.fillStyle = needleStyles.textColor;
  ctx.font = needleStyles.font;
  const textMetrics = ctx.measureText(text);

  const actualTextWidth =
    textMetrics.actualBoundingBoxRight - textMetrics.actualBoundingBoxLeft;
  const actualTextHeight = ctx.measureText('O').width;
  ctx.fillText(text, -actualTextWidth / 2, actualTextHeight / 2);

  ctx.restore();
};

const drawPlayerLoopIcon = (
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
): void => {
  ctx.save();

  const qAndDTranslateXSvg = -12;
  const qAndDTranslateYSvg = 0;
  ctx.translate(x + qAndDTranslateXSvg, y + qAndDTranslateYSvg);

  const svgIconPath = new Path2D(
    'M8.843 1.029c-.56 0-.814.362-.814.699 0 .179.065.36.19.54l3.06 4.34c.065.093.14.171.221.234v8.373c-.082.064-.156.142-.22.234L8.219 19.79c-.125.18-.19.361-.19.54 0 .337.255.699.814.699h6.373c.56 0 .813-.361.813-.7 0-.178-.064-.36-.19-.538l-3.06-4.343c-.08-.112-.174-.204-.278-.273V6.883c.104-.069.199-.16.278-.273l3.06-4.343c.126-.179.19-.36.19-.539 0-.338-.254-.7-.813-.7z',
  );

  ctx.fillStyle = needleStyles.fillColor;
  ctx.fill(svgIconPath);

  ctx.restore();
};

const drawNewNeedleLabel = (
  ctx: CanvasRenderingContext2D,
  needleWidth: number,
  needleHeight: number,
  selectedTime: number,
  selectedPx: number,
  animationStartPx: number,
  animationEndPx: number,
): void => {
  const needleX = selectedPx - needleWidth / 2;
  const needleY = needleStyles.lineWidth;

  drawRoundedRectangle(
    ctx,
    needleX,
    needleY,
    needleWidth,
    needleHeight,
    needleGeom.cornerRadius,
  );

  const timeFormat = 'HH:mm';
  const timeText = moment
    .utc(selectedTime * 1000)
    .format(timeFormat)
    .toString();

  drawTextAnnotation(
    ctx,
    needleX + needleWidth / 2,
    needleY + needleHeight / 2,
    timeText,
  );

  if (!animationStartPx || !animationEndPx) {
    drawPlayerLoopIcon(
      ctx,
      selectedPx,
      needleY + needleHeight - needleStyles.lineWidth,
    );
    return;
  }

  drawPlayerLoopIcon(
    ctx,
    animationStartPx,
    needleY + needleHeight - needleStyles.lineWidth,
  );

  drawPlayerLoopIcon(
    ctx,
    animationEndPx,
    needleY + needleHeight - needleStyles.lineWidth,
  );
};

// top spacing
const y = 31;

const clipRoundedRailArea = (
  ctx: CanvasRenderingContext2D,
  width: number,
  height: number,
): void => {
  const r = 4;

  // Create a clipping path that clips rounded corners
  // for the rail
  ctx.beginPath();
  ctx.moveTo(r, y);
  ctx.lineTo(width - r, y);
  // top right corner
  ctx.quadraticCurveTo(width, y, width, r + y);
  ctx.lineTo(width, height - r + y);
  // bottom right corner
  ctx.quadraticCurveTo(width, height + y, width - r, height + y);
  ctx.lineTo(r, height + y);
  // bottom left corner
  ctx.quadraticCurveTo(0, height + y, 0, height - r + y);
  ctx.lineTo(0, r + y);
  // top left corner
  ctx.quadraticCurveTo(0, y, r, y);
  ctx.closePath();
  ctx.fill();
};

const railColors = {
  WHITE: '#ffffff',
  GREY: '#313131',
  BLUE: '#0075a9',
  RED: '#992727',
  YELLOW: '#ffa800',
  TRANSPARENCY: 'rgba(255, 255, 255, 0.5)',
};

const drawRailColors = (
  ctx: CanvasRenderingContext2D,
  width: number,
  height: number,
  selectedPx: number,
  currentTimePx: number,
  animationStartPx?: number,
  animationEndPx?: number,
  dataStartPx?: number,
  dataEndPx?: number,
): void => {
  // rail background
  ctx.fillStyle = railColors.GREY;
  ctx.fillRect(0, y, width, height);
  if (dataStartPx < currentTimePx) {
    ctx.fillStyle = railColors.BLUE;
    ctx.fillRect(
      Math.max(dataStartPx, 0),
      y,
      Math.min(currentTimePx, dataEndPx) - Math.max(dataStartPx, 0),
      height,
    );
  }
  if (dataEndPx > currentTimePx) {
    ctx.fillStyle = railColors.RED;
    ctx.fillRect(
      Math.max(currentTimePx, dataStartPx),
      y,
      Math.min(width, dataEndPx) - Math.max(currentTimePx, dataStartPx),
      height,
    );
  }
  if (dataStartPx > 0) {
    ctx.fillStyle = railColors.GREY;
    ctx.fillRect(0, y, dataStartPx, height);
  }
  if (!animationStartPx || !animationEndPx) {
    ctx.fillStyle = railColors.WHITE;
    ctx.fillRect(selectedPx, y, 1, height);
    ctx.fillStyle = railColors.YELLOW;
    ctx.fillRect(currentTimePx, y, 1, height);
    return;
  }
  ctx.fillStyle = railColors.TRANSPARENCY;
  ctx.fillRect(animationStartPx, y, animationEndPx - animationStartPx, height);
  ctx.fillStyle = railColors.WHITE;
  ctx.fillRect(selectedPx, y, 1, height);
  ctx.fillStyle = railColors.YELLOW;
  ctx.fillRect(currentTimePx, y, 1, height);
};

export const renderTimeSliderRailWithNeedle = (
  ctx: CanvasRenderingContext2D,
  width: number,
  height: number,
  centerTime: number,
  selected: number,
  secondsPerPx: number,
  animationStart?: number,
  animationEnd?: number,
  dataStartTime?: number,
  dataEndTime?: number,
  currentTime?: number,
): void => {
  // map unix timestamps to pixel values
  const [
    selectedPx,
    animationStartPx,
    animationEndPx,
    currentTimePx,
    dataStartPx,
    dataEndPx,
  ] = [
    selected,
    animationStart,
    animationEnd,
    currentTime,
    dataStartTime,
    dataEndTime,
  ].map(t => timestampToPixel(t, centerTime, width, secondsPerPx));

  ctx.clearRect(0, 0, width, height);
  clipRoundedRailArea(ctx, width, 8);
  ctx.globalCompositeOperation = 'source-atop';
  drawRailColors(
    ctx,
    width,
    8,
    selectedPx,
    currentTimePx,
    animationStartPx,
    animationEndPx,
    dataStartPx,
    dataEndPx,
  );
  ctx.globalCompositeOperation = 'source-over';
  drawNewNeedleLabel(
    ctx,
    needleGeom.width,
    needleGeom.height,
    selected,
    selectedPx,
    animationStartPx,
    animationEndPx,
  );
};
