/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import * as mapSelectors from '../../../store/mapStore/selectors';
import * as mapActions from '../../../store/mapStore/actions';
import { AppStore } from '../../../types/types';
import TimeSliderRail from './TimeSliderRail';
import { Dimension, Layer } from '../../../store/mapStore/types';
import {
  getDataLimitsFromLayers,
  getTimeBounds,
  TimeBounds,
} from '../TimeSliderUtils';

interface TimeSliderRailConnectProps {
  mapId: string;
  isTimeSliderHoverOn?: boolean;
}

interface TimeSliderRailConnectComponentProps {
  mapId: string;
  timeStep?: number;
  layers: Layer[];
  dimensions: Dimension[];
  scale: number;
  animationStartTime: moment.Moment;
  animationEndTime: moment.Moment;
  isAnimating: boolean;
  centerTime: number;
  secondsPerPx: number;
  isTimeSliderHoverOn?: boolean;
  setAnimationStartTime: typeof mapActions.setAnimationStartTime;
  setAnimationEndTime: typeof mapActions.setAnimationEndTime;
  stopMapAnimation: typeof mapActions.mapStopAnimation;
  mapChangeDimension: typeof mapActions.mapChangeDimension;
  mapSetTimeSliderScale?: typeof mapActions.setTimeSliderScale;
}

const connectRedux = connect(
  (store: AppStore, props: TimeSliderRailConnectProps) => ({
    timeStep: mapSelectors.getMapTimeStep(store, props.mapId),
    layers: mapSelectors.getMapLayers(store, props.mapId),
    dimensions: mapSelectors.getMapDimensions(store, props.mapId),
    scale: mapSelectors.getMapTimeSliderScale(store, props.mapId),
    animationStartTime: mapSelectors.getAnimationStartTime(store, props.mapId),
    animationEndTime: mapSelectors.getAnimationEndTime(store, props.mapId),
    isAnimating: mapSelectors.isAnimating(store, props.mapId),
    centerTime: mapSelectors.getMapTimeSliderCenterTime(store, props.mapId),
    secondsPerPx: mapSelectors.getMapTimeSliderSecondsPerPx(store, props.mapId),
    isTimeSliderHoverOn: mapSelectors.isTimeSliderHoverOn(store, props.mapId),
  }),
  {
    setAnimationStartTime: mapActions.setAnimationStartTime,
    setAnimationEndTime: mapActions.setAnimationEndTime,
    mapChangeDimension: mapActions.mapChangeDimension,
    stopMapAnimation: mapActions.mapStopAnimation,
    mapSetTimeSliderScale: mapActions.setTimeSliderScale,
  },
);

const TimeSliderRailConnectComponent: React.FC<TimeSliderRailConnectComponentProps> = ({
  mapId,
  timeStep,
  layers,
  dimensions,
  scale,
  animationStartTime,
  animationEndTime,
  isAnimating,
  centerTime,
  secondsPerPx,
  isTimeSliderHoverOn,
  setAnimationStartTime,
  setAnimationEndTime,
  stopMapAnimation,
  mapChangeDimension,
  mapSetTimeSliderScale,
}: TimeSliderRailConnectComponentProps) => {
  const { selectedTime }: TimeBounds = getTimeBounds(dimensions);
  const currentTime = moment.utc().unix();
  const [dataStartTime, dataEndTime] = getDataLimitsFromLayers(layers);

  return (
    <TimeSliderRail
      centerTime={centerTime}
      secondsPerPx={secondsPerPx}
      timeStep={timeStep}
      selectedTime={moment.utc(selectedTime * 1000)}
      scale={scale}
      animationStartTime={animationStartTime}
      animationEndTime={animationEndTime}
      currentTime={currentTime}
      dataStartTime={dataStartTime}
      dataEndTime={dataEndTime}
      onSetAnimationStartTime={(time: moment.Moment): void => {
        setAnimationStartTime({ mapId, animationStartTime: time });
      }}
      onSetAnimationEndTime={(time: moment.Moment): void => {
        setAnimationEndTime({ mapId, animationEndTime: time });
      }}
      onSetNewDate={(newDate): void => {
        if (isAnimating) {
          stopMapAnimation({ mapId });
        }
        mapChangeDimension({
          origin,
          mapId,
          dimension: { name: 'time', currentValue: newDate },
        });
      }}
      onSetNewScale={(newScale): void => {
        mapSetTimeSliderScale({
          mapId,
          timeSliderScale: newScale,
        });
      }}
      isTimeSliderHoverOn={isTimeSliderHoverOn}
    />
  );
};

const TimeSliderRailConnect: React.FC<TimeSliderRailConnectProps> = connectRedux(
  TimeSliderRailConnectComponent,
);

export default TimeSliderRailConnect;
