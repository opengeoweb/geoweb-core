/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import TimeSlider from './TimeSlider';
import TimeSliderButtonsConnect from './TimeSliderButtons/TimeSliderButtonsConnect';
import TimeSliderLegendConnect from './TimeSliderLegend/TimeSliderLegendConnect';
import TimeSliderRailConnect from './TimeSliderRail/TimeSliderRailConnect';
import TimeSliderScaleSliderConnect from './TimeSliderScaleSlider/TimeSliderScaleSliderConnect';
import { AppStore } from '../../types/types';
import * as mapActions from '../../store/mapStore/actions';
import * as mapSelectors from '../../store/mapStore/selectors';

interface TimeSliderConnectProps {
  mapId: string;
  isTimeSliderHoverOn: boolean;
  toggleTimeSliderHover?: typeof mapActions.toggleTimeSliderHover;
}

const connectRedux = connect(
  (store: AppStore, props: TimeSliderConnectProps) => ({
    isTimeSliderHoverOn: mapSelectors.isTimeSliderHoverOn(store, props.mapId),
  }),
  {
    toggleTimeSliderHover: mapActions.toggleTimeSliderHover,
  },
);

const ConnectedTimeSlider: React.FC<TimeSliderConnectProps> = ({
  mapId,
  isTimeSliderHoverOn,
  toggleTimeSliderHover,
}: TimeSliderConnectProps) => {
  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent): void => {
      if (event.ctrlKey && event.altKey && event.key === 'h') {
        toggleTimeSliderHover({
          mapId,
          isTimeSliderHoverOn: !isTimeSliderHoverOn,
        });
      }
    };
    document.addEventListener('keydown', handleKeyDown);
    return (): void => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [mapId, isTimeSliderHoverOn, toggleTimeSliderHover]);
  return (
    <TimeSlider
      buttons={<TimeSliderButtonsConnect mapId={mapId} />}
      rail={<TimeSliderRailConnect mapId={mapId} />}
      scaleSlider={<TimeSliderScaleSliderConnect mapId={mapId} />}
      legend={<TimeSliderLegendConnect mapId={mapId} />}
    />
  );
};

/**
 * TimeSlider component with components connected to the store displaying the time slider
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <TimeSliderConnect mapId={mapId} />```
 */
const TimeSliderConnect = connectRedux(ConnectedTimeSlider);

export default TimeSliderConnect;
