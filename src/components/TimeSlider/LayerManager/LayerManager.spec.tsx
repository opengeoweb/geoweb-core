/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, findByRole } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { mockStateMapWithAnimationDelayWithoutLayers } from '../../../utils/testUtils';

import LayerManagerButton from './LayerManager';

describe('src/components/TimeSlider/LayerManager', () => {
  const props = {
    mapId: 'mapId_1',
  };

  it('should render the component with correct icon', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = mockStore(mockState);
    const { queryByTestId } = render(
      <Provider store={store}>
        <LayerManagerButton {...props} />
      </Provider>,
    );

    expect(queryByTestId('addIcon')).toBeTruthy();
  });

  it('should open popover when button clicked', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = mockStore(mockState);
    const { queryByTestId } = render(
      <Provider store={store}>
        <LayerManagerButton {...props} />
      </Provider>,
    );
    expect(queryByTestId('layerManagerPopOver')).toBeFalsy();
    fireEvent.click(queryByTestId('layerManagerButton'));
    expect(queryByTestId('layerManagerPopOver')).toBeTruthy();
  });

  it('should show a proper title in tooltip when hovering', async () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = mockStore(mockState);
    const { getByTestId, container } = render(
      <Provider store={store}>
        <LayerManagerButton {...props} />
      </Provider>,
    );
    fireEvent.mouseOver(getByTestId('layerManagerButton'));
    // Wait until tooltip appears
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain('Layer Manager');
  });
});
