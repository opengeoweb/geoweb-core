/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  IconButton,
  makeStyles,
  withStyles,
  Popover,
  Grid,
  Tooltip,
  Typography,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { Layer, LayerType } from '../../../store/mapStore/types';
import { Service } from '../../WMSLoader/services';
import WMSLoaderConnect from '../../WMSLoader/WMSLoaderConnect';
import GeoWebSortableLayersConnect from '../../GeoWebLayerSelect/GeoWebSortableLayers/GeoWebSortableLayersConnect';
import GeoWebBaseLayersConnect from '../../GeoWebLayerSelect/GeoWebBaseLayers/GeoWebBaseLayersConnect';

const colors = {
  DARK_GREY: '#484848',
  LIGHT_GREY: '#c3c3c3',
  BLUE: '#0075a9',
  LIGHT_BLUE: '#d9eaf2',
  BLACK: '#000',
  WHITE: '#ffffff',
};

interface LayerManagerProps {
  mapId: string;
  preloadedAvailableBaseLayers?: Layer[];
  preloadedMapServices?: Service[];
  preloadedBaseServices?: Service[];
}

const useStyles = makeStyles({
  layerManagerButton: {
    height: '24px',
    width: '24px',
    marginLeft: '8px',
    color: colors.BLUE,
    backgroundColor: colors.WHITE,
    borderRadius: '4.5px',
    '&&:hover': {
      background: colors.LIGHT_BLUE,
    },
  },
});

const LayerManagerPopOver = withStyles({
  paper: {
    height: 'auto',
    width: '800px',
    padding: '5px',
    borderRadius: '0',
    overflow: 'visible',
  },
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
})(Popover);

const CustomTooltip = withStyles({
  tooltip: {
    borderRadius: '2px',
    fontSize: '16px',
    fontWeight: 'normal',
    padding: '10px',
    backgroundColor: colors.DARK_GREY,
  },
})(Tooltip);

const LayerManagerButton: React.FC<LayerManagerProps> = ({
  mapId,
  preloadedAvailableBaseLayers = [],
  preloadedMapServices = [],
  preloadedBaseServices = [],
}: LayerManagerProps) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const onClickButton = (event): void => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <div>
      <CustomTooltip title="Layer Manager">
        <IconButton
          onClick={onClickButton}
          className={classes.layerManagerButton}
          id="layerManagerButton"
          data-testid="layerManagerButton"
          disableRipple
        >
          <AddIcon data-testid="addIcon" fontSize="small" />
        </IconButton>
      </CustomTooltip>
      <LayerManagerPopOver
        id={id}
        open={open}
        data-testid="layerManagerPopOver"
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorReference="none"
      >
        <Grid container spacing={1}>
          <Grid item xs={2}>
            <Typography>Add layer</Typography>
            <WMSLoaderConnect
              mapId={mapId}
              tooltip="Add WMS layer to the map"
              {...(preloadedMapServices.length === 0
                ? {}
                : { preloadedServices: preloadedMapServices })}
            />
          </Grid>
          <Grid item xs={2}>
            <Typography>Add baselayer</Typography>
            <WMSLoaderConnect
              mapId={mapId}
              layerType={LayerType.baseLayer}
              tooltip="Add WMS baselayer to the map"
              {...(preloadedBaseServices.length === 0
                ? {}
                : { preloadedServices: preloadedBaseServices })}
            />
          </Grid>
          <Grid item xs={4}>
            <GeoWebBaseLayersConnect
              mapId={mapId}
              preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
            />
          </Grid>
        </Grid>
        <GeoWebSortableLayersConnect mapId={mapId} />
      </LayerManagerPopOver>
    </div>
  );
};

export default LayerManagerButton;
