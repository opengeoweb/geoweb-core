/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import moment from 'moment';
import { Dimension, Layer } from '../../store/mapStore/types';
import { getWMJSTimeDimensionForLayerId } from '../../store/mapStore/utils/helpers';

export interface TimeBounds {
  selectedTime: number;
  startTime: number;
  endTime: number;
}

/**
 * Returns time bounds from the given dimension. If no time dimension given, current time is returned as default
 */
export const getTimeBounds = (dimensions: Dimension[]): TimeBounds => {
  const defaultStartEnd = moment
    .utc()
    .set({ second: 0, millisecond: 0 })
    .unix();
  const timeDimension = dimensions
    ? dimensions.find(dim => dim.name === 'time')
    : null;
  const startTime =
    timeDimension && timeDimension.minValue
      ? moment.utc(timeDimension.minValue).unix()
      : defaultStartEnd;
  const endTime =
    timeDimension && timeDimension.maxValue
      ? moment.utc(timeDimension.maxValue).unix()
      : defaultStartEnd;
  const currentValueIsValid =
    timeDimension &&
    timeDimension.currentValue &&
    moment.utc(timeDimension.currentValue).isValid();
  const selectedTime = currentValueIsValid
    ? moment.utc(timeDimension.currentValue).unix()
    : moment.utc().unix();
  return {
    selectedTime,
    startTime,
    endTime,
  };
};

export const scalingCoefficient = (
  end: number,
  start: number,
  width: number,
): number => width / (end - start);

export const timestampToPixelEdges = (
  timestamp: number,
  start: number,
  end: number,
  width: number,
): number => (timestamp - start) * scalingCoefficient(end, start, width);

export const timestampToPixel = (
  timestamp: number,
  centerTime: number,
  widthPx: number,
  secondsPerPx: number,
): number => widthPx / 2 - (centerTime - timestamp) / secondsPerPx;

export const pixelToTimestamp = (
  timePx: number,
  centerTime: number,
  widthPx: number,
  secondsPerPx: number,
): number => centerTime - (widthPx / 2 - timePx) * secondsPerPx;

export const getMainLayer = (layers: Layer[]): Layer | null => {
  if (layers.length > 0) {
    return layers[0];
  }
  return null;
};

let onSetNewDateDebouncer;

export const onsetNewDateDebounced = (
  dateToSet: string,
  onSetNewDate: (newDate: string) => void,
): void => {
  /* Debounce the dateToSet with a simple timeout */
  if (onSetNewDateDebouncer === undefined) {
    onSetNewDateDebouncer = dateToSet;
    onSetNewDate(dateToSet);
    window.setTimeout(() => {
      if (
        onSetNewDateDebouncer !== undefined &&
        onSetNewDateDebouncer !== dateToSet
      ) {
        onSetNewDate(onSetNewDateDebouncer);
      }
      onSetNewDateDebouncer = undefined;
    }, 10);
  } else {
    onSetNewDateDebouncer = dateToSet;
  }
};

export const roundWithTimeStep = (
  unixTime: number,
  timeStep: number,
  type?: string,
): number => {
  const adjustedTimeStep = timeStep * 60;
  if (!type || type === 'round') {
    return Math.round(unixTime / adjustedTimeStep) * adjustedTimeStep;
  }
  if (type === 'floor') {
    return Math.floor(unixTime / adjustedTimeStep) * adjustedTimeStep;
  }
  if (type === 'ceil') {
    return Math.ceil(unixTime / adjustedTimeStep) * adjustedTimeStep;
  }
  return undefined;
};

export const setNewRoundedTime = (
  x: number,
  centerTime: number,
  width: number,
  secondsPerPx: number,
  timeStep: number,
  dataStartTime: number,
  dataEndTime: number,
  onSetNewDate: (newDate: string) => void,
): void => {
  const unixTime = pixelToTimestamp(x, centerTime, width, secondsPerPx);
  const roundedTime = roundWithTimeStep(unixTime, timeStep);
  const selectedTimeString = moment
    .unix(Math.min(Math.max(roundedTime, dataStartTime), dataEndTime))
    .toISOString();
  onsetNewDateDebounced(selectedTimeString, onSetNewDate);
};

export const getDataLimitsFromLayers = (layers: Layer[]): Array<number> => {
  return layers.reduce(
    ([start, end], layer) => {
      const dimension = getWMJSTimeDimensionForLayerId(layer.id);
      if (dimension) {
        const lastValue = moment(dimension.getLastValue()).unix();
        const firstValue = moment(dimension.getFirstValue()).unix();

        const newLast = lastValue > end ? lastValue : end;
        const newFirst = firstValue < start ? firstValue : start;

        return [newFirst, newLast];
      }
      return [start, end];
    },
    /* Using the maximum 32-bit value and 0 as starting points
     * bigger values like Number.MAX_VALUE or Number.MAX_SAFE_INTEGER
     * cause weird behaviour as timestamps break at 32bit limit (year 2038)
     */
    [2147483647, 0],
  );
};
