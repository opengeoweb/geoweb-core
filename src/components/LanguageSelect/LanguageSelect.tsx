/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import getLanguages from '../../utils/languagesService';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 20,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }),
);

const renderOptions = (): JSX.Element[] => {
  return getLanguages.map((dt, i) => {
    return (
      <MenuItem value={dt.code} key={i.toString()}>
        <span aria-label={dt.code} role="img">
          {dt.flag}
        </span>{' '}
        {dt.code.toUpperCase()}
      </MenuItem>
    );
  });
};

interface LanguageSelectProps {
  onChangeLanguage: (currentLang: string) => void;
  currentLang?: string;
  defaultLang?: string;
}

const LanguageSelect: React.FC<LanguageSelectProps> = ({
  onChangeLanguage,
  currentLang = '',
  defaultLang = 'en',
}: LanguageSelectProps) => {
  const classes = useStyles();

  const [lang, selectLang] = React.useState(
    getLanguages.find(value => value.code === currentLang)
      ? currentLang
      : defaultLang,
  );

  const handleChange = (value: string): void => {
    selectLang(value);
    onChangeLanguage(value);
  };

  return (
    <div>
      <FormControl className={classes.formControl}>
        <Select
          inputProps={{
            SelectDisplayProps: {
              'data-testid': 'select',
            },
          }}
          value={lang}
          onChange={(e: React.ChangeEvent<{ value: string }>): void =>
            handleChange(e.target.value)
          }
        >
          {renderOptions()}
        </Select>
      </FormControl>
    </div>
  );
};

export default LanguageSelect;
