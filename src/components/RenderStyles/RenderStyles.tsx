/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import MenuItem from '@material-ui/core/MenuItem';

import { Layer, Service } from '../../store/mapStore/types';
import TooltipSelect from '../TooltipSelect/TooltipSelect';

interface RenderStylesProps {
  layer: Layer;
  services: Service;
  mapId: string;
  onChangeLayerStyle: (mapId: string, layerId: string, style: string) => void;
}

const RenderStyles: React.FC<RenderStylesProps> = ({
  services,
  layer,
  mapId,
  onChangeLayerStyle,
}: RenderStylesProps) => {
  const styles = [
    {
      Name: { value: 'default' },
      Title: { value: 'default' },
    },
  ];
  let styleValue = styles[0];
  // TODO: (Sander de Snaijer, 2020-03-19) Needs to tidy this up to make it more readable
  if (
    services &&
    layer &&
    layer.service &&
    services[layer.service] &&
    services[layer.service].layer
  ) {
    const serviceLayer = services[layer.service].layer[layer.name];

    if (serviceLayer && serviceLayer.styles && serviceLayer.styles.length > 0) {
      styles.push(...serviceLayer.styles);
    }
    const currentStyle = styles.filter(l => l.Name.value === layer.style)[0];
    if (
      currentStyle &&
      styleValue.Name.value === 'default' &&
      styleValue !== currentStyle
    )
      styleValue = currentStyle;
  }

  const selectStyle = (event: React.ChangeEvent<{ value: string }>): void => {
    event.stopPropagation();
    onChangeLayerStyle(mapId, layer.id, event.target.value);
  };

  return (
    <TooltipSelect
      tooltip={`Select a style for layer ${layer.name}`}
      inputProps={{
        SelectDisplayProps: {
          'data-testid': 'selectStyle',
        },
      }}
      style={{ width: '100%' }}
      value={styleValue.Name.value}
      onChange={selectStyle}
    >
      {styles.map(styleFromlayer => (
        <MenuItem
          key={styleFromlayer.Name.value}
          value={styleFromlayer.Name.value}
        >
          {styleFromlayer.Title.value}
        </MenuItem>
      ))}
    </TooltipSelect>
  );
};

export default RenderStyles;
