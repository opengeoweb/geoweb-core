/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import RenderStyles from './RenderStyles';
import * as mapActions from '../../store/mapStore/actions';
import * as serviceSelectors from '../../store/mapStore/service/selectors';
import * as layersSelectors from '../../store/mapStore/layers/selectors';
import { Layer, Service } from '../../store/mapStore/types';
import { AppStore } from '../../types/types';

interface RenderStylesProps {
  mapId: string;
  services: Service;
  layer: Layer;
  layerChangeStyle: typeof mapActions.layerChangeStyle;
  // eslint-disable-next-line react/no-unused-prop-types
  layerId?: string;
}
const connectRedux = connect(
  (store: AppStore, props: RenderStylesProps) => ({
    // TODO: (Sander de Snaijer 2020-03-30 make specific selectors with layerId to prevent rerenders)
    services: serviceSelectors.getServices(store),
    layer: layersSelectors.getLayerById(store, props.layerId),
  }),
  {
    layerChangeStyle: mapActions.layerChangeStyle,
  },
);

const RenderStylesConnected: React.FC<RenderStylesProps> = ({
  services,
  layer,
  mapId,
  layerChangeStyle,
}: RenderStylesProps) => (
  <RenderStyles
    mapId={mapId}
    layer={layer}
    services={services}
    onChangeLayerStyle={(_, layerId, style): void => {
      layerChangeStyle({ layerId, style });
    }}
  />
);

const RenderStylesConnect = connectRedux(RenderStylesConnected);

export default RenderStylesConnect;
