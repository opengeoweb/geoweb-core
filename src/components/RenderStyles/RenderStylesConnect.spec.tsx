/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { defaultReduxLayerRadarKNMI } from '../../utils/defaultTestSettings';

import RenderStylesConnect from './RenderStylesConnect';
import { mockStateMapWithLayer } from '../../utils/testUtils';
import * as layerActionTypes from '../../store/mapStore/layers/constants';

describe('src/components/RenderStyles/RenderStylesConnect', () => {
  it('should set styles', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = mockStateMapWithLayer(layer, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);

    const { findByText, getByTestId } = render(
      <Provider store={store}>
        <RenderStylesConnect mapId={mapId} layerId={layer.id} />
      </Provider>,
    );

    const select = getByTestId('selectStyle');

    fireEvent.mouseDown(select);
    const newName = {
      value: 'precip/nearest',
      title: 'precip/nearest',
    };

    const menuItem = await findByText(newName.title);

    fireEvent.click(menuItem);

    const expectedAction = [
      {
        payload: { layerId: layer.id, style: newName.value },
        type: layerActionTypes.WEBMAP_LAYER_CHANGE_STYLE,
      },
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
