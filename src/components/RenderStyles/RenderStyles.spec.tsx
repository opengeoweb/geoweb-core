/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, findByRole } from '@testing-library/react';
import RenderStyles from './RenderStyles';
import {
  defaultReduxLayerRadarKNMI,
  defaultReduxServices,
} from '../../utils/defaultTestSettings';

describe('src/components/RenderStyles/RenderStyles', () => {
  const defaultProps = {
    services: defaultReduxServices,
    layer: defaultReduxLayerRadarKNMI,
    mapId: 'map_1',
    onChangeLayerStyle: jest.fn(),
  };

  it('should show the select style component with default selected if no services are provided', () => {
    const mockProps = {
      ...defaultProps,
      services: {},
    };

    const { getByTestId } = render(<RenderStyles {...mockProps} />);
    const styleSelect = getByTestId('selectStyle');

    expect(styleSelect).toBeTruthy();
    expect(styleSelect.textContent).toEqual('default');
  });

  it('should show the select style component with the first style selected if services are provided', () => {
    const { getByTestId } = render(<RenderStyles {...defaultProps} />);
    const styleSelect = getByTestId('selectStyle');

    expect(styleSelect).toBeTruthy();
    expect(styleSelect.textContent).toEqual(
      defaultProps.services['https://testservice'].layer
        .RADNL_OPER_R___25PCPRR_L3_KNMI.styles[0].name,
    );
  });

  it('should trigger onChangeLayerStyle when a new style is selected', async () => {
    const { getByTestId, findByText } = render(
      <RenderStyles {...defaultProps} />,
    );
    const newStyleName =
      defaultProps.services['https://testservice'].layer
        .RADNL_OPER_R___25PCPRR_L3_KNMI.styles[1].name;
    const styleSelect = getByTestId('selectStyle');

    fireEvent.mouseDown(styleSelect);

    const menuItem = await findByText(newStyleName);
    fireEvent.click(menuItem);

    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledWith(
      defaultProps.mapId,
      defaultProps.layer.id,
      newStyleName,
    );
    // showing the newStyleName in the select only works when this component is connected to the store, so no checking on that here
  });

  it('should show the layer name in the tooltip', async () => {
    const { container, getByTestId } = render(
      <RenderStyles {...defaultProps} />,
    );
    const styleSelect = getByTestId('selectStyle');
    fireEvent.mouseOver(styleSelect);

    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain(defaultProps.layer.name);
  });
});
