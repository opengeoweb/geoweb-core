/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Rnd } from 'react-rnd';
import { IconButton, makeStyles, Tooltip } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
import GrainIcon from '@material-ui/icons/Grain';
import DescriptionRow from './DescriptionRow/DescriptionRow';
import LayerRow from './LayerRow/LayerRow';
import BaseLayerRow from './BaseLayerRow/BaseLayerRow';

interface LayerManagerProps {
  mapId: string;
}

const colors = {
  DARKEST_GREY: '#393939',
  DARK_GREY: '#484848',
  LIGHT_GREY: '#c3c3c3',
  BLUE: '#0075a9',
  LIGHT_BLUE: '#d9eaf2',
  BLACK: '#000000',
  WHITE: '#ffffff',
};

const useStyles = makeStyles({
  rnd: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    background: colors.WHITE,
    zIndex: 1000,
  },
  layerManager: {
    position: 'absolute',
    backgroundColor: colors.DARKEST_GREY,
    color: 'white',
    height: '100%',
    width: '100%',
  },
  layerManagerButton: {
    margin: '8px 8px 0 8px',
    height: '24px',
    width: '24px',
    backgroundColor: colors.WHITE,
    color: colors.DARKEST_GREY,
    borderRadius: '4.5px',
    '&&:hover': {
      background: colors.LIGHT_BLUE,
    },
  },
  layerRowContainer: {
    position: 'relative',
    width: '100%',
    height: 'calc(100% - 59px)',
    display: 'inline-block',
  },
  header: {
    width: '100%',
    height: '40px',
    backgroundColor: colors.DARK_GREY,
    color: '#ffffff',
  },
  footer: {
    position: 'fixed',
    width: '100%',
    height: '16px',
    backgroundColor: colors.DARK_GREY,
    borderTop: `3px solid ${colors.DARKEST_GREY}`,
    bottom: '0',
  },
  resizeIcon: {
    height: '16px',
    width: '16px',
    margin: '0 0 0 0',
    float: 'right',
    cursor: 'auto',
  },
  layerGroups: {
    display: 'inline-block',
    height: '30px',
    width: '100px',
    margin: '0 0 0 0',
    float: 'left',
  },
  closeIcon: {
    height: '30px',
    width: '30px',
    margin: '5px 5px 0 0',
    float: 'right',
  },
  icon: {
    color: colors.WHITE,
  },
  headerTitle: {
    display: 'block',
    width: '100%',
    height: '30px',
    margin: '5px 0 0 0',
    paddingTop: '5px',
    textAlign: 'center',
    position: 'absolute',
    fontFamily: 'Roboto',
    fontSize: '18px',
    fontWeight: 500,
  },
});

const LayerManager: React.FC<LayerManagerProps> = ({
  mapId,
}: LayerManagerProps) => {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);
  const [width, setWidth] = React.useState(700);
  const [height, setHeight] = React.useState(300);
  const [x, setX] = React.useState(100);
  const [y, setY] = React.useState(-300);

  const onClickButton = (): void => {
    setOpen(!open);
  };

  return (
    <>
      <Tooltip title="Layer Manager">
        <IconButton
          onClick={onClickButton}
          className={classes.layerManagerButton}
          id="layerManagerButton"
          data-testid="layerManagerButton"
          disableRipple
        >
          <AddIcon data-testid="addIcon" fontSize="small" />
        </IconButton>
      </Tooltip>
      {open ? (
        <Rnd
          className={classes.rnd}
          data-testid="layerManagerWindow"
          dragHandleClassName={classes.header}
          size={{ width, height }}
          minWidth={320}
          minHeight={190}
          position={{ x, y }}
          bounds="#root"
          // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
          onDragStop={(e, d) => {
            setX(d.x);
            setY(d.y);
          }}
          // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
          onResize={(e, direction, ref, delta, position) => {
            setWidth(parseInt(ref.style.width, 10));
            setHeight(parseInt(ref.style.height, 10));
            setX(position.x);
            setY(position.y);
          }}
          enableResizing={{
            bottomRight: true,
          }}
          resizeHandleStyles={{
            bottomRight: {
              height: '20px',
              width: '20px',
              right: '0px',
              bottom: '0px',
            },
          }}
        >
          <div className={classes.layerManager}>
            <div
              id="layerManagerHeader"
              data-testid="layerManagerHeader"
              className={classes.header}
            >
              <div
                data-testid="layerManagerTitle"
                className={classes.headerTitle}
              >
                Layer Manager
              </div>
              <div className={classes.closeIcon}>
                <IconButton
                  onClick={onClickButton}
                  data-testid="layerManagerClose"
                  size="small"
                  disableRipple
                >
                  <CloseIcon className={classes.icon} />
                </IconButton>
              </div>
            </div>
            <div
              data-testid="layerManagerRowContainer"
              className={classes.layerRowContainer}
            >
              <DescriptionRow mapId={mapId} />
              <LayerRow mapId={mapId} />
              <BaseLayerRow mapId={mapId} />
            </div>
            <div
              id="layerManagerFooter"
              data-testid="layerManagerFooter"
              className={classes.footer}
            >
              <IconButton
                className={classes.resizeIcon}
                data-testid="resizeLayerManager"
                size="small"
                disableRipple
              >
                <GrainIcon className={classes.icon} />
              </IconButton>
            </div>
          </div>
        </Rnd>
      ) : null}
    </>
  );
};

export default LayerManager;
