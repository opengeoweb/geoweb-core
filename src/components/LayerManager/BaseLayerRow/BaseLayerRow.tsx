/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import PinDropIcon from '@material-ui/icons/PinDrop';
import { makeStyles, IconButton } from '@material-ui/core';
import GeoWebBaseLayersConnect from '../../GeoWebLayerSelect/GeoWebBaseLayers/GeoWebBaseLayersConnect';
import {
  baseLayerGrey,
  baseLayerOpenStreetMapNL,
  baseLayerWorldMap,
  baseLayerArcGisSat,
  baseLayerArcGisCanvas,
} from '../../../utils/testLayers';

interface BaseLayerRowProps {
  mapId: string;
}

const colors = {
  DARKEST_GREY: '#393939',
  DARK_GREY: '#484848',
  WHITE: '#ffffff',
};

const useStyles = makeStyles({
  layerRow: {
    width: '100%',
    height: '36px',
    borderTop: `3px solid ${colors.DARKEST_GREY}`,
    backgroundColor: colors.DARK_GREY,
  },
  icon: {
    width: '24px',
    height: '24px',
    color: colors.WHITE,
  },
});

const BaseLayerRow: React.FC<BaseLayerRowProps> = ({
  mapId,
}: BaseLayerRowProps) => {
  const classes = useStyles();

  const preloadedAvailableBaseLayers = [
    { ...baseLayerGrey, id: `baseGrey-${mapId}` },
    baseLayerOpenStreetMapNL,
    baseLayerArcGisSat,
    baseLayerArcGisCanvas,
    baseLayerWorldMap,
  ];

  return (
    <div className={classes.layerRow} data-testid="baseLayerRow">
      <IconButton data-testid="pinIcon" size="small" disableRipple>
        <PinDropIcon className={classes.icon} />
      </IconButton>
      <GeoWebBaseLayersConnect
        mapId={mapId}
        preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
      />
    </div>
  );
};

export default BaseLayerRow;
