/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { makeStyles } from '@material-ui/core';
import GeoWebSortableLayersConnect from '../../GeoWebLayerSelect/GeoWebSortableLayers/GeoWebSortableLayersConnect';

interface LayerRowProps {
  mapId: string;
}

const colors = {
  DARKEST_GREY: '#393939',
  DARK_GREY: '#484848',
  WHITE: '#ffffff',
};

const useStyles = makeStyles({
  layerRow: {
    width: '100%',
    maxHeight: 'calc(100% - 86px)',
    borderTop: `3px solid ${colors.DARKEST_GREY}`,
    backgroundColor: colors.DARK_GREY,
    color: colors.WHITE,
    overflow: 'auto',
  },
});

const LayerRow: React.FC<LayerRowProps> = ({ mapId }: LayerRowProps) => {
  const classes = useStyles();
  return (
    <div className={classes.layerRow} data-testid="layerRow">
      <GeoWebSortableLayersConnect mapId={mapId} />
    </div>
  );
};
export default LayerRow;
