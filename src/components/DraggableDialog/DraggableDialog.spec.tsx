/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import ExploreIcon from '@material-ui/icons/Explore';
import DraggableDialog from './DraggableDialog';

describe('src/components/draggableOverview/DraggableDialog', () => {
  it('should close and open the draggable dialog', () => {
    const mockContent = <p data-testid="mock">Hello world</p>;
    const { getByTestId, queryByTestId } = render(
      <DraggableDialog>{mockContent}</DraggableDialog>,
    );
    // draggable dialog should be opened by default if there is content (children)
    expect(getByTestId('draggable-dialog')).toBeTruthy();
    // draggable dialog should display the content provided as a child
    expect(getByTestId('mock')).toBeTruthy();

    // close the draggable dialog
    fireEvent.click(getByTestId('hideDraggable'));
    expect(queryByTestId('draggable-dialog')).toBeFalsy();

    // open the draggable dialog
    fireEvent.click(getByTestId('showDraggable'));
    expect(getByTestId('draggable-dialog')).toBeTruthy();
  });

  it('should hide the dialog by default if there is no content', () => {
    const { getByTestId, queryByTestId, getByText } = render(
      <DraggableDialog />,
    );

    // draggable dialog should be closed by default
    expect(queryByTestId('draggable-dialog')).toBeFalsy();

    // open the draggable dialog
    fireEvent.click(getByTestId('showDraggable'));
    expect(getByTestId('draggable-dialog')).toBeTruthy();
    expect(getByText('Empty')).toBeTruthy();
  });

  it('should render the provided icon when closed', () => {
    const { container } = render(<DraggableDialog mainIcon={ExploreIcon} />);
    const { container: iconContainer } = render(<ExploreIcon />);
    const icon = container.querySelector('svg');
    const expectedIcon = iconContainer.querySelector('svg');
    expect(icon).toEqual(expectedIcon);
  });

  it('should render the title when provided', () => {
    const mockContent = <p>Hello world</p>;
    const name = 'Information';
    const { getByText } = render(
      <DraggableDialog name={name} showTitle>
        {mockContent}
      </DraggableDialog>,
    );
    expect(getByText('Information')).toBeTruthy();
  });
});
