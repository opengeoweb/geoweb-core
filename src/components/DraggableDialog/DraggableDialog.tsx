/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Tooltip, Paper, IconButton, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import ControlCameraIcon from '@material-ui/icons/ControlCamera';
import Draggable from 'react-draggable';
import { makeStyles } from '@material-ui/core/styles';
import WebAssetIcon from '@material-ui/icons/WebAsset';

const useStyles = makeStyles(() => ({
  draggableDialog: {
    position: 'absolute',
    display: 'flex',
    right: '5%',
    top: '10%',
    flexDirection: 'column',
    padding: '10px',
    minWidth: 100,
    maxHeight: 500,
    zIndex: 500,
  },
  draggableHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    minHeight: '30px',
    minWidth: '100px',
  },
  showDraggableButton: {
    margin: '15px',
    background: 'rgba(32, 32, 32, 0.2)',
    borderRadius: '25px',
    zIndex: 100,
  },
}));

interface DraggableDialogProps {
  mainIcon?: typeof WebAssetIcon;
  name?: string;
  showTitle?: boolean;
  showAsOpen?: boolean;
  children?: React.ReactNode;
  openTooltipPosition?: 'left' | 'right' | 'top' | 'bottom';
  closeTooltipPosition?: 'left' | 'right' | 'top' | 'bottom';
  dragTooltipPosition?: 'left' | 'right' | 'top' | 'bottom';
}

const DraggableDialog: React.FC<DraggableDialogProps> = ({
  mainIcon = WebAssetIcon,
  children,
  name = 'Dialog',
  showTitle = false,
  showAsOpen = children !== undefined,
  openTooltipPosition = 'left',
  closeTooltipPosition = 'top',
  dragTooltipPosition = 'top',
}: DraggableDialogProps) => {
  const [open, setOpen] = React.useState(showAsOpen);
  React.useEffect(() => {
    setOpen(showAsOpen);
  }, [showAsOpen]);

  const classes = useStyles();

  const MainIcon = mainIcon;
  return open ? (
    <Draggable bounds="body" handle="strong">
      <Paper data-testid="draggable-dialog" className={classes.draggableDialog}>
        <strong>
          <div className={classes.draggableHeader}>
            <Tooltip
              title={`Drag ${name.toLowerCase()}`}
              placement={dragTooltipPosition}
            >
              <IconButton data-testid="dragDialog" size="small">
                <ControlCameraIcon />
              </IconButton>
            </Tooltip>
            <Tooltip
              title={`Hide ${name.toLowerCase()}`}
              placement={closeTooltipPosition}
            >
              <IconButton
                data-testid="hideDraggable"
                onClick={(): void => setOpen(false)}
                size="small"
              >
                <CloseIcon />
              </IconButton>
            </Tooltip>
          </div>
        </strong>
        {showTitle && (
          <Typography variant="body1" style={{ fontWeight: 500 }}>
            {name}
          </Typography>
        )}
        {children || <div>Empty</div>}
      </Paper>
    </Draggable>
  ) : (
    <Tooltip
      title={`Show ${name.toLowerCase()}`}
      placement={openTooltipPosition}
    >
      <IconButton
        data-testid="showDraggable"
        className={classes.showDraggableButton}
        onClick={(): void => setOpen(true)}
      >
        <MainIcon />
      </IconButton>
    </Tooltip>
  );
};

export default DraggableDialog;
