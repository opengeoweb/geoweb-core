/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import { DynamicModuleLoader } from 'redux-dynamic-modules';

import MapViewLayer from './MapViewLayer';
import { AppStore } from '../../types/types';
import { Layer, Dimension, Bbox } from '../../store/mapStore/types';
import mapConfig from '../../store/mapStore/config';
import * as mapSelectors from '../../store/mapStore/selectors';
import * as mapActions from '../../store/mapStore/actions';
import * as genericActions from '../../store/generic/actions';

import MapView from './MapView';
import {
  SYNCGROUPS_TYPE_SETTIME,
  SYNCGROUPS_TYPE_SETBBOX,
} from '../../store/generic/synchronizationGroups/constants';
import synchronizationGroupConfig from '../../store/generic/config';

interface MapViewConnectProps {
  baseLayers: Layer[];
  layers: Layer[];
  overLayers: Layer[];
  mapId: string;
  mapDimensions: Dimension[];
  activeLayerId?: string;
  srs: string;
  bbox: Bbox;
  animationDelay?: number;
  mapChangeDimension?: typeof mapActions.mapChangeDimension;
  mapUpdateAllDimensions?: typeof mapActions.mapUpdateAllMapDimensions;
  layerChangeStyle?: typeof mapActions.layerChangeStyle;
  layerChangeDimension?: typeof mapActions.layerChangeDimension;
  serviceSetLayers?: typeof mapActions.serviceSetLayers;
  layerSetStyles?: typeof mapActions.layerSetStyles;
  setLayerDimensionsForService?: typeof mapActions.setLayerDimensionsForService;
  genericSetBbox?: typeof genericActions.setBbox;
  registerMap?: typeof mapActions.registerMap;
  syncGroupAddSource?: typeof genericActions.syncGroupAddSource;
  syncGroupRemoveSource?: typeof genericActions.syncGroupRemoveSource;
  unregisterMap?: typeof mapActions.unregisterMap;
  layerError?: typeof mapActions.layerError;
  layerSetDimensions?: typeof mapActions.layerSetDimensions;
  children?: React.ReactNode;
}

const connectRedux = connect(
  (store: AppStore, props: MapViewConnectProps) => ({
    mapDimensions: mapSelectors.getMapDimensions(store, props.mapId),
    layers: mapSelectors.getMapLayers(store, props.mapId),
    baseLayers: mapSelectors.getMapBaseLayers(store, props.mapId),
    overLayers: mapSelectors.getMapOverLayers(store, props.mapId),
    bbox: mapSelectors.getBbox(store, props.mapId),
    srs: mapSelectors.getSrs(store, props.mapId),
    activeLayerId: mapSelectors.getActiveLayerId(store, props.mapId),
    animationDelay: mapSelectors.getMapAnimationDelay(store, props.mapId),
  }),
  {
    mapChangeDimension: mapActions.mapChangeDimension,
    layerChangeStyle: mapActions.layerChangeStyle,
    layerChangeDimension: mapActions.layerChangeDimension,
    layerSetDimensions: mapActions.layerSetDimensions,
    mapUpdateAllDimensions: mapActions.mapUpdateAllMapDimensions,
    serviceSetLayers: mapActions.serviceSetLayers,
    layerSetStyles: mapActions.layerSetStyles,
    setLayerDimensionsForService: mapActions.setLayerDimensionsForService,
    registerMap: mapActions.registerMap,
    unregisterMap: mapActions.unregisterMap,
    genericSetBbox: genericActions.setBbox,
    syncGroupAddSource: genericActions.syncGroupAddSource,
    syncGroupRemoveSource: genericActions.syncGroupRemoveSource,
    layerError: mapActions.layerError,
  },
);

const MapViewConnectComponent: React.FC<MapViewConnectProps> = ({
  baseLayers,
  layers,
  overLayers,
  mapId,
  registerMap,
  syncGroupAddSource,
  unregisterMap,
  syncGroupRemoveSource,
  layerError,
  setLayerDimensionsForService,
  genericSetBbox,
  srs,
  bbox,
  mapDimensions,
  activeLayerId,
  mapChangeDimension,
  mapUpdateAllDimensions,
  layerChangeStyle,
  layerChangeDimension,
  layerSetDimensions,
  serviceSetLayers,
  layerSetStyles,
  animationDelay,
  children,
  ...props
}: MapViewConnectProps) => (
  <div style={{ height: '100%' }}>
    <DynamicModuleLoader modules={[mapConfig, synchronizationGroupConfig]}>
      <MapView
        {...props}
        mapId={mapId}
        onMount={(): void => {
          registerMap(mapId);

          syncGroupAddSource({
            id: mapId,
            type: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
          });
        }}
        onUnMount={(): void => {
          unregisterMap(mapId);
          syncGroupRemoveSource({
            id: mapId,
          });
        }}
        srs={srs}
        bbox={bbox}
        dimensions={mapDimensions}
        activeLayerId={activeLayerId}
        animationDelay={animationDelay}
        onMapChangeDimension={mapChangeDimension}
        onMapUpdateAllDimensions={mapUpdateAllDimensions}
        onLayerChangeStyle={layerChangeStyle}
        onLayerChangeDimension={layerChangeDimension}
        onLayerSetDimensions={layerSetDimensions}
        onServiceSetLayers={serviceSetLayers}
        onLayerSetStyles={layerSetStyles}
        onSetLayerDimensionsForService={setLayerDimensionsForService}
        onMapZoomEnd={(a): void => {
          genericSetBbox({ bbox: a.bbox, srs: a.srs, sourceId: mapId });
        }}
      >
        {baseLayers.map(layer => (
          <MapViewLayer
            id={`baselayer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id, error });
            }}
            {...layer}
          />
        ))}
        {layers.map(layer => (
          <MapViewLayer
            id={`layer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id, error });
            }}
            {...layer}
          />
        ))}
        {overLayers.map(layer => (
          <MapViewLayer
            id={`baselayer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id, error });
            }}
            {...layer}
          />
        ))}
        {children}
      </MapView>
    </DynamicModuleLoader>
  </div>
);

/**
 * Connected component used to display the map and selected layers.
 * Includes options to disable the map controls and legend.
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {object} [controls.zoomControls] **optional** controls: object - toggle the map controls, zoomControls defaults to true
 * @param {boolean} [displayTimeInMap] **optional** displayTimeInMap: boolean, toggles the mapTime, defaults to true
 * @example
 * ```<MapViewConnect mapId={mapId} />```
 * @example
 * ```<MapViewConnect mapId={mapId} controls={{ zoomControls: false }} displayTimeInMap={false} />```
 */

const MapViewConnect = connectRedux(MapViewConnectComponent);

export default MapViewConnect;
