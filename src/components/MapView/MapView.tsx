/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { ReactMapView } from '../ReactMapView';
import ZoomControls from '../Mapcontrols/ZoomControls';
import MapTime from '../MapTime/MapTime';
import { ReactMapViewProps } from '../ReactMapView/types';
import { MapViewProps } from './types';

const useStyles = makeStyles(() => ({
  mapViewContainer: {
    display: 'grid',
    width: '100%',
    height: '100%',
    position: 'relative',
    overflow: 'hidden',
  },
  mapView: {
    gridColumnStart: 1,
    gridRowStart: 1,
  },
  zoomControls: {
    position: 'absolute',
    overflow: 'visible',
    display: 'block',
    right: 0,
    margin: '10px',
    zIndex: 100,
    background: 'rgba(32, 32, 32, 0.2)',
    borderRadius: '25px',
  },
}));

let adagucRef;

const MapView: React.FC<MapViewProps> = ({
  children,
  controls = {
    zoomControls: true,
  },
  displayTimeInMap = true,
  // rest props
  ...props
}: MapViewProps) => {
  const classes = useStyles();
  const { dimensions } = props;

  return (
    <div className={classes.mapViewContainer}>
      <div className={classes.zoomControls}>
        {controls && controls.zoomControls && (
          <ZoomControls
            onZoomIn={(): void => adagucRef.zoomIn()}
            onZoomOut={(): void => adagucRef.zoomOut()}
            onZoomReset={(): void =>
              adagucRef.zoomToLayer(adagucRef.getActiveLayer())
            }
          />
        )}
      </div>
      <div className={classes.mapView}>
        <ReactMapView
          {...(props as ReactMapViewProps)}
          onRegisterAdaguc={(adaguc): void => {
            adagucRef = adaguc;
          }}
          showLegend={false}
          displayTimeInMap={false}
        >
          {children}
        </ReactMapView>
      </div>
      {displayTimeInMap && dimensions && <MapTime dimensions={dimensions} />}
    </div>
  );
};

export default MapView;
