/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import MapViewConnect from './MapViewConnect';
import {
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
  WmdefaultReduxLayerRadarKNMI,
} from '../../utils/defaultTestSettings';
import { mockStateMapWithMultipleLayers } from '../../utils/testUtils';
import {
  baseLayerGrey,
  baseLayerWorldMap,
  overLayer,
  overLayer2,
  simplePolygonGeoJSON,
} from '../../utils/testLayers';
import { registerWMLayer } from '../../store/mapStore/utils/helpers';
import * as genericActionTypes from '../../store/generic/constants';
import {
  SYNCGROUPS_TYPE_SETTIME,
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_ADD_SOURCE,
} from '../../store/generic/synchronizationGroups/constants';

import { WEBMAP_REGISTER_MAP } from '../../store/mapStore/constants';

import MapViewLayer from './MapViewLayer';
import * as services from '../../geoweb-webmap/WMJSService';

describe('src/components/MapView/MapViewConnect', () => {
  // mock WMJSService since fetch is not available in jest
  jest.spyOn(services, 'WMJSService').mockImplementation(jest.fn());

  it('should show a layer for each maplayer, baselayer and overlayer', () => {
    const mapId = 'map-1';
    const testLayers = [
      defaultReduxLayerRadarKNMI,
      baseLayerGrey,
      overLayer,
      defaultReduxLayerRadarColor,
      baseLayerWorldMap,
      overLayer2,
    ];
    const mockState = mockStateMapWithMultipleLayers(testLayers, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { container } = render(
      <Provider store={store}>
        <MapViewConnect mapId={mapId} />
      </Provider>,
    );

    const layerList = container.querySelectorAll(
      '[class^=makeStyles-MapViewLayer]',
    );

    expect(layerList.length).toEqual(testLayers.length);

    // baselayers should be shown first
    expect(layerList[0].textContent).toContain(baseLayerGrey.id);
    expect(layerList[1].textContent).toContain(baseLayerWorldMap.id);

    // maplayers should be shown next
    expect(layerList[2].textContent).toContain(defaultReduxLayerRadarKNMI.id);
    expect(layerList[3].textContent).toContain(defaultReduxLayerRadarColor.id);

    // overlayers should be shown last
    expect(layerList[4].textContent).toContain(overLayer.id);
    expect(layerList[5].textContent).toContain(overLayer2.id);
  });

  it('should zoom the map', async () => {
    const mapId = 'map-1';
    const layers = [defaultReduxLayerRadarKNMI, baseLayerGrey, overLayer];
    registerWMLayer(
      WmdefaultReduxLayerRadarKNMI,
      defaultReduxLayerRadarKNMI.id,
    );
    const mockState = mockStateMapWithMultipleLayers(layers, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, container } = render(
      <Provider store={store}>
        <MapViewConnect mapId={mapId} />
      </Provider>,
    );

    expect(getByTestId('zoom-controls')).toBeTruthy();

    const expectedActionsA = [
      { payload: { mapId }, type: WEBMAP_REGISTER_MAP },
      {
        payload: {
          id: mapId,
          type: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
        },
        type: SYNCGROUPS_ADD_SOURCE,
      },
    ];

    await waitFor(() => expect(store.getActions()).toEqual(expectedActionsA));

    store.clearActions();

    /* Now check the bbox action */
    fireEvent.click(container.querySelector('.zoom-out'));

    const expectedActionsB = [
      {
        payload: {
          bbox: {
            bottom: -25333333.333333332,
            left: -25333333.333333332,
            right: 25333333.333333332,
            top: 25333333.333333332,
          },
          sourceId: mapId,
          srs: 'EPSG:3857',
        },
        type: genericActionTypes.GENERIC_SETBBOX,
      },
    ];

    await waitFor(() => expect(store.getActions()).toEqual(expectedActionsB));
  });

  it('should be able to render layers as children', async () => {
    const mapId = 'map-1';
    const layers = [defaultReduxLayerRadarKNMI, baseLayerGrey, overLayer];

    registerWMLayer(
      WmdefaultReduxLayerRadarKNMI,
      defaultReduxLayerRadarKNMI.id,
    );
    const mockState = mockStateMapWithMultipleLayers(layers, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { container } = render(
      <Provider store={store}>
        <MapViewConnect mapId={mapId}>
          <MapViewLayer id="test-layer-A" geojson={simplePolygonGeoJSON} />
          <MapViewLayer id="test-layer-B" geojson={simplePolygonGeoJSON} />
        </MapViewConnect>
      </Provider>,
    );

    const layerList = container.querySelectorAll(
      '[class^=makeStyles-MapViewLayer]',
    );

    expect(layerList.length).toEqual(layers.length + 2);

    // order: baselayers, maplayers, overlayers
    expect(layerList[0].textContent).toContain(baseLayerGrey.id);
    expect(layerList[1].textContent).toContain(defaultReduxLayerRadarKNMI.id);
    expect(layerList[2].textContent).toContain(overLayer.id);
    // child as last
    expect(layerList[3].textContent).toContain('test-layer-A');
    expect(layerList[4].textContent).toContain('test-layer-B');
  });
});
