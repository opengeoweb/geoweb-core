/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import MapView from './MapView';
import { generateMapId } from '../../store/mapStore/utils/helpers';
import { baseLayerGrey, overLayer } from '../../utils/testLayers';
import MapViewLayer from './MapViewLayer';
import { defaultReduxLayerRadarColor } from '../../utils/defaultTestSettings';
import * as services from '../../geoweb-webmap/WMJSService';

describe('src/components/MapView/MapView', () => {
  // mock WMJSService since fetch is not available in jest
  jest.spyOn(services, 'WMJSService').mockImplementation(jest.fn());

  const mapViewProps = {
    mapId: generateMapId(),
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    onMount: (): void => {},
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    onUnMount: (): void => {},
  };

  it('should show  map', () => {
    const { container } = render(<MapView {...mapViewProps} />);
    expect(container.querySelector('[class^=MapViewComponent]')).toBeTruthy();
  });

  it('should show the zoomControls', () => {
    const { queryByTestId } = render(<MapView {...mapViewProps} />);
    expect(queryByTestId('zoom-controls')).toBeTruthy();
  });

  it('should hide the zoomControls', () => {
    const { queryByTestId } = render(
      <MapView {...mapViewProps} controls={{}} />,
    );
    expect(queryByTestId('zoom-controls')).toBeFalsy();
  });

  it('should render map with layers when layers exist', () => {
    const { container } = render(
      <MapView {...mapViewProps}>
        <MapViewLayer {...baseLayerGrey} />
        <MapViewLayer {...overLayer} />
        <MapViewLayer {...defaultReduxLayerRadarColor} />
      </MapView>,
    );

    const layerList = container.querySelectorAll(
      '[class^=makeStyles-MapViewLayer]',
    );

    expect(layerList.length).toEqual(3);
    expect(layerList[0].textContent).toContain(baseLayerGrey.id);
    expect(layerList[1].textContent).toContain(overLayer.id);
    expect(layerList[2].textContent).toContain(defaultReduxLayerRadarColor.id);
  });

  it('should show the time display by default', () => {
    const { queryByTestId } = render(
      <MapView
        {...mapViewProps}
        dimensions={[{ name: 'time', currentValue: new Date().toISOString() }]}
      />,
    );
    expect(queryByTestId('map-time')).toBeTruthy();
  });

  it('should hide the time display', () => {
    const { queryByTestId } = render(
      <MapView
        {...mapViewProps}
        displayTimeInMap={false}
        dimensions={[{ name: 'time', currentValue: new Date().toISOString() }]}
      />,
    );
    expect(queryByTestId('map-time')).toBeFalsy();
  });

  it('should support null objects as children', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const MapWithOptionalChildren: React.FC = (layer1: any, layer2: any) => (
      <MapView mapId={generateMapId()}>
        {layer1 && <MapViewLayer {...layer1} />}
        {layer2 && <MapViewLayer {...layer2} />}
      </MapView>
    );

    const { container } = render(MapWithOptionalChildren(null, baseLayerGrey));

    expect(container.querySelector('[class^=MapViewComponent]')).toBeTruthy();
    const layerList = container.querySelectorAll(
      '[class^=makeStyles-MapViewLayer]',
    );
    expect(layerList.length).toEqual(1);
    expect(layerList[0].textContent).toContain(baseLayerGrey.id);
  });
});
