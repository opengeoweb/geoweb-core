/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import { CustomDate } from '../../../geoweb-webmap/WMTime';
import { ParseISOTimeRangeDuration } from '../../../geoweb-webmap';
import { Layer } from '../../../store/mapStore/types';

import { getWMLayerById } from '../../../store/mapStore/utils/helpers';

const drawTimeIndicationBlocks = (
  /* eslint-disable no-param-reassign */
  ctx: CanvasRenderingContext2D,
  timeBlockStartIndex: number,
  timeBlockStopIndex: number,
  canvasDateIntervalHour: ParseISOTimeRangeDuration,
  sliderStopIndex: number,
  scaleWidth: number,
  canvasHeight: number,
  canvasDateInterval: ParseISOTimeRangeDuration,
): void => {
  for (let j = timeBlockStartIndex - 1; j < timeBlockStopIndex + 1; j += 1) {
    const dateAtTimeStep = canvasDateIntervalHour.getDateAtTimeStep(j);
    const layerTimeIndex = canvasDateInterval.getTimeStepFromDate(
      dateAtTimeStep,
    );
    const layerTimeIndexNext = canvasDateInterval.getTimeStepFromDate(
      canvasDateIntervalHour.getDateAtTimeStep(j + 1),
    );
    const pos = layerTimeIndex / sliderStopIndex;
    const width = (layerTimeIndexNext - layerTimeIndex) / sliderStopIndex;
    ctx.fillStyle = moment.utc(dateAtTimeStep).isBefore(moment.utc())
      ? '#fff3cd'
      : '#d4edda';
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 0.5;
    const x = Math.round(pos * scaleWidth);
    ctx.fillRect(
      x + 0.5,
      canvasHeight - 16 + 0.5,
      width * scaleWidth + 0.5,
      15,
    );
    ctx.strokeRect(
      x + 0.5,
      canvasHeight - 16 + 0.5,
      width * scaleWidth + 0.5,
      15,
    );
    ctx.fillStyle = '#000';
    ctx.fillText(
      `${dateAtTimeStep.getUTCHours()}H`,
      pos * scaleWidth + 3,
      canvasHeight - 3,
    );
  }
};

const drawLayerBlocks = (
  ctx: CanvasRenderingContext2D,
  canvasWidth: number,
  sliderStopIndex: number,
  sliderMapIndex: number,
  scaleWidth: number,
  startDate: string,
  endDate: string,
  layers: Layer[],
  activeLayerId: string,
  canvasDateInterval,
  layerHeight: number,
): void => {
  // TODO: (Maarten Plieger, 2020-03-19): Efficiency: if only time changes just redraw that part?
  const blockHeight = layerHeight - 4;
  ctx.beginPath();
  ctx.lineWidth = 1;
  for (let j = 0; j < layers.length; j += 1) {
    const y = j * layerHeight + 1;
    const layer = getWMLayerById(layers[j].id);
    if (layer) {
      const activeLayer = layers[j].id === activeLayerId;
      const dim = layer.getDimension('time');
      ctx.fillStyle = '#FFF';
      ctx.strokeStyle = '#BBB';
      if (activeLayer) {
        ctx.strokeStyle = '#88F';
      }
      ctx.fillRect(0, y, canvasWidth, blockHeight);
      ctx.strokeRect(0, y + 0.5, canvasWidth, blockHeight);
      if (dim) {
        ctx.fillStyle = '#CCC';
        ctx.strokeStyle = '#888';
        ctx.fillRect(0, y + 6.5, canvasWidth, blockHeight - 12);
        const layerStartIndex = dim.getIndexForValue(startDate, false);
        const layerStopIndex = dim.getIndexForValue(endDate, false);
        for (let q = layerStartIndex - 1; q < layerStopIndex + 1; q += 1) {
          let layerTimeIndex;
          let layerTimeIndexNext;
          let sliderMapIndex2 = 0;
          try {
            const fstVal = dim.getValueForIndex(q);
            const sndVal = dim.getValueForIndex(q + 1);

            // Check if actual dates are coming back
            if (
              fstVal.length === 20 &&
              fstVal[19] === 'Z' &&
              fstVal[10] === 'T' &&
              sndVal.length === 20 &&
              sndVal[19] === 'Z' &&
              sndVal[10] === 'T'
            ) {
              layerTimeIndex = canvasDateInterval.getTimeStepFromISODate(
                fstVal,
              );
              layerTimeIndexNext = canvasDateInterval.getTimeStepFromISODate(
                sndVal,
              );
            }
          } catch (error) {
            // eslint-disable-next-line no-continue
            continue;
          }
          const pos = layerTimeIndex / sliderStopIndex;
          const posNext = layerTimeIndexNext / sliderStopIndex;
          ctx.fillStyle = activeLayer ? '#beeef8' : '#f0f0f0';
          try {
            sliderMapIndex2 = canvasDateInterval.getTimeStepFromISODate(
              dim.currentValue,
            );
            if (
              sliderMapIndex2 >= layerTimeIndex &&
              sliderMapIndex2 < layerTimeIndexNext
            ) {
              ctx.fillStyle = '#FFFF60';
            }
            // eslint-disable-next-line no-empty
          } catch (e) {}

          ctx.strokeStyle = '#AAA';
          const x = Math.round(pos * scaleWidth);
          const w = Math.round(posNext * scaleWidth) - x;

          ctx.fillRect(x + 0.5, y + 1, w, blockHeight - 1);
          ctx.beginPath();
          ctx.moveTo(x + 0.5, y + 1.5);
          ctx.lineTo(x + 0.5, y + blockHeight);
          ctx.moveTo(x + w + 0.5, y + 1.5);
          ctx.lineTo(x + w + 0.5, y + blockHeight);
          ctx.stroke();
        }
      }
    }
  }
};

export const renderCanvas = (
  ctx: CanvasRenderingContext2D,
  canvasWidth: number,
  ctxHeight: number,
  timeDimension: string,
  layers: Layer[],
  activeLayerId: string,
  startDate: CustomDate,
  endDate: CustomDate,
  layerHeight: number,
): void => {
  if (!timeDimension) {
    return;
  }
  if (
    (timeDimension.length !== 20 && timeDimension.length !== 24) ||
    (timeDimension[19] !== 'Z' && timeDimension[23] !== 'Z') ||
    timeDimension[10] !== 'T'
  ) {
    return;
  }

  const numlayers = Math.max(2, layers.length + 1);
  const canvasHeight = Math.max(20 * (numlayers - 2), ctxHeight);

  ctx.fillStyle = '#FFF';
  ctx.fillRect(0, 0, canvasWidth, canvasHeight);
  ctx.strokeStyle = '#FF0000';

  ctx.font = '14px Helvetica';
  ctx.lineWidth = 1;
  const scaleWidth = canvasWidth;

  const canvasDateIntervalStr = `${startDate.toISO8601()}/${endDate.toISO8601()}/PT1M`;
  const canvasDateInterval = new ParseISOTimeRangeDuration(
    canvasDateIntervalStr,
  );
  let sliderCurrentIndex = -1;
  try {
    sliderCurrentIndex = canvasDateInterval.getTimeStepFromISODate(
      moment.utc().format('YYYY-MM-DDTHH:mm:ss[Z]'),
      true,
    );
  } catch (e) {
    /*
        getTimeStepFromISODate throws exceptions if the requested date is outside the daterange of the dateinterval
      */
    sliderCurrentIndex = 0; // TODO: (Maarten Plieger, 2020-03-19): Maybe set default of the WMLayer here
  }
  const sliderMapIndex = canvasDateInterval.getTimeStepFromISODate(
    timeDimension,
    true,
  );
  const sliderStopIndex = canvasDateInterval.getTimeStepFromISODate(
    endDate.toISO8601(),
    true,
  );

  const canvasDateIntervalStrHour = `${startDate.toISO8601()}/${endDate.toISO8601()}/PT1H`;
  const canvasDateIntervalHour = new ParseISOTimeRangeDuration(
    canvasDateIntervalStrHour,
  );
  const timeBlockStartIndex = canvasDateIntervalHour.getTimeStepFromDate(
    startDate,
    true,
  );
  const timeBlockStopIndex = canvasDateIntervalHour.getTimeStepFromISODate(
    endDate.toISO8601(),
    true,
  );

  /* Draw system time, past and future */
  let x = Math.round((sliderCurrentIndex / sliderStopIndex) * scaleWidth) + 0.5;
  ctx.fillStyle = '#ffffff';
  ctx.fillRect(0, 0, canvasWidth, canvasHeight);

  /* Draw time indication blocks */
  drawTimeIndicationBlocks(
    ctx,
    timeBlockStartIndex,
    timeBlockStopIndex,
    canvasDateIntervalHour,
    sliderStopIndex,
    scaleWidth,
    canvasHeight,
    canvasDateInterval,
  );
  /* Draw blocks for layer -- this function takes the longest */
  drawLayerBlocks(
    ctx,
    canvasWidth,
    sliderStopIndex,
    sliderMapIndex,
    scaleWidth,
    (startDate as unknown) as string,
    (endDate as unknown) as string,
    layers,
    activeLayerId,
    canvasDateInterval,
    layerHeight,
  );

  /* Draw current system time */
  if (sliderCurrentIndex !== -1) {
    ctx.lineWidth = 2;
    ctx.beginPath();
    ctx.strokeStyle = '#0000FF';
    x = Math.round((sliderCurrentIndex / sliderStopIndex) * scaleWidth) + 0.5;
    ctx.moveTo(x, 0);
    ctx.lineTo(x, canvasHeight);
    ctx.stroke();
  }

  /* Draw current map time */
  ctx.lineWidth = 2;

  x = Math.round((sliderMapIndex / sliderStopIndex) * scaleWidth);
  ctx.fillStyle = '#333';
  ctx.strokeStyle = '#444';
  ctx.lineWidth = 2;
  ctx.beginPath();
  ctx.moveTo(x + 0.0, 0);
  ctx.lineTo(x + 0.0, canvasHeight);
  ctx.stroke();

  ctx.strokeStyle = '#000000';
  ctx.lineWidth = 0.5;
  ctx.fillStyle = '#FFF';
  ctx.fillRect(x - 26, canvasHeight - 15, 52, 14);
  ctx.fillStyle = '#000000';
  ctx.font = 'bold 14px Helvetica';
  ctx.fillText(timeDimension.substring(11, 16), x - 15, canvasHeight - 3);
};

let onSetNewDateDebouncer: string;

export const onsetNewDateDebounced = (
  dateToSet: string,
  clickedLayerId: string,
  onSetNewDate: (dateToSet: string, clickedLayerId: string) => void,
): void => {
  /* Debounce the dateToSet with a simple timeout */
  if (onSetNewDateDebouncer === undefined) {
    onSetNewDateDebouncer = dateToSet;
    onSetNewDate(dateToSet, clickedLayerId);
    window.setTimeout(() => {
      if (
        onSetNewDateDebouncer !== undefined &&
        onSetNewDateDebouncer !== dateToSet
      ) {
        onSetNewDate(onSetNewDateDebouncer, clickedLayerId);
      }
      onSetNewDateDebouncer = undefined;
    }, 10);
  } else {
    onSetNewDateDebouncer = dateToSet;
  }
};

export const onCanvasClick = (
  x: number,
  y: number,
  layers: Layer[],
  canvasWidth: number,
  startDate: CustomDate,
  endDate: CustomDate,
  onSetNewDate: (dateToSet: string, clickedLayerId: string) => void,
  onSetActiveLayerId: (newActiveLayerId: string) => void,
  layerHeight: number,
  activeLayerId: string,
): void => {
  const canvasDateIntervalStr = `${startDate.toISO8601()}/${endDate.toISO8601()}/PT1M`;
  const canvasDateInterval = new ParseISOTimeRangeDuration(
    canvasDateIntervalStr,
  );
  const t = x / canvasWidth;

  const layerClicked = Math.floor(y / layerHeight);
  const clickedLayerId = layers[layerClicked] && layers[layerClicked].id;
  if (!clickedLayerId) return;
  if (
    layerClicked >= 0 &&
    layerClicked < layers.length &&
    activeLayerId !== clickedLayerId
  ) {
    onSetActiveLayerId(clickedLayerId);
  }
  const layerHasTime =
    layers[layerClicked].dimensions.filter(dim => {
      return dim.name === 'time';
    }).length > 0;
  if (!canvasDateInterval || !layerHasTime) {
    return;
  }
  const s = canvasDateInterval.getTimeSteps() - 1;
  const newTimeStep = Math.round(t * s);
  const newDate = canvasDateInterval.getDateAtTimeStep(newTimeStep);
  const dateToSet = newDate.toISO8601();
  onsetNewDateDebounced(dateToSet, clickedLayerId, onSetNewDate);
};
