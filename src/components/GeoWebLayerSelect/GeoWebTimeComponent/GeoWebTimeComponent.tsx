/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */
import React, { Component } from 'react';
import moment from 'moment';
import CanvasComponent from '../../CanvasComponent/CanvasComponent';
import { parseISO8601DateToDate } from '../../../geoweb-webmap';
import { Layer, Dimension } from '../../../store/mapStore/types';
import {
  onCanvasClick,
  renderCanvas,
  onsetNewDateDebounced,
} from './GeoWebTimeComponentRenderFunctions';
import {
  getPreviousTimeStepForLayerId,
  getNextTimeStepForLayerId,
  getFirstTimeStepForLayerId,
  getLastTimeStepForLayerId,
} from '../../../store/mapStore/utils/helpers';

interface TimeComponentProps {
  layers: Layer[];
  activeLayerId: string;
  dimensions: Dimension[];
  onSetNewDate: (newDate: string, activeLayerId: string) => void;
  onSetActiveLayerId: (newActiveLayerId: string) => void;
}

export default class TimeComponent extends Component<TimeComponentProps> {
  ctx = null;

  width = null;

  height = null;

  // eslint-disable-next-line react/static-property-placement
  static defaultProps = {
    onSetNewDate: (): void => {
      /* nothing */
    },
    onSetActiveLayerId: (): void => {
      /* nothing */
    },
  };

  setPreviousTimeStep(): void {
    const { activeLayerId, onSetNewDate } = this.props;
    const prevTimeStep = getPreviousTimeStepForLayerId(activeLayerId);
    if (!prevTimeStep) return;
    onsetNewDateDebounced(prevTimeStep, activeLayerId, onSetNewDate);
  }

  setNextTimeStep(): void {
    const { activeLayerId, onSetNewDate } = this.props;
    const nextTimeStep = getNextTimeStepForLayerId(activeLayerId);
    if (!nextTimeStep) return;
    onsetNewDateDebounced(nextTimeStep, activeLayerId, onSetNewDate);
  }

  setFirstTimeStep(): void {
    const { activeLayerId, onSetNewDate } = this.props;
    const firstTimeStep = getFirstTimeStepForLayerId(activeLayerId);
    if (!firstTimeStep) return;
    onsetNewDateDebounced(firstTimeStep, activeLayerId, onSetNewDate);
  }

  setLastTimeStep(): void {
    const { activeLayerId, onSetNewDate } = this.props;
    const lastTimeStep = getLastTimeStepForLayerId(activeLayerId);
    if (!lastTimeStep) return;
    onsetNewDateDebounced(lastTimeStep, activeLayerId, onSetNewDate);
  }

  getTimeDim(): Dimension {
    const { layers, dimensions } = this.props;
    if (layers.length === 0 || !dimensions) return null;
    const timeDim = dimensions.find(dim => dim.name === 'time');
    if (!timeDim) return null;
    return timeDim;
  }

  render(): React.ReactElement {
    const {
      layers,
      activeLayerId,
      dimensions,
      onSetNewDate,
      onSetActiveLayerId,
    } = this.props;
    const layerHeight = 45;
    const timeWidth = 24;
    let startDate = parseISO8601DateToDate(moment().toISOString());
    let endDate = parseISO8601DateToDate(moment().toISOString());
    let currentValue = moment().toISOString();
    if (layers.length > 0 && dimensions) {
      const timeDim = dimensions.find(dim => dim.name === 'time');

      if (timeDim && timeDim.currentValue) currentValue = timeDim.currentValue;

      const startDateFromCurrentValue = parseISO8601DateToDate(currentValue);
      const endDateFromCurrentValue = parseISO8601DateToDate(currentValue);
      if (startDateFromCurrentValue && endDateFromCurrentValue) {
        const hours = startDate.getUTCHours();
        const h = parseInt((hours / timeWidth).toString(), 10) * timeWidth;
        startDateFromCurrentValue.setUTCHours(h);
        startDateFromCurrentValue.setUTCMinutes(0);
        startDateFromCurrentValue.setUTCSeconds(0);
        endDateFromCurrentValue.setUTCHours(h);
        endDateFromCurrentValue.setUTCMinutes(0);
        endDateFromCurrentValue.setUTCSeconds(0);
        endDateFromCurrentValue.setUTCHours(
          endDateFromCurrentValue.getUTCHours() + timeWidth,
        );
        startDate = startDateFromCurrentValue;
        endDate = endDateFromCurrentValue;
      }
    }
    return (
      <CanvasComponent
        onWheel={(wheel): void => {
          if (wheel.deltaY < 0) {
            this.setPreviousTimeStep();
          } else {
            this.setNextTimeStep();
          }
        }}
        onCanvasClick={(x, y, width): void => {
          if (x >= 0 && x < width) {
            onCanvasClick(
              x,
              y,
              layers,
              width,
              startDate,
              endDate,
              onSetNewDate,
              onSetActiveLayerId,
              layerHeight,
              activeLayerId,
            );
          }
        }}
        onKeyDown={(event): void => {
          switch (event.keyCode) {
            case 37:
              this.setPreviousTimeStep();
              break;
            case 39:
              this.setNextTimeStep();
              break;
            case 36:
              this.setFirstTimeStep();
              break;
            case 35:
              this.setLastTimeStep();
              break;
            default:
          }
        }}
        onRenderCanvas={(
          ctx: CanvasRenderingContext2D,
          width: number,
          height: number,
        ): void => {
          this.ctx = ctx;
          renderCanvas(
            ctx,
            width,
            height,
            currentValue,
            layers,
            activeLayerId,
            startDate,
            endDate,
            layerHeight,
          );
        }}
      />
    );
  }
}
