/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import * as mapActions from '../../../store/mapStore/actions';
import TimeComponent from './GeoWebTimeComponent';
import { Layer, Dimension } from '../../../store/mapStore/types';
import { AppStore } from '../../../types/types';
import * as mapSelectors from '../../../store/mapStore/selectors';
import * as genericActions from '../../../store/generic/actions';

interface ConnectedTimeComponentProps {
  dispatch?: () => void;
  layers: Layer[];
  mapId: string;
  dimensions: Dimension[];
  mapSetActiveLayerId?: typeof mapActions.setActiveLayerId;
}

const connectReduxTimeComponent = connect(
  (store: AppStore, props: ConnectedTimeComponentProps) => ({
    layers: mapSelectors.getMapLayers(store, props.mapId),
    dimensions: mapSelectors.getMapDimensions(store, props.mapId),
    activeLayerId: mapSelectors.getActiveLayerId(store, props.mapId),
    isAnimating: mapSelectors.isAnimating(store, props.mapId),
  }),
  {
    mapSetActiveLayerId: mapActions.setActiveLayerId,
    stopMapAnimation: mapActions.mapStopAnimation,
    setTime: genericActions.setTime,
  },
);

const TimeComponentConnect: React.FC<{
  mapId: string;
}> = connectReduxTimeComponent(
  ({
    layers,
    activeLayerId,
    dimensions,
    mapId,
    setTime,
    mapSetActiveLayerId,
    isAnimating,
    stopMapAnimation,
  }) => (
    <TimeComponent
      layers={layers}
      dimensions={dimensions}
      activeLayerId={activeLayerId}
      onSetNewDate={(newDate): void => {
        if (isAnimating) {
          stopMapAnimation({ mapId });
        }
        setTime({
          sourceId: mapId,
          value: newDate,
        });
      }}
      onSetActiveLayerId={(_activeLayerId): void => {
        mapSetActiveLayerId({
          mapId,
          layerId: _activeLayerId,
        });
      }}
    />
  ),
);
export default TimeComponentConnect;
