/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import GeoWebTimeComponentConnect from './GeoWebTimeComponentConnect';
import { mockStateMapWithLayer } from '../../../utils/testUtils';
import {
  defaultReduxLayerRadarKNMI,
  layerWithoutTimeDimension,
} from '../../../utils/defaultTestSettings';

describe('src/components/GeoWebLayerSelect/GeoWebTimeComponent/GeoWebTimeComponentConnect', () => {
  it('should render the time component when the map has a layer with time dimension', () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { container } = render(
      <Provider store={store}>
        <GeoWebTimeComponentConnect mapId={mapId} />
      </Provider>,
    );

    expect(container.querySelector('canvas')).toBeTruthy();
  });

  it('should also render the time component when the layer on the map does not have a time dimension', () => {
    const mapId = 'mapid_1';
    const layer = layerWithoutTimeDimension;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { container } = render(
      <Provider store={store}>
        <GeoWebTimeComponentConnect mapId={mapId} />
      </Provider>,
    );

    expect(container.querySelector('canvas')).toBeTruthy();
  });

  // [Tineke 2020-04-16] can't make these tests yet because the component only renders a <canvas /> element without anything in it, impossible to unit test properly
  // it should set the active layer and change the dimension when clicking on the time component for the second layer
  // it should stop animation when clicking time component if map was animating
});
