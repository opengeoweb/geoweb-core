/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, findByRole } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { GeoWebLayerSelectConnect } from '.';
import { defaultReduxLayerRadarColor } from '../../utils/defaultTestSettings';
import * as layerActionTypes from '../../store/mapStore/layers/constants';
import * as mapActionTypes from '../../store/mapStore/map/constants';
import { mockStateMapWithLayer } from '../../utils/testUtils';

describe('src/components/GeoWebLayerSelect/GeoWebLayerSelectConnect', () => {
  it('should delete a layer', () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { getAllByTestId } = render(
      <Provider store={store}>
        <GeoWebLayerSelectConnect mapId={mapId} />
      </Provider>,
    );
    const sortableRows = getAllByTestId('sortableRow');
    expect(sortableRows.length).toEqual(mockState.layers.allIds.length);

    const expectedActions = {
      payload: { layerId: layer.id, mapId },
      type: layerActionTypes.WEBMAP_LAYER_DELETE,
    };

    const deleteButtons = getAllByTestId('deleteButton');
    fireEvent.click(deleteButtons[0]);

    expect(store.getActions()).toContainEqual(expectedActions);
  });

  it('should move a layer', async () => {
    const mapId = 'mapid_2';
    const layer = defaultReduxLayerRadarColor;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { container, getAllByTestId } = render(
      <Provider store={store}>
        <GeoWebLayerSelectConnect mapId={mapId} />
      </Provider>,
    );

    const dragHandles = getAllByTestId('dragHandle');
    fireEvent.mouseOver(dragHandles[0]);

    // wait for the tooltip to appear before continuing
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain('Drag a layer');

    // simulate moving the first layer
    fireEvent.mouseDown(dragHandles[0]);
    fireEvent.mouseUp(dragHandles[0]);

    const expectedActions = {
      payload: { mapId, oldIndex: 0, newIndex: 0 },
      type: mapActionTypes.WEBMAP_LAYER_MOVE,
    };
    expect(store.getActions()).toContainEqual(expectedActions);
  });
});
