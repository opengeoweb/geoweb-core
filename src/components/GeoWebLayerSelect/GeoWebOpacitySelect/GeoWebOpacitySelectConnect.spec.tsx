/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { defaultReduxLayerRadarColor } from '../../../utils/defaultTestSettings';

import OpacitySelectConnect from './GeoWebOpacitySelectConnect';
import { mockStateMapWithLayer } from '../../../utils/testUtils';
import * as layerActionTypes from '../../../store/mapStore/layers/constants';

describe('src/components/GeoWebLayerSelect/GeoWebOpacitySelect/GeoWebOpacitySelectConnect', () => {
  it('should change opacity of layer', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = mockStateMapWithLayer(layer, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);

    const { findByText, getByTestId } = render(
      <Provider store={store}>
        <OpacitySelectConnect mapId={mapId} layerId={layer.id} />
      </Provider>,
    );

    const select = getByTestId('selectOpacity');

    fireEvent.mouseDown(select);
    const newOpacity = { value: 0.1, title: '10 %' };

    const menuItem = await findByText(newOpacity.title);

    fireEvent.click(menuItem);

    const expectedAction = [
      {
        payload: { layerId: layer.id, opacity: newOpacity.value },
        type: layerActionTypes.WEBMAP_LAYER_CHANGE_OPACITY,
      },
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
