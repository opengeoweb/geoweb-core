/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, findByRole } from '@testing-library/react';
import OpacitySelect from './GeoWebOpacitySelect';

describe('src/components/GeoWebLayerSelect/GeoWebOpacitySelect/GeoWebOpacitySelect', () => {
  const props = {
    currentOpacity: 0.8,
    tooltipTitle: 'someTitle',
    onLayerChangeOpacity: jest.fn(),
  };

  const newOpacity = { value: 0.1, title: '10 %' };

  it('should display passed opacity value', async () => {
    const { container } = render(<OpacitySelect {...props} />);

    expect(container.querySelector('input').value).toEqual(
      props.currentOpacity.toString(),
    );
  });

  it('should call onChangeOpacity if value selected', async () => {
    const { getByTestId, findByText } = render(<OpacitySelect {...props} />);
    const select = getByTestId('selectOpacity');

    fireEvent.mouseDown(select);

    const menuItem = await findByText(newOpacity.title);
    fireEvent.click(menuItem);

    expect(props.onLayerChangeOpacity).toHaveBeenCalledWith(newOpacity.value);
  });

  it('should have the correct tooltip', async () => {
    const { container, getByTestId } = render(<OpacitySelect {...props} />);
    fireEvent.mouseOver(getByTestId('selectOpacity'));
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toMatch(props.tooltipTitle);
  });
});
