/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import * as mapSelectors from '../../../store/mapStore/selectors';
import * as mapActions from '../../../store/mapStore/actions';
import { AppStore } from '../../../types/types';

import OpacitySelectComponent from './GeoWebOpacitySelect';

const opacitySelectTitle = 'Select opacity of the layer';

interface OpacitySelectConnectProps {
  layerId: string;
  opacity: number;
  onLayerChangeOpacity: typeof mapActions.layerChangeOpacity;
}

const connectRedux = connect(
  (store: AppStore, props: OpacitySelectConnectProps) => ({
    opacity: mapSelectors.getLayerOpacity(store, props.layerId),
  }),
  {
    onLayerChangeOpacity: mapActions.layerChangeOpacity,
  },
);

const OpacitySelect: React.FC<OpacitySelectConnectProps> = ({
  layerId,
  opacity,
  onLayerChangeOpacity,
}: OpacitySelectConnectProps) => (
  <OpacitySelectComponent
    currentOpacity={opacity}
    tooltipTitle={opacitySelectTitle}
    onLayerChangeOpacity={(newOpacity: number): void => {
      onLayerChangeOpacity({
        layerId,
        opacity: newOpacity,
      });
    }}
  />
);

/**
 * Contains a select allowing you to change the opacity of a layer on the map
 *
 * Expects the following props:
 * @param {string} layerId layerId: string - Id of the layer
 * @example
 * ``` <OpacitySelectConnect layerId="layerid_1"/> ```
 */
const OpacitySelectConnect = connectRedux(OpacitySelect);

export default OpacitySelectConnect;
