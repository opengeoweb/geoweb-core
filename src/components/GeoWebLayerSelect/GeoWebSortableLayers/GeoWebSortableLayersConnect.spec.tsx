/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, findByRole } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import GeoWebSortableLayerConnect from './GeoWebSortableLayersConnect';
import * as mapActionTypes from '../../../store/mapStore/constants';
import {
  defaultReduxLayerRadarColor,
  defaultReduxLayerRadarKNMI,
} from '../../../utils/defaultTestSettings';
import { mockStateMapWithMultipleLayers } from '../../../utils/testUtils';

describe('src/components/GeoWebLayerSelect/GeoWebSortableLayers/GeoWebSortableLayerConnect', () => {
  it('should change active layerId when clicking on a non active layer row', async () => {
    const mapId = 'mapid_1';
    const layer1 = defaultReduxLayerRadarColor;
    const layer2 = defaultReduxLayerRadarKNMI;
    const mockState = mockStateMapWithMultipleLayers([layer1, layer2], mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);

    const { getAllByTestId } = render(
      <Provider store={store}>
        <GeoWebSortableLayerConnect mapId={mapId} />
      </Provider>,
    );

    expect(store.getState().webmap.byId[mapId].activeLayerId).toEqual(
      layer1.id,
    );

    const layerRows = getAllByTestId('layerRowItem');

    // active layer row
    const [activeLayerRow, nonActiveLayerRow] = layerRows;
    fireEvent.click(activeLayerRow);
    expect(store.getActions()).toEqual([]);

    // non active layer row
    fireEvent.click(nonActiveLayerRow);
    const expectedAction = {
      payload: { layerId: layer2.id, mapId },
      type: mapActionTypes.WEBMAP_SET_ACTIVELAYERID,
    };
    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should move a layer when dragging a layer', async () => {
    const mapId = 'mapid_1';
    const layer1 = defaultReduxLayerRadarColor;
    const layer2 = defaultReduxLayerRadarKNMI;
    const mockState = mockStateMapWithMultipleLayers([layer1, layer2], mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);

    const { getAllByTestId, container } = render(
      <Provider store={store}>
        <GeoWebSortableLayerConnect mapId={mapId} />
      </Provider>,
    );

    const dragHandles = getAllByTestId('dragHandle');
    fireEvent.mouseOver(dragHandles[0]);

    // wait for the tooltip to appear before continuing
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain('Drag a layer');

    // simulate moving the first layer
    fireEvent.mouseDown(dragHandles[0]);
    fireEvent.mouseUp(dragHandles[0]);

    const expectedActions = [
      {
        payload: { mapId, oldIndex: 0, newIndex: 0 },
        type: mapActionTypes.WEBMAP_LAYER_MOVE,
      },
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });
});
