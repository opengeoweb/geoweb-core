/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { produce } from 'immer';
import { render, getAllByTestId, getByTestId } from '@testing-library/react';
import { GeoWebSortableLayers } from '..';
import {
  defaultReduxServices,
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
} from '../../../utils/defaultTestSettings';

const layerList = [
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
  produce(defaultReduxLayerRadarColor, draft => {
    draft.id = 'otherid';
  }),
];

const props = {
  mapId: 'mapid_1',
  layers: layerList,
  activeLayerId: '',
  services: defaultReduxServices,
  onSortEnd: jest.fn(),
  onLayerChangeOpacity: {},
  onLayerChangeName: {},
  onLayerChangeStyle: {},
  onLayerChangeDimension: {},
  onLayerDelete: {},
  onLayerEnable: {},
};

describe('src/components/GeoWebLayerSelect/GeoWebSortableLayers/GeoWebSortableLayers', () => {
  it('should have a row for each layer in the default order', () => {
    const { container } = render(<GeoWebSortableLayers {...props} />);
    const sortableRows = getAllByTestId(container, 'sortableRow');

    expect(sortableRows.length).toEqual(layerList.length);
    sortableRows.map((row, index): void => {
      const currEl: HTMLElement = row.querySelector('.MuiCheckbox-root');
      expect(currEl.title).toEqual(layerList[index].title);
      return null;
    });
  });

  it('should show a draghandle for each row', async () => {
    const { container } = render(<GeoWebSortableLayers {...props} />);
    const sortableRows = getAllByTestId(container, 'sortableRow');

    sortableRows.forEach(row => {
      expect(getByTestId(row, 'dragHandle')).toBeTruthy();
    });
  });
});
