/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import { Grid } from '@material-ui/core';

import { Layer } from '../../../store/mapStore/types';
import GeoWebLayerRow from '../GeoWebLayerRow/GeoWebLayerRow';

const SortableReactWMJSLayerRow = SortableElement(({ children }) => (
  <div className="noselect">
    <Grid container alignItems="center" data-testid="sortableRow">
      {children}
    </Grid>
  </div>
));

// SimpleSortableRowContainer which returns GeoWebLayerRow with onRenderRow callback to render custom component
export const SimpleSortableRowContainer = SortableContainer(
  ({ layerIds, onRenderRow }) => (
    <div>
      {layerIds.map((layerId, layerIndex) => (
        <SortableReactWMJSLayerRow
          key={`SortableReactWMJSLayerRow${layerId}`}
          index={layerIndex}
        >
          {onRenderRow(layerId)}
        </SortableReactWMJSLayerRow>
      ))}
    </div>
  ),
);

// default SortableRowContainer to render default stateless component of GeoWebLayerRow
export const SortableRowContainer = SortableContainer(
  ({
    mapId,
    services,
    layers,
    activeLayerId,
    onLayerChangeOpacity,
    onLayerChangeName,
    onLayerChangeStyle,
    onLayerChangeDimension,
    onLayerDelete,
    onLayerEnable,
  }) => (
    <div>
      {layers.map((layer: Layer, layerIndex: number) => (
        <SortableReactWMJSLayerRow
          key={`SortableReactWMJSLayerRow${layer.id}`}
          index={layerIndex}
        >
          <GeoWebLayerRow
            mapId={mapId}
            layer={layer}
            activeLayerId={activeLayerId}
            layerId={layer.id}
            services={services}
            onLayerChangeOpacity={onLayerChangeOpacity}
            onLayerChangeName={onLayerChangeName}
            onLayerChangeStyle={onLayerChangeStyle}
            onLayerChangeDimension={onLayerChangeDimension}
            onLayerDelete={onLayerDelete}
            onLayerEnable={onLayerEnable}
          />
        </SortableReactWMJSLayerRow>
      ))}
    </div>
  ),
);

export default SortableRowContainer;
