/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import * as mapActions from '../../../store/mapStore/actions';
import { SimpleSortableRowContainer } from './GeoWebSortableLayers';
import { Layer } from '../../../store/mapStore/types';
import { AppStore } from '../../../types/types';
import * as mapSelectors from '../../../store/mapStore/selectors';
import GeoWebLayerRow from '../GeoWebLayerRow/GeoWebLayerRow';

import DeleteLayerConnect from '../../DeleteLayer/DeleteLayerConnect';
import EnableLayerConnect from '../../EnableLayer/EnableLayerConnect';
import OpacityLayerConnect from '../GeoWebOpacitySelect/GeoWebOpacitySelectConnect';
import RenderLayersConnect from '../../RenderLayers/RenderLayersConnect';
import RenderStylesConnect from '../../RenderStyles/RenderStylesConnect';
import DimensionSelectConnect from '../../DimensionSelect/DimensionSelectConnect';

interface SortableLayerConnectProps {
  layers: Layer[];
  mapId: string;
  moveLayer?: typeof mapActions.layerMoveLayer;
  setActiveLayerId?: typeof mapActions.setActiveLayerId;
}

const connectRedux = connect(
  (store: AppStore, props: SortableLayerConnectProps) => ({
    layerIds: mapSelectors.getLayerIds(store, props.mapId),
    activeLayerId: mapSelectors.getActiveLayerId(store, props.mapId),
  }),
  {
    moveLayer: mapActions.layerMoveLayer,
    setActiveLayerId: mapActions.setActiveLayerId,
  },
);

const SortableLayerConnect: React.FC<{
  mapId: string;
}> = connectRedux(
  ({ layerIds, mapId, activeLayerId, moveLayer, setActiveLayerId }) => (
    <SimpleSortableRowContainer
      layerIds={layerIds}
      useDragHandle
      onSortEnd={({ oldIndex, newIndex }): void => {
        moveLayer({
          mapId,
          oldIndex,
          newIndex,
        });
      }}
      onRenderRow={(layerId): React.ReactChild => (
        <GeoWebLayerRow
          mapId={mapId}
          layerId={layerId}
          activeLayerId={activeLayerId}
          onLayerRowClick={(newLayerId): void => {
            if (newLayerId !== activeLayerId) {
              setActiveLayerId({ layerId: newLayerId, mapId });
            }
          }}
          layerDeleteLayout={
            <DeleteLayerConnect mapId={mapId} layerId={layerId} />
          }
          layerEnableLayout={
            <EnableLayerConnect mapId={mapId} layerId={layerId} />
          }
          layerOpacityLayout={
            <OpacityLayerConnect mapId={mapId} layerId={layerId} />
          }
          layerServicesLayout={
            <RenderLayersConnect mapId={mapId} layerId={layerId} />
          }
          layerStylesLayout={
            <RenderStylesConnect mapId={mapId} layerId={layerId} />
          }
          layerDimensionLayout={
            <DimensionSelectConnect mapId={mapId} layerId={layerId} />
          }
        />
      )}
    />
  ),
);
export default SortableLayerConnect;
