/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import GeoWebLayerSelect from './GeoWebLayerSelect';

import GeoWebTimeControlsConnect from './GeoWebTimeControls/GeoWebTimeControlsConnect';
import GeoWebTimeComponentConnect from './GeoWebTimeComponent/GeoWebTimeComponentConnect';
import GeoWebSortableLayersConnect from './GeoWebSortableLayers/GeoWebSortableLayersConnect';
import GeoWebBaseLayersConnect from './GeoWebBaseLayers/GeoWebBaseLayersConnect';
import WMSLoaderConnect from '../WMSLoader/WMSLoaderConnect';

import { Layer, LayerType } from '../../store/mapStore/types';
import { Service } from '../WMSLoader/services';

interface ConnectedGeoWebLayerSelectProps {
  mapId: string;
  preloadedAvailableBaseLayers?: Layer[];
  preloadedMapServices?: Service[];
  preloadedBaseServices?: Service[];
}

/**
 * Layer and Time controls
 * Allows you to add/remove/edit layers shown on the map and step through their timesteps. Also allows you to start/stop an animation and change the base layer shown
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {Layer[]} [preloadedAvailableBaseLayers] **optional** preloadedAvailableBaseLayers: array of Layer objects - contains an array of Layer objects (consisting solely of type baseLayer) that are available to be shown as basemaps
 * @param {Service[]} [preloadedMapServices] **optional** preloadedMapServices: array of Service objects - contains an array of Service objects that are available in the WMSLoader from which maplayers can be loaded
 * @param {Service[]} [preloadedBaseServices] **optional** preloadedBaseServices: array of Service objects - contains an array of Service objects that are available in the baselayer WMSLoader from which baselayers can be loaded
 * @example
 * ``` <GeowebLayerSelectConnect mapId="mapid_1" preloadedAvailableBaseLayers = {baseLayersList} preloadedMapServices = {mapServicesList} preloadedBaseServices = {baseServicesList} /> ```
 */
const GeowebLayerSelectConnect: React.FC<ConnectedGeoWebLayerSelectProps> = ({
  mapId,
  preloadedAvailableBaseLayers = [],
  preloadedMapServices = [],
  preloadedBaseServices = [],
}: ConnectedGeoWebLayerSelectProps) => (
  <GeoWebLayerSelect
    mapId={mapId}
    baseLayersLayout={
      <GeoWebBaseLayersConnect
        mapId={mapId}
        preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
      />
    }
    timeControls={<GeoWebTimeControlsConnect mapId={mapId} />}
    timeSlider={<GeoWebTimeComponentConnect mapId={mapId} />}
    sortableLayerLayout={<GeoWebSortableLayersConnect mapId={mapId} />}
    wmsLoaderLayout={
      <WMSLoaderConnect
        mapId={mapId}
        tooltip="Add WMS layer to the map"
        {...(preloadedMapServices.length === 0
          ? {}
          : { preloadedServices: preloadedMapServices })}
      />
    }
    wmsLoaderBaseLayerLayout={
      <WMSLoaderConnect
        mapId={mapId}
        layerType={LayerType.baseLayer}
        tooltip="Add WMS baselayer to the map"
        {...(preloadedBaseServices.length === 0
          ? {}
          : { preloadedServices: preloadedBaseServices })}
      />
    }
  />
);

export default GeowebLayerSelectConnect;
