/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Layer, Dimension, Services } from '../../../store/mapStore/types';
import { filterNonTimeDimensions } from '../../../store/mapStore/utils/helpers';

import RenderStyles from '../../RenderStyles/RenderStyles';
import DeleteButton from '../../DeleteLayer/DeleteButton';
import EnableLayer from '../../EnableLayer/EnableLayer';
import RenderLayers from '../../RenderLayers/RenderLayers';
import OpacitySelect from '../GeoWebOpacitySelect/GeoWebOpacitySelect';
import DragHandle from '../../DragHandle/DragHandle';
import DimensionSelect from '../../DimensionSelect/DimensionSelect';

const useStyles = makeStyles({
  layerRowContainer: {
    background: '#eee',
    borderBottom: '1px solid #BBB',
    borderTop: '1px solid #BBB',
    height: '42px' /* Plus 2x margin makes 45 */,
    overflow: 'hidden',
    marginTop: '1px',
    marginBottom: '2px',
  },
  layerRowContainerActive: {
    background: '#beeef8',
    borderBottom: '1px solid #88F',
    borderTop: '1px solid #88F',
    height: '42px' /* Plus 2x margin makes 45 */,
    overflow: 'hidden',
    marginTop: '1px',
    marginBottom: '2px',
  },
  layerRowItem: {
    marginTop: '5px',
    paddingLeft: '3px',
    paddingRight: '3px',
  },
  dragHandle: {
    display: 'inline-flex',
  },
});

interface GeoWebLayerRowProps {
  mapId: string;
  layerId?: string;
  layer?: Layer;
  activeLayerId?: string;

  onLayerChangeOpacity?: (payload: {
    layerId: string;
    opacity: number;
  }) => void;
  onLayerChangeName?: (payload: { layerId: string; name: string }) => void;
  onLayerChangeStyle?: (payload: { layerId: string; style: string }) => void;
  onLayerChangeDimension?: (payload: {
    origin: string;
    layerId: string;
    dimension: Dimension;
  }) => void;
  onLayerDelete?: (payload: { mapId: string; layerId: string }) => void;
  onLayerEnable?: (payload: { layerId: string; enabled: boolean }) => void;
  onLayerRowClick?: (layerId?: string) => void;
  layerDeleteLayout?: React.ReactChild;
  layerEnableLayout?: React.ReactChild;
  layerOpacityLayout?: React.ReactChild;
  layerServicesLayout?: React.ReactChild;
  layerStylesLayout?: React.ReactChild;
  layerDimensionLayout?: React.ReactChild;
  services?: Services;
}

const GeoWebLayerRow: React.FC<GeoWebLayerRowProps> = ({
  mapId,
  layerId,
  layer,
  activeLayerId,
  services,
  onLayerChangeOpacity,
  onLayerChangeName,
  onLayerChangeStyle,
  onLayerDelete,
  onLayerEnable,
  onLayerChangeDimension,
  onLayerRowClick = (): void => null,
  // layout
  layerDeleteLayout,
  layerEnableLayout,
  layerOpacityLayout,
  layerServicesLayout,
  layerStylesLayout,
  layerDimensionLayout,
}: GeoWebLayerRowProps) => {
  const classes = useStyles();
  const activeLayer = layerId === activeLayerId;
  const onClickRow = (): void => onLayerRowClick(layerId);

  return (
    <Grid
      container
      className={
        activeLayer
          ? classes.layerRowContainerActive
          : classes.layerRowContainer
      }
      data-testid="layerRowItem"
      justify="flex-start"
      onClick={onClickRow}
    >
      <Grid item sm={4} md={3} className={classes.layerRowItem}>
        <Grid container>
          <div className={classes.dragHandle}>
            <DragHandle />
          </div>

          {layerDeleteLayout || (
            <DeleteButton
              tooltipTitle="Delete"
              onClickDelete={(): void => {
                onLayerDelete({ mapId, layerId });
              }}
            />
          )}

          {layerEnableLayout || (
            <EnableLayer
              enableLayer={layer.enabled}
              title={layer.name}
              onChangeEnableLayer={(): void => {
                onLayerEnable({ layerId, enabled: !layer.enabled });
              }}
            />
          )}
          <Grid item sm={4} md={5} lg={6}>
            {layerOpacityLayout || (
              <OpacitySelect
                tooltipTitle="Select opacity of the layer"
                currentOpacity={layer.opacity}
                onLayerChangeOpacity={(_opacity: number): void => {
                  onLayerChangeOpacity({
                    layerId: layer.id,
                    opacity: _opacity,
                  });
                }}
              />
            )}
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={2} className={classes.layerRowItem}>
        {layerServicesLayout || (
          <RenderLayers
            layerName={layer.name}
            layers={
              (services &&
                layer.service &&
                services[layer.service] &&
                services[layer.service].layers) ||
              []
            }
            onChangeLayerName={(name): void => {
              onLayerChangeName({ layerId: layer.id, name });
            }}
          />
        )}
      </Grid>
      <Grid item xs={2} className={classes.layerRowItem}>
        {layerStylesLayout || (
          <RenderStyles
            services={services}
            mapId={mapId}
            layer={layer}
            onChangeLayerStyle={(
              _mapId: string,
              _layerId: string,
              style: string,
            ): void => {
              onLayerChangeStyle({ layerId: _layerId, style });
            }}
          />
        )}
      </Grid>
      <Grid item sm={4} md={5} className={classes.layerRowItem}>
        {layerDimensionLayout || (
          <DimensionSelect
            layer={layer}
            mapId={mapId}
            availableLayerDimensions={filterNonTimeDimensions(layer.dimensions)}
            onLayerChangeDimension={(
              _mapId: string,
              _layerId: string,
              dimensionName: string,
              dimensionValue: string,
            ): void => {
              const dimension: Dimension = {
                name: dimensionName,
                currentValue: dimensionValue,
              };

              onLayerChangeDimension({
                origin: 'layerrow',
                layerId,
                dimension,
              });
            }}
          />
        )}
      </Grid>
    </Grid>
  );
};

export default GeoWebLayerRow;
