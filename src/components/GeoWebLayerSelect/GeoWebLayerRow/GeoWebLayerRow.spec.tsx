/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import GeoWebLayerRow from './GeoWebLayerRow';
import {
  defaultReduxLayerRadarKNMI,
  defaultReduxServices,
  multiDimensionLayer,
  WmMultiDimensionLayer,
} from '../../../utils/defaultTestSettings';
import { registerWMLayer } from '../../../store/mapStore/utils/helpers';

describe('src/components/GeoWebLayerSelect/GeoWebLayerRow/GeoWebLayerRow', () => {
  it('should show the layer row with all its items', () => {
    const defaultProps = {
      services: defaultReduxServices,
      layer: defaultReduxLayerRadarKNMI,
      mapId: 'map_1',
      onLayerChangeOpacity: jest.fn(),
      onLayerChangeName: jest.fn(),
      onLayerChangeStyle: jest.fn(),
      onLayerChangeDimension: jest.fn(),
      onLayerDelete: jest.fn(),
      onLayerEnable: jest.fn(),
    };

    const { queryByTestId } = render(<GeoWebLayerRow {...defaultProps} />);
    const row = queryByTestId('layerRowItem');
    expect(row).toBeTruthy();
    expect(row.querySelector('button').getAttribute('title')).toEqual('Delete');
    expect(row.querySelector('[type=checkbox]')).toBeTruthy(); // EnableLayer
    expect(queryByTestId('selectOpacity')).toBeTruthy();
    expect(queryByTestId('selectLayer')).toBeTruthy();
    expect(queryByTestId('selectStyle')).toBeTruthy();
    expect(queryByTestId('selectDimension')).toBeFalsy();
  });

  it('should show the dimension select for a layer with dimensions', async () => {
    const defaultProps = {
      services: defaultReduxServices,
      layer: multiDimensionLayer,
      mapId: 'map_1',
      onLayerChangeOpacity: jest.fn(),
      onLayerChangeName: jest.fn(),
      onLayerChangeStyle: jest.fn(),
      onLayerChangeDimension: jest.fn(),
      onLayerDelete: jest.fn(),
      onLayerEnable: jest.fn(),
    };

    registerWMLayer(WmMultiDimensionLayer, 'multiDimensionLayerMock');
    const { queryByTestId } = render(<GeoWebLayerRow {...defaultProps} />);
    const row = queryByTestId('layerRowItem');
    expect(row).toBeTruthy();
    expect(row.querySelector('button').getAttribute('title')).toEqual('Delete');
    expect(row.querySelector('[type=checkbox]')).toBeTruthy(); // EnableLayer
    expect(queryByTestId('selectOpacity')).toBeTruthy();
    expect(queryByTestId('selectLayer')).toBeTruthy();
    expect(queryByTestId('selectStyle')).toBeTruthy();
    await waitFor(() => expect(queryByTestId('selectDimension')).toBeTruthy());
  });

  it('should be able to click on a row', () => {
    const defaultProps = {
      services: defaultReduxServices,
      layer: defaultReduxLayerRadarKNMI,
      mapId: 'map_1',
      layerId: 'test-1',
      onLayerRowClick: jest.fn(),
    };

    const { queryByTestId } = render(<GeoWebLayerRow {...defaultProps} />);
    const row = queryByTestId('layerRowItem');

    fireEvent.click(row);

    expect(defaultProps.onLayerRowClick).toHaveBeenCalledWith(
      defaultProps.layerId,
    );
  });
});
