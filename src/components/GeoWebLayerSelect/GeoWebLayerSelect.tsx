/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Layer, Dimension, Services } from '../../store/mapStore/types';
import WMSLoader from '../WMSLoader/WMSLoader';
import { GeoWebTimeControls } from './GeoWebTimeControls/GeoWebTimeControls';
import GeoWebBaseLayers from './GeoWebBaseLayers/GeoWebBaseLayers';
import GeoWebSortableLayers from './GeoWebSortableLayers/GeoWebSortableLayers';
import GeoWebTimeComponent from './GeoWebTimeComponent/GeoWebTimeComponent';

const useStyles = makeStyles({
  root: {
    height: '100%',
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  geoWebLayerSelectContainer: {
    height: '100%',
  },
});

interface GeoWebLayerSelectProps {
  mapId: string;
  layers?: Layer[];
  activeLayerId?: string;
  dimensions?: Dimension[];
  baseLayers?: Layer[];
  availableBaseLayers?: Layer[];
  onLayerChangeOpacity?: (payload: {
    layerId: string;
    opacity: number;
  }) => void;
  onLayerChangeName?: (payload: { layerId: string; name: string }) => void;
  onLayerChangeStyle?: (payload: { layerId: string; style: string }) => void;
  onLayerChangeDimension?: (payload: {
    origin: string;
    layerId: string;
    dimension: Dimension;
  }) => void;
  onLayerDelete?: (payload: { mapId: string; layerId: string }) => void;
  onLayerEnable?: (payload: { layerId: string; enabled: boolean }) => void;
  onSetNewDate?: (newDate: string, layerId: string) => void;
  onSetActiveLayerId?: (activeLayerId: string) => void;
  onLayerMove?: (payload: {
    mapId: string;
    oldIndex: number;
    newIndex: number;
  }) => void;
  isAnimating?: boolean;
  onStartAnimation?: () => void;
  onStopAnimation?: () => void;
  onChangeBaseLayers?: (baselayerId: string) => void;
  onClickService?: (serviceURL: string, layerName: string) => void;
  onGoToBegin?: () => void;
  onGoToEnd?: () => void;
  onGoToPrevious?: () => void;
  onGoToNext?: () => void;
  onGoToAccessTime?: () => void;
  timeControls?: React.ReactChild;
  timeSlider?: React.ReactChild;
  sortableLayerLayout?: React.ReactChild;
  baseLayersLayout?: React.ReactChild;
  wmsLoaderLayout?: React.ReactChild;
  wmsLoaderBaseLayerLayout?: React.ReactChild;
  services?: Services;
}
const GeoWebLayerSelect: React.FC<GeoWebLayerSelectProps> = ({
  mapId,
  baseLayers = [],
  availableBaseLayers = [],
  layers = [],
  activeLayerId = null,
  services = {},
  dimensions = [],
  onLayerChangeOpacity = (): null => null,
  onLayerChangeName = (): null => null,
  onLayerChangeStyle = (): null => null,
  onLayerChangeDimension = (): null => null,
  onLayerDelete = (): null => null,
  onLayerEnable = (): null => null,
  onSetNewDate = (): null => null,
  onSetActiveLayerId = (): null => null,
  onLayerMove = (): null => null,
  isAnimating = false,
  onStartAnimation = (): null => null,
  onStopAnimation = (): null => null,
  onChangeBaseLayers = (): null => null,
  onClickService = (): null => null,
  onGoToBegin = (): null => null,
  onGoToEnd = (): null => null,
  onGoToPrevious = (): null => null,
  onGoToNext = (): null => null,
  onGoToAccessTime = (): null => null,
  // layout slots
  timeControls,
  timeSlider,
  sortableLayerLayout,
  baseLayersLayout,
  wmsLoaderLayout,
  wmsLoaderBaseLayerLayout,
}: GeoWebLayerSelectProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container className={classes.geoWebLayerSelectContainer}>
        {/* Top bar */}
        <Grid item xs={5}>
          {wmsLoaderLayout || (
            <WMSLoader
              onClickService={onClickService}
              tooltip="Add WMS layer to the map"
            />
          )}
        </Grid>
        <Grid item xs={4}>
          <Grid container alignContent="flex-start" direction="row">
            {timeControls || (
              <GeoWebTimeControls
                activeLayerId={activeLayerId}
                isAnimating={isAnimating}
                onStartAnimation={onStartAnimation}
                onStopAnimation={onStopAnimation}
                onGoToBegin={onGoToBegin}
                onGoToEnd={onGoToEnd}
                onGoToPrevious={onGoToPrevious}
                onGoToNext={onGoToNext}
                onGoToAccessTime={onGoToAccessTime}
              />
            )}
          </Grid>
        </Grid>
        <Grid item xs={3}>
          <Grid container>
            <Grid item xs={10}>
              <Grid container justify="flex-end" direction="row">
                <Grid item>
                  {baseLayersLayout || (
                    <GeoWebBaseLayers
                      selectedBaseLayers={baseLayers}
                      availableBaseLayers={availableBaseLayers}
                      onChangeBaseLayers={onChangeBaseLayers}
                    />
                  )}
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={2}>
              {wmsLoaderBaseLayerLayout || (
                <WMSLoader
                  onClickService={onClickService}
                  tooltip="Add WMS baselayer to the map"
                />
              )}
            </Grid>
          </Grid>
        </Grid>
        {/* Layer Rows */}
        <Grid item xs={8} style={{ overflow: 'hidden' }}>
          {sortableLayerLayout || (
            <GeoWebSortableLayers
              mapId={mapId}
              layers={layers}
              activeLayerId={activeLayerId}
              services={services}
              onSortEnd={(a): void => {
                onLayerMove({
                  mapId,
                  oldIndex: a.oldIndex,
                  newIndex: a.newIndex,
                });
              }}
              onLayerChangeOpacity={onLayerChangeOpacity}
              onLayerChangeName={onLayerChangeName}
              onLayerChangeStyle={onLayerChangeStyle}
              onLayerChangeDimension={onLayerChangeDimension}
              onLayerDelete={onLayerDelete}
              onLayerEnable={onLayerEnable}
            />
          )}
          <br />
        </Grid>
        <Grid item xs={4}>
          {timeSlider || (
            <GeoWebTimeComponent
              layers={layers}
              activeLayerId={activeLayerId}
              dimensions={dimensions}
              onSetNewDate={onSetNewDate}
              onSetActiveLayerId={onSetActiveLayerId}
            />
          )}
        </Grid>
      </Grid>
    </div>
  );
};

export default GeoWebLayerSelect;
