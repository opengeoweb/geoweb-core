/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import produce from 'immer';
import GeoWebLayerSelect from './GeoWebLayerSelect';
import {
  defaultReduxServices,
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
} from '../../utils/defaultTestSettings';

const layerList = [
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
  produce(defaultReduxLayerRadarKNMI, draft => {
    draft.id = 'otherid';
  }),
];

const mockDimensions = [
  {
    name: 'time',
    units: 'ISO8601',
    currentValue: '2020-03-13T13:30:00Z',
  },
];

const props = {
  mapId: 'mapid_1',
  services: defaultReduxServices,
  layers: layerList,
  dimensions: mockDimensions,
  baseLayers: [],
  activeLayerId: layerList[0].id,
  opacitySelect: '',
  onLayerChangeOpacity: jest.fn(),
  onLayerChangeName: jest.fn(),
  onLayerChangeStyle: jest.fn(),
  onLayerChangeDimension: jest.fn(),
  onLayerDelete: jest.fn(),
  onLayerEnable: jest.fn(),
  onSetNewDate: jest.fn(),
  onSetActiveLayerId: jest.fn(),
  onLayerMove: jest.fn(),
};

describe('src/components/GeoWebLayerSelect/GeoWebLayerSelect', () => {
  it('should have as many rows as layers ', () => {
    const { container } = render(<GeoWebLayerSelect {...props} />);
    expect(
      container.querySelectorAll('[class*=makeStyles-layerRowContainer]')
        .length,
    ).toEqual(layerList.length);
  });

  it('should warn if no services available ', () => {
    const mockProps = {
      ...props,
      services: null,
    };
    const { container } = render(<GeoWebLayerSelect {...mockProps} />);
    const layerRowContainer = container.querySelectorAll(
      '[class*=makeStyles-layerRowContainer]',
    );

    layerRowContainer.forEach((row): void => {
      expect(row.textContent.includes('No service available')).toBeTruthy();
    });
  });

  it('should fire onLayerChangeOpacity', async () => {
    const newOpacity = { value: 0.1, title: '10 %' };
    const { getAllByTestId, findByText } = render(
      <GeoWebLayerSelect {...props} />,
    );
    const selects = getAllByTestId('selectOpacity');
    expect(selects.length === layerList.length);

    fireEvent.mouseDown(selects[0]);

    const menuItem = await findByText(newOpacity.title);

    fireEvent.click(menuItem);

    expect(props.onLayerChangeOpacity).toHaveBeenCalled();
  });

  it('should fire onLayerChangeStyle', async () => {
    const { getAllByTestId, findByText } = render(
      <GeoWebLayerSelect {...props} />,
    );
    const newStyleName =
      defaultReduxServices['https://testservice'].layer
        .RADNL_OPER_R___25PCPRR_L3_KNMI.styles[1].name;

    const selects = getAllByTestId('selectStyle');
    expect(selects.length === layerList.length);

    fireEvent.mouseDown(selects[0]);

    const menuItem = await findByText(newStyleName);

    fireEvent.click(menuItem);

    expect(props.onLayerChangeStyle).toHaveBeenCalled();
  });

  it('should fire onLayerDelete', () => {
    const { getAllByTestId } = render(<GeoWebLayerSelect {...props} />);
    const deleteButtons = getAllByTestId('deleteButton');
    fireEvent.click(deleteButtons[0]);
    expect(props.onLayerDelete).toHaveBeenCalled();
  });

  it('should fire onLayerEnable', () => {
    const { getAllByRole } = render(<GeoWebLayerSelect {...props} />);
    const checkboxes = getAllByRole('checkbox');
    fireEvent.click(checkboxes[0]);
    expect(props.onLayerEnable).toHaveBeenCalled();
  });
});
