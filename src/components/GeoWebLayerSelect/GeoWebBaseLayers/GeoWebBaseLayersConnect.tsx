/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import { Layer } from '../../../store/mapStore/types';
import GeoWebBaseLayersSelect from './GeoWebBaseLayers';
import * as mapSelectors from '../../../store/mapStore/selectors';
import { AppStore } from '../../../types/types';
import { mapActions } from '../../..';

interface GeoWebBaseLayersConnectProps {
  mapId: string;
  currentAvailableBaseLayers: Layer[];
  preloadedAvailableBaseLayers: Layer[];
  selectedBaseLayers: Layer[];
  setBaseLayers: typeof mapActions.setBaseLayers;
  addAvailableBaseLayers: typeof mapActions.addAvailableBaseLayers;
}

const connectRedux = connect(
  (store: AppStore, props: GeoWebBaseLayersConnectProps) => ({
    selectedBaseLayers: mapSelectors.getMapBaseLayers(store, props.mapId),
    currentAvailableBaseLayers: mapSelectors.getAvailableBaseLayers(store),
  }),
  {
    setBaseLayers: mapActions.setBaseLayers,
    addAvailableBaseLayers: mapActions.addAvailableBaseLayers,
  },
);

const GeoWebBaseLayers: React.FC<GeoWebBaseLayersConnectProps> = ({
  mapId,
  selectedBaseLayers,
  setBaseLayers,
  currentAvailableBaseLayers,
  preloadedAvailableBaseLayers,
  addAvailableBaseLayers,
}: GeoWebBaseLayersConnectProps) => {
  React.useEffect(() => {
    addAvailableBaseLayers({ layers: preloadedAvailableBaseLayers });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onChangeBaseLayers = (newSelectedLayerId: string): void => {
    const newBaseLayer = currentAvailableBaseLayers.filter(
      layer => layer.id === newSelectedLayerId,
    );
    setBaseLayers({ mapId, layers: newBaseLayer });
  };

  return (
    <GeoWebBaseLayersSelect
      selectedBaseLayers={selectedBaseLayers}
      availableBaseLayers={currentAvailableBaseLayers}
      onChangeBaseLayers={onChangeBaseLayers}
    />
  );
};

/**
 * Allows you to select a baselayer
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {Layer[]} preloadedAvailableBaseLayers preloadedAvailableBaseLayers: array of Layer objects - contains an array of Layer objects (consisting solely of type baseLayer) that are available to be shown as basemaps
 * @example
 * ``` <GeoWebBaseLayersConnect mapId="mapid_1" preloadedAvailableBaseLayers = {baseLayersList} /> ```
 */
const GeoWebBaseLayersConnect = connectRedux(GeoWebBaseLayers);
export default GeoWebBaseLayersConnect;
