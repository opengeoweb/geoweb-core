/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { MenuItem, FormControl, FormHelperText } from '@material-ui/core';

import { Layer } from '../../../store/mapStore/types';
import TooltipSelect from '../../TooltipSelect/TooltipSelect';

interface GeowebBaseLayersProps {
  selectedBaseLayers: Layer[];
  availableBaseLayers: Layer[];
  onChangeBaseLayers: (name: string) => void;
}

const GeowebBaseLayers: React.FC<GeowebBaseLayersProps> = ({
  selectedBaseLayers,
  availableBaseLayers,
  onChangeBaseLayers,
}: GeowebBaseLayersProps) => {
  if (!availableBaseLayers || !availableBaseLayers.length) {
    return <div>No service available</div>;
  }

  const selectedBaseLayer =
    typeof selectedBaseLayers[0] !== 'undefined'
      ? selectedBaseLayers[0]
      : { id: '' };

  return (
    <FormControl>
      <TooltipSelect
        tooltip="Select base layer"
        inputProps={{
          SelectDisplayProps: {
            'data-testid': 'selectBaseLayer',
          },
        }}
        style={{ maxWidth: '100%' }}
        value={selectedBaseLayer.id}
        onChange={(event: React.ChangeEvent<{ value: string }>): void => {
          onChangeBaseLayers(event.target.value);
        }}
      >
        {availableBaseLayers.map((baseLayer: Layer) => (
          <MenuItem key={baseLayer.id} value={baseLayer.id}>
            {baseLayer.name}
          </MenuItem>
        ))}
      </TooltipSelect>
      <FormHelperText>Base layer</FormHelperText>
    </FormControl>
  );
};

export default GeowebBaseLayers;
