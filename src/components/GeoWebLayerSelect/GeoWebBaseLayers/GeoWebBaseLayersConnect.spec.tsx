/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import GeoWebBaseLayersConnect from './GeoWebBaseLayersConnect';
import { baseLayerGrey, baseLayerWorldMap } from '../../../utils/testLayers';
import * as layerActionTypes from '../../../store/mapStore/layers/constants';
import { mockStateMapWithLayer } from '../../../utils/testUtils';

describe('src/components/GeoWebLayerSelect/GeoWebBaseLayers/GeoWebBaseLayersConnect', () => {
  it('should add a base layer', async () => {
    const mapId = 'mapid_1';
    const preloadedAvailableBaseLayers = [baseLayerGrey, baseLayerWorldMap];
    const mockState = mockStateMapWithLayer(
      preloadedAvailableBaseLayers[0],
      mapId,
    );
    mockState.layers.availableBaseLayers = {
      allIds: [baseLayerGrey.id, baseLayerWorldMap.id],
      byId: {
        [baseLayerGrey.id]: baseLayerGrey,
        [baseLayerWorldMap.id]: baseLayerWorldMap,
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { getByTestId, findByText } = render(
      <Provider store={store}>
        <GeoWebBaseLayersConnect
          mapId={mapId}
          preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
        />
      </Provider>,
    );

    const baseLayerSelect = getByTestId('selectBaseLayer');
    fireEvent.mouseDown(baseLayerSelect);

    const newBaseLayer = preloadedAvailableBaseLayers[1];
    const menuItem = await findByText(newBaseLayer.name);
    fireEvent.click(menuItem);

    const expectedActions = {
      payload: { layers: [newBaseLayer], mapId },
      type: layerActionTypes.WEBMAP_SET_BASELAYERS,
    };
    expect(store.getActions()).toContainEqual(expectedActions);
  });
});
