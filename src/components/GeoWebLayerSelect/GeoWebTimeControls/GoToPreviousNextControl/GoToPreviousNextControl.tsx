/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { IconButton } from '@material-ui/core';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

import moment from 'moment';

import {
  getPreviousTimeStepForLayerId,
  getNextTimeStepForLayerId,
} from '../../../../store/mapStore/utils/helpers';
import { Dimension } from '../../../../store/mapStore/types';

export type GoToDirection = 'previous' | 'next';

export const getPreviousNextDimensionValue = (
  activeLayerId: string,
  direction: GoToDirection,
): string => {
  const newDimensionValue = moment
    .utc(
      direction === 'previous'
        ? getPreviousTimeStepForLayerId(activeLayerId)
        : getNextTimeStepForLayerId(activeLayerId),
      'YYYY-MM-DDTHH:mm:SS',
    )
    .format();

  return newDimensionValue;
};

export interface GoToPreviousNextControlProps {
  onGoTo: (dimension: Dimension) => void;
  layerId?: string;
  direction?: GoToDirection;
  disabled?: boolean;
}

const GoToPreviousNextControl: React.FC<GoToPreviousNextControlProps> = ({
  onGoTo,
  layerId,
  direction = 'next',
  disabled = false,
}: GoToPreviousNextControlProps) => {
  const onClick = (): void => {
    const newDimension = getPreviousNextDimensionValue(layerId, direction);

    if (newDimension) {
      onGoTo({
        name: 'time',
        currentValue: newDimension,
      });
    }
  };

  return (
    <IconButton
      disabled={disabled}
      role="button"
      onClick={layerId ? onClick : (): void => onGoTo(null)}
    >
      {direction === 'next' ? (
        <NavigateNextIcon className="goto-next" />
      ) : (
        <NavigateBeforeIcon className="goto-previous" />
      )}
    </IconButton>
  );
};

export default GoToPreviousNextControl;
