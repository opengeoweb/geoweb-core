/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import * as mapSelectors from '../../../../store/mapStore/selectors';
import * as mapActions from '../../../../store/mapStore/actions';
import { AppStore } from '../../../../types/types';
import GoToPreviousNextControl, {
  GoToDirection,
} from './GoToPreviousNextControl';
import { Dimension } from '../../../../store/mapStore/types';

export const getActiveDimension = (dimensions: Dimension[]): Dimension =>
  dimensions.find(dimension => dimension.name === 'time');

interface GoToPreviousNextControlConnectProps {
  layerId: string;
  mapId: string;
  dimensions: Dimension[];
  direction: GoToDirection;
  onLayerChangeDimension: typeof mapActions.layerChangeDimension;
  onMapStopAnimation: typeof mapActions.mapStopAnimation;
}

const connectRedux = connect(
  (store: AppStore, props: GoToPreviousNextControlConnectProps) => ({
    dimensions: mapSelectors.getLayerDimensions(store, props.layerId),
  }),
  {
    onLayerChangeDimension: mapActions.layerChangeDimension,
    onMapStopAnimation: mapActions.mapStopAnimation,
  },
);

const GoToPreviousNextControlConnect: React.FC<GoToPreviousNextControlConnectProps> = ({
  layerId,
  mapId,
  dimensions,
  direction,
  onLayerChangeDimension,
  onMapStopAnimation,
}: GoToPreviousNextControlConnectProps) => {
  const activeDimension = getActiveDimension(dimensions);
  const onGoTo = (newDimension: Dimension): void => {
    onMapStopAnimation({ mapId });
    onLayerChangeDimension({
      origin: `goto-${direction}`,
      layerId,
      dimension: newDimension,
    });
  };

  return (
    <GoToPreviousNextControl
      layerId={layerId}
      disabled={!activeDimension}
      direction={direction}
      onGoTo={onGoTo}
    />
  );
};

export default connectRedux(GoToPreviousNextControlConnect);
