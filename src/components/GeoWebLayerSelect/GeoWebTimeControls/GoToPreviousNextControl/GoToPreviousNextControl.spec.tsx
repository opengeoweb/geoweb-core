/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import GoToPreviousNextControl, {
  GoToPreviousNextControlProps,
  getPreviousNextDimensionValue,
} from './GoToPreviousNextControl';
import { WmMultiDimensionLayer } from '../../../../utils/defaultTestSettings';
import WMLayer from '../../../../geoweb-webmap/WMLayer';
import { registerWMLayer } from '../../../../store/mapStore/utils/helpers';

describe('src/components/GeoWebLayerSelect/GeoWebTimeControl/GoToPreviousNextControl/GoToPreviousNextControl', () => {
  it('should have (not disabled) navigateNextIcon with default props', () => {
    const props = {
      onGoTo: jest.fn(),
      layerId: 'layer1',
    } as GoToPreviousNextControlProps;

    const { container } = render(<GoToPreviousNextControl {...props} />);
    const button = getByRole(container, 'button');

    expect(button.querySelectorAll('.goto-next')).toHaveLength(1);
    expect(button).toBeDefined();

    fireEvent.click(button);
    expect(props.onGoTo).toHaveBeenCalled();
  });

  it('should have NavigateBeforeIcon when direction = previous', () => {
    const props = {
      onGoTo: jest.fn(),
      direction: 'previous',
      layerId: 'layer1',
    } as GoToPreviousNextControlProps;

    const { container } = render(<GoToPreviousNextControl {...props} />);
    const button = getByRole(container, 'button');

    expect(button).toBeDefined();
    expect(button.querySelectorAll('.goto-previous')).toHaveLength(1);

    fireEvent.click(button);
    expect(props.onGoTo).toHaveBeenCalled();
  });

  it('should set timestep to next if direction = previous', () => {
    const testLayer: WMLayer = WmMultiDimensionLayer;
    registerWMLayer(testLayer, testLayer.id);

    const props = {
      onGoTo: jest.fn(),
      layerId: testLayer.id,
      direction: 'previous',
    } as GoToPreviousNextControlProps;

    const { container } = render(<GoToPreviousNextControl {...props} />);
    const button = getByRole(container, 'button');

    fireEvent.click(button);

    const previousValue = getPreviousNextDimensionValue(
      testLayer.id,
      props.direction,
    );

    expect(props.onGoTo).toHaveBeenCalledWith({
      name: 'time',
      currentValue: previousValue,
    });
  });

  it('should set timestep to next if direction = next', () => {
    const testLayer: WMLayer = WmMultiDimensionLayer;
    registerWMLayer(testLayer, testLayer.id);

    const props = {
      onGoTo: jest.fn(),
      layerId: testLayer.id,
      direction: 'next',
    } as GoToPreviousNextControlProps;

    const { container } = render(<GoToPreviousNextControl {...props} />);
    const button = getByRole(container, 'button');

    fireEvent.click(button);

    const nextValue = getPreviousNextDimensionValue(
      testLayer.id,
      props.direction,
    );

    expect(props.onGoTo).toHaveBeenCalledWith({
      name: 'time',
      currentValue: nextValue,
    });
  });
});
