/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import GoToPreviousNextControlConnect from './GoToPreviousNextControlConnect';
import * as mapActionTypes from '../../../../store/mapStore/constants';
import { mockStateMapWithMultipleLayers } from '../../../../utils/testUtils';
import {
  multiDimensionLayer,
  WmMultiDimensionLayer,
} from '../../../../utils/defaultTestSettings';
import { registerWMLayer } from '../../../../store/mapStore/utils/helpers';
import {
  getPreviousNextDimensionValue,
  GoToDirection,
} from './GoToPreviousNextControl';
import WMLayer from '../../../../geoweb-webmap/WMLayer';

describe('src/components/GeoWebLayerSelect/GeoWebTimeControl/GoToPreviousNextControl/GoToPreviousNextControlConnect', () => {
  it('should trigger action to go to previous time step', () => {
    const testLayer: WMLayer = WmMultiDimensionLayer;
    const mapId = 'map-1';
    const props = {
      mapId,
      layerId: testLayer.id,
      direction: 'previous' as GoToDirection,
    };

    registerWMLayer(testLayer, testLayer.id);

    const mockStore = configureStore();
    const mockState = mockStateMapWithMultipleLayers(
      [multiDimensionLayer],
      mapId,
    );

    const store = mockStore(mockState);

    const { container } = render(
      <Provider store={store}>
        <GoToPreviousNextControlConnect {...props} />
      </Provider>,
    );

    const button = getByRole(container, 'button');

    fireEvent.click(button);

    const previousValue = getPreviousNextDimensionValue(
      testLayer.id,
      props.direction,
    );

    const expectedActions = [
      {
        type: mapActionTypes.WEBMAP_STOP_ANIMATION,
        payload: {
          mapId,
        },
      },
      {
        type: mapActionTypes.WEBMAP_LAYER_CHANGE_DIMENSION,
        payload: {
          layerId: testLayer.id,
          origin: `goto-${props.direction}`,
          dimension: {
            name: 'time',
            currentValue: previousValue,
          },
        },
      },
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should trigger action to go to next time step', () => {
    const testLayer: WMLayer = WmMultiDimensionLayer;
    const mapId = 'map-1';
    const props = {
      mapId,
      layerId: multiDimensionLayer.id,
      direction: 'next' as GoToDirection,
    };

    registerWMLayer(testLayer, testLayer.id);

    const mockStore = configureStore();
    const mockState = mockStateMapWithMultipleLayers(
      [multiDimensionLayer],
      mapId,
    );

    const store = mockStore(mockState);

    const { container } = render(
      <Provider store={store}>
        <GoToPreviousNextControlConnect {...props} />
      </Provider>,
    );

    const button = getByRole(container, 'button');

    fireEvent.click(button);

    const nextValue = getPreviousNextDimensionValue(
      testLayer.id,
      props.direction,
    );

    const expectedActions = [
      {
        type: mapActionTypes.WEBMAP_STOP_ANIMATION,
        payload: {
          mapId,
        },
      },
      {
        type: mapActionTypes.WEBMAP_LAYER_CHANGE_DIMENSION,
        payload: {
          layerId: testLayer.id,
          origin: `goto-${props.direction}`,
          dimension: {
            name: 'time',
            currentValue: nextValue,
          },
        },
      },
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });
});
