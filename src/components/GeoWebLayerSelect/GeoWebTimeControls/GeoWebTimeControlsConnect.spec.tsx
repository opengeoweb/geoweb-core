/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import moment from 'moment';

import * as mapActionTypes from '../../../store/mapStore/map/constants';
import GeoWebTimeControlsConnect, {
  defaultAnimationProps,
} from './GeoWebTimeControlsConnect';

describe('src/components/GeoWebLayerSelect/GeoWebTimeControls/GeoWebTimeControlsConnect', () => {
  const dimensions = [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T13:30:00Z',
    },
  ];

  const mapId = 'map-1';

  const props = {
    mapId,
  };

  const newAnimationDuration = { value: 9, title: '9h' };

  it('should start animation with default animation duration and interval', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            isAnimating: false,
            dimensions,
            activeLayerId: 'layer1',
          },
        },
      },
    });

    const { getByTestId } = render(
      <Provider store={store}>
        <GeoWebTimeControlsConnect {...props} />
      </Provider>,
    );

    const button = getByTestId('play-icon');
    fireEvent.click(button);

    const expectedAction = {
      type: mapActionTypes.WEBMAP_START_ANIMATION,
      payload: {
        mapId,
        start: moment(dimensions[0].currentValue).subtract(
          defaultAnimationProps.animationDurationDefault,
          'h',
        ),
        end: moment(dimensions[0].currentValue),
        interval: defaultAnimationProps.animationInterval,
      },
    };

    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should update the animation duration when changed and this should be used when starting animation', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            isAnimating: false,
            dimensions,
            activeLayerId: 'layer1',
          },
        },
      },
    });

    const { getByTestId, findByText } = render(
      <Provider store={store}>
        <GeoWebTimeControlsConnect {...props} />
      </Provider>,
    );

    const select = getByTestId('selectAnimationDuration');

    fireEvent.mouseDown(select);

    const menuItem = await findByText(newAnimationDuration.title);
    fireEvent.click(menuItem);

    expect(select.textContent).toEqual(newAnimationDuration.title);

    const button = getByTestId('play-icon');
    fireEvent.click(button);

    const expectedAction2 = {
      type: mapActionTypes.WEBMAP_START_ANIMATION,
      payload: {
        mapId,
        start: moment(dimensions[0].currentValue).subtract(
          newAnimationDuration.value,
          'h',
        ),
        end: moment(dimensions[0].currentValue),
        interval: defaultAnimationProps.animationInterval,
      },
    };

    expect(store.getActions()).toEqual([expectedAction2]);
  });
});
