/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import GoToBeginEndControl, {
  GoToBeginEndControlProps,
  getBeginEndDimensionValue,
} from './GoToBeginEndControl';
import { WmMultiDimensionLayer } from '../../../../utils/defaultTestSettings';
import { WMLayer } from '../../../../geoweb-webmap';
import { registerWMLayer } from '../../../../store/mapStore/utils/helpers';

describe('src/components/GeoWebLayerSelect/GeoWebTimeControl/GoToBeginEndControl/GoToBeginEndControl', () => {
  it('should have GotoStartIcon with default props', () => {
    const props = {
      onGoTo: jest.fn(),
    } as GoToBeginEndControlProps;

    const { container } = render(<GoToBeginEndControl {...props} />);
    const button = getByRole(container, 'button');

    expect(button.querySelectorAll('.goto-start')).toHaveLength(1);
    expect(button).toBeDefined();

    fireEvent.click(button);
    expect(props.onGoTo).toHaveBeenCalled();
  });

  it('should have GoToEndIcon when direction = end', () => {
    const props = {
      onGoTo: jest.fn(),
      direction: 'end',
    } as GoToBeginEndControlProps;

    const { container } = render(<GoToBeginEndControl {...props} />);
    const button = getByRole(container, 'button');

    expect(button).toBeDefined();
    expect(button.querySelectorAll('.goto-end')).toHaveLength(1);

    fireEvent.click(button);
    expect(props.onGoTo).toHaveBeenCalled();
  });

  it('should set timestep to next if direction = end', () => {
    const testLayer: WMLayer = WmMultiDimensionLayer;
    registerWMLayer(testLayer, testLayer.id);

    const props = {
      onGoTo: jest.fn(),
      layerId: testLayer.id,
      direction: 'end',
    } as GoToBeginEndControlProps;

    const { container } = render(<GoToBeginEndControl {...props} />);
    const button = getByRole(container, 'button');

    fireEvent.click(button);

    const endValue = getBeginEndDimensionValue(testLayer.id, props.direction);

    expect(props.onGoTo).toHaveBeenCalledWith({
      name: 'time',
      currentValue: endValue,
    });
  });

  it('should set timestep to end if direction = start', () => {
    const testLayer: WMLayer = WmMultiDimensionLayer;
    registerWMLayer(testLayer, testLayer.id);

    const props = {
      onGoTo: jest.fn(),
      layerId: testLayer.id,
      direction: 'start',
    } as GoToBeginEndControlProps;

    const { container } = render(<GoToBeginEndControl {...props} />);
    const button = getByRole(container, 'button');

    fireEvent.click(button);

    const endValue = getBeginEndDimensionValue(testLayer.id, props.direction);

    expect(props.onGoTo).toHaveBeenCalledWith({
      name: 'time',
      currentValue: endValue,
    });
  });
});
