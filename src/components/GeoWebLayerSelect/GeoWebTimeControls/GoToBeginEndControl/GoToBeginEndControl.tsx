/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { IconButton } from '@material-ui/core';
import moment from 'moment';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import LastPageIcon from '@material-ui/icons/LastPage';
import {
  getFirstTimeStepForLayerId,
  getLastTimeStepForLayerId,
} from '../../../../store/mapStore/utils/helpers';
import { Dimension } from '../../../../store/mapStore/types';

export type GoToDirection = 'start' | 'end';

export const getBeginEndDimensionValue = (
  activeLayerId: string,
  direction: GoToDirection,
): string => {
  const newDimensionValue = moment
    .utc(
      direction === 'start'
        ? getFirstTimeStepForLayerId(activeLayerId)
        : getLastTimeStepForLayerId(activeLayerId),
      'YYYY-MM-DDTHH:mm:SS',
    )
    .format();

  return newDimensionValue;
};

export interface GoToBeginEndControlProps {
  onGoTo: (dimension: Dimension) => void;
  layerId?: string;
  direction?: GoToDirection;
  disabled?: boolean;
}

const GoToBeginEndControl: React.FC<GoToBeginEndControlProps> = ({
  onGoTo,
  layerId,
  direction = 'start',
  disabled = false,
}: GoToBeginEndControlProps) => {
  const onClick = (): void => {
    const newDimension = getBeginEndDimensionValue(layerId, direction);
    if (newDimension) {
      onGoTo({
        name: 'time',
        currentValue: newDimension,
      });
    }
  };

  return (
    <IconButton
      disabled={disabled}
      role="button"
      onClick={layerId ? onClick : (): void => onGoTo(null)}
    >
      {direction === 'start' ? (
        <FirstPageIcon className="goto-start" />
      ) : (
        <LastPageIcon className="goto-end" />
      )}
    </IconButton>
  );
};

export default GoToBeginEndControl;
