/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import GoToBeginEndControlConnect from './GoToBeginEndControlConnect';
import * as mapActionTypes from '../../../../store/mapStore/constants';
import { mockStateMapWithMultipleLayers } from '../../../../utils/testUtils';
import {
  multiDimensionLayer,
  WmMultiDimensionLayer,
} from '../../../../utils/defaultTestSettings';
import { registerWMLayer } from '../../../../store/mapStore/utils/helpers';
import {
  getBeginEndDimensionValue,
  GoToDirection,
} from './GoToBeginEndControl';
import WMLayer from '../../../../geoweb-webmap/WMLayer';

describe('src/components/GeoWebLayerSelect/GeoWebTimeControl/GoToBeginEndControl/GoToBeginEndControlConnect', () => {
  it('should goto start of time ', () => {
    const testLayer: WMLayer = WmMultiDimensionLayer;
    const mapId = 'map-1';
    const props = {
      mapId,
      layerId: multiDimensionLayer.id,
      direction: 'start' as GoToDirection,
    };

    registerWMLayer(testLayer, testLayer.id);

    const mockStore = configureStore();
    const mockState = mockStateMapWithMultipleLayers(
      [multiDimensionLayer],
      mapId,
    );

    const store = mockStore(mockState);

    const { container } = render(
      <Provider store={store}>
        <GoToBeginEndControlConnect {...props} />
      </Provider>,
    );

    const button = getByRole(container, 'button');

    fireEvent.click(button);

    const currentValue = getBeginEndDimensionValue(
      testLayer.id,
      props.direction,
    );

    const expectedActions = [
      {
        type: mapActionTypes.WEBMAP_STOP_ANIMATION,
        payload: {
          mapId,
        },
      },
      {
        type: mapActionTypes.WEBMAP_LAYER_CHANGE_DIMENSION,
        payload: {
          layerId: testLayer.id,
          origin: `skip-${props.direction}`,
          dimension: {
            name: 'time',
            currentValue,
          },
        },
      },
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should goto end of time ', () => {
    const testLayer: WMLayer = WmMultiDimensionLayer;
    const mapId = 'map-1';
    const props = {
      mapId,
      layerId: testLayer.id,
      direction: 'end' as GoToDirection,
    };

    registerWMLayer(testLayer, testLayer.id);

    const mockStore = configureStore();
    const mockState = mockStateMapWithMultipleLayers(
      [multiDimensionLayer],
      mapId,
    );

    const store = mockStore(mockState);

    const { container } = render(
      <Provider store={store}>
        <GoToBeginEndControlConnect {...props} />
      </Provider>,
    );

    const button = getByRole(container, 'button');

    fireEvent.click(button);

    const currentValue = getBeginEndDimensionValue(
      testLayer.id,
      props.direction,
    );

    const expectedActions = [
      {
        type: mapActionTypes.WEBMAP_STOP_ANIMATION,
        payload: {
          mapId,
        },
      },
      {
        type: mapActionTypes.WEBMAP_LAYER_CHANGE_DIMENSION,
        payload: {
          layerId: testLayer.id,
          origin: `skip-${props.direction}`,
          dimension: {
            name: 'time',
            currentValue,
          },
        },
      },
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });
});
