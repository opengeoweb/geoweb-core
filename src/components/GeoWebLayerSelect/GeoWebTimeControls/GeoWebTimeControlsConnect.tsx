/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import { MenuItem } from '@material-ui/core';

import StartStopControlConnect from './StartStopControl/StartStopControlConnect';
import GoToBeginEndControlConnect from './GoToBeginEndControl/GoToBeginEndControlConnect';
import GoToPreviousNextControlConnect from './GoToPreviousNextControl/GoToPreviousNextControlConnect';
import GoToAccessTimeControlConnect from './GoToAccessTimeControl/GoToAccessTimeControlConnect';
import AutoUpdateConnect from '../../AutoUpdate/AutoUpdateConnect';

import * as mapSelectors from '../../../store/mapStore/selectors';
import { AppStore } from '../../../types/types';
import TooltipSelect from '../../TooltipSelect/TooltipSelect';

export const defaultAnimationProps = {
  animationInterval: 300,
  animationDurationDefault: 6,
};

interface AnimDurObject {
  value: number;
  title: string;
}

const animationDurationOptions: AnimDurObject[] = [
  { value: 0.5, title: '30mins' },
  { value: 1, title: '1h' },
  { value: 2, title: '2h' },
  { value: 3, title: '3h' },
  { value: 4, title: '4h' },
  { value: 5, title: '5h' },
  { value: 6, title: '6h' },
  { value: 7, title: '7h' },
  { value: 8, title: '8h' },
  { value: 9, title: '9h' },
  { value: 10, title: '10h' },
];

interface TimeControlsConnect {
  mapId: string;
  activeLayerId: string;
  isAnimating: boolean;
}

const connectRedux = connect((store: AppStore, props: TimeControlsConnect) => ({
  activeLayerId: mapSelectors.getActiveLayerId(store, props.mapId),
  isAnimating: mapSelectors.isAnimating(store, props.mapId),
}));

/**
 * Controls allowing you to manage the animation (start/stop)
 * Also includes controls to jump to the fist/last time step
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <GeoWebTimeControlsConnect mapId="mapid_1"/> ```
 */
const GeoWebTimeControlsConnect = connectRedux(
  ({ mapId, activeLayerId, isAnimating }: TimeControlsConnect) => {
    const [animationDuration, setAnimationDuration] = React.useState(
      defaultAnimationProps.animationDurationDefault,
    );

    return (
      <>
        <AutoUpdateConnect mapId={mapId} />
        <TooltipSelect
          tooltip="Select animation duration"
          inputProps={{
            SelectDisplayProps: {
              'data-testid': 'selectAnimationDuration',
            },
          }}
          value={animationDuration}
          onChange={(event: React.ChangeEvent<{ value: number }>): void =>
            setAnimationDuration(event.target.value)
          }
          disabled={isAnimating}
        >
          {animationDurationOptions.map(mapDuration => (
            <MenuItem key={mapDuration.value} value={mapDuration.value}>
              {mapDuration.title}
            </MenuItem>
          ))}
        </TooltipSelect>
        <GoToBeginEndControlConnect
          direction="start"
          layerId={activeLayerId}
          mapId={mapId}
        />
        <GoToPreviousNextControlConnect
          direction="previous"
          layerId={activeLayerId}
          mapId={mapId}
        />
        <StartStopControlConnect
          mapId={mapId}
          isDisabled={activeLayerId ? !activeLayerId.length : true}
          animationInterval={defaultAnimationProps.animationInterval}
          animationDuration={animationDuration}
        />
        <GoToPreviousNextControlConnect
          direction="next"
          layerId={activeLayerId}
          mapId={mapId}
        />
        <GoToBeginEndControlConnect
          direction="end"
          layerId={activeLayerId}
          mapId={mapId}
        />
        <GoToAccessTimeControlConnect mapId={mapId} />
      </>
    );
  },
);

export default GeoWebTimeControlsConnect;
