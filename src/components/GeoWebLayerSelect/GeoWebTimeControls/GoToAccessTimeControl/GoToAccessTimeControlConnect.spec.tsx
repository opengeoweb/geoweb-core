/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import moment from 'moment';
import GoToAccessTimeControlConnect from './GoToAccessTimeControlConnect';
import * as mapActionTypes from '../../../../store/mapStore/constants';
import { mockStateMapWithDimensionsWithoutLayers } from '../../../../utils/testUtils';

describe('src/components/GeoWebLayerSelect/GeoWebTimeControl/GoToAccessTimeControl/GoToAccessTimeControlConnect', () => {
  it('should trigger action to go to accessTime (wall clock) time step', () => {
    const mapId = 'map-1';
    const props = {
      mapId,
    };

    const mockStore = configureStore();
    const mockState = mockStateMapWithDimensionsWithoutLayers(mapId);

    const store = mockStore(mockState);
    const { container } = render(
      <Provider store={store}>
        <GoToAccessTimeControlConnect {...props} />
      </Provider>,
    );

    const button = getByRole(container, 'button');

    fireEvent.click(button);

    const accessTimeValue = moment.utc().format();

    const expectedActions = [
      {
        type: mapActionTypes.WEBMAP_STOP_ANIMATION,
        payload: {
          mapId,
        },
      },
      {
        type: mapActionTypes.WEBMAP_MAP_CHANGE_DIMENSION,
        payload: {
          mapId,
          origin: 'goto-accesstime',
          dimension: {
            name: 'time',
            currentValue: accessTimeValue,
          },
        },
      },
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });
});
