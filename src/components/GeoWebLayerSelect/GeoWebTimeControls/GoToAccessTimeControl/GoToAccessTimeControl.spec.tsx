/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import moment from 'moment';
import GoToAccessTimeControl, {
  GoToAccessTimeControlProps,
} from './GoToAccessTimeControl';

describe('src/components/GeoWebLayerSelect/GeoWebTimeControl/GoToAccessTimeControl/GoToAccessTimeControl', () => {
  it('should have (not disabled) accessTimeIcon with default props', () => {
    const props = {
      onGoTo: jest.fn(),
    } as GoToAccessTimeControlProps;

    const { container } = render(<GoToAccessTimeControl {...props} />);
    const button = getByRole(container, 'button');

    expect(button.querySelectorAll('.goto-accesstime')).toHaveLength(1);

    fireEvent.click(button);
    expect(props.onGoTo).toHaveBeenCalled();
  });

  it('should set timestep to AccessTime time', () => {
    const props = {
      onGoTo: jest.fn(),
    } as GoToAccessTimeControlProps;

    const { container } = render(<GoToAccessTimeControl {...props} />);
    const button = getByRole(container, 'button');

    fireEvent.click(button);

    const accessTimeValue = moment.utc().format();
    expect(props.onGoTo).toHaveBeenCalledWith({
      name: 'time',
      currentValue: accessTimeValue,
    });
  });
});
