/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import * as mapSelectors from '../../../../store/mapStore/selectors';
import * as mapActions from '../../../../store/mapStore/actions';
import { AppStore } from '../../../../types/types';
import GoToAccessTimeControl from './GoToAccessTimeControl';
import { Dimension } from '../../../../store/mapStore/types';

export const getTimeDimension = (mapDimensions: Dimension[]): Dimension =>
  mapDimensions.find(mapDimension => mapDimension.name === 'time');

interface GoToAccessTimeControlConnectProps {
  mapId: string;
  mapDimensions: Dimension[];
  onMapChangeDimension: typeof mapActions.mapChangeDimension;
  onMapStopAnimation: typeof mapActions.mapStopAnimation;
}

const connectRedux = connect(
  (store: AppStore, props: GoToAccessTimeControlConnectProps) => ({
    mapDimensions: mapSelectors.getMapDimensions(store, props.mapId),
  }),
  {
    onMapChangeDimension: mapActions.mapChangeDimension,
    onMapStopAnimation: mapActions.mapStopAnimation,
  },
);

const GoToAccessTimeControlConnect: React.FC<GoToAccessTimeControlConnectProps> = ({
  mapId,
  mapDimensions,
  onMapChangeDimension,
  onMapStopAnimation,
}: GoToAccessTimeControlConnectProps) => {
  const timeDimension = getTimeDimension(mapDimensions);
  const onGoTo = (newDimension: Dimension): void => {
    onMapStopAnimation({ mapId });
    onMapChangeDimension({
      origin: 'goto-accesstime',
      dimension: newDimension,
      mapId,
    });
  };

  return <GoToAccessTimeControl disabled={!timeDimension} onGoTo={onGoTo} />;
};

export default connectRedux(GoToAccessTimeControlConnect);
