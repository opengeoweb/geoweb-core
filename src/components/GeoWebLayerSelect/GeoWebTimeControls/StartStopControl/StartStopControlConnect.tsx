/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import moment from 'moment';

import StartStopControl from './StartStopControl';

import * as mapSelectors from '../../../../store/mapStore/map/selectors';
import * as mapActions from '../../../../store/mapStore/map/actions';
import { AppStore } from '../../../../types/types';
import { Dimension } from '../../../../store/mapStore/types';

interface StartStopControlConnectProps {
  mapId: string;
  isAnimating: boolean;
  dimensions: Dimension[];
  animationDuration: number;
  animationInterval: number;
  startAnimation: typeof mapActions.mapStartAnimation;
  stopAnimation: typeof mapActions.mapStopAnimation;
  isDisabled: boolean;
}

const connectRedux = connect(
  (store: AppStore, props: StartStopControlConnectProps) => ({
    isAnimating: mapSelectors.isAnimating(store, props.mapId),
    dimensions: mapSelectors.getMapDimensions(store, props.mapId),
  }),
  {
    startAnimation: mapActions.mapStartAnimation,
    stopAnimation: mapActions.mapStopAnimation,
  },
);

/**
 * Controls allowing you to manage the animation (start/stop)
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {boolean} isDisabled isDisabled: boolean - status of the controls button - disabled or enable
 * @example
 * ``` <StartStopControlConnect mapId="mapid_1" isDisabled={isDisabled}/> ```
 */
const StartStopControlConnect = connectRedux(
  ({
    mapId,
    isAnimating,
    dimensions,
    animationDuration,
    animationInterval,
    startAnimation,
    stopAnimation,
    isDisabled,
  }: StartStopControlConnectProps) => {
    const timeDim = dimensions.find(dim => dim.name === 'time');
    const curSelectedTime = timeDim
      ? timeDim.currentValue
      : moment().toISOString(); // TODO: Should be adjusted to timezone?
    return (
      <StartStopControl
        isDisabled={isDisabled}
        isAnimating={isAnimating}
        onStartAnimation={(): void => {
          const startValue = moment(curSelectedTime).subtract(
            animationDuration,
            'h',
          );
          const endValue = moment(curSelectedTime);
          startAnimation({
            mapId,
            start: startValue,
            end: endValue,
            interval: animationInterval,
          });
        }}
        onStopAnimation={(): void => {
          stopAnimation({ mapId });
        }}
      />
    );
  },
);

export default StartStopControlConnect;
