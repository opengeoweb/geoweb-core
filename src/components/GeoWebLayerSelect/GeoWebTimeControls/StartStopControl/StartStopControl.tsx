/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { IconButton } from '@material-ui/core';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';

interface StartStopControlProps {
  isAnimating: boolean;
  isDisabled?: boolean;
  onStartAnimation: () => void;
  onStopAnimation: () => void;
}

const StartStopControl: React.FC<StartStopControlProps> = ({
  isAnimating,
  onStartAnimation,
  onStopAnimation,
  isDisabled = false,
}: StartStopControlProps) => {
  const onClickAnimationBtn = (): void => {
    if (isAnimating) {
      onStopAnimation();
    } else {
      onStartAnimation();
    }
  };

  return (
    <IconButton
      disabled={isDisabled}
      role="button"
      onClick={onClickAnimationBtn}
    >
      {isAnimating ? (
        <StopIcon data-testid="stop-icon" />
      ) : (
        <PlayArrowIcon data-testid="play-icon" />
      )}
    </IconButton>
  );
};

export default StartStopControl;
