/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  render,
  getByRole,
  fireEvent,
  getByTestId,
} from '@testing-library/react';

import StartStopControl from './StartStopControl';

describe('src/components/GeoWebLayerSelect/GeoWebTimeControl/StartStopControl/StartStopControl', () => {
  it('should have PlayIcon when !isAnimating and trigger startAnimation when clicking on button', () => {
    const props = {
      isAnimating: false,
      onStartAnimation: jest.fn(),
      onStopAnimation: jest.fn(),
    };

    const { container } = render(<StartStopControl {...props} />);
    expect(getByTestId(container, 'play-icon')).toBeDefined();
    const button = getByRole(container, 'button');
    fireEvent.click(button);
    expect(props.onStartAnimation).toHaveBeenCalled();
  });

  it('should have StopIcon  when isAnimating and trigger stopAnimation when clicking on button', () => {
    const props = {
      isAnimating: true,
      onStartAnimation: jest.fn(),
      onStopAnimation: jest.fn(),
    };

    const { container } = render(<StartStopControl {...props} />);
    expect(getByTestId(container, 'stop-icon')).toBeDefined();
    const button = getByRole(container, 'button');
    fireEvent.click(button);
    expect(props.onStopAnimation).toHaveBeenCalled();
  });

  it('it does not trigger an action when clicking on a disabled button', () => {
    const props = {
      isAnimating: false,
      onStartAnimation: jest.fn(),
      onStopAnimation: jest.fn(),
      isDisabled: true,
    };

    const { container } = render(<StartStopControl {...props} />);
    const button = getByRole(container, 'button');
    fireEvent.click(button);

    expect(button.getAttributeNames().includes('disabled')).toBeTruthy();
    expect(props.onStartAnimation).toHaveBeenCalledTimes(0);
    expect(props.onStopAnimation).toHaveBeenCalledTimes(0);
  });
});
