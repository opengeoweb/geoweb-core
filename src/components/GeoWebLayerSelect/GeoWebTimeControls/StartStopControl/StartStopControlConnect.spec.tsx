/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import moment from 'moment';

import * as mapActionTypes from '../../../../store/mapStore/map/constants';
import StartStopControlConnect from './StartStopControlConnect';

describe('src/components/GeoWebLayerSelect/GeoWebTimeControl/StartStopControl/StartStopControlConnect', () => {
  const dimensions = [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T13:30:00Z',
    },
  ];

  const mapId = 'map-1';

  const props = {
    mapId,
    animationDuration: 6,
    animationInterval: 300,
  };

  it('should start animation with the correct start and end times', () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            isAnimating: false,
            dimensions,
          },
        },
      },
    });

    const { container } = render(
      <Provider store={store}>
        <StartStopControlConnect {...props} />
      </Provider>,
    );
    const button = getByRole(container, 'button');

    fireEvent.click(button);

    const expectedAction = {
      type: mapActionTypes.WEBMAP_START_ANIMATION,
      payload: {
        mapId,
        start: moment(dimensions[0].currentValue).subtract(
          props.animationDuration,
          'h',
        ),
        end: moment(dimensions[0].currentValue),
        interval: props.animationInterval,
      },
    };

    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should stop animation when map is animating', () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            isAnimating: true,
            dimensions,
          },
        },
      },
    });

    const { container } = render(
      <Provider store={store}>
        <StartStopControlConnect {...props} />
      </Provider>,
    );
    const button = getByRole(container, 'button');

    fireEvent.click(button);

    const expectedAction = {
      type: mapActionTypes.WEBMAP_STOP_ANIMATION,
      payload: {
        mapId,
      },
    };

    expect(store.getActions()).toEqual([expectedAction]);
  });
});
