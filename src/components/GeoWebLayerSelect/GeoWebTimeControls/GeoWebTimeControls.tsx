/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import GeoWebStartStopControls from './StartStopControl/StartStopControl';

import GoToBeginEndControls from './GoToBeginEndControl/GoToBeginEndControl';
import GoToPreviousNextControl from './GoToPreviousNextControl/GoToPreviousNextControl';
import GoToAccessTimeControl from './GoToAccessTimeControl/GoToAccessTimeControl';

interface GeoWebTimeControlsProps {
  isAnimating: boolean;
  onStartAnimation: () => void;
  onStopAnimation: () => void;
  onGoToBegin: () => void;
  onGoToEnd: () => void;
  onGoToPrevious: () => void;
  onGoToNext: () => void;
  onGoToAccessTime: () => void;
  activeLayerId: string;
}

export const GeoWebTimeControls: React.FC<GeoWebTimeControlsProps> = ({
  isAnimating,
  onStartAnimation,
  onStopAnimation,
  onGoToBegin,
  onGoToEnd,
  onGoToPrevious,
  onGoToNext,
  onGoToAccessTime,
  activeLayerId,
}: GeoWebTimeControlsProps) => (
  <>
    <GoToBeginEndControls
      layerId={activeLayerId}
      direction="start"
      onGoTo={onGoToBegin}
    />
    <GoToPreviousNextControl
      direction="previous"
      layerId={activeLayerId}
      onGoTo={onGoToPrevious}
    />
    <GeoWebStartStopControls
      isAnimating={isAnimating}
      onStartAnimation={onStartAnimation}
      onStopAnimation={onStopAnimation}
    />
    <GoToPreviousNextControl
      direction="next"
      layerId={activeLayerId}
      onGoTo={onGoToNext}
    />
    <GoToBeginEndControls
      layerId={activeLayerId}
      direction="end"
      onGoTo={onGoToEnd}
    />
    <GoToAccessTimeControl onGoTo={onGoToAccessTime} />
  </>
);

export default GeoWebTimeControls;
