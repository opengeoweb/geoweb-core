/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';

import ReactMapView, {
  isAMapLayer,
  getFeatureLayers,
  isAGeoJSONLayer,
} from './ReactMapView';
import { baseLayerGrey, overLayer } from '../../utils/testLayers';
import { defaultReduxLayerRadarKNMI } from '../../utils/defaultTestSettings';
import { getWMJSMapById } from '../..';

export const mockGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [],
};

describe('src/components/ReactMapView/ReactMapView', () => {
  it('should trigger onClick when the passive map is clicked', async () => {
    const props = {
      mapId: 'map1',
      passiveMap: true,
      onClick: jest.fn(),
    };
    const { getByRole } = render(<ReactMapView {...props} />);

    fireEvent.click(getByRole('button'));

    expect(props.onClick).toHaveBeenCalledTimes(1);
  });

  it('should return false for a baselayer without geojson', () => {
    expect(isAMapLayer(baseLayerGrey)).toBeFalsy();
  });

  it('should return false for an overlayer without geojson', () => {
    expect(isAMapLayer(overLayer)).toBeFalsy();
  });

  it('should return true for a maplayer without geojson', () => {
    expect(isAMapLayer(defaultReduxLayerRadarKNMI)).toBeTruthy();
  });

  it('should return false for a baselayer with geojson', () => {
    const mockLayer = {
      ...baseLayerGrey,
      geojson: mockGeoJSON,
    };
    expect(isAMapLayer(mockLayer)).toBeFalsy();
  });

  it('should detect geojson layer for a baselayer with geojson', () => {
    const mockLayer = {
      ...baseLayerGrey,
      geojson: mockGeoJSON,
    };
    expect(isAGeoJSONLayer(mockLayer)).toBeTruthy();
  });

  it('should return false for an overlayer with geojson', () => {
    const mockLayer = {
      ...overLayer,
      geojson: mockGeoJSON,
    };
    expect(isAMapLayer(mockLayer)).toBeFalsy();
  });

  it('should detect geojson layer for an overlayer with geojson', () => {
    const mockLayer = {
      ...overLayer,
      geojson: mockGeoJSON,
    };
    expect(isAGeoJSONLayer(mockLayer)).toBeTruthy();
  });

  it('should return true for a maplayer with geojson', () => {
    const mockLayer = {
      ...defaultReduxLayerRadarKNMI,
      geojson: mockGeoJSON,
    };
    expect(isAMapLayer(mockLayer)).toBeTruthy();
  });

  it('should return a list of props if it has geojson', () => {
    const mockLayer = {
      ...defaultReduxLayerRadarKNMI,
      props: {
        geojson: 'string',
      },
    };
    const mockLayer2 = {
      ...baseLayerGrey,
      props: {
        geojson: 'another-string',
      },
    };
    expect(getFeatureLayers([mockLayer, mockLayer2])).toEqual([
      mockLayer2.props,
      mockLayer.props,
    ]);
  });

  it('should return an empty list if it has no geojson', () => {
    const mockLayer = {
      ...defaultReduxLayerRadarKNMI,
      props: {
        otherProp: 'string',
      },
    };
    const mockLayer2 = {
      ...baseLayerGrey,
      props: {
        anotherProp: 'another-string',
      },
    };
    expect(getFeatureLayers([mockLayer, mockLayer2])).toEqual([]);
  });

  it('should trigger onMapCursorChangeLocation when the map is clicked at the same location', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapCursorLocation: { lat: 52, lon: 5 },
      showCursor: true,
      onMapCursorChangeLocation: jest.fn(),
    };
    render(<ReactMapView {...props} />);

    /* Click at same location should trigger onMapCursorChangeLocation */
    getWMJSMapById('map1').mouseDown(10, 10, null);
    getWMJSMapById('map1').mouseUp(10, 10, null);
    expect(props.onMapCursorChangeLocation).toHaveBeenCalledTimes(1);

    /* Click at almost the same location should trigger onMapCursorChangeLocation */
    getWMJSMapById('map1').mouseDown(10, 10, null);
    getWMJSMapById('map1').mouseUp(8, 8, null);
    expect(props.onMapCursorChangeLocation).toHaveBeenCalledTimes(2);

    /* Click at different location should NOT trigger onMapCursorChangeLocation */
    getWMJSMapById('map1').mouseDown(100, 100, null);
    getWMJSMapById('map1').mouseUp(3, 3, null);
    expect(props.onMapCursorChangeLocation).toHaveBeenCalledTimes(2);
  });
});
