/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  WMJSMap as WMJSMapType,
  SetMapDimensionPayload,
  SetLayerDimensionsForServicePayload,
  SetLayerStylePayload,
  SetLayerDimensionPayload,
  SetLayersForServicePayload,
  SetLayerStylesForServicePayload,
  UpdateAllMapDimensionsPayload,
  Dimension,
  SetBboxPayload,
  SetLayerDimensionsPayload,
  Bbox,
} from '../../store/mapStore/types';

export interface ReactMapViewProps {
  listeners?: {
    name?: string;
    data: string;
    keep: string;
    callbackfunction: (p1: string, p2: string) => void;
  }[];
  srs?: string;
  bbox?: Bbox;
  children?: React.ReactNode;
  mapId: string;
  activeLayerId?: string;
  showScaleBar?: boolean;
  showLegend?: boolean;
  passiveMap?: boolean;
  displayTimeInMap?: boolean;
  animationDelay?: number;
  dimensions?: Dimension[];
  onClick?: () => void;
  xml2jsonrequestURL?: string;
  showCursor?: boolean;
  mapCursorLocation?: { lon: number; lat: number };
  shouldAutoFetch?: boolean;
  /* Callback actions */
  onMount?: (mapId: string, WMJSMapInstance: WMJSMapType) => void;
  onUnMount?: (mapId: string, WMJSMapInstance: WMJSMapType) => void;
  onMapChangeDimension?: (payload: SetMapDimensionPayload) => void;
  onMapUpdateAllDimensions?: (payload: UpdateAllMapDimensionsPayload) => void;
  onLayerChangeStyle?: (payload: SetLayerStylePayload) => void;
  onLayerChangeDimension?: (payload: SetLayerDimensionPayload) => void;
  onServiceSetLayers?: (payload: SetLayersForServicePayload) => void;
  onLayerSetStyles?: (payload: SetLayerStylesForServicePayload) => void;
  onSetLayerDimensionsForService?: (
    payload: SetLayerDimensionsForServicePayload,
  ) => void;
  onMapZoomEnd?: (payload: SetBboxPayload) => void;
  onRegisterAdaguc?: (WMJSMapInstance: WMJSMapType) => void;
  onLayerSetDimensions?: (payload: SetLayerDimensionsPayload) => void;
  onMapCursorChangeLocation?: (payload: { lon: number; lat: number }) => void;
}
