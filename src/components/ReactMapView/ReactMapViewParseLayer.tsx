/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { WMLayer } from 'src/geoweb-webmap';
import WMGetServiceFromStore from '../../geoweb-webmap/WMGetServiceFromStore';

import {
  SetLayerStylePayload,
  SetLayersForServicePayload,
  SetLayerStylesForServicePayload,
  SetLayerDimensionsForServicePayload,
  UpdateAllMapDimensionsPayload,
  WMJSMap,
  SetLayerDimensionsPayload,
} from '../../store/mapStore/types';
import { MapViewProps } from '../MapView/types';

export const setServiceMetadata = (
  wmLayer: WMLayer,
  mapId: string,
  xml2jsonrequestURL: string,
  webMapJSInstance: WMJSMap,
  mapProperties: MapViewProps,
  onMapUpdateAllDimensions?: (payload: UpdateAllMapDimensionsPayload) => void,
  onLayerChangeStyle?: (payload: SetLayerStylePayload) => void,
  onServiceSetLayers?: (payload: SetLayersForServicePayload) => void,
  onLayerSetStyles?: (payload: SetLayerStylesForServicePayload) => void,
  onSetLayerDimensionsForService?: (
    payload: SetLayerDimensionsForServicePayload,
  ) => void,
  onLayerSetDimensions?: (payload: SetLayerDimensionsPayload) => void,
): void => {
  const origin = 'ReactMapViewParseLayer';
  const service = WMGetServiceFromStore(wmLayer.service, xml2jsonrequestURL);
  /* Update list of layers for service */
  const done = (layers): void => {
    onServiceSetLayers({
      service: wmLayer.service,
      layers,
    });
    /* Update style information in services for a layer */
    onLayerSetStyles({
      origin,
      service: wmLayer.service,
      name: wmLayer.name,
      styles: wmLayer.getStyles(),
    });
    /* Select first style in service for a layer */
    onLayerChangeStyle({
      origin,
      layerId: wmLayer.ReactWMJSLayerId,
      style: wmLayer.currentStyle, // || wmjsLayer.getStyles().length > 0 ? wmjsLayer.getStyles()[0].Name.value : 'default'
    });
    /* Update dimensions information in services for a layer */
    onSetLayerDimensionsForService({
      origin,
      service: wmLayer.service,
      name: wmLayer.name,
      dimensions: wmLayer.dimensions.map(dim => {
        return {
          name: dim.name,
          currentValue: dim.currentValue,
          units: dim.units,
          synced: dim.synced,
        };
      }),
    });
    let mapNeedsUpdate = false;

    /* Synchronize layer dimension properties to wmjslayer dimension values */
    for (let d = 0; d < wmLayer.dimensions.length; d += 1) {
      if (
        wmLayer.reactWebMapJSLayer &&
        wmLayer.reactWebMapJSLayer.props.dimensions
      ) {
        /* Try to set the dimension according to the react layer properties */
        const reactLayerDimension = wmLayer.reactWebMapJSLayer.props.dimensions.find(
          dim => dim.name === wmLayer.dimensions[d].name,
        );
        if (reactLayerDimension && reactLayerDimension.currentValue) {
          if (
            reactLayerDimension.currentValue !==
            wmLayer.dimensions[d].currentValue
          ) {
            // eslint-disable-next-line no-param-reassign
            wmLayer.dimensions[d].currentValue =
              reactLayerDimension.currentValue;
            // eslint-disable-next-line no-param-reassign
            wmLayer.dimensions[d].synced = reactLayerDimension.synced;
            mapNeedsUpdate = true;
          }
        }
      }
    }

    /* Trigger update of all map dimensions */
    onMapUpdateAllDimensions({
      origin,
      mapId,
      dimensions: webMapJSInstance.mapdimensions.map(
        ({ name, units, currentValue, synced }) => {
          const reactDim =
            mapProperties.dimensions &&
            mapProperties.dimensions.find(dim => dim.name === name);
          const newDimValue =
            (reactDim && reactDim.currentValue) || currentValue;
          const newDimSynced = (reactDim && reactDim.synced) || synced;
          return {
            name,
            units,
            currentValue: newDimValue,
            synced: newDimSynced,
          };
        },
      ),
    });

    /* Trigger update layer dimensions */
    onLayerSetDimensions({
      origin,
      layerId: wmLayer.id,
      dimensions: wmLayer.dimensions.map(
        ({
          name,
          units,
          currentValue,
          dimMinValue,
          dimMaxValue,
          dimTimeInterval,
          dimAllDates,
          values,
          synced,
        }) => {
          return {
            name,
            units,
            currentValue,
            maxValue: dimMaxValue,
            minValue: dimMinValue,
            timeInterval: dimTimeInterval,
            allDates: dimAllDates,
            values,
            synced,
          };
        },
      ),
    });

    if (mapNeedsUpdate) {
      if (wmLayer.parentMaps && wmLayer.parentMaps.length > 0) {
        wmLayer.parentMaps[0].draw();
      }
    }
  };

  service.getLayerObjectsFlat(
    done,
    () => {
      // do nothing
    },
    false,
    null,
    {
      headers: wmLayer.headers,
    },
  );
};
