/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import IconButton from '@material-ui/core/IconButton';
import UpdateIcon from '@material-ui/icons/Update';

import { Tooltip } from '@material-ui/core';

interface AutoUpdateProps {
  isAutoUpdating: boolean;
  disabled?: boolean;
  onToggleAutoUpdate: () => void;
}

const AutoUpdate: React.FC<AutoUpdateProps> = ({
  isAutoUpdating,
  disabled = false,
  onToggleAutoUpdate,
}: AutoUpdateProps) => (
  <Tooltip
    placement="top"
    title={isAutoUpdating ? 'Stop auto-update' : 'Start auto-update'}
  >
    <span data-testid="autoupdatetooltip">
      <IconButton
        disabled={disabled}
        onClick={(): void => onToggleAutoUpdate()}
        color={isAutoUpdating ? 'primary' : undefined}
      >
        <UpdateIcon />
      </IconButton>
    </span>
  </Tooltip>
);

export default AutoUpdate;
