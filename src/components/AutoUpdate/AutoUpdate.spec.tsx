/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { render, getByRole, fireEvent } from '@testing-library/react';
import AutoUpdate from './AutoUpdate';

describe('src/components/AutoUpdate/AutoUpdate', () => {
  it('should show correct tooltip and color when auto-updating', () => {
    const props = {
      isAutoUpdating: true,
      onToggleAutoUpdate: jest.fn(),
      disabled: false,
    };

    const { container, getByTestId } = render(<AutoUpdate {...props} />);
    const button = getByRole(container, 'button');

    fireEvent.click(button);

    expect(props.onToggleAutoUpdate).toHaveBeenCalled();
    expect(button.className.includes('Primary')).toBeTruthy();
    expect(getByTestId('autoupdatetooltip').title).toEqual('Stop auto-update');
  });

  it('should show correct tooltip and color when not auto-updating', () => {
    const props = {
      isAutoUpdating: false,
      onToggleAutoUpdate: jest.fn(),
      disabled: false,
    };

    const { container, getByTestId } = render(<AutoUpdate {...props} />);
    const button = getByRole(container, 'button');

    expect(button.className.includes('Primary')).toBeFalsy();
    expect(getByTestId('autoupdatetooltip').title).toEqual('Start auto-update');
  });
});
