/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import HomeIcon from '@material-ui/icons/Home';
import { render, fireEvent } from '@testing-library/react';

import ZoomControls, {
  renderZoomIcon,
  ZoomButton,
  ZoomType,
} from './ZoomControls';

describe('src/components/ZoomControls/ZoomControls', () => {
  it('should render correct zoomIcon', () => {
    expect(renderZoomIcon('zoomIn')).toEqual(<AddIcon />);
    expect(renderZoomIcon('zoomOut')).toEqual(<RemoveIcon />);
    expect(renderZoomIcon('zoomReset')).toEqual(<HomeIcon />);
    expect(renderZoomIcon(null)).toEqual(<HomeIcon />);
  });

  it('should render ZoomButton', () => {
    const props = {
      zoomType: 'zoomIn' as ZoomType,
      onClick: jest.fn(),
      className: 'test-className',
    };
    const { container } = render(<ZoomButton {...props} />);

    fireEvent.click(container.querySelector(`.${props.className}`));

    expect(props.onClick).toHaveBeenCalled();
  });

  it('should render ZoomControls and trigger correct callbacks', () => {
    const props = {
      onZoomIn: jest.fn(),
      onZoomOut: jest.fn(),
      onZoomReset: jest.fn(),
    };
    const { container } = render(<ZoomControls {...props} />);

    fireEvent.click(container.querySelector('.zoom-out'));
    expect(props.onZoomOut).toHaveBeenCalled();

    fireEvent.click(container.querySelector('.zoom-in'));
    expect(props.onZoomIn).toHaveBeenCalled();

    fireEvent.click(container.querySelector('.zoom-reset'));
    expect(props.onZoomReset).toHaveBeenCalled();
  });
});
