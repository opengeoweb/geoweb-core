/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import HomeIcon from '@material-ui/icons/Home';
import IconButton from '@material-ui/core/IconButton';

export type ZoomType = 'zoomIn' | 'zoomOut' | 'zoomReset';

export const renderZoomIcon = (zoomType: ZoomType): React.ReactChild => {
  switch (zoomType) {
    case 'zoomIn':
      return <AddIcon />;
    case 'zoomOut':
      return <RemoveIcon />;
    case 'zoomReset':
      return <HomeIcon />;
    default:
      return <HomeIcon />;
  }
};

interface ZoomButtonProps {
  zoomType: ZoomType;
  onClick: () => void;
  className: string;
}

export const ZoomButton: React.FC<ZoomButtonProps> = ({
  zoomType,
  onClick,
  className,
}: ZoomButtonProps) => (
  <IconButton className={className} onClick={onClick}>
    {renderZoomIcon(zoomType)}
  </IconButton>
);

interface ZoomControlsProps {
  onZoomIn: () => void;
  onZoomOut: () => void;
  onZoomReset: () => void;
}

const ZoomControls: React.FC<ZoomControlsProps> = ({
  onZoomIn,
  onZoomOut,
  onZoomReset,
}: ZoomControlsProps) => (
  <div data-testid="zoom-controls">
    <ZoomButton className="zoom-out" zoomType="zoomOut" onClick={onZoomOut} />
    <ZoomButton
      className="zoom-reset"
      zoomType="zoomReset"
      onClick={onZoomReset}
    />
    <ZoomButton className="zoom-in" zoomType="zoomIn" onClick={onZoomIn} />
  </div>
);

export default ZoomControls;
