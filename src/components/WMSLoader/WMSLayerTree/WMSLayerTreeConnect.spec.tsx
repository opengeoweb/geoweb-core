/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import * as layerActionTypes from '../../../store/mapStore/layers/constants';
import WMSLayerTreeConnect from './WMSLayerTreeConnect';
import { defaultReduxLayerRadarColor } from '../../../utils/defaultTestSettings';
import { mockStateMapWithLayer } from '../../../utils/testUtils';
import { MOCK_URL_WITH_CHILDREN } from '../../../utils/__mocks__/getCapabilities';
import { LayerType } from '../../../store/mapStore/types';

jest.mock('../../../utils/getCapabilities');

describe('src/components/WMSLoader/WMSLayerTree/WMSLayerTreeConnect', () => {
  it('should add a layer', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);

    const mockProps = {
      service: {
        url: MOCK_URL_WITH_CHILDREN,
      },
      mapId,
      addLayer: jest.fn(),
    };
    const { findAllByTestId } = render(
      <Provider store={store}>
        <WMSLayerTreeConnect {...mockProps} />
      </Provider>,
    );

    const list = await findAllByTestId('selectableLayer');
    expect(list).toBeTruthy();

    fireEvent.click(list[0]);

    const expectedActions = [
      {
        payload: {
          layer: {
            id: layer.id,
            layerType: layer.layerType,
            name: layer.name,
            service: MOCK_URL_WITH_CHILDREN,
          },
          layerId: layer.id,
          mapId,
        },
        type: layerActionTypes.WEBMAP_ADD_LAYER,
      },
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should add a baselayer if layerType baselayer is passed', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    layer.id = 'layerid_2';
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);

    const mockProps = {
      service: {
        url: MOCK_URL_WITH_CHILDREN,
      },
      mapId,
      layerType: LayerType.baseLayer,
    };
    const { findAllByTestId } = render(
      <Provider store={store}>
        <WMSLayerTreeConnect {...mockProps} />
      </Provider>,
    );

    const list = await findAllByTestId('selectableLayer');
    expect(list).toBeTruthy();

    fireEvent.click(list[0]);

    const expectedActions = [
      {
        type: layerActionTypes.WEBMAP_ADD_AVAILABLE_BASELAYER,
        payload: {
          layer: {
            id: layer.id,
            layerType: LayerType.baseLayer,
            name: layer.name,
            service: MOCK_URL_WITH_CHILDREN,
          },
        },
      },
      {
        type: layerActionTypes.WEBMAP_SET_BASELAYERS,
        payload: {
          layers: [
            {
              id: layer.id,
              layerType: LayerType.baseLayer,
              name: layer.name,
              service: MOCK_URL_WITH_CHILDREN,
            },
          ],
          mapId,
        },
      },
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });
});
