/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { connect } from 'react-redux';
import { generateLayerId } from '../../..';
import WMSLayerTree from './WMSLayerTree';

import { Layer, LayerType } from '../../../store/mapStore/types';
import * as mapActions from '../../../store/mapStore/actions';
import * as mapSelectors from '../../../store/mapStore/selectors';
import { Service } from '../services';
import { AppStore } from '../../../types/types';

interface WMSLayerTreeConnectProps {
  service: Service;
  addLayer: typeof mapActions.addLayer;
  setBaseLayers: typeof mapActions.setBaseLayers;
  addAvailableBaseLayer: typeof mapActions.addAvailableBaseLayer;
  mapId: string;
  loadedLayers: Layer[];
  layerType?: LayerType;
}

const connectRedux = connect(
  (store: AppStore, props: WMSLayerTreeConnectProps) => ({
    loadedLayers: mapSelectors.getMapLayers(store, props.mapId),
  }),
  {
    addLayer: mapActions.addLayer,
    setBaseLayers: mapActions.setBaseLayers,
    addAvailableBaseLayer: mapActions.addAvailableBaseLayer,
  },
);

const ConnectedTree: React.FC<WMSLayerTreeConnectProps> = ({
  addLayer,
  setBaseLayers,
  addAvailableBaseLayer,
  service,
  mapId,
  loadedLayers,
  layerType = LayerType.mapLayer,
}: WMSLayerTreeConnectProps) => {
  const addMapLayer = (serviceURL: string, layerName: string): void => {
    const newWms = {
      service: serviceURL,
      name: layerName,
      id: generateLayerId(),
      layerType: LayerType.mapLayer,
    };
    addLayer({ mapId, layerId: newWms.id, layer: newWms });
  };

  const addBaseLayer = (serviceURL: string, layerName: string): void => {
    const newBaseLayer = {
      service: serviceURL,
      name: layerName,
      id: generateLayerId(),
      layerType: LayerType.baseLayer,
    };
    addAvailableBaseLayer({ layer: newBaseLayer });
    setBaseLayers({ mapId, layers: [newBaseLayer] });
  };

  return (
    <WMSLayerTree
      service={service}
      onClickLayer={(serviceURL, layerName): void => {
        if (layerType === LayerType.baseLayer) {
          addBaseLayer(serviceURL, layerName);
        } else addMapLayer(serviceURL, layerName);
      }}
      highlightedLayers={loadedLayers}
    />
  );
};

const WMSLayerTreeConnect = connectRedux(ConnectedTree);

export default WMSLayerTreeConnect;
