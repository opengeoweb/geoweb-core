/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Delete from '@material-ui/icons/Delete';
import { Tooltip } from '@material-ui/core';

interface DeleteButtonProps {
  tooltipTitle: string | JSX.Element;
  onClickDelete: () => void;
}

const DeleteButton: React.FC<DeleteButtonProps> = ({
  tooltipTitle,
  onClickDelete,
}: DeleteButtonProps) => (
  <Tooltip
    title={tooltipTitle}
    placement="top"
    style={{ verticalAlign: 'baseLine' }}
  >
    <IconButton
      data-testid="deleteButton"
      size="small"
      onClick={(event: React.MouseEvent<HTMLInputElement>): void => {
        event.stopPropagation();
        onClickDelete();
      }}
    >
      <Delete />
    </IconButton>
  </Tooltip>
);

export default DeleteButton;
