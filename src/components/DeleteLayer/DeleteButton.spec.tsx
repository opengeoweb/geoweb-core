/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  render,
  getByRole,
  fireEvent,
  findByRole,
} from '@testing-library/react';
import DeleteButton from './DeleteButton';

describe('src/components/DeleteLayer/DeleteButton', () => {
  const props = {
    onClickDelete: jest.fn(),
    tooltipTitle: 'Tooltip',
  };

  it('should call function when clicking button', () => {
    const { container } = render(<DeleteButton {...props} />);
    const button = getByRole(container, 'button');

    fireEvent.click(button);

    expect(props.onClickDelete).toBeCalled();
  });

  it('should show tooltip on hover', async () => {
    const { container } = render(<DeleteButton {...props} />);

    fireEvent.mouseOver(container.querySelector('button'));

    const tooltip = await findByRole(container.parentElement, 'tooltip');

    expect(tooltip.textContent).toMatch(props.tooltipTitle);
  });
});
