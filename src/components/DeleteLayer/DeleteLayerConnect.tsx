/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import * as mapActions from '../../store/mapStore/actions';

import DeleteButton from './DeleteButton';

const tooltipText = 'Remove layer';

const connectRedux = connect(null, {
  layerDelete: mapActions.layerDelete,
});

interface DeleteLayerProps {
  layerId: string;
  mapId: string;
  layerDelete: typeof mapActions.layerDelete;
}

const DeleteLayer: React.FC<DeleteLayerProps> = ({
  layerId,
  mapId,
  layerDelete,
}: DeleteLayerProps) => (
  <DeleteButton
    tooltipTitle={tooltipText}
    onClickDelete={(): void => {
      layerDelete({ mapId, layerId });
    }}
  />
);

/**
 * Deletes a maplayer from the store
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {string} layerId layerId: string - Id of the layer that is deleted
 * @example
 * ``` <DeleteLayerConnect mapId={mapId} layerId={layerId} />```
 */
const DeleteLayerConnect = connectRedux(DeleteLayer);

export default DeleteLayerConnect;
