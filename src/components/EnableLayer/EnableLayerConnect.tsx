/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import EnableButton from './EnableLayer';
import * as mapActions from '../../store/mapStore/actions';
import * as layerSelectors from '../../store/mapStore/layers/selectors';

const EnableLayerTitle = "Toggle this layer's visibility";

const connectRedux = connect(
  (store, props) => ({
    isEnabled: layerSelectors.getLayerEnabled(store, props.layerId),
  }),
  {
    layerChangeEnabled: mapActions.layerChangeEnabled,
  },
);

interface EnableLayerProps {
  layerId: string;
  isEnabled: boolean;
  layerChangeEnabled: typeof mapActions.layerChangeEnabled;
}

const EnableLayer: React.FC<EnableLayerProps> = ({
  layerId,
  layerChangeEnabled,
  isEnabled,
}: EnableLayerProps) => (
  <EnableButton
    enableLayer={isEnabled}
    title={EnableLayerTitle}
    onChangeEnableLayer={(enable: boolean): void => {
      layerChangeEnabled({
        layerId,
        enabled: enable,
      });
    }}
  />
);

/**
 * Allows you to show/hide a layer on the map
 *
 * Expects the following props:
 * @param {string} layerId layerId: string - Id of the layer that is toggled
 * @example
 * ``` <EnableLayerConnect layerId={layerId} />```
 */
const EnableLayerConnect = connectRedux(EnableLayer);

export default EnableLayerConnect;
