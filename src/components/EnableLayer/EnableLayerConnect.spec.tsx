/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { defaultReduxLayerRadarColor } from '../../utils/defaultTestSettings';

import EnableLayerConnect from './EnableLayerConnect';
import { mockStateMapWithLayer } from '../../utils/testUtils';
import * as layerActionTypes from '../../store/mapStore/layers/constants';

describe('src/components/EnableLayer/EnableLayerConnect', () => {
  it('should enable/disable layer', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = mockStateMapWithLayer(layer, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);

    const { getByRole } = render(
      <Provider store={store}>
        <EnableLayerConnect layerId={layer.id} />
      </Provider>,
    );

    const checkbox = getByRole('checkbox');

    fireEvent.click(checkbox);

    const expectedAction = [
      {
        payload: {
          layerId: layer.id,
          enabled: !defaultReduxLayerRadarColor.enabled,
        },
        type: layerActionTypes.WEBMAP_LAYER_CHANGE_ENABLED,
      },
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
