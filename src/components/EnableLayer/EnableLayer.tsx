/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Tooltip, makeStyles } from '@material-ui/core';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Visibility from '@material-ui/icons/Visibility';
import Checkbox from '@material-ui/core/Checkbox';

interface EnableLayerProps {
  enableLayer: boolean;
  title: string;
  onChangeEnableLayer: (enable: boolean) => void;
}

const useStyles = makeStyles({
  buttonSize: {
    padding: 4,
    verticalAlign: 'baseLine',
  },
});

const EnableLayer: React.FC<EnableLayerProps> = ({
  enableLayer,
  title,
  onChangeEnableLayer,
}: EnableLayerProps) => {
  const classes = useStyles();
  const onClick = (event: React.MouseEvent<HTMLInputElement>): void => {
    const element = event.target as HTMLInputElement;
    event.stopPropagation();
    onChangeEnableLayer(element.checked);
  };

  return (
    <Tooltip className={classes.buttonSize} title={title} placement="top">
      <Checkbox
        icon={<VisibilityOff />}
        checkedIcon={<Visibility />}
        checked={enableLayer || false}
        onClick={onClick}
      />
    </Tooltip>
  );
};

export default EnableLayer;
