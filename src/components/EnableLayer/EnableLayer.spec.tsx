/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import EnableLayer from './EnableLayer';

describe('src/components/EnableLayer/EnableLayer', () => {
  // Test 1
  const propsTest1 = {
    onChangeEnableLayer: jest.fn(),
    title: 'Test1',
    enableLayer: true,
  };

  it('should be checked if enabled passed in as true', () => {
    const { container } = render(<EnableLayer {...propsTest1} />);
    const checkbox = getByRole(container, 'checkbox');
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(checkbox.checked).toEqual(propsTest1.enableLayer);
  });

  // Test 2
  const propsTest2 = {
    onChangeEnableLayer: jest.fn(),
    title: 'Test2',
    enableLayer: false,
  };

  it('should not be checked if enabled passed in as false', () => {
    const { container } = render(<EnableLayer {...propsTest2} />);
    const checkbox = getByRole(container, 'checkbox');
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(checkbox.checked).toEqual(propsTest2.enableLayer);
  });

  // Test 3
  it('should call onChangeEnableLayer when checkbox clicked', () => {
    const { container } = render(<EnableLayer {...propsTest1} />);
    const checkbox = getByRole(container, 'checkbox');
    fireEvent.click(checkbox);

    expect(propsTest1.onChangeEnableLayer).toHaveBeenCalledWith(
      !propsTest1.enableLayer,
    );
  });
});
