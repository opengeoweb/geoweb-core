/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import {
  multiDimensionLayer2,
  WmMultiDimensionLayer2,
} from '../../utils/defaultTestSettings';

import DimensionSelectConnect from './DimensionSelectConnect';
import { mockStateMapWithDimensions } from '../../utils/testUtils';
import * as layerActionTypes from '../../store/mapStore/layers/constants';
import { registerWMLayer } from '../../store/mapStore/utils/helpers';

describe('src/components/DimensionSelectConnect/DimensionSelectConnect', () => {
  it('should set dimension value', async () => {
    const mapId = 'mapid_1';
    const layer = multiDimensionLayer2;
    const mockState = mockStateMapWithDimensions(layer, mapId);
    registerWMLayer(WmMultiDimensionLayer2, 'multiDimensionLayerMock2');

    const mockStore = configureStore();
    const store = mockStore(mockState);

    const { findByText, findByTestId } = render(
      <Provider store={store}>
        <DimensionSelectConnect mapId={mapId} layerId={layer.id} />
      </Provider>,
    );

    const select = await findByTestId('selectDimensionValue');

    fireEvent.mouseDown(select);
    const newName = {
      value: 'member3',
      title: 'member3',
    };

    const menuItem = await findByText(newName.title);

    fireEvent.click(menuItem);

    const expectedAction = [
      {
        payload: {
          origin: 'layerrow',
          layerId: layer.id,
          dimension: {
            name: 'member',
            currentValue: newName.value,
          },
        },
        type: layerActionTypes.WEBMAP_LAYER_CHANGE_DIMENSION,
      },
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
