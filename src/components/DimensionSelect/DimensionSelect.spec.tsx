/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import DimensionSelect from './DimensionSelect';
import {
  defaultReduxLayerRadarColor,
  WmMultiDimensionLayer,
  multiDimensionLayer,
} from '../../utils/defaultTestSettings';
import { registerWMLayer } from '../../store/mapStore/utils/helpers';
import { Layer } from '../../store/mapStore/types';

describe('src/components/DimensionSelect', () => {
  it('should not show the dimension selector if the layer has no dimensions', () => {
    const mockProps = {
      layer: defaultReduxLayerRadarColor,
      mapId: 'map_1',
      onLayerChangeDimension: jest.fn(),
      availableLayerDimensions: [],
    };
    const { queryByTestId } = render(<DimensionSelect {...mockProps} />);

    expect(queryByTestId('selectDimension')).toBeFalsy();
    expect(queryByTestId('selectDimensionValue')).toBeFalsy();
  });

  it('should show the dimension selector with the first dimension and value selected if the layer has dimensions', () => {
    const mockProps = {
      layer: multiDimensionLayer,
      mapId: 'map_1',
      onLayerChangeDimension: jest.fn(),
      availableLayerDimensions: [
        {
          name: 'flight level',
          units: 'hft',
          currentValue: '625',
          values: '25,325,625',
        },
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
        },
      ],
    };

    registerWMLayer(WmMultiDimensionLayer, 'multiDimensionLayerMock');

    const { getByTestId } = render(<DimensionSelect {...mockProps} />);

    // check the textContent of the first dropdown (dimension)
    const dimensionSelect = getByTestId('selectDimension');
    expect(dimensionSelect).toBeTruthy();
    expect(dimensionSelect.textContent).toEqual(
      (multiDimensionLayer as Layer).dimensions[0].name,
    );

    // check the value property of the second dropdown (dimension value)
    const valueSelect = getByTestId('selectDimensionValue');
    const valueSelectInput = valueSelect.parentElement.querySelector('input');
    expect(valueSelectInput.getAttribute('value')).toEqual(
      (multiDimensionLayer as Layer).dimensions[0].currentValue,
    );
  });

  it('should update the dimension values list if a new dimension name is selected', async () => {
    const mockProps = {
      layer: multiDimensionLayer,
      mapId: 'map_1',
      onLayerChangeDimension: jest.fn(),
      availableLayerDimensions: [
        {
          name: 'flight level',
          units: 'hft',
          currentValue: '625',
          values: '25,325,625',
        },
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
        },
      ],
    };
    const { getByTestId, getAllByRole, findByText } = render(
      <DimensionSelect {...mockProps} />,
    );

    const newDimension = mockProps.availableLayerDimensions[1];
    const dimensionSelect = getByTestId('selectDimension');
    fireEvent.mouseDown(dimensionSelect);

    const menuItem = await findByText(newDimension.name);
    fireEvent.click(menuItem);

    await waitFor(() =>
      // It should show the new dimension name as selected
      expect(dimensionSelect.textContent).toEqual(newDimension.name),
    );

    // It should not trigger an action
    expect(mockProps.onLayerChangeDimension).not.toHaveBeenCalled();

    // It should update the values in the dimension values list
    const valueSelect = getByTestId('selectDimensionValue');
    fireEvent.mouseDown(valueSelect);
    const valuesList = getAllByRole('option');
    valuesList.shift(); // remove the first item because it is a placeholder
    valuesList.forEach(value => {
      expect(value.textContent).toContain(newDimension.units);
    });
  });

  it('should trigger onLayerChangeDimension if a new dimension value is selected', async () => {
    const mockProps = {
      layer: multiDimensionLayer,
      mapId: 'map_1',
      onLayerChangeDimension: jest.fn(),
      availableLayerDimensions: [
        {
          name: 'flight level',
          units: 'hft',
          currentValue: '625',
          values: '25,325,625',
        },
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
        },
      ],
    };

    const { getByTestId, findByText } = render(
      <DimensionSelect {...mockProps} />,
    );

    const newDimensionValue = '325';
    const valueSelect = getByTestId('selectDimensionValue');
    fireEvent.mouseDown(valueSelect);

    const menuItemValue = await findByText(newDimensionValue);
    fireEvent.click(menuItemValue);

    expect(mockProps.onLayerChangeDimension).toHaveBeenCalled();
  });
});
