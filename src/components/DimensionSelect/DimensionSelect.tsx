/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { MenuItem, Box, Grid } from '@material-ui/core';
import { styled } from '@material-ui/core/styles';
import { Dimension, Layer } from '../../store/mapStore/types';
import { getWMLayerById } from '../../store/mapStore/utils/helpers';
import TooltipSelect from '../TooltipSelect/TooltipSelect';

const StyledLabelBox = styled(Box)({
  paddingTop: '8px',
  marginRight: '2px',
  color: 'rgba(0, 0, 0, 0.54)',
  fontSize: '0.8rem',
  fontFamily: 'Roboto Helvetica, Arial',
  lineHeight: 1,
  letterSpacing: '0.00938em',
  textAlign: 'right',
});

interface DimensionSelectProps {
  layer: Layer;
  mapId: string;
  availableLayerDimensions?: Dimension[];
  onLayerChangeDimension: (
    mapId: string,
    layerId: string,
    dimensionName: string,
    dimensionValue: string,
  ) => void;
}

const DimensionSelect: React.FC<DimensionSelectProps> = ({
  layer,
  mapId,
  onLayerChangeDimension,
  availableLayerDimensions = [],
}: DimensionSelectProps) => {
  const defaultDimName = availableLayerDimensions.length
    ? availableLayerDimensions[0].name
    : '';

  /* Use state */
  const [activeDimName, setActiveDimName] = React.useState(defaultDimName);

  /* Use effect to update the active dimension */
  React.useEffect(() => {
    const activeDim = availableLayerDimensions.find(
      d => d.name === activeDimName,
    );
    if (!activeDim) {
      setActiveDimName(
        availableLayerDimensions.length ? availableLayerDimensions[0].name : '',
      );
    }
  }, [availableLayerDimensions, activeDimName]);

  if (!availableLayerDimensions.length) return null;
  if (!layer || !layer.id) return null;

  const wmjsLayer = getWMLayerById(layer.id);
  if (!wmjsLayer) return null;
  const activeDim =
    wmjsLayer.getDimension(activeDimName) ||
    wmjsLayer.getDimension(availableLayerDimensions[0].name);
  if (!activeDim) return null;

  const availableDimValues = [];
  for (let j = 0; j < activeDim.size(); j += 1) {
    availableDimValues.push(activeDim.getValueForIndex(j));
  }
  availableDimValues.reverse();

  // Only display last 100 values, to avoid performance issues
  // TODO: (2020-06-29 Tineke van Rijn) replace this with lazy loading
  const valuesToDisplay = availableDimValues.slice(0, 100);

  const selectDimensionValue = (
    event: React.ChangeEvent<HTMLInputElement>,
  ): void => {
    event.stopPropagation();
    if (activeDim) {
      onLayerChangeDimension(
        mapId,
        layer.id,
        activeDim.name,
        event.target.value,
      );
    }
  };

  const onChangeActiveDimensionName = (
    event: React.ChangeEvent<HTMLInputElement>,
  ): void => {
    event.stopPropagation();
    setActiveDimName(event.target.value);
  };

  return (
    <Grid container direction="row" justify="flex-start" alignItems="center">
      <Grid item xs={2}>
        <StyledLabelBox>
          {`(${1 +
            availableLayerDimensions.findIndex(
              d => d.name === activeDimName,
            )}/${availableLayerDimensions.length})`}
        </StyledLabelBox>
      </Grid>
      <Grid item xs={10}>
        <TooltipSelect
          tooltip={`Select the dimension to adjust, currently it is set to ${activeDimName} with value ${activeDim.currentValue}`}
          inputProps={{
            SelectDisplayProps: {
              'data-testid': 'selectDimension',
            },
          }}
          style={{ maxWidth: '45%', marginLeft: '2px' }}
          value={activeDimName}
          onChange={onChangeActiveDimensionName}
        >
          <MenuItem disabled>Select dimension</MenuItem>
          {availableLayerDimensions.map(dimension => (
            <MenuItem key={dimension.name} value={dimension.name}>
              {dimension.name}
            </MenuItem>
          ))}
        </TooltipSelect>

        <TooltipSelect
          tooltip={`Dimension ${activeDimName} is set to ${activeDim.currentValue} with units ${activeDim.units}`}
          inputProps={{
            SelectDisplayProps: {
              'data-testid': 'selectDimensionValue',
            },
          }}
          style={{ maxWidth: '45%', marginLeft: '6px' }}
          value={activeDim && activeDim.currentValue}
          onChange={selectDimensionValue}
        >
          <MenuItem disabled>
            Select dimension value. ({valuesToDisplay.length} options)
          </MenuItem>
          {valuesToDisplay.map(dimValue => (
            <MenuItem key={dimValue} value={dimValue}>
              {`${dimValue}`}
              <>
                &nbsp;
                <i>{`${activeDim.units}`}</i>
              </>
            </MenuItem>
          ))}
        </TooltipSelect>
      </Grid>
    </Grid>
  );
};

export default DimensionSelect;
