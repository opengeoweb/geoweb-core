/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import DimensionSelect from './DimensionSelect';
import * as mapActions from '../../store/mapStore/actions';
import * as layerSelectors from '../../store/mapStore/layers/selectors';
import * as serviceSelectors from '../../store/mapStore/service/selectors';
import { AppStore } from '../../types/types';
import { Dimension, Layer } from '../../store/mapStore/types';

interface DimensionSelectProps {
  layerId: string;
  mapId: string;
  layer: Layer;
  availableLayerDimensions: Dimension[];
  onLayerChangeDimension: typeof mapActions.layerChangeDimension;
}

const connectRedux = connect(
  (store: AppStore, props: DimensionSelectProps) => ({
    layer: layerSelectors.getLayerById(store, props.layerId),
    availableLayerDimensions: serviceSelectors.getServiceLayerNonTimeDimensions(
      store,
      props.layerId,
    ),
  }),
  {
    onLayerChangeDimension: mapActions.layerChangeDimension,
  },
);

const DimensionSelectConnect: React.FC<DimensionSelectProps> = ({
  layerId,
  mapId,
  layer,
  availableLayerDimensions,
  onLayerChangeDimension,
}: DimensionSelectProps) => (
  <DimensionSelect
    layer={layer}
    mapId={mapId}
    availableLayerDimensions={availableLayerDimensions}
    onLayerChangeDimension={(
      _mapId: string,
      _layerId: string,
      dimensionName: string,
      dimensionValue: string,
    ): void => {
      const dimension: Dimension = {
        name: dimensionName,
        currentValue: dimensionValue,
      };

      onLayerChangeDimension({
        origin: 'layerrow',
        layerId,
        dimension,
      });
    }}
  />
);

export default connectRedux(DimensionSelectConnect);
