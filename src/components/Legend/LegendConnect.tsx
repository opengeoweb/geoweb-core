/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import LegendDialog from './LegendDialog';
import { AppStore } from '../../types/types';
import * as mapSelectors from '../../store/mapStore/selectors';
import { Layer } from '../../store/mapStore/types';

interface LegendConnectProps {
  mapLayers: Layer[];
  // eslint-disable-next-line react/no-unused-prop-types
  mapId?: string;
}

const connectRedux = connect((store: AppStore, props: LegendConnectProps) => ({
  mapLayers: mapSelectors.getMapLayers(store, props.mapId),
}));

const Legend: React.FC<LegendConnectProps> = ({
  mapLayers,
}: LegendConnectProps) => <LegendDialog layers={mapLayers} />;

/**
 * Legend component connected to the store displaying a legend for every layer shown on the corresponding map
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <LegendConnect mapId={mapId} />```
 */
const LegendConnect = connectRedux(Legend);

export default LegendConnect;
