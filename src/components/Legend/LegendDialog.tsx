/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MapIcon from '@material-ui/icons/Map';
import Legend from './Legend';
import { Layer } from '../../store/mapStore/types';
import DraggableDialog from '../DraggableDialog/DraggableDialog';

const useStyles = makeStyles(() => ({
  legendList: {
    display: 'flex',
    flexDirection: 'column',
    overflowY: 'scroll',
  },
  // TODO: (Sander de Snaijer 2020-06-09) after we have some default theming, we could make some 'helpers' like this mapcontrols container
  legendContainer: {
    display: 'grid',
    width: '100%',
    height: '100%',
    position: 'absolute',
    overflow: 'hidden',
    pointerEvents: 'none',
    zIndex: 502,
  },
  legendControl: {
    gridColumnStart: 1,
    gridRowStart: 1,
    justifySelf: 'end',
    alignSelf: 'top',
    marginTop: '50px',
    pointerEvents: 'all',
    height: 0,
    width: 'auto',
  },
}));

interface LegendDialogProps {
  layers: Layer[];
}

const LegendDialog: React.FC<LegendDialogProps> = ({
  layers,
}: LegendDialogProps) => {
  const layersEnabled =
    layers &&
    layers.length > 0 &&
    layers.find(layer => layer.enabled) !== undefined;

  const [open, setOpen] = React.useState(layersEnabled);

  React.useEffect(() => {
    if (layersEnabled) {
      setOpen(true);
    } else {
      setOpen(false);
    }
  }, [layersEnabled]);

  const classes = useStyles();

  return (
    <div className={classes.legendContainer}>
      <div className={classes.legendControl}>
        <DraggableDialog name="Legend" mainIcon={MapIcon} showAsOpen={open}>
          {layers && layers.length > 0 ? (
            <div data-testid="LegendList" className={classes.legendList}>
              {layers.map(layer => (
                <Legend key={layer.id} layer={layer} />
              ))}
            </div>
          ) : (
            <Typography
              data-testid="NoLayers"
              variant="body1"
              style={{ fontWeight: 500, fontSize: 14 }}
            >
              No Layers
            </Typography>
          )}
        </DraggableDialog>
      </div>
    </div>
  );
};

export default LegendDialog;
