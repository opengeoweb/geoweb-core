/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { LegendDialog } from '.';
import {
  defaultReduxLayerRadarColor,
  defaultReduxLayerRadarKNMI,
  WmdefaultReduxLayerRadarKNMI,
  WmMultiDimensionLayer,
  multiDimensionLayer,
} from '../../utils/defaultTestSettings';
import { registerWMLayer } from '../../store/mapStore/utils/helpers';

describe('src/components/LegendOverview/LegendDialog', () => {
  it('should close and open the legend dialog', () => {
    const { getByTestId, queryByTestId } = render(
      <LegendDialog
        layers={[defaultReduxLayerRadarColor, defaultReduxLayerRadarKNMI]}
      />,
    );

    // legend dialog should be opened by default if there are layers
    expect(getByTestId('draggable-dialog')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(getByTestId('hideDraggable'));
    expect(queryByTestId('draggable-dialog')).toBeFalsy();

    // open the legend dialog
    fireEvent.click(getByTestId('showDraggable'));
    expect(getByTestId('draggable-dialog')).toBeTruthy();
  });
  it('should show the drag button on the legend dialog if there are layers', () => {
    const { getByTestId } = render(
      <LegendDialog layers={[defaultReduxLayerRadarColor]} />,
    );

    expect(getByTestId('draggable-dialog')).toBeTruthy();
    expect(getByTestId('dragDialog')).toBeTruthy();
  });
  it('should hide the dialog by default if there are no layers', () => {
    const { getByTestId, queryByTestId } = render(<LegendDialog layers={[]} />);
    // legend dialog should be closed by default
    expect(queryByTestId('draggable-dialog')).toBeFalsy();
    // open the legend dialog
    fireEvent.click(getByTestId('showDraggable'));
    expect(getByTestId('draggable-dialog')).toBeTruthy();
    expect(getByTestId('NoLayers')).toBeTruthy();
  });
  it('should hide the dialog by default if all layers are disabled', () => {
    const defaultReduxLayerRadarColorDisabled = {
      ...defaultReduxLayerRadarColor,
      enabled: false,
    };
    const defaultReduxLayerRadarKNMIDisabled = {
      ...defaultReduxLayerRadarKNMI,
      enabled: false,
    };

    const { queryByTestId } = render(
      <LegendDialog
        layers={[
          defaultReduxLayerRadarColorDisabled,
          defaultReduxLayerRadarKNMIDisabled,
        ]}
      />,
    );

    // legend dialog should be closed by default
    expect(queryByTestId('legend-dialog')).toBeFalsy();
  });

  it('should show the legend of the layers in the legend dialog if there are layers loaded', () => {
    const layers = [defaultReduxLayerRadarKNMI, defaultReduxLayerRadarColor];
    const { getByTestId } = render(<LegendDialog layers={layers} />);

    expect(getByTestId('LegendList')).toBeTruthy();
  });

  it('should show a legend for each layer', () => {
    const multiDimensionLayerMock = {
      ...multiDimensionLayer,
      id: WmMultiDimensionLayer.id,
    };
    const layers = [defaultReduxLayerRadarKNMI, multiDimensionLayerMock];
    registerWMLayer(WmMultiDimensionLayer, WmMultiDimensionLayer.id);
    registerWMLayer(
      WmdefaultReduxLayerRadarKNMI,
      WmdefaultReduxLayerRadarKNMI.id,
    );
    const { getAllByTestId } = render(<LegendDialog layers={layers} />);
    expect(getAllByTestId('legend').length).toEqual(layers.length);
  });
});
