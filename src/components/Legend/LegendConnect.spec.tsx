/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import LegendConnect from './LegendConnect';
import {
  defaultReduxLayerRadarKNMI,
  WmdefaultReduxLayerRadarKNMI,
} from '../../utils/defaultTestSettings';
import { mockStateMapWithMultipleLayers } from '../../utils/testUtils';
import { baseLayerGrey, overLayer } from '../../utils/testLayers';
import { registerWMLayer } from '../../store/mapStore/utils/helpers';

describe('src/components/Legend/LegendConnect', () => {
  it('should show a legend when there are maplayers', () => {
    const mapId = 'map-1';
    const layers = [defaultReduxLayerRadarKNMI, baseLayerGrey, overLayer];
    registerWMLayer(
      WmdefaultReduxLayerRadarKNMI,
      defaultReduxLayerRadarKNMI.id,
    );
    const mockState = mockStateMapWithMultipleLayers(layers, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getAllByTestId, getByTestId } = render(
      <Provider store={store}>
        <LegendConnect mapId={mapId} />
      </Provider>,
    );

    expect(getAllByTestId('legend').length).toEqual(1);
    expect(getByTestId('legend-title').textContent).toEqual(
      defaultReduxLayerRadarKNMI.title,
    );
    expect(getByTestId('LegendList')).toBeTruthy();
  });

  it('should not show a legend when no mapLayers', () => {
    const mapId = 'map-1';
    registerWMLayer(WmdefaultReduxLayerRadarKNMI, overLayer.id);
    const mockState = mockStateMapWithMultipleLayers([overLayer], mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { container } = render(
      <Provider store={store}>
        <LegendConnect mapId={mapId} />
      </Provider>,
    );

    expect(container.querySelector('[data-testid=legend]')).toBeNull();
    expect(container.querySelector('[data-testid=LegendList]')).toBeNull();
  });
});
