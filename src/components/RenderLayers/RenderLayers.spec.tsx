/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import RenderLayers from './RenderLayers';
import {
  defaultReduxLayerRadarColor,
  defaultReduxServices,
} from '../../utils/defaultTestSettings';

describe('src/components/RenderLayers/RenderLayers', () => {
  const defaultProps = {
    layers: defaultReduxServices[defaultReduxLayerRadarColor.service].layers,
    layerName: defaultReduxLayerRadarColor.name,
    onChangeLayerName: jest.fn(),
  };

  it('should show a message if no layers are provided', async () => {
    const mockProps = {
      ...defaultProps,
      layers: [],
    };

    const { queryByTestId, queryByText } = render(
      <RenderLayers {...mockProps} />,
    );

    expect(queryByText('No service available')).toBeTruthy();
    expect(queryByTestId('selectLayer')).toBeFalsy();
  });

  it('should show the select layer component with the first layer selected if layers are provided', () => {
    const { queryByText, getByTestId } = render(
      <RenderLayers {...defaultProps} />,
    );
    const selectLayer = getByTestId('selectLayer');

    expect(queryByText('No service available')).toBeFalsy();
    expect(selectLayer).toBeTruthy();
    expect(selectLayer.textContent).toEqual(defaultProps.layers[0].text);
  });

  it('should trigger onChangeLayerName if a new layer is selected', async () => {
    const { getByTestId, findByText } = render(
      <RenderLayers {...defaultProps} />,
    );
    const newLayer = defaultProps.layers[1];
    const selectLayer = getByTestId('selectLayer');

    fireEvent.mouseDown(selectLayer);

    const menuItem = await findByText(newLayer.text);
    fireEvent.click(menuItem);

    expect(defaultProps.onChangeLayerName).toHaveBeenCalledWith(newLayer.name);
  });

  it('should show only categories as disabled', async () => {
    const mockProps = {
      ...defaultProps,
      layers: [
        {
          name: 'Category',
          text: 'Category',
          leaf: false,
          path: '',
        },
        {
          name: 'Layer',
          text: 'Layer',
          leaf: true,
          path: 'Category',
        },
      ],
    };
    const { getByTestId, findByText } = render(<RenderLayers {...mockProps} />);

    const selectLayer = getByTestId('selectLayer');
    fireEvent.mouseDown(selectLayer);

    const category = await findByText(mockProps.layers[0].text);
    expect(category.className).toContain('disabled');

    const layer = await findByText(mockProps.layers[1].text);
    expect(layer.className).not.toContain('disabled');
  });
});
