/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import RenderLayers from './RenderLayers';
import * as mapActions from '../../store/mapStore/actions';
import * as mapSelectors from '../../store/mapStore/selectors';
import { ServiceLayer } from '../../store/mapStore/types';
import { AppStore } from '../../types/types';

interface RenderServiceLayersProps {
  layerService: string;
  layers: ServiceLayer[];
  onRender: (layers: ServiceLayer[]) => React.ReactChild;
}

const RenderServiceLayers = connect(
  (store: AppStore, props: RenderServiceLayersProps) => ({
    layers: mapSelectors.getServiceLayersByName(store, props.layerService),
  }),
)(({ layers, onRender }) => onRender(layers));

interface RenderLayersProps {
  layerId: string;
  layerChangeName: typeof mapActions.layerChangeName;
  layerService: string;
  layerName: string;
}
const connectRedux = connect(
  (store: AppStore, props: RenderLayersProps) => ({
    layerName: mapSelectors.getLayerName(store, props.layerId),
    layerService: mapSelectors.getLayerService(store, props.layerId),
  }),
  {
    layerChangeName: mapActions.layerChangeName,
  },
);

const RenderLayersConnect: React.FC<RenderLayersProps> = ({
  layerId,
  layerName,
  layerService,
  layerChangeName,
}: RenderLayersProps) => (
  <RenderServiceLayers
    layerService={layerService}
    onRender={(layers): React.ReactChild => (
      <RenderLayers
        layers={layers}
        layerName={layerName}
        onChangeLayerName={(name): void => {
          layerChangeName({ layerId, name });
        }}
      />
    )}
  />
);

export default connectRedux(RenderLayersConnect);
