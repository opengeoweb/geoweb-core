/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';

import { ServiceLayer } from '../../store/mapStore/types';
import TooltipSelect from '../TooltipSelect/TooltipSelect';

const useStyles = makeStyles(() =>
  createStyles({
    dropdownEmpty: {
      paddingTop: 8,
      fontFamily: 'Roboto, Helvetica, Arial',
    },
  }),
);

interface RenderLayersProps {
  onChangeLayerName: (name: string) => void;
  layerName: string;
  layers: ServiceLayer[];
}

const RenderLayers: React.FC<RenderLayersProps> = ({
  onChangeLayerName,
  layerName,
  layers = [],
}: RenderLayersProps) => {
  const classes = useStyles();

  if (!layers || !layers.length) {
    return <div className={classes.dropdownEmpty}>No service available</div>;
  }

  const filteredLayers = layers.filter(l => l.name === layerName);

  const currentValue =
    filteredLayers.length === 1 && filteredLayers[0].name
      ? filteredLayers[0].name
      : '';

  const selectLayer = (event: React.ChangeEvent<{ value: string }>): void => {
    event.stopPropagation();
    onChangeLayerName(event.target.value);
  };

  return (
    <TooltipSelect
      tooltip="Select a layer"
      inputProps={{
        SelectDisplayProps: {
          'data-testid': 'selectLayer',
        },
      }}
      style={{ width: '100%' }}
      value={currentValue}
      onChange={selectLayer}
    >
      {layers.map((layerFromServiceLayers: ServiceLayer) => (
        <MenuItem
          key={layerFromServiceLayers.name}
          value={layerFromServiceLayers.name}
          disabled={!layerFromServiceLayers.leaf} // show categories as disabled
        >
          {layerFromServiceLayers.text}
        </MenuItem>
      ))}
    </TooltipSelect>
  );
};

export default RenderLayers;
