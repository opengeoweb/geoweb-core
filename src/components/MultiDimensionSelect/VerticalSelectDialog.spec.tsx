/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  render,
  getAllByText,
  cleanup,
  getAllByTestId,
  fireEvent,
} from '@testing-library/react';
import VerticalSelectDialog from './VerticalSelectDialog';
import {
  elevationDimMapTwoLayers,
  elevationInvalidValue,
} from '../../utils/testDimensionData';

afterEach(cleanup);

describe('src/components/MultiDimensionSelect/VerticalSelectDialog', () => {
  it('should render a marker for each dimension value', () => {
    const { container } = render(
      <VerticalSelectDialog
        dimension="elevation"
        isDialogOpen
        sliderDataArray={elevationDimMapTwoLayers}
        handleSyncChanged={jest.fn()}
        handleDimensionValueChanged={jest.fn()}
      />,
    );
    const labelFor200 = getAllByText(container, '200 hPa');
    const labelFor400 = getAllByText(container, '400 hPa');
    const labelFor1000 = getAllByText(container, '1000 hPa');
    // two layers has elevation 200
    expect(labelFor200.length).toBe(2);
    // one layer has elevation 400
    expect(labelFor400.length).toBe(1);
    // two layers has elevation 1000
    expect(labelFor1000.length).toBe(2);
  });

  it('show red slider if invalid value is selected', () => {
    const { container } = render(
      <VerticalSelectDialog
        dimension="elevation"
        isDialogOpen
        sliderDataArray={elevationInvalidValue}
        handleSyncChanged={jest.fn()}
        handleDimensionValueChanged={jest.fn()}
      />,
    );
    const sliders = getAllByTestId(container, 'verticalSlider');
    expect(sliders[0].getAttribute('style').includes('red')).toBeTruthy();
  });

  it('should fire callback correctly', () => {
    const handleSyncChanged = jest.fn();
    const handleDimensionValueChanged = jest.fn();
    const { container } = render(
      <VerticalSelectDialog
        dimension="elevation"
        isDialogOpen
        sliderDataArray={elevationDimMapTwoLayers}
        handleSyncChanged={handleSyncChanged}
        handleDimensionValueChanged={handleDimensionValueChanged}
      />,
    );
    const button = getAllByTestId(container, 'syncButton');
    fireEvent.click(button[0]);
    expect(handleSyncChanged.mock.calls.length).toBe(1);
    expect(handleSyncChanged.mock.calls[0][0]).toBe('layer_1');

    fireEvent.click(button[1]);
    expect(handleSyncChanged.mock.calls.length).toBe(2);
    expect(handleSyncChanged.mock.calls[1][0]).toBe('layer_2');
  });
});
