/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  makeStyles,
  Grid,
  Typography,
  IconButton,
  Tooltip,
} from '@material-ui/core';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import LinkIcon from '@material-ui/icons/Link';
import LinkOffIcon from '@material-ui/icons/LinkOff';
import DraggableDialog from '../DraggableDialog/DraggableDialog';
import { DimensionConfig, DimensionSliderData } from './types';
import VerticalDimensionSelect from '../VerticalDimensionSelect/VerticalDimensionSelect';
import { dimensionConfig, dimensionIcons } from './MultiDimensionSelectConfig';

const useStyles = makeStyles(() => ({
  verticalLevelDimensionSelectContainer: {
    height: '320px',
    margin: '10px',
    marginLeft: '-5px',
  },
  layerName: {
    maxWidth: 130,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
}));

interface Props {
  sliderDataArray: DimensionSliderData[];
  isDialogOpen: boolean;
  dimension: string;
  handleSyncChanged: (layerId: string, dimensionName: string) => void;
  handleDimensionValueChanged: (
    layerId: string,
    dimensionName: string,
    dimensionValue: string,
    changeSynched: boolean,
  ) => void;
}

const VerticalSelectDialog: React.FC<Props> = ({
  handleDimensionValueChanged,
  handleSyncChanged,
  sliderDataArray,
  isDialogOpen,
  dimension,
}: Props) => {
  const classes = useStyles();
  const [dimConfig, setDimConfig] = React.useState<DimensionConfig>();
  React.useEffect(() => {
    setDimConfig(dimensionConfig.find(cnf => cnf.name === dimension));
  }, [dimension]);
  const buttonIcon = dimConfig
    ? dimensionIcons.get(dimConfig.iconType)
    : ImportExportIcon;

  const sliders = sliderDataArray.map(data => {
    return (
      <Grid key={data.layerId} item xs="auto">
        <IconButton
          data-testid="syncButton"
          size="small"
          onClick={(): void => handleSyncChanged(data.layerId, dimension)}
        >
          {data.synced && (
            <Tooltip
              title={`Click to disconnect ${data.layerId}`}
              placement="top"
            >
              <LinkIcon data-testid="syncIcon" />
            </Tooltip>
          )}
          {!data.synced && (
            <Tooltip title={`Click to connect ${data.layerId}`} placement="top">
              <LinkOffIcon data-testid="syncDisIcon" />
            </Tooltip>
          )}
        </IconButton>
        <div className={classes.verticalLevelDimensionSelectContainer}>
          <VerticalDimensionSelect
            marks={data.marks}
            reverse={dimConfig ? dimConfig.reversed : false}
            managedValue={Number(data.currentValue)}
            onChangeDimensionValue={(value): void => {
              handleDimensionValueChanged(
                data.layerId,
                dimension,
                value,
                data.synced,
              );
            }}
            isDisabled={false}
            validSelection={data.validSelection}
          />
        </div>
        <Tooltip title={data.layerName}>
          <Typography variant="body2" className={classes.layerName}>
            {data.layerName}
          </Typography>
        </Tooltip>
      </Grid>
    );
  });

  return (
    <DraggableDialog
      name={dimConfig ? dimConfig.label : dimension}
      showTitle
      mainIcon={buttonIcon}
      showAsOpen={isDialogOpen}
    >
      <Grid container spacing={1} direction="row">
        {sliders}
      </Grid>
    </DraggableDialog>
  );
};

export default VerticalSelectDialog;
