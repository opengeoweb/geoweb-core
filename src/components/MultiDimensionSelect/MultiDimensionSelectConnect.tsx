/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import { makeStyles, Grid } from '@material-ui/core';
import produce from 'immer';
import * as mapActions from '../../store/mapStore/actions';

import * as mapSelectors from '../../store/mapStore/map/selectors';
import { AppStore } from '../../types/types';
import VerticalSelectDialog from './VerticalSelectDialog';
import { DimensionSliderData } from './types';
import { getWMLayerById } from '../../store/mapStore/utils/helpers';
import { Dimension, Layer } from '../../store/mapStore/types';
import { marksByDimension } from '../../utils/DimensionUtils';

const useStyles = makeStyles(() => ({
  verticalLevelControlContainer: {
    position: 'absolute',
    zIndex: 501,
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
    pointerEvents: 'none',
    height: '700px',
  },
  verticalLevelControl: {
    alignSelf: 'top',
    marginTop: '105px',
    pointerEvents: 'all',
  },
  verticalLevelDimensionSelectContainer: {
    height: '320px',
    margin: '10px',
    marginLeft: '-5px',
  },
  layerName: {
    fontWeight: 200,
    maxWidth: 130,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
}));

interface ConnectProps {
  mapId: string;
}

interface Props {
  layers: Layer[];
  dimensions: Dimension[];
  showDialogs?: boolean;
  onLayerChangeDimension: typeof mapActions.layerChangeDimension;
}

const connectRedux = connect(
  (store: AppStore, props: ConnectProps) => ({
    dimensions: mapSelectors.getMapDimensions(store, props.mapId),
    layers: mapSelectors.getMapLayers(store, props.mapId),
  }),
  {
    onLayerChangeDimension: mapActions.layerChangeDimension,
  },
);

const MultiDimensionSelectConnectComponent: React.FC<Props> = ({
  dimensions,
  showDialogs = false,
  layers,
  onLayerChangeDimension,
}: Props) => {
  const [sliderData, setSliderData] = React.useState<DimensionSliderData[]>();
  const classes = useStyles();

  React.useEffect(() => {
    if (!dimensions || !layers) return;
    // TODO improve filtering of time dimmensions
    const availableDimensions = dimensions.filter(
      dimension => !dimension.name.includes('time'),
    );
    if (!availableDimensions || !availableDimensions.length) return;

    const data: DimensionSliderData[] = [];
    availableDimensions.forEach(currentDimension => {
      if (currentDimension.name) {
        layers.forEach(layer => {
          const wmLayer = getWMLayerById(layer.id);
          const wmsDimension = wmLayer.getDimension(currentDimension.name);
          if (wmsDimension) {
            const marks = marksByDimension(wmsDimension);
            if (marks && marks.length) {
              const prevValue = sliderData
                ? sliderData.find(
                    d =>
                      d.dimensionName === currentDimension.name &&
                      d.layerId === layer.id,
                  )
                : null;
              const prevCurValue = prevValue && prevValue.currentValue;
              const newCurValue = wmsDimension.currentValue
                ? wmsDimension.currentValue
                : wmsDimension.defaultValue;
              const cValue = prevCurValue || newCurValue;
              const isSynced = prevValue
                ? prevValue.synced
                : wmsDimension.synced;
              const isValid = prevValue ? prevValue.validSelection : true;
              data.push({
                dimensionName: currentDimension.name,
                unit: currentDimension.units,
                layerId: layer.id,
                layerName: wmLayer.getLayerName(),
                marks,
                synced: isSynced,
                validSelection: isValid,
                currentValue: cValue,
              });
            }
          }
        });
      }
    });
    setSliderData(data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [layers, dimensions]);

  const handleSyncChanged = (layerId: string, dimensionName: string): void => {
    const nextState = produce(sliderData, draftState => {
      const index = draftState.findIndex(
        d => d.layerId === layerId && d.dimensionName === dimensionName,
      );
      if (index >= 0) {
        // eslint-disable-next-line no-param-reassign
        draftState[index].synced = !draftState[index].synced;
        if (!draftState[index].synced) {
          // eslint-disable-next-line no-param-reassign
          draftState[index].validSelection = true;
        }
        onLayerChangeDimension({
          origin: 'dimension_select',
          layerId: draftState[index].layerId,
          dimension: {
            name: draftState[index].dimensionName,
            currentValue: draftState[index].currentValue,
            synced: draftState[index].synced,
          },
        });
      }
    });
    setSliderData(nextState);
  };

  const handleDimensionValueChanged = (
    layerId: string,
    dimensionName: string,
    newValue: string,
    changeSynched: boolean,
  ): void => {
    const nextState = produce(sliderData, draftState => {
      const newValueIndex: number[] = [];
      const invalidValueIndex: number[] = [];
      draftState.forEach(d => {
        const currentSlider =
          d.layerId === layerId && d.dimensionName === dimensionName;
        const slaveSlider = d.dimensionName === dimensionName && d.synced;
        const affected = currentSlider || (changeSynched && slaveSlider);
        if (affected && d.marks.some(mark => mark.value === Number(newValue))) {
          newValueIndex.push(draftState.indexOf(d));
          onLayerChangeDimension({
            origin: 'dimension_select',
            layerId: d.layerId,
            dimension: {
              name: dimensionName,
              currentValue: newValue,
              synced: changeSynched,
            },
          });
        } else if (affected) {
          // affected slider does not have mark for new value
          invalidValueIndex.push(draftState.indexOf(d));
        }
      });
      newValueIndex.forEach(i => {
        // eslint-disable-next-line no-param-reassign
        draftState[i].currentValue = newValue;
        // eslint-disable-next-line no-param-reassign
        draftState[i].validSelection = true;
      });
      invalidValueIndex.forEach(i => {
        // eslint-disable-next-line no-param-reassign
        draftState[i].validSelection = false;
      });
    });
    setSliderData(nextState);
  };

  // dimensions that has slider data
  const sliderDimensions = sliderData
    ? dimensions.filter(d => sliderData.some(x => x.dimensionName === d.name))
    : [];

  return (
    <div className={classes.verticalLevelControlContainer}>
      <div className={classes.verticalLevelControl}>
        <Grid
          container
          direction="column"
          justify="flex-start"
          alignItems="flex-end"
        >
          {sliderData &&
            sliderDimensions.map(dim => (
              <Grid key={`grid-${dim.name}`} item>
                <VerticalSelectDialog
                  key={`v-select-${dim.name}`}
                  dimension={dim.name}
                  sliderDataArray={sliderData.filter(
                    d => d.dimensionName === dim.name,
                  )}
                  isDialogOpen={showDialogs}
                  handleSyncChanged={handleSyncChanged}
                  handleDimensionValueChanged={handleDimensionValueChanged}
                />
              </Grid>
            ))}
        </Grid>
      </div>
    </div>
  );
};

/**
 * Component used to control values for all dimension other than time and ref.time
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <MultiDimensionSelectConnect mapId="mapid_1"  /> ```
 */
const MultiDimensionSelectConnect = connectRedux(
  MultiDimensionSelectConnectComponent,
);

export default MultiDimensionSelectConnect;
