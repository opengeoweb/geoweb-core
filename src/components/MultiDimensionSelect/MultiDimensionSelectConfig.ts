/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  ImportExport,
  SettingsEthernet,
  NetworkCheck,
  SignalCellularAlt,
} from '@material-ui/icons';
import { DimensionConfig } from './types';

export const dimensionConfig: DimensionConfig[] = [
  {
    name: 'elevation',
    label: 'Vertical level',
    defaultUnit: 'hPa',
    reversed: true,
    iconType: 'ImportExport',
  },
  {
    name: 'ensemble_member',
    label: 'Ensemble member',
    defaultUnit: 'member',
    reversed: false,
    iconType: 'SettingsEthernet',
  },
  {
    name: 'probability',
    label: 'Probability',
    defaultUnit: '%',
    reversed: false,
    iconType: 'ImportExport',
  },
  {
    name: 'level',
    label: 'Level',
    defaultUnit: 'level',
    reversed: false,
    iconType: 'ImportExport',
  },
];

export const dimensionIcons = new Map<string, typeof ImportExport>([
  ['ImportExport', ImportExport],
  ['SettingsEthernet', SettingsEthernet],
  ['NetworkCheck', NetworkCheck],
  ['SignalCellularAlt', SignalCellularAlt],
]);
