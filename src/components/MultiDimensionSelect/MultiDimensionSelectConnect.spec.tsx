/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getAllByText, cleanup } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import MultiDimensionSelectConnect from './MultiDimensionSelectConnect';
import {
  flDimensionLayer,
  multiDimensionLayer,
  WmMultiDimensionLayer,
  WmMultiDimensionLayer3,
} from '../../utils/defaultTestSettings';
import { mockStateMapWithDimensions } from '../../utils/testUtils';
import { registerWMLayer } from '../../store/mapStore/utils/helpers';
import { dimensionConfig } from './MultiDimensionSelectConfig';

afterEach(cleanup);

describe('src/components/MultiDimensionSelect/MultiDimensionSelectConnect', () => {
  it('should render a slider for elevation when layers has elevation dimension', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    registerWMLayer(WmMultiDimensionLayer, 'multiDimensionLayerMock');
    const mockState = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const store = mockStore(mockState);

    const { container } = render(
      <Provider store={store}>
        <MultiDimensionSelectConnect mapId={mapId} showDialogs />
      </Provider>,
    );
    const dimCnf = dimensionConfig.find(cnf => cnf.name === 'elevation');
    expect(getAllByText(container, dimCnf.label).length).toBe(1);
  });

  it('should not render slider for elevation when layers has no elevation dimension', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    registerWMLayer(WmMultiDimensionLayer3, 'multiDimensionLayerMock');
    const mockState = mockStateMapWithDimensions(flDimensionLayer, mapId);
    const store = mockStore(mockState);

    const { container } = render(
      <Provider store={store}>
        <MultiDimensionSelectConnect mapId={mapId} showDialogs />
      </Provider>,
    );
    const dimCnf = dimensionConfig.find(cnf => cnf.name === 'elevation');
    expect(container.innerHTML.includes(dimCnf.label)).toBeFalsy();
  });
});
