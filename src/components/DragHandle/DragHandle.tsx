/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { SortableHandle } from 'react-sortable-hoc';
import { Tooltip } from '@material-ui/core';
import ReorderIcon from '@material-ui/icons/Reorder';

const DragHandle = SortableHandle(() => (
  <Tooltip title="Drag a layer up or down" placement="top">
    <ReorderIcon
      style={{
        margin: '3px',
        cursor: 'move',
      }}
      htmlColor="#444"
      data-testid="dragHandle"
    />
  </Tooltip>
));

export default DragHandle;
