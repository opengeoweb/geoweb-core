/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import VerticalDimensionSelectConnect from './VerticalDimensionSelectConnect';
import {
  multiDimensionLayer2,
  defaultReduxLayerRadarColor,
} from '../../utils/defaultTestSettings';
import {
  mockStateMapWithDimensions,
  mockStateMapWithLayer,
} from '../../utils/testUtils';

describe('src/components/VerticalDimensionSelect/VerticalDimensionSelectConnect', () => {
  it('should render an enabled slider if a layer with vertical dimension is present', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    const mockState = mockStateMapWithDimensions(multiDimensionLayer2, mapId);
    const store = mockStore(mockState);
    const props = {
      marks: [
        {
          value: 10,
          label: 'val0',
        },
        {
          value: 50,
          label: 'val1',
        },
        {
          value: 90,
          label: 'val2',
        },
      ],
      mapId,
    };
    const { container, getByTestId } = render(
      <Provider store={store}>
        <VerticalDimensionSelectConnect {...props} />
      </Provider>,
    );
    const slider = getByRole(container, 'slider');
    expect(slider).toBeTruthy();

    expect(
      getByTestId('verticalSlider').classList.contains('Mui-disabled'),
    ).toBeFalsy();
  });

  it('should render a disabled slider in a closed dialog if no layer with vertical dimension is present', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    const mockState = mockStateMapWithLayer(defaultReduxLayerRadarColor, mapId);
    const store = mockStore(mockState);
    const props = {
      marks: [
        {
          value: 10,
          label: 'val0',
        },
        {
          value: 50,
          label: 'val1',
        },
        {
          value: 90,
          label: 'val2',
        },
      ],
      mapId,
    };
    const { queryByTestId, getByTestId } = render(
      <Provider store={store}>
        <VerticalDimensionSelectConnect {...props} />
      </Provider>,
    );
    expect(queryByTestId('draggable-dialog')).toBeFalsy();

    fireEvent.click(getByTestId('showDraggable'));
    expect(getByTestId('draggable-dialog')).toBeTruthy();

    expect(
      getByTestId('verticalSlider').classList.contains('Mui-disabled'),
    ).toBeTruthy();
  });
});
