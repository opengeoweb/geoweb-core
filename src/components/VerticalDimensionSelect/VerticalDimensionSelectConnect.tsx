/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import { Mark } from '@material-ui/core';
import * as mapActions from '../../store/mapStore/actions';
import * as serviceSelectors from '../../store/mapStore/service/selectors';
import { Layer } from '../../store/mapStore/layers/types';
import { AppStore } from '../../types/types';
import VerticalSelectDialog from './VerticalSelectDialog';
import { defaultVerticalLevels } from '../../utils/verticalLevels';
import { Dimension } from '../../store/mapStore/types';

const dimName = 'elevation';

interface VerticalDimensionSelectConnectProps {
  layers: Layer[];
  marks: Mark[];
  reverse?: boolean;
  onLayerChangeDimension: typeof mapActions.layerChangeDimension;
  // eslint-disable-next-line react/no-unused-prop-types
  mapId?: string;
}

const connectRedux = connect(
  (store: AppStore, props: VerticalDimensionSelectConnectProps) => ({
    layers: serviceSelectors.getMapServiceLayersByDimension(
      store,
      props.mapId,
      dimName,
    ),
  }),
  {
    onLayerChangeDimension: mapActions.layerChangeDimension,
  },
);

const VerticalDimensionSelectConnectComponent: React.FC<VerticalDimensionSelectConnectProps> = ({
  layers,
  marks = defaultVerticalLevels,
  reverse = false,
  onLayerChangeDimension,
}: VerticalDimensionSelectConnectProps) => {
  const appropriateDimLayers =
    layers &&
    layers.length > 0 &&
    layers.find(layer => layer.enabled) !== undefined;

  const [open, setOpen] = React.useState(appropriateDimLayers);

  React.useEffect(() => {
    if (appropriateDimLayers) {
      setOpen(true);
    } else {
      setOpen(false);
    }
  }, [appropriateDimLayers]);

  return (
    <VerticalSelectDialog
      reverse={reverse}
      marks={marks}
      isDialogOpen={open}
      onChangeDimensionValue={(dimVal: string): void => {
        const dimension: Dimension = {
          name: dimName,
          currentValue: dimVal,
        };
        //  TODO (Belén Torrente Torrente, 19-05-20): use setMapDimensions in the future, when the option to set dimensions linked to the map is made available
        for (let i = 0; i < layers.length; i += 1) {
          onLayerChangeDimension({
            origin: 'verticalselect',
            layerId: layers[i].id,
            dimension,
          });
        }
      }}
    />
  );
};

/**
 * Connected component used to show a vertical slider in a dialog.
 * Allows you to modify a dimension for all layers on the specified map at once
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {array} marks **optional** marks: Mark[] - array with the desired levels, if not passed in, a standard set is used (1000 to 200 hPa)
 * @param {boolean} [reverse=false] **optional** reverse: boolean - reverse slider (larger values at the bottom), defaults to false
 * @example
 * ``` <VerticalDimensionSelectConnect mapId="mapid_1" marks=[{value: 10,label: 'val0',},{value: 50,label: 'val1'}] /> ```
 */
const VerticalDimensionSelectConnect = connectRedux(
  VerticalDimensionSelectConnectComponent,
);

export default VerticalDimensionSelectConnect;
