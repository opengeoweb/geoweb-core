/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, getByRole } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import {
  multiDimensionLayer,
  defaultReduxLayerRadarColor,
} from '../../utils/defaultTestSettings';
import VerticalSelectDialog from './VerticalSelectDialog';
import { mockStateMapWithLayer } from '../../utils/testUtils';

describe('src/components/VerticalDimensionSelect/VerticalSelectDialog', () => {
  it('should be open if isDialogOpen is true and you should be able to close/open it', () => {
    const mapId = 'mapid_1';
    const mockState = mockStateMapWithLayer(multiDimensionLayer, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    const props = {
      marks: [
        {
          value: 10,
          label: 'val0',
        },
        {
          value: 50,
          label: 'val1',
        },
        {
          value: 90,
          label: 'val2',
        },
      ],
      isDialogOpen: true,
      onChangeDimensionValue: jest.fn(),
    };

    const { container, getByTestId, queryByTestId } = render(
      <Provider store={store}>
        <VerticalSelectDialog {...props} />
      </Provider>,
    );

    // vertical select dialog should be opened
    expect(getByTestId('draggable-dialog')).toBeTruthy();

    // Slider should be enabled
    expect(
      getByTestId('verticalSlider').classList.contains('Mui-disabled'),
    ).toBeFalsy();

    // vertical select dialog should contain a slider
    const slider = getByRole(container, 'slider');
    expect(slider).toBeTruthy();

    // close the vertical select dialog
    fireEvent.click(getByTestId('hideDraggable'));
    expect(queryByTestId('draggable-dialog')).toBeFalsy();

    // open the vertical select dialog
    fireEvent.click(getByTestId('showDraggable'));
    expect(getByTestId('draggable-dialog')).toBeTruthy();
  });
  it('should be closed and disabled if isDialogOpen is false', () => {
    const mapId = 'mapid_1';
    const mockState = mockStateMapWithLayer(defaultReduxLayerRadarColor, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    const props = {
      marks: [
        {
          value: 10,
          label: 'val0',
        },
        {
          value: 50,
          label: 'val1',
        },
        {
          value: 90,
          label: 'val2',
        },
      ],
      isDialogOpen: false,
      onChangeDimensionValue: jest.fn(),
    };

    const { container, getByTestId, queryByTestId } = render(
      <Provider store={store}>
        <VerticalSelectDialog {...props} />
      </Provider>,
    );

    // vertical select dialog should be closed
    expect(queryByTestId('draggable-dialog')).toBeFalsy();

    // open the vertical select dialog
    fireEvent.click(getByTestId('showDraggable'));
    expect(getByTestId('draggable-dialog')).toBeTruthy();

    // slider should be disabled
    expect(
      getByTestId('verticalSlider').classList.contains('Mui-disabled'),
    ).toBeTruthy();

    // vertical select dialog should contain a slider
    const slider = getByRole(container, 'slider');
    expect(slider).toBeTruthy();
  });
});
