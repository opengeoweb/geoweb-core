/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Mark, makeStyles } from '@material-ui/core';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import VerticalDimensionSelect from './VerticalDimensionSelect';
import { defaultVerticalLevels } from '../../utils/verticalLevels';
import DraggableDialog from '../DraggableDialog/DraggableDialog';

const useStyles = makeStyles(() => ({
  verticalLevelControlContainer: {
    position: 'absolute',
    zIndex: 501,
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
    pointerEvents: 'none',
  },
  verticalLevelControl: {
    gridColumnStart: 1,
    gridRowStart: 1,
    justifySelf: 'end',
    alignSelf: 'top',
    marginTop: '105px',
    pointerEvents: 'all',
  },
  verticalLevelDimensionSelectContainer: {
    height: '320px',
    margin: '10px',
    marginLeft: '-5px',
  },
}));

interface VerticalSelectDialogProps {
  marks?: Mark[];
  reverse?: boolean;
  isDialogOpen: boolean;
  onChangeDimensionValue: (dimensionValue: string) => void;
}

const VerticalSelectDialog: React.FC<VerticalSelectDialogProps> = ({
  reverse,
  marks = defaultVerticalLevels,
  onChangeDimensionValue,
  isDialogOpen,
}: VerticalSelectDialogProps) => {
  const classes = useStyles();

  return (
    <div className={classes.verticalLevelControlContainer}>
      <div className={classes.verticalLevelControl}>
        <DraggableDialog
          name="Vertical level"
          showTitle
          mainIcon={ImportExportIcon}
          showAsOpen={isDialogOpen}
        >
          <div className={classes.verticalLevelDimensionSelectContainer}>
            <VerticalDimensionSelect
              marks={marks}
              reverse={reverse}
              onChangeDimensionValue={onChangeDimensionValue}
              isDisabled={!isDialogOpen}
            />
          </div>
        </DraggableDialog>
      </div>
    </div>
  );
};

export default VerticalSelectDialog;
