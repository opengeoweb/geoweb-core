/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box, Slider, Mark, withStyles } from '@material-ui/core';
import { useState } from 'react';

interface VerticalDimensionSelectProps {
  marks: Mark[];
  reverse?: boolean;
  isDisabled?: boolean;
  managedValue?: number;
  validSelection?: boolean;
  alertColor?: string;
  onChangeDimensionValue: (dimensionValue: string) => void;
}

const SmallFontSlider = withStyles({
  markLabel: {
    fontSize: '80%',
  },
})(Slider);

const VerticalDimensionSelect: React.FC<VerticalDimensionSelectProps> = ({
  marks,
  reverse = false,
  isDisabled = false,
  managedValue = null,
  validSelection = true,
  alertColor = 'red',
  onChangeDimensionValue,
}: VerticalDimensionSelectProps) => {
  const [currentIndex, setCurrentIndex] = useState(0);

  if (!marks || !marks.length) {
    return null;
  }
  let sortedMarks = [...marks];
  sortedMarks = sortedMarks.sort((a, b) => a.value - b.value);
  const adaptedMarks = reverse ? sortedMarks.reverse() : sortedMarks;

  const values = adaptedMarks.map(m => m.value);
  // using index as values in slider to get constant spacing between marks
  const indexLabelMarks = adaptedMarks.map((m, index) => {
    return { value: index, label: m.label };
  });

  const sliderValue =
    managedValue !== null
      ? values.findIndex(v => v === managedValue)
      : currentIndex;

  return (
    <Box component="span" m={1}>
      <SmallFontSlider
        data-testid="verticalSlider"
        orientation="vertical"
        aria-labelledby="vertical-slider"
        step={null}
        min={0}
        max={indexLabelMarks.length - 1}
        marks={indexLabelMarks}
        style={!validSelection ? { color: alertColor } : {}}
        disabled={isDisabled}
        value={sliderValue}
        onChange={(e, val: number): void => {
          onChangeDimensionValue(values[val].toString());
          if (!managedValue) setCurrentIndex(val);
        }}
      />
    </Box>
  );
};

export default VerticalDimensionSelect;
