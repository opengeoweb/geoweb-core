/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';

import VerticalDimensionSelect from './VerticalDimensionSelect';

const mockProps = {
  marks: [
    {
      value: 1000,
      label: '1000',
    },
    {
      value: 5000,
      label: '5000',
    },
    {
      value: 9000,
      label: '9000',
    },
  ],
  onChangeDimensionValue: jest.fn(),
};

describe('src/components/VerticalDimensionSelect/VerticalDimensionSelect', () => {
  it('should render a slider', () => {
    const { getByRole, getByTestId } = render(
      <VerticalDimensionSelect {...mockProps} />,
    );
    expect(getByRole('slider')).toBeTruthy();

    // slider should be enabled
    expect(
      getByTestId('verticalSlider').classList.contains('Mui-disabled'),
    ).toBeFalsy();
  });

  it('should render the provided marks in an ascending order', () => {
    const { container } = render(<VerticalDimensionSelect {...mockProps} />);
    const markers = container.getElementsByClassName('MuiSlider-markLabel');
    expect(markers[0].innerHTML).toBe('1000');
    expect(markers[2].innerHTML).toBe('9000');
  });

  it('should render the provided marks in a descending (reverse) order', () => {
    const { container } = render(
      <VerticalDimensionSelect {...mockProps} reverse />,
    );
    const markers = container.getElementsByClassName('MuiSlider-markLabel');
    expect(markers[0].innerHTML).toBe('9000');
    expect(markers[2].innerHTML).toBe('1000');
  });

  it('should set the slider to first value by default', () => {
    const { container } = render(<VerticalDimensionSelect {...mockProps} />);
    const activeLabel = container.getElementsByClassName(
      'MuiSlider-markLabelActive',
    );
    expect(activeLabel.length).toBe(1);
    expect(activeLabel[0].textContent === '1000').toBeTruthy();
  });

  it('should not show a slider when there are no marks provided', () => {
    const props = {
      ...mockProps,
      marks: [],
    };
    const { queryByRole } = render(<VerticalDimensionSelect {...props} />);
    expect(queryByRole('slider')).toBeFalsy();
  });

  it('should be disabled if isDisabled is true', () => {
    const props = {
      ...mockProps,
      isDisabled: true,
    };
    const { getByTestId } = render(<VerticalDimensionSelect {...props} />);

    expect(
      getByTestId('verticalSlider').classList.contains('Mui-disabled'),
    ).toBeTruthy();
  });
});
