/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { MenuItem } from '@material-ui/core';

import { Layer } from '../../store/mapStore/types';
import TooltipSelect from '../TooltipSelect/TooltipSelect';

interface OpacityObject {
  value: number;
  title: string;
}

const opacities: OpacityObject[] = [
  { value: 0.0, title: '0 %' },
  { value: 0.1, title: '10 %' },
  { value: 0.2, title: '20 %' },
  { value: 0.3, title: '30 %' },
  { value: 0.4, title: '40 %' },
  { value: 0.5, title: '50 %' },
  { value: 0.6, title: '60 %' },
  { value: 0.7, title: '70 %' },
  { value: 0.8, title: '80 %' },
  { value: 0.9, title: '90 %' },
  { value: 1.0, title: '100 %' },
];
opacities.reverse();

// TODO: (Sander de Snaijer, 2020-03-19) Try to make it functional
const getClosestOpacity = (opacity: number): OpacityObject => {
  let smallest = null;
  let foundOpacity = opacities[0];
  opacities.forEach(op => {
    const difference = Math.abs(opacity - op.value);
    if (smallest === null) {
      smallest = difference;
      foundOpacity = op;
    }
    if (difference < smallest) {
      smallest = difference;
      foundOpacity = op;
    }
  });
  return foundOpacity;
};

interface OpacitySelectProps {
  layer: Layer;
  onLayerChangeOpacity: (layerId: string, opacity: number) => void;
}

const OpacitySelect: React.FC<OpacitySelectProps> = ({
  layer,
  onLayerChangeOpacity,
}: OpacitySelectProps) => {
  const [opacity, setOpacity] = React.useState(
    getClosestOpacity(layer.opacity),
  );

  const selectOpacity = (event: React.ChangeEvent<{ value: number }>): void => {
    setOpacity(getClosestOpacity(event.target.value));
    onLayerChangeOpacity(layer.id, event.target.value);
  };
  return (
    <TooltipSelect
      tooltip={`Select opacity, currently it is set to ${opacity.title}`}
      inputProps={{
        SelectDisplayProps: {
          'data-testid': 'selectOpacity',
        },
      }}
      value={opacity.value}
      onChange={selectOpacity}
    >
      {opacities.map(mapOpacity => (
        <MenuItem key={mapOpacity.value} value={mapOpacity.value}>
          {mapOpacity.title}
        </MenuItem>
      ))}
    </TooltipSelect>
  );
};

export default OpacitySelect;
