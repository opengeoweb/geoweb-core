/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, findByRole } from '@testing-library/react';
import OpacitySelect from './OpacitySelect';

import { LayerType } from '../../store/mapStore/types';

describe('src/components/OpacitySelect/OpacitySelect', () => {
  const props = {
    layer: { id: '1', layerType: LayerType.mapLayer },
    onLayerChangeOpacity: jest.fn(),
  };

  const newOpacity = { value: 0.1, title: '10 %' };

  it('should update the shown opacity value when another opacity is selected', async () => {
    const { getByTestId, findByText } = render(<OpacitySelect {...props} />);
    const select = getByTestId('selectOpacity');

    fireEvent.mouseDown(select);

    const menuItem = await findByText(newOpacity.title);
    fireEvent.click(menuItem);

    expect(props.onLayerChangeOpacity).toHaveBeenCalledWith(
      props.layer.id,
      newOpacity.value,
    );
    expect(select.textContent).toEqual(newOpacity.title);
  });

  it('should update the tooltip opacity when another opacity is selected', async () => {
    const { container, getByTestId, findByText } = render(
      <OpacitySelect {...props} />,
    );
    const select = getByTestId('selectOpacity');

    fireEvent.mouseDown(select);

    const menuItem = await findByText(newOpacity.title);
    fireEvent.click(menuItem);

    fireEvent.mouseOver(getByTestId('selectOpacity'));
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toMatch(newOpacity.title);
  });
});
