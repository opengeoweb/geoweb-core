/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import puppeteer from 'puppeteer';
import initStoryshots from '@storybook/addon-storyshots';
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer';
import { request } from 'http';
import path from 'path';

const chromePath = process.env.CHROME_BIN;

const getBrowserUrl = (): Promise<string> =>
  new Promise((resolve, reject) => {
    const req = request(
      {
        method: 'GET',
        hostname: 'localhost',
        port: 9222,
        path: '/json/version',
      },
      res => {
        res.on('data', d => {
          resolve(JSON.parse(d).webSocketDebuggerUrl);
        });
      },
    );
    req.on('error', (error: Error) => reject(error));
    req.end();
  });

let browser: puppeteer.Browser;

const localTest = imageSnapshot({
  getCustomBrowser: async (): Promise<puppeteer.Browser> => {
    const url = await getBrowserUrl().catch((error: Error) => {
      throw error;
    });
    const webBrowser = await puppeteer.connect({
      browserWSEndpoint: url,
    });
    browser = webBrowser;
    return webBrowser;
  },
  storybookUrl: 'file:///usr/src/app/storybook-opengeoweb-core',
});

localTest.afterAll = async (): Promise<void> =>
  browser ? browser.disconnect() : null;

const pipelineTest = imageSnapshot({
  chromeExecutablePath: chromePath,
  storybookUrl: `file://${path.resolve(
    __dirname,
    '../storybook-opengeoweb-core',
  )}`,
});

initStoryshots({
  test: chromePath ? pipelineTest : localTest,
  storyNameRegex: /.?takeSnapshot.?/,
});
