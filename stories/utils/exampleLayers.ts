/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { generateLayerId } from '../../src/store/mapStore/utils/helpers';
import { LayerType } from '../../src/store/mapStore/types';

/* TODO: 2021-04-13, Harmonie WMS not externally available yet */
const harmonieWMSService =
  'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserver?dataset=HARM_N25&';

export const baseLayer = {
  name: 'arcGisSat',
  title: 'arcGisSat',
  type: 'twms',
  layerType: LayerType.baseLayer,
  enabled: true,
  id: generateLayerId(),
};

export const baseLayerGrey = {
  id: 'base-layer-1',
  name: 'WorldMap_Light_Grey_Canvas',
  type: 'twms',
  layerType: LayerType.baseLayer,
};

export const baseLayerOpenStreetMapNL = {
  id: 'base-layer-2',
  name: 'OpenStreetMap_NL',
  type: 'twms',
  layerType: LayerType.baseLayer,
  enabled: true,
};

export const baseLayerArcGisCanvas = {
  id: 'base-layer-3',
  name: 'arcGisCanvas',
  type: 'twms',
  layerType: LayerType.baseLayer,
  enabled: true,
};

export const baseLayerWorldMap = {
  id: 'base-layer-4',
  name: 'WorldMap',
  type: 'twms',
  layerType: LayerType.baseLayer,
  enabled: true,
};

export const overLayer = {
  service: 'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
  name: 'countryborders',
  format: 'image/png',
  layerType: LayerType.overLayer,
  enabled: true,
  id: 'over-layer-1',
};

export const radarLayer = {
  service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
  name: 'RAD_NL25_PCP_CM',
  format: 'image/png',
  enabled: true,
  style: 'knmiradar/nearest',
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const radarLayerWithError = {
  service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
  name: 'radarLayer',
  format: 'image/png',
  enabled: true,
  style: 'knmiradar/nearest',
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const msgCppLayer = {
  // doesn't work with https
  service: 'http://msgcpp-ogc-realtime.knmi.nl/msgrt.cgi?',
  name: 'lwe_precipitation_rate',
  format: 'image/png',
  enabled: true,
  style: 'precip-transparent/nearest',
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const harmoniePrecipitationLayer = {
  service: harmonieWMSService,
  name: 'precipitation_flux',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};
export const harmoniePressureLayer = {
  service: harmonieWMSService,
  name: 'air_pressure_at_sea_level',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};
export const obsTemperatureLayer = {
  service:
    'https://adaguc-services-geoweb.knmi.nl//adaguc-services//adagucserver?dataset=OBS&',
  name: '10M/ta',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const dwdWarningLayer = {
  service:
    'https://maps.dwd.de/geoserver/dwd/Warnungen_Gemeinden_vereinigt/ows?',
  name: 'Warnungen_Gemeinden_vereinigt',
  format: 'image/png',
  // style: 'warnungen_gemeinden_vereinigt_event_seamless_param',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const dwdRadarLayer = {
  service: 'https://maps.dwd.de/geoserver/dwd/WX-Produkt/ows?',
  name: 'WX-Produkt',
  format: 'image/png',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

// this needs authentication to work
export const dwdObservationsWetterLayer = {
  service: 'https://maps.dwd.de/geoserver/dwd/Wetter_Beobachtungen/ows?',
  name: 'Wetter_Beobachtungen',
  style: 'Wetter_Symbole',
  format: 'image/png',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
  // headers: [{ name: 'Authorization', value: 'Basic ...' }]
};

export const FMITemp = {
  service: 'https://openwms.fmi.fi/geoserver/wms?',
  name: 'temperature-forecast',
  format: 'image/png',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const MetNoTemp = {
  service: 'https://wms-e1.geoweb.met.no/wms?',
  name: 'thredds_meps_latest_wind',
  format: 'image/png',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const metNorwayWind1 = {
  service: 'https://wms-e1.geoweb.met.no/wms?',
  name: 'thredds_meps_latest_wind',
  format: 'image/png',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const metNorwayWind2 = {
  service: 'https://wms-e1.geoweb.met.no/wms?',
  name: 'thredds_aromearctic_extracted_t',
  format: 'image/png',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const metNorwayWind3 = {
  service: 'https://wms-e1.geoweb.met.no/wms?',
  name: 'thredds_nk800_temperature',
  format: 'image/png',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const metNorwayLatestT = {
  service: 'https://wms-e1.geoweb.met.no/wms?',
  name: 'thredds_meps_latest_t',
  format: 'image/png',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const metNorwaySalinaty = {
  service: 'https://wms-e1.geoweb.met.no/wms?',
  name: 'thredds_barents_2_5km_1h_salinity',
  format: 'image/png',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const dwdObservationsWetterLayerWithHeader = {
  service: 'https://maps.dwd.de/geoserver/dwd/Wetter_Beobachtungen/ows?',
  name: 'Wetter_Beobachtungen',
  style: 'Wetter_Symbole',
  format: 'image/png',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
  headers: [
    { name: 'authorization', value: 'Basic aW50cmFuZXQtdXNlcjpDQnMjMTEh' },
  ],
};
// this needs authentication to work
export const dwdObservationsWindLayer = {
  service: 'https://maps.dwd.de/geoserver/dwd/Wetter_Beobachtungen/ows?',
  name: 'Wetter_Beobachtungen',
  style: 'Wetter_Wind',
  format: 'image/png',
  enabled: true,
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
};

export const harmonieRelativeHumidityPl = {
  service: harmonieWMSService,
  name: 'relative_humidity__at_pl',
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
  enabled: true,
};

export const harmonieWindPl = {
  service: harmonieWMSService,
  name: 'wind__at_pl',
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
  enabled: true,
};

export const harmonieAirTemperature = {
  service: harmonieWMSService,
  name: 'air_temperature__at_2m',
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
  enabled: true,
};
export const harmoniePrecipitation = {
  service: harmonieWMSService,
  name: 'precipitation_flux',
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
  enabled: true,
};
export const harmoniePressure = {
  service: harmonieWMSService,
  name: 'air_pressure_at_sea_level',
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
  enabled: true,
};
export const obsTemperature = {
  service:
    'https://adaguc-services-geoweb.knmi.nl//adaguc-services//adagucserver?dataset=OBS&',
  name: '10M/ta',
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
  enabled: true,
};

export const ashLayer = {
  // doesn't work with https
  service:
    'http://eunadics-ecs-lb-production-771820040.eu-west-1.elb.amazonaws.com/adaguc-services//wms?DATASET=wp6_etna_exercise&',
  name: 'volcanic_ash_75percentile',
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
  enabled: true,
};

export const multiDimensionLayer = {
  service: 'https://geoservices.knmi.nl/cgi-bin/geoweb-demo.cgi?',
  name: 'netcdf_5dims',
  id: generateLayerId(),
  layerType: LayerType.mapLayer,
  enabled: true,
};
