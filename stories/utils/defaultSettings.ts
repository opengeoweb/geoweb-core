/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch } from 'react-redux';

import * as mapActions from '../../src/store/mapStore/actions';
import { overLayer, baseLayerGrey } from './exampleLayers';
import {
  Layer,
  Bbox,
  LayerActions,
  DeleteLayerPayload,
  SetLayerDimensionPayload,
  SetLayerEnabledPayload,
  SetLayerStylePayload,
  SetLayerNamePayload,
  SetLayerOpacityPayload,
} from '../../src/store/mapStore/types';

export const initialBbox = {
  srs: 'EPSG:3857',
  bbox: {
    left: -450651.2255879827,
    bottom: 6490531.093143953,
    right: 1428345.8183648037,
    top: 7438773.776232235,
  },
};

export const handleLayerActionsCallback = (
  payload:
    | DeleteLayerPayload
    | SetLayerDimensionPayload
    | SetLayerEnabledPayload
    | SetLayerStylePayload
    | SetLayerNamePayload
    | SetLayerOpacityPayload,
): LayerActions => {
  // eslint-disable-next-line no-console
  console.log('handleLayerActionsCallback', JSON.stringify(payload));
  return null;
};

interface UseDefaultMapSettingsProps {
  mapId?: string;
  layers?: Layer[];
  baseLayers?: Layer[];
  bbox?: Bbox;
  srs?: string;
}

export const useDefaultMapSettings = (
  props: UseDefaultMapSettingsProps = {},
): void => {
  const dispatch = useDispatch();
  const {
    mapId = 'test-map-1',
    layers = [],
    baseLayers = [baseLayerGrey, overLayer],
    bbox = initialBbox.bbox,
    srs = initialBbox.srs,
  } = props;
  React.useEffect(() => {
    // set layers
    dispatch(
      mapActions.setLayers({
        mapId,
        layers,
      }),
    );
    // baseLayers
    dispatch(
      mapActions.setBaseLayers({
        mapId,
        layers: baseLayers,
      }),
    );
    // bbox
    dispatch(
      mapActions.setBbox({
        bbox,
        srs,
        mapId,
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
