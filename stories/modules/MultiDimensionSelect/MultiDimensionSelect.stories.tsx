/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { ThemeProvider } from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';

import * as mapActions from '../../../src/store/mapStore/actions';
import { MapViewConnect } from '../../../src/components/MapView';
import { store } from '../../../src/store';
import MultiDimensionSelectConnect from '../../../src/components/MultiDimensionSelect/MultiDimensionSelectConnect';
import TimeSliderConnect from '../../../src/components/TimeSlider/TimeSliderConnect';
import {
  metNorwayLatestT,
  metNorwaySalinaty,
  baseLayerGrey,
  overLayer,
} from '../../utils/exampleLayers';
import { useDefaultMapSettings } from '../../utils/defaultSettings';

const connectRedux = connect(null, {
  addLayer: mapActions.addLayer,
  addBaseLayer: mapActions.addBaseLayer,
  setBbox: mapActions.setBbox,
  onLayerChangeDimension: mapActions.layerChangeDimension,
});

const DimensionExampleComponent: React.FC = connectRedux(() => {
  const layers = [
    { ...metNorwayLatestT, id: 'thredds_meps_latest_t' },
    { ...metNorwaySalinaty, id: 'thredds_barents_2_5km_1h_salinity' },
  ];

  const initialBbox = {
    srs: 'EPSG:3857',
    bbox: {
      left: -7264356.781958314,
      bottom: 5486720.808524769,
      right: 12998111.264068486,
      top: 13399817.799776679,
    },
  };

  useDefaultMapSettings({
    mapId: 'mapid_1',
    layers,
    baseLayers: [{ ...baseLayerGrey, id: 'baseGrey' }, overLayer],
    bbox: initialBbox.bbox,
    srs: initialBbox.srs,
  });

  return (
    <div style={{ height: '100vh' }}>
      <div style={{ height: '100vh' }}>
        <MultiDimensionSelectConnect mapId="mapid_1" />
        <MapViewConnect mapId="mapid_1" />
        <div
          style={{
            position: 'absolute',
            left: '0px',
            bottom: '0px',
            zIndex: 10,
            right: '0px',
          }}
        >
          <TimeSliderConnect mapId="mapid_1" />
        </div>
      </div>
    </div>
  );
});

export const ElevationDimensionDemo: React.FC = () => (
  <ThemeProvider theme={GWTheme}>
    <Provider store={store}>
      <DimensionExampleComponent />
    </Provider>
  </ThemeProvider>
);
