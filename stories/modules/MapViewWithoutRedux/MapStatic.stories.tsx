/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { MapView, MapViewLayer } from '../../../src/components/MapView';

import {
  generateMapId,
  generateLayerId,
} from '../../../src/store/mapStore/utils/helpers';

import { baseLayer, overLayer } from '../../utils/exampleLayers';
import { simplePointsGeojson } from '../../utils/geojsonExamples';

import MapDrawGeoJSON from './components/MapDrawGeoJSON';
import Synops from './components/Synops';
import FeatureInfo from './components/FeatureInfo';
import FeatureInfoHTML from './components/FeatureInfoHTML';

const srsAndBboxDefault = {
  bbox: {
    left: -2324980.5498391856,
    bottom: 5890854.775012179,
    right: 6393377.702660825,
    top: 11652109.058827976,
  },
  srs: 'EPSG:3857',
};

// --- example 1: Drawing and editing GeoJSON
export const Map1 = (): React.ReactElement => (
  <div style={{ height: '100vh' }}>
    <MapDrawGeoJSON />
  </div>
);

Map1.storyName = 'Drawing and editing GeoJSON';

// --- example 2: Display GeoJSON Points
export const Map2 = (): React.ReactElement => (
  <div style={{ height: '100vh' }}>
    <MapView
      mapId={generateMapId()}
      srs={srsAndBboxDefault.srs}
      bbox={srsAndBboxDefault.bbox}
    >
      <MapViewLayer {...baseLayer} />
      <MapViewLayer {...overLayer} />
      <MapViewLayer id={generateLayerId()} geojson={simplePointsGeojson} />
    </MapView>
  </div>
);

Map2.storyName = 'Display GeoJSON Points';

// --- example 3: Synops along buffered route via WFS CQL
export const Map3 = (): React.ReactElement => (
  <div style={{ height: '100vh' }}>
    <Synops />
  </div>
);

Map3.storyName = 'Synops along buffered route via WFS CQL';

// --- example 4: Custom GetFeatureInfo as JSON
export const Map4 = (): React.ReactElement => (
  <div style={{ height: '100vh' }}>
    <FeatureInfo />
  </div>
);

Map4.storyName = 'Custom GetFeatureInfo as JSON';

// --- example 5: Custom GetFeatureInfo as HTML
export const Map5 = (): React.ReactElement => (
  <div style={{ height: '100vh' }}>
    <FeatureInfoHTML />
  </div>
);

Map5.storyName = 'Custom GetFeatureInfo as HTML';
