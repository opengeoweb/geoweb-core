/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { DynamicModuleLoader } from 'redux-dynamic-modules';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { LayerType } from '../../../src/store/mapStore/types';
import { store } from '../../../src/store';

import { radarLayer } from '../../utils/exampleLayers';

import synchronizationGroupConfig from '../../../src/store/generic/config';

import { SimpleTimeSliderConnect } from './storysupportcomponents/SimpleTimeSliderConnect';
import { ConfigurableConnectedMap } from './storysupportcomponents/ConfigurableConnectedMap';
import { SyncGroupViewerConnect } from './storysupportcomponents/SyncGroupViewerConnect';

import {
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETTIME,
} from '../../../src/store/generic/synchronizationGroups/constants';

import * as synchronizationGroupActions from '../../../src/store/generic/synchronizationGroups/actions';

const useStyles = makeStyles(() => ({
  root: {
    padding: 0,
    margin: 0,
  },
  container: {
    display: 'grid',
    gridTemplateAreas: '"header"\n" content"\n',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '40px 1fr',
    gridGap: '0px',
    height: '100vh',
    background: 'black',
  },
  header: {
    gridArea: 'header',
    margin: '0',
    padding: '2px',
    background: 'white',
  },
  content: {
    gridArea: 'content',
    margin: '0',
    padding: 0,
    display: 'grid',
    gridTemplateAreas: '"windowA windowB""windowC windowD"',
    gridTemplateColumns: '1fr 1fr',
    gridTemplateRows: '1fr 1fr',
    gridGap: '0px',
    height: '100%',
    overflow: 'hidden',
    zIndex: 3,
  },
  windowA: {
    gridArea: 'windowA',
    background: 'lightgray',
    height: '100%',
    display: 'grid',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr 40px',
    gridGap: '0px',
    zIndex: 1,
    margin: '0',
  },
  windowB: {
    gridArea: 'windowB',
    height: '100%',
    display: 'grid',
    gridTemplateAreas: '"mapA""timeSlider"',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr 40px',
    gridGap: '0px',
    overflow: 'hidden',
    margin: '0',
  },
  windowC: {
    gridArea: 'windowC',
    height: '100%',
    display: 'grid',
    gridTemplateAreas: '"mapA""timeSlider"',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr 40px',
    gridGap: '0px',
    overflow: 'hidden',
    margin: '0',
  },
  windowD: {
    gridArea: 'windowD',
    background: 'lightgray',
    height: '100%',
    display: 'grid',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr 40px',
    gridGap: '0px',
    zIndex: 1,
    margin: '0',
  },
  singleMap: {
    gridColumn: 1,
    gridRowStart: 1,
    gridRowEnd: 3,
    background: 'green',

    height: '100%',
    display: 'grid',
    gridTemplateAreas: '"map"',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr',
    gridGap: '0px',
    overflow: 'hidden',
    zIndex: 1,
  },
  fourMaps: {
    gridColumn: 1,
    gridRowStart: 1,
    gridRowEnd: 3,
    background: 'green',

    height: '100%',
    display: 'grid',
    gridTemplateAreas: '"mapA mapB ""mapC mapD"',
    gridTemplateColumns: '1fr 1fr',
    gridTemplateRows: '1fr 1fr',
    gridGap: '0px',
    overflow: 'hidden',
    zIndex: 1,
  },
  timeSlider: {
    zIndex: 2,
    gridColumn: 1,
    gridRowStart: 2,
    gridRowEnd: 3,
  },
  mapTitle: {
    position: 'absolute',
    padding: '5px',
    zIndex: 1000,
  },
}));

export default {
  title: 'SyncGroups/Sync many',
};

interface InitSyncGroupsProps {
  syncGroupAddGroup: typeof synchronizationGroupActions.syncGroupAddGroup;
  syncGroupAddTarget: typeof synchronizationGroupActions.syncGroupAddTarget;
}
const InitSyncGroups: React.FC = connect(null, {
  syncGroupAddGroup: synchronizationGroupActions.syncGroupAddGroup,
  syncGroupAddTarget: synchronizationGroupActions.syncGroupAddTarget,
})(({ syncGroupAddGroup, syncGroupAddTarget }: InitSyncGroupsProps) => {
  React.useEffect(() => {
    syncGroupAddGroup({
      groupId: 'Time_A',
      title: 'Group 1 for time',
      type: SYNCGROUPS_TYPE_SETTIME,
    });
    syncGroupAddGroup({
      groupId: 'Area_A',
      title: 'Group 2 for area',
      type: SYNCGROUPS_TYPE_SETBBOX,
    });
    syncGroupAddGroup({
      groupId: 'Time_B',
      title: 'Group 3 for time',
      type: SYNCGROUPS_TYPE_SETTIME,
    });
    syncGroupAddGroup({
      groupId: 'Area_B',
      title: 'Group 4 for area',
      type: SYNCGROUPS_TYPE_SETBBOX,
    });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_A' });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_B' });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_C' });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_D' });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_E' });

    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_F' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_G' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_H' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_I' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_J' });

    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_F' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_G' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_H' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_I' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_J' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'timesliderC' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'timesliderD' });

    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'timesliderB' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_E' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'timesliderA' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_A' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_B' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_C' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_D' });
  }, [syncGroupAddGroup, syncGroupAddTarget]);
  return null;
});

// --- example 1
export const Example1 = (): React.ReactElement => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const bboxEurope = {
    left: -918150.9603356163,
    bottom: 4839330.715139853,
    right: 2745893.275372317,
    top: 9376670.468166774,
  };

  return (
    <Provider store={store}>
      <DynamicModuleLoader modules={[synchronizationGroupConfig]}>
        <InitSyncGroups />
        <Dialog
          maxWidth="xl"
          onClose={(): void => {
            setOpen(false);
          }}
          open={open}
        >
          <DialogTitle>Sync group manager</DialogTitle>
          <DialogContent style={{ width: '50vw' }}>
            <SyncGroupViewerConnect />
          </DialogContent>
          <DialogActions>
            <Button
              autoFocus
              onClick={(): void => {
                setOpen(false);
              }}
              color="primary"
            >
              Close
            </Button>
          </DialogActions>
        </Dialog>
        <div className={classes.container}>
          <div className={classes.header}>
            <Button
              variant="contained"
              color="primary"
              onClick={(): void => {
                setOpen(true);
              }}
            >
              Open SyncGroups
            </Button>
          </div>
          <div className={classes.content}>
            <div className={classes.windowA}>
              <div className={classes.fourMaps}>
                <ConfigurableConnectedMap
                  id="map_A"
                  layers={[
                    {
                      service:
                        'https://adaguc-server-knmi-geoservices.pub.knmi.cloud//adagucserver?dataset=OBS&&service=WMS&',
                      name: '10M/ta',
                      layerType: LayerType.mapLayer,
                    },
                  ]}
                />
                <ConfigurableConnectedMap
                  id="map_B"
                  layers={[
                    {
                      service:
                        'https://adaguc-server-knmi-geoservices.pub.knmi.cloud//adagucserver?dataset=OBS&&service=WMS&',
                      name: '10M/wind',
                      layerType: LayerType.mapLayer,
                    },
                  ]}
                />
                <ConfigurableConnectedMap
                  id="map_C"
                  layers={[
                    {
                      service:
                        'https://adaguc-server-knmi-geoservices.pub.knmi.cloud//adagucserver?dataset=OBS&&service=WMS&',
                      name: '10M/pg',
                      layerType: LayerType.mapLayer,
                    },
                  ]}
                />{' '}
                <ConfigurableConnectedMap
                  id="map_D"
                  layers={[
                    {
                      service:
                        'https://adaguc-server-knmi-geoservices.pub.knmi.cloud//adagucserver?dataset=OBS&&service=WMS&',
                      name: '10M/pp',
                      layerType: LayerType.mapLayer,
                    },
                  ]}
                />
              </div>
              <div className={classes.timeSlider}>
                <SimpleTimeSliderConnect id="timesliderA" />
              </div>
            </div>
            <div className={classes.windowB}>
              <div className={classes.singleMap}>
                <ConfigurableConnectedMap
                  id="map_E"
                  bbox={bboxEurope}
                  srs="EPSG:3857"
                  layers={[
                    radarLayer,
                    {
                      service:
                        'https://adaguc-server-knmi-geoservices.pub.knmi.cloud//adagucserver?dataset=OBS&&service=WMS&',
                      name: '10M/ta',
                      layerType: LayerType.mapLayer,
                    },
                  ]}
                />
              </div>
              <div className={classes.timeSlider}>
                <SimpleTimeSliderConnect id="timesliderB" />
              </div>
            </div>
            <div className={classes.windowC}>
              <div className={classes.singleMap}>
                <ConfigurableConnectedMap
                  layers={[
                    {
                      service: 'https://eumetview.eumetsat.int/geoserv/wms?',
                      name: 'meteosat:msg_naturalenhncd',
                      layerType: LayerType.mapLayer,
                    },
                  ]}
                  id="map_F"
                />
              </div>
              <div className={classes.timeSlider}>
                <SimpleTimeSliderConnect id="timesliderC" />
              </div>
            </div>
            <div className={classes.windowD}>
              <div className={classes.fourMaps}>
                <ConfigurableConnectedMap
                  id="map_G"
                  bbox={bboxEurope}
                  srs="EPSG:3857"
                  layers={[
                    {
                      service: 'https://eumetview.eumetsat.int/geoserv/wms?',
                      name: 'meteosat:msg_ash',
                      layerType: LayerType.mapLayer,
                    },
                  ]}
                />
                <ConfigurableConnectedMap
                  id="map_H"
                  bbox={bboxEurope}
                  srs="EPSG:3857"
                  layers={[
                    {
                      service: 'https://eumetview.eumetsat.int/geoserv/wms?',
                      name: 'meteosat:msg_cth',
                      layerType: LayerType.mapLayer,
                    },
                  ]}
                />
                <ConfigurableConnectedMap
                  id="map_I"
                  bbox={bboxEurope}
                  srs="EPSG:3857"
                  layers={[
                    {
                      service: 'https://eumetview.eumetsat.int/geoserv/wms?',
                      name: 'meteosat:msg_fog',
                      layerType: LayerType.mapLayer,
                    },
                  ]}
                />
                <ConfigurableConnectedMap
                  id="map_J"
                  bbox={bboxEurope}
                  srs="EPSG:3857"
                  layers={[
                    {
                      service: 'https://eumetview.eumetsat.int/geoserv/wms?',
                      name: 'msg_natural',
                      layerType: LayerType.mapLayer,
                    },
                  ]}
                />
              </div>
              <div className={classes.timeSlider}>
                <SimpleTimeSliderConnect id="timesliderD" />
              </div>
            </div>
          </div>
        </div>
      </DynamicModuleLoader>
    </Provider>
  );
};
