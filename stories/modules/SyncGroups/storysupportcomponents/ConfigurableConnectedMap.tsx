/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { Typography, makeStyles } from '@material-ui/core';
import React from 'react';

import {
  generateLayerId,
  generateMapId,
  MapViewConnect,
} from '../../../../src';
import { baseLayerGrey, overLayer } from '../../../../src/utils/testLayers';
import {
  initialBbox,
  useDefaultMapSettings,
} from '../../../utils/defaultSettings';
import { Bbox, Layer } from '../../../../src/store/mapStore/types';

const useStyles = makeStyles(() => ({
  draggableThing: {
    boxShadow: '4px 4px 15px 1px rgba(0,0,0,0.75)',
  },
  mapTitle: {
    position: 'absolute',
    padding: '5px',
    zIndex: 1000,
  },
  mapContainer: { background: 'red', zIndex: 10000, display: 'inline' },
}));

interface ConfigurableConnectedMapProps {
  id?: string;
  className?: string;
  layers: Layer[];
  bbox?: Bbox;
  srs?: string;
}

export const ConfigurableConnectedMap: React.FC<ConfigurableConnectedMapProps> = ({
  id,
  className,
  layers,
  bbox,
  srs,
}: ConfigurableConnectedMapProps) => {
  const myMapId = React.useRef(id || generateMapId()).current;
  useDefaultMapSettings({
    mapId: myMapId,
    bbox: bbox || initialBbox.bbox,
    srs: srs || initialBbox.srs,
    layers: layers.map(layer => {
      return { ...layer, id: generateLayerId() };
    }),
    baseLayers: [
      { ...baseLayerGrey, id: generateLayerId() },
      { ...overLayer, id: generateLayerId() },
    ],
  });
  const classes = useStyles();
  return (
    <div className={className || classes.mapContainer}>
      <Typography className={classes.mapTitle}>{myMapId}</Typography>
      <MapViewConnect
        controls={{}}
        displayTimeInMap={false}
        showScaleBar={false}
        mapId={myMapId}
      />
    </div>
  );
};
