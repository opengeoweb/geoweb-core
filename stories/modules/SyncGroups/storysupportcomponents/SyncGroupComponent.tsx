/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  Checkbox,
  CardActions,
  IconButton,
} from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import AddSyncGroupTargetIcon from '@material-ui/icons/AddCircleOutline';
import RemoveSyncGroupIcon from '@material-ui/icons/RemoveCircleOutline';
import React from 'react';
import {
  SynchronizationGroup,
  SynchronizationSources,
} from '../../../../src/store/generic/synchronizationGroups/types';
import * as synchronizationGroupActions from '../../../../src/store/generic/synchronizationGroups/actions';

interface SyncGroupComponentProps {
  groupId: string;
  group: SynchronizationGroup;
  sources: SynchronizationSources;
  syncGroupAddTarget: typeof synchronizationGroupActions.syncGroupAddTarget;
  syncGroupRemoveTarget: typeof synchronizationGroupActions.syncGroupRemoveTarget;
  syncGroupLinkTarget: typeof synchronizationGroupActions.syncGroupLinkTarget;
}

export const SyncGroupComponent: React.FC<SyncGroupComponentProps> = ({
  groupId,
  group,
  syncGroupAddTarget,
  syncGroupRemoveTarget,
  syncGroupLinkTarget,
  sources,
}: SyncGroupComponentProps) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClickListItem = (event: React.MouseEvent<HTMLElement>): void => {
    setAnchorEl(event.currentTarget);
  };

  const handleSyncGroupAddTargetClick = (
    event: React.MouseEvent<HTMLElement>,
    sourceId: string,
  ): void => {
    syncGroupAddTarget({ targetId: sourceId, groupId, linked: true });
    setAnchorEl(null);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };
  return (
    <>
      <Card key={groupId} variant="outlined">
        <CardHeader title={group.title} />
        <CardContent style={{ padding: '0px 20px' }}>
          <Typography
            variant="body2"
            color="textSecondary"
            component="div"
            style={{ padding: 0 }}
          >
            <ol style={{ margin: 0 }}>
              {group.targets.allIds.map(targetId => {
                return (
                  <li
                    key={`${groupId}_${targetId}`}
                    style={{
                      padding: 0,
                    }}
                  >
                    <Checkbox
                      style={{ padding: 0 }}
                      checked={group.targets.byId[targetId].linked}
                      onChange={(
                        event: React.ChangeEvent<HTMLInputElement>,
                      ): void => {
                        syncGroupLinkTarget({
                          linked: event.target.checked,
                          groupId,
                          targetId,
                        });
                      }}
                    />
                    <Typography
                      variant="body1"
                      color="textSecondary"
                      component="span"
                    >
                      {targetId}
                    </Typography>
                    <IconButton
                      style={{ marginLeft: 'auto', padding: 0, float: 'right' }}
                      aria-label="add to favorites"
                      onClick={(): void => {
                        syncGroupRemoveTarget({ groupId, targetId });
                      }}
                    >
                      <RemoveSyncGroupIcon />
                    </IconButton>
                  </li>
                );
              })}
            </ol>
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <IconButton
            style={{ marginLeft: 'auto' }}
            aria-label="add to favorites"
            onClick={handleClickListItem}
          >
            <AddSyncGroupTargetIcon />
          </IconButton>
        </CardActions>
      </Card>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem
          key="all"
          onClick={(event): void => {
            sources.allIds.forEach(sourceId => {
              /* Only allow to add synchronization of source matching one of the types of the synchronization group */
              if (!sources.byId[sourceId].types.includes(group.type)) return;

              handleSyncGroupAddTargetClick(event, sourceId);
            });
          }}
        >
          Add all sources to this group
        </MenuItem>
        {sources.allIds.map(sourceId => {
          /* Only allow to add synchronization of source matching one of the types of the synchronization group */
          if (!sources.byId[sourceId].types.includes(group.type)) return null;
          return (
            <MenuItem
              key={sourceId}
              onClick={(event): void =>
                handleSyncGroupAddTargetClick(event, sourceId)
              }
            >
              {sourceId}
            </MenuItem>
          );
        })}
      </Menu>
    </>
  );
};
