/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { connect } from 'react-redux';
import { AppStore } from '../../../../src/types/types';
import * as syncGroupSelectors from '../../../../src/store/generic/synchronizationGroups/selectors';
import * as synchronizationGroupActions from '../../../../src/store/generic/synchronizationGroups/actions';
import { SyncGroupViewer } from './SyncGroupViewer';

const syncGroupViewerConnect = connect(
  (state: AppStore) => ({
    syncState: syncGroupSelectors.getSynchronizationGroupState(state),
  }),
  {
    syncGroupAddTarget: synchronizationGroupActions.syncGroupAddTarget,
    syncGroupRemoveTarget: synchronizationGroupActions.syncGroupRemoveTarget,
    syncGroupLinkTarget: synchronizationGroupActions.syncGroupLinkTarget,
    syncGroupAddGroup: synchronizationGroupActions.syncGroupAddGroup,
  },
);

export const SyncGroupViewerConnect = syncGroupViewerConnect(SyncGroupViewer);
