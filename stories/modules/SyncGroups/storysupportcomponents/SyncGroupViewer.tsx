/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { Typography } from '@material-ui/core';

import { SynchronizationGroupState } from '../../../../src/store/generic/synchronizationGroups/types';

import * as synchronizationGroupActions from '../../../../src/store/generic/synchronizationGroups/actions';
import { SyncGroupComponent } from './SyncGroupComponent';

export interface SyncGroupViewerProps {
  syncState: SynchronizationGroupState;
  syncGroupAddTarget: typeof synchronizationGroupActions.syncGroupAddTarget;
  syncGroupRemoveTarget: typeof synchronizationGroupActions.syncGroupRemoveTarget;
  syncGroupLinkTarget: typeof synchronizationGroupActions.syncGroupLinkTarget;
}

export const SyncGroupViewer: React.FC<SyncGroupViewerProps> = ({
  syncState,
  syncGroupAddTarget,
  syncGroupRemoveTarget,
  syncGroupLinkTarget,
}: SyncGroupViewerProps) => {
  return (
    <div style={{ padding: '0 5px' }}>
      <Typography variant="body1" color="textSecondary" component="p">
        Synchronization groups:
      </Typography>
      {syncState.groups.allIds.map(id => {
        return (
          <SyncGroupComponent
            key={`SyncGroupComponent${id}`}
            syncGroupAddTarget={syncGroupAddTarget}
            syncGroupRemoveTarget={syncGroupRemoveTarget}
            syncGroupLinkTarget={syncGroupLinkTarget}
            groupId={id}
            group={syncState.groups.byId[id]}
            sources={syncState.sources}
          />
        );
      })}
    </div>
  );
};
