/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { DynamicModuleLoader } from 'redux-dynamic-modules';

import { Button } from '@material-ui/core';
import produce from 'immer';
import { makeStyles } from '@material-ui/core/styles';

import { store } from '../../../src/store';

import { radarLayer } from '../../utils/exampleLayers';

import GeowebLayerSelectConnect from '../../../src/components/GeoWebLayerSelect/GeoWebLayerSelectConnect';
import synchronizationGroupConfig from '../../../src/store/generic/config';

import { generateMapId } from '../../../src';
import {
  DraggableThings,
  DraggableThingProps,
} from './storysupportcomponents/DraggableThings';
import { ConfigurableConnectedMap } from './storysupportcomponents/ConfigurableConnectedMap';
import { SyncGroupViewerConnect } from './storysupportcomponents/SyncGroupViewerConnect';
import {
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETTIME,
} from '../../../src/store/generic/synchronizationGroups/constants';

import * as synchronizationGroupActions from '../../../src/store/generic/synchronizationGroups/actions';

const useStyles = makeStyles(() => ({
  container: {
    display: 'grid',
    gridTemplateAreas: '"nav content content"\n"footer footer footer"',
    gridTemplateColumns: '350px 1fr',
    gridTemplateRows: '1fr 200px',
    gridGap: '1px',
    height: '100vh',
    background: 'black',
  },
  nav: {
    gridArea: 'nav',
    margin: '0 0 0 1px',
    background: 'white',
    overflowY: 'scroll',
  },
  content: {
    gridArea: 'content',
    margin: '0 1px 0px 0px',
    display: 'grid',
    gridTemplateAreas: '"mapA mapB mapC"',
    gridTemplateColumns: '1fr 1fr 1fr',
    gridTemplateRows: 'auto',
    gridGap: '1px',
    height: '100%',
  },
  footer: {
    margin: '0 1px 1px 1px',
    gridArea: 'footer',
    background: '#FEFEFF',
  },
  mapA: {
    gridArea: 'mapA',
    background: 'lightgray',
    height: '100%',
  },
  mapB: {
    gridArea: 'mapB',
    background: 'gray',
  },
  mapC: {
    gridArea: 'mapC',
    background: 'darkgray',
  },
}));

export default {
  title: 'SyncGroups/Sync maps',
};

interface InitSyncGroupsProps {
  syncGroupAddGroup: typeof synchronizationGroupActions.syncGroupAddGroup;
  syncGroupAddTarget: typeof synchronizationGroupActions.syncGroupAddTarget;
}
const InitSyncGroups: React.FC = connect(null, {
  syncGroupAddGroup: synchronizationGroupActions.syncGroupAddGroup,
  syncGroupAddTarget: synchronizationGroupActions.syncGroupAddTarget,
})(({ syncGroupAddGroup, syncGroupAddTarget }: InitSyncGroupsProps) => {
  React.useEffect(() => {
    syncGroupAddGroup({
      groupId: 'Time_A',
      title: 'Group 1 for time',
      type: SYNCGROUPS_TYPE_SETTIME,
    });
    syncGroupAddGroup({
      groupId: 'Area_A',
      title: 'Group 2 for area',
      type: SYNCGROUPS_TYPE_SETBBOX,
    });
    syncGroupAddGroup({
      groupId: 'Time_B',
      title: 'Group 3 for time',
      type: SYNCGROUPS_TYPE_SETTIME,
    });
    syncGroupAddGroup({
      groupId: 'Area_B',
      title: 'Group 4 for area',
      type: SYNCGROUPS_TYPE_SETBBOX,
    });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_A' });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_B' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_C' });

    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_A' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_B' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_C' });
  }, [syncGroupAddGroup, syncGroupAddTarget]);
  return null;
});

let sliderCounterId = 0;
// --- example 1
export const Example1 = (): React.ReactElement => {
  const [openDialogs, setOpenDialogs] = React.useState<DraggableThingProps[]>(
    [],
  );

  const addFloatingMap = (): void => {
    setOpenDialogs(
      produce(openDialogs, draft => {
        draft.push({
          id: generateMapId(),
          type: 'map',
          width: 300,
          height: 300,
        });
      }),
    );
  };
  const addFloatingTimeSlider = (): void => {
    setOpenDialogs(
      produce(openDialogs, draft => {
        draft.push({
          id: `slider${(sliderCounterId += 1)}`,
          type: 'slider',
          width: 300,
          height: 100,
        });
      }),
    );
  };
  const handleCloseDialog = (mapId): void => {
    setOpenDialogs(
      produce(openDialogs, draft => {
        draft.splice(
          draft.findIndex(a => a.id === mapId),
          1,
        );
      }),
    );
  };

  const classes = useStyles();

  return (
    <Provider store={store}>
      <DynamicModuleLoader modules={[synchronizationGroupConfig]}>
        <InitSyncGroups />
        <DraggableThings
          openDialogs={openDialogs}
          handleClose={handleCloseDialog}
        />

        <div className={classes.container}>
          <div className={classes.nav}>
            <Button
              variant="contained"
              color="primary"
              onClick={(): void => {
                addFloatingMap();
              }}
            >
              Add map
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={(): void => {
                addFloatingTimeSlider();
              }}
            >
              add slider
            </Button>
            <SyncGroupViewerConnect />
          </div>
          <div className={classes.content}>
            <div className={classes.mapA}>
              <ConfigurableConnectedMap id="map_A" layers={[radarLayer]} />
            </div>
            <div className={classes.mapB}>
              <ConfigurableConnectedMap id="map_B" layers={[radarLayer]} />
            </div>
            <div className={classes.mapC}>
              <ConfigurableConnectedMap id="map_C" layers={[radarLayer]} />
            </div>
          </div>
          <div className={classes.footer}>
            <GeowebLayerSelectConnect mapId="map_A" />
          </div>
        </div>
      </DynamicModuleLoader>
    </Provider>
  );
};
