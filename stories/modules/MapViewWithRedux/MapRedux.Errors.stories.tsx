/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider, connect } from 'react-redux';
import {
  FormControlLabel,
  FormControl,
  FormHelperText,
  ListItem,
  Checkbox,
  List,
  Paper,
  Grid,
} from '@material-ui/core';
import { MapViewConnect } from '../../../src/components/MapView';
import { store } from '../../../src/store';
import * as mapSelectors from '../../../src/store/mapStore/selectors';
import * as mapActions from '../../../src/store/mapStore/actions';

import { useDefaultMapSettings } from '../../utils/defaultSettings';

import {
  radarLayerWithError,
  radarLayer,
  harmoniePressure,
} from '../../utils/exampleLayers';

import { LayerStatus } from '../../../src/store/mapStore/types';
import { AppStore } from '../../../src/types/types';

const connectLayerControls = connect(
  (state, props) => ({
    hasError:
      mapSelectors.getLayerStatus(state, props.layerId) === LayerStatus.error,
    name: mapSelectors.getLayerName(state, props.layerId),
    isEnabled: mapSelectors.getLayerEnabled(state, props.layerId),
  }),
  {
    layerChangeEnabled: mapActions.layerChangeEnabled,
  },
);

const LayerControl = connectLayerControls(
  ({ layerId, isEnabled, name, layerChangeEnabled, hasError }) => (
    <ListItem dense>
      <Grid container direction="column">
        <Grid item sm={12}>
          <FormControl error={hasError}>
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  inputProps={{ 'aria-label': 'secondary checkbox' }}
                  checked={isEnabled}
                  disabled={hasError}
                  onChange={(): void => {
                    layerChangeEnabled({ layerId, enabled: !isEnabled });
                  }}
                />
              }
              label={name}
            />
            {hasError ? (
              <FormHelperText>Error loading layer</FormHelperText>
            ) : null}
          </FormControl>
        </Grid>
      </Grid>
    </ListItem>
  ),
);

const connectRedux = connect((state: AppStore, props) => ({
  layerIds: mapSelectors.getLayerIds(state, props.mapId),
}));

const MapWithError = connectRedux(({ mapId, layerIds }) => {
  useDefaultMapSettings({
    mapId,
    layers: [harmoniePressure, radarLayerWithError, radarLayer],
  });

  return (
    <Paper>
      <List>
        {layerIds.map(layerId => (
          <LayerControl key={layerId} layerId={layerId} />
        ))}
      </List>
    </Paper>
  );
});

export const MapError = (): React.ReactElement => (
  <Provider store={store}>
    <div style={{ height: '100vh' }}>
      <MapViewConnect mapId="mapid_1" />
    </div>
    <div
      style={{
        position: 'absolute',
        left: '10px',
        top: '10px',
        zIndex: 99910000,
      }}
    >
      <MapWithError mapId="mapid_1" />
    </div>
  </Provider>
);
