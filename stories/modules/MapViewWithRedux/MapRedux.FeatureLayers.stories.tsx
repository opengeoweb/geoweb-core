/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Provider, useDispatch } from 'react-redux';

import {
  Button,
  Select,
  FormControl,
  InputLabel,
  MenuItem,
  Grid,
  Box,
} from '@material-ui/core';
import { MapViewConnect, MapViewLayer } from '../../../src/components/MapView';
import { store } from '../../../src/store';

import {
  radarLayer,
  dwdWarningLayer,
  harmoniePressure,
  harmoniePrecipitation,
} from '../../utils/exampleLayers';

import { useDefaultMapSettings } from '../../utils/defaultSettings';
import { ActionCard } from '../../utils/HelperComponents';
import { setLayers } from '../../../src/store/mapStore/actions';
import {
  useDrawPolyStoryStyles,
  useGeoJSON,
} from '../MapViewWithoutRedux/components/MapDrawGeoJSON';
import { simplePolygonGeoJSON } from '../../utils/geojsonExamples';

interface PresetsConnectProps {
  mapId: string;
}

const PresetsConnect: React.FC<PresetsConnectProps> = ({
  mapId,
}: PresetsConnectProps) => {
  const dispatch = useDispatch();
  const onSetLayers = ({ layers }): typeof setLayers =>
    dispatch(setLayers({ layers, mapId }));

  return (
    <ActionCard
      name="setLayers"
      exampleLayers={[
        {
          layers: [radarLayer],
          title: 'Radar',
        },
        {
          layers: [harmoniePrecipitation, harmoniePressure],
          title: 'Precip + Obs',
        },
        {
          layers: [radarLayer, dwdWarningLayer],
          title: 'Radar + DWD Warnings',
        },
      ]}
      description="sets new layers on a map while removing all current ones"
      onClickBtn={onSetLayers}
    />
  );
};

const Demo: React.FC = () => {
  const mapId = 'mapid_1';
  const classes = useDrawPolyStoryStyles();
  const {
    geojson,
    isInEditMode,
    currentFeatureNrToEdit,
    drawMode,
    changeDrawMode,
    editModes,
    setGeojson,
    setEditMode,
  } = useGeoJSON();

  useDefaultMapSettings({ mapId });

  return (
    <div className={classes.MapDrawGeoJSONContainer}>
      <div className={classes.MapDrawGeoJSONMapContainer}>
        <MapViewConnect mapId={mapId}>
          <MapViewLayer
            id="geojson-layer-simplepoly"
            geojson={simplePolygonGeoJSON}
            isInEditMode={false}
          />
          <MapViewLayer
            id="geojson-layer"
            geojson={geojson}
            isInEditMode={isInEditMode}
            drawMode={drawMode}
            updateGeojson={(updatedGeojson): void => {
              setGeojson(updatedGeojson);
            }}
            exitDrawModeCallback={(): void => {
              setEditMode(!isInEditMode);
            }}
            featureNrToEdit={currentFeatureNrToEdit}
          />
        </MapViewConnect>
      </div>
      <div className={classes.MapDrawGeoJSONControlsContainer}>
        <Box p={2}>
          <Grid spacing={2} container>
            <Grid item sm={12}>
              <FormControl variant="filled" style={{ minWidth: 120 }}>
                <InputLabel id="demo-feature-type">Feature type</InputLabel>
                <Select
                  labelId="demo-feature-type"
                  value={drawMode}
                  onChange={(
                    event: React.ChangeEvent<HTMLInputElement>,
                  ): void => {
                    changeDrawMode(event.target.value);
                  }}
                >
                  {editModes.map(mode => (
                    <MenuItem key={mode.key} value={mode.key}>
                      {mode.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>

            <Grid item sm={12}>
              <Button
                variant="contained"
                color="primary"
                onClick={(): void => {
                  setEditMode(!isInEditMode);
                  if (!isInEditMode) {
                    // reset
                    setGeojson({
                      type: 'FeatureCollection',
                      features: [
                        {
                          geometry: { type: 'LineString', coordinates: [] },
                          properties: {
                            stroke: '#66F',
                            'stroke-opacity': '1',
                            'stroke-width': 5,
                          },
                          type: 'Feature',
                        },
                      ],
                    });
                  }
                }}
              >
                {isInEditMode ? 'Finish edit' : 'Start edit'}
              </Button>
            </Grid>
            <Grid item sm={12}>
              <PresetsConnect mapId={mapId} />
            </Grid>
          </Grid>
        </Box>
      </div>
    </div>
  );
};

export const FeatureLayers = (): React.ReactElement => {
  return (
    <Provider store={store}>
      <Demo />
    </Provider>
  );
};
