/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider } from 'react-redux';
import { Paper, Card, CardContent, Button } from '@material-ui/core';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Visibility from '@material-ui/icons/Visibility';
import { MapViewConnect } from '../../../src/components/MapView';
import VerticalDimensionSelectConnect from '../../../src/components/VerticalDimensionSelect/VerticalDimensionSelectConnect';
import { LegendConnect } from '../../../src/components/Legend';

import { useDefaultMapSettings } from '../../utils/defaultSettings';

import { store } from '../../../src/store';
import { harmonieRelativeHumidityPl } from '../../utils/exampleLayers';

const ExampleComponent: React.FC = () => {
  const [controls, toggleControls] = React.useState({
    zoomControls: true,
  });

  const [showLegend, toggleLegend] = React.useState(true);
  const [showVerticalLevel, toggleVerticalLevel] = React.useState(true);
  const [displayTimeInMap, toggleTimeInMap] = React.useState(true);

  useDefaultMapSettings({
    mapId: 'mapid_1',
    layers: [harmonieRelativeHumidityPl],
  });

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ height: '100vh', width: '100%', position: 'relative' }}>
        <div
          style={{
            position: 'absolute',
            left: '10px',
            top: '10px',
            zIndex: 10000,
          }}
        >
          <Paper>
            <Card>
              <CardContent>
                <Button
                  startIcon={
                    controls.zoomControls ? <Visibility /> : <VisibilityOff />
                  }
                  onClick={(): void =>
                    toggleControls({ zoomControls: !controls.zoomControls })
                  }
                >
                  Toggle controls
                </Button>
                <br />
                <Button
                  startIcon={showLegend ? <Visibility /> : <VisibilityOff />}
                  onClick={(): void => toggleLegend(!showLegend)}
                >
                  Toggle legend
                </Button>
                <br />
                <Button
                  startIcon={
                    showVerticalLevel ? <Visibility /> : <VisibilityOff />
                  }
                  onClick={(): void => toggleVerticalLevel(!showVerticalLevel)}
                >
                  Toggle vertical level
                </Button>
                <br />
                <Button
                  startIcon={
                    displayTimeInMap ? <Visibility /> : <VisibilityOff />
                  }
                  onClick={(): void => toggleTimeInMap(!displayTimeInMap)}
                >
                  Toggle map time
                </Button>
              </CardContent>
            </Card>
          </Paper>
        </div>
        {showVerticalLevel && (
          <VerticalDimensionSelectConnect mapId="map_id1" reverse />
        )}
        {showLegend && <LegendConnect mapId="mapid_1" />}
        <MapViewConnect
          mapId="mapid_1"
          controls={controls}
          displayTimeInMap={displayTimeInMap}
        />
      </div>
    </div>
  );
};

export const MapToggleControls: React.FC = () => (
  <Provider store={store}>
    <ExampleComponent />
  </Provider>
);
