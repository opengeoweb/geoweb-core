/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider, connect } from 'react-redux';
import {
  Button,
  Paper,
  DialogContent,
  makeStyles,
  Grid,
} from '@material-ui/core';

import {
  MapView,
  MapViewConnect,
  MapViewLayer,
} from '../../../src/components/MapView';
import { store } from '../../../src/store';
import VerticalDimensionSelectConnect from '../../../src/components/VerticalDimensionSelect/VerticalDimensionSelectConnect';
import { LegendConnect } from '../../../src/components/Legend';
import {
  radarLayer,
  overLayer,
  baseLayerGrey,
  baseLayerArcGisCanvas,
} from '../../utils/exampleLayers';

import GeowebLayerSelectConnect from '../../../src/components/GeoWebLayerSelect/GeoWebLayerSelectConnect';
import * as mapActions from '../../../src/store/mapStore/actions';
import {
  generateLayerId,
  generateMapId,
} from '../../../src/store/mapStore/utils/helpers';

const initialBbox = {
  srs: 'EPSG:3857',
  bbox: {
    left: -450651.2255879827,
    bottom: 6490531.093143953,
    right: 1428345.8183648037,
    top: 7438773.776232235,
  },
};

const useStyles = makeStyles({
  dialogChildren: {
    display: 'flex',
    padding: 20,
    minHeight: 200,
    minWidth: 400,
    height: '100%',
  },
  dialog: {
    position: 'absolute',
    top: '5%',
    left: '10%',
    maxWidth: '90%',
    maxHeight: '95%',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
    zIndex: 650,
  },
});

const connectRedux = connect(null, {
  setLayers: mapActions.setLayers,
  setBaseLayers: mapActions.setBaseLayers,
  setBbox: mapActions.setBbox,
});

interface MapWithGeoWebLayerSelectProps {
  mapId: string;
  setLayers: typeof mapActions.setLayers;
  setBaseLayers: typeof mapActions.setBaseLayers;
  setBbox: typeof mapActions.setBbox;
}

const GeowebFakeModuleDemo: React.FC<MapWithGeoWebLayerSelectProps> = ({
  mapId,
  setLayers,
  setBaseLayers,
  setBbox,
}: MapWithGeoWebLayerSelectProps) => {
  const availableBaseLayers = [baseLayerGrey, baseLayerArcGisCanvas];
  React.useEffect(() => {
    setLayers({ layers: [radarLayer], mapId });
    setBaseLayers({
      mapId,
      layers: [baseLayerGrey, overLayer],
    });
    setBbox({
      bbox: initialBbox.bbox,
      srs: initialBbox.srs,
      mapId,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div style={{ height: '70vh' }}>
        <VerticalDimensionSelectConnect mapId={mapId} reverse />
        <LegendConnect mapId={mapId} />
        <MapViewConnect mapId={mapId} displayTimeInMap />
      </div>
      <div style={{ height: '30vh' }}>
        <GeowebLayerSelectConnect
          mapId={mapId}
          preloadedAvailableBaseLayers={availableBaseLayers}
        />
      </div>
    </>
  );
};

const ConnectedGeoweb = connectRedux(GeowebFakeModuleDemo);

interface ModuleProps {
  handleClose: () => void;
}
const FakeModuleDialog: React.FC<ModuleProps> = ({
  handleClose,
}: ModuleProps) => {
  const classes = useStyles();
  const mapId = React.useRef(`sigmet-${generateMapId()}`).current;

  return (
    <Paper className={classes.dialog} elevation={24}>
      <Button onClick={handleClose} color="secondary" size="medium">
        CLOSE
      </Button>
      <DialogContent className={classes.dialogChildren}>
        <Grid container>
          <MapView mapId={mapId} srs={initialBbox.srs} bbox={initialBbox.bbox}>
            <MapViewLayer {...baseLayerGrey} id={generateLayerId()} />
          </MapView>
        </Grid>
      </DialogContent>
    </Paper>
  );
};

export const MapModuleOverDefaultMap = (): React.ReactElement => {
  const [showSigmet, setShowSigmet] = React.useState(false);
  const mapId = React.useRef(`app-${generateMapId()}`).current;

  return (
    <React.StrictMode>
      <Provider store={store}>
        <ConnectedGeoweb mapId={mapId} />
        <div
          style={{
            position: 'absolute',
            left: '10px',
            top: '10px',
            zIndex: 10000,
          }}
        >
          <Button
            variant="contained"
            color="primary"
            onClick={(): void => setShowSigmet(true)}
          >
            Open Module
          </Button>
        </div>
        {showSigmet && (
          <FakeModuleDialog handleClose={(): void => setShowSigmet(false)} />
        )}
      </Provider>
    </React.StrictMode>
  );
};
