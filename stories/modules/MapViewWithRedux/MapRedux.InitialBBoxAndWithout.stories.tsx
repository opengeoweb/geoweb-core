/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { Paper, Card, Typography, CardContent } from '@material-ui/core';
import { MapViewConnect } from '../../../src/components/MapView';
import * as mapActions from '../../../src/store/mapStore/actions';
import {
  radarLayer,
  overLayer,
  baseLayerGrey,
} from '../../utils/exampleLayers';
import { initialBbox } from '../../utils/defaultSettings';

import { store } from '../../../src/store';

interface ExampleComponentProps {
  addLayer: typeof mapActions.addLayer;
  addBaseLayer: typeof mapActions.addBaseLayer;
  setBbox: typeof mapActions.setBbox;
}

const connectRedux = connect(null, {
  addLayer: mapActions.addLayer,
  addBaseLayer: mapActions.addBaseLayer,
  setBbox: mapActions.setBbox,
});

const ExampleComponent: React.FC = connectRedux(
  ({ addLayer, setBbox, addBaseLayer }: ExampleComponentProps) => {
    React.useEffect(() => {
      addLayer({ layer: radarLayer, layerId: 'layerid_1', mapId: 'mapid_1' });
      addLayer({
        layer: radarLayer,
        layerId: 'layerid_2',
        mapId: 'mapid_2',
      });
      setBbox({
        bbox: initialBbox.bbox,
        srs: initialBbox.srs,
        mapId: 'mapid_2',
      });
      addBaseLayer({
        mapId: 'mapid_1',
        layer: { ...overLayer, id: 'overLayer-011' },
        layerId: 'overLayer-011',
      });
      addBaseLayer({
        mapId: 'mapid_1',
        layer: { ...baseLayerGrey, id: 'base-011' },
        layerId: 'base-011',
      });
      addBaseLayer({
        mapId: 'mapid_2',
        layer: { ...overLayer, id: 'overLayer-012' },
        layerId: 'overLayer-012',
      });
      addBaseLayer({
        mapId: 'mapid_2',
        layer: { ...baseLayerGrey, id: 'base-012' },
        layerId: 'base-012',
      });

      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
      <div style={{ display: 'flex' }}>
        <div style={{ height: '100vh', width: '50%', position: 'relative' }}>
          <div
            style={{
              position: 'absolute',
              left: '10px',
              top: '10px',
              zIndex: 10000,
            }}
          >
            <Paper>
              <Card>
                <CardContent>
                  <Typography variant="h6">No initial BBOX and SRS</Typography>
                  <Typography variant="body1">
                    Fallen back to default settings
                  </Typography>
                </CardContent>
              </Card>
            </Paper>
          </div>
          <MapViewConnect mapId="mapid_1" />
        </div>
        <div style={{ width: '50%', position: 'relative' }}>
          <div
            style={{
              position: 'absolute',
              left: '10px',
              top: '10px',
              zIndex: 10000,
            }}
          >
            <Paper>
              <Card>
                <CardContent>
                  <Typography variant="h6">Initial SRS and BBox</Typography>
                  <Typography variant="body1">
                    Passed in SRS and BBox used
                  </Typography>
                </CardContent>
              </Card>
            </Paper>
          </div>
          <MapViewConnect mapId="mapid_2" />
        </div>
      </div>
    );
  },
);

export const MapWithAndWithoutInitialBBOX: React.FC = () => (
  <Provider store={store}>
    <ExampleComponent />
  </Provider>
);
