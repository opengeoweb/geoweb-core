/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { Slider, Paper, Box, Grid, Button } from '@material-ui/core';
import { store } from '../../../src/store';

import { MapViewConnect } from '../../../src/components/MapView';
import * as mapActions from '../../../src/store/mapStore/actions';

import { radarLayer } from '../../utils/exampleLayers';
import { useDefaultMapSettings } from '../../utils/defaultSettings';
import { AppStore } from '../../../src/types/types';
import { Layer } from '../../../src/store/mapStore/types';
import { mapSelectors } from '../../../src';

interface LayerChangeOpacityInputProps {
  mapId: string;
  layerChangeOpacity: typeof mapActions.layerChangeOpacity;
  layers: Layer[];
}

const connectRedux = connect(
  (state: AppStore, props: LayerChangeOpacityInputProps) => ({
    layers: mapSelectors.getMapLayers(state, props.mapId),
  }),
  {
    layerChangeOpacity: mapActions.layerChangeOpacity,
  },
);

/* Just a button inside a component to connect it to redux */

const LayerChangeOpacityInput: React.FC<LayerChangeOpacityInputProps> = ({
  mapId,
  layers,
  layerChangeOpacity,
}: LayerChangeOpacityInputProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [radarLayer],
  });

  if (layers.length === 0) return <div>No layers</div>;
  const layerOpacity = layers[0].opacity;

  return (
    <div
      style={{
        width: '200px',
      }}
    >
      <Box>
        <Paper>
          <Box p={2}>
            <Grid spacing={2} container direction="column">
              <Grid item sm={12}>
                <Slider
                  min={0}
                  max={1}
                  step={0.1}
                  value={layerOpacity}
                  onChange={(
                    _event: React.ChangeEvent<HTMLInputElement>,
                    value: number,
                  ): void => {
                    layerChangeOpacity({
                      layerId: radarLayer.id,
                      opacity: value,
                    });
                  }}
                />
              </Grid>
              <Grid item sm={12}>
                <span>
                  Current opacity:
                  {layers[0].opacity}
                </span>
              </Grid>
            </Grid>
          </Box>
        </Paper>
      </Box>
    </div>
  );
};

const ConnectedLayerChangeOpacityInput = connectRedux(LayerChangeOpacityInput);

// Enable button
const connectReduxEnable = connect(
  (state, props) => ({
    layers: mapSelectors.getMapLayers(state, props.mapId),
  }),
  {
    layerChangeEnabled: mapActions.layerChangeEnabled,
  },
);

const LayerEnableButton = connectReduxEnable(
  ({
    layers,

    layerChangeEnabled,
    mapId,
  }) => {
    useDefaultMapSettings({ mapId, layers: [radarLayer] });

    if (layers.length === 0) return <div>No layers</div>;
    const isLayerEnabled = layers[0].enabled;
    return (
      <div>
        <Button
          variant="contained"
          color="primary"
          onClick={(): void =>
            layerChangeEnabled({
              layerId: radarLayer.id,
              mapId,
              enabled: !isLayerEnabled,
            })
          }
        >
          {!isLayerEnabled ? 'Enable' : 'Disable'}
        </Button>
      </div>
    );
  },
);

export const LayerChangeEnableOpacityAction = (): React.ReactElement => (
  <Provider store={store}>
    <div style={{ height: '100vh' }}>
      <MapViewConnect mapId="mapid_1" />
    </div>
    <div
      style={{
        position: 'absolute',
        left: '10px',
        top: '10px',
        zIndex: 10000,
      }}
    >
      <LayerEnableButton mapId="mapid_1" />
    </div>
    <div
      style={{
        position: 'absolute',
        left: '10px',
        top: '60px',
        zIndex: 10000,
      }}
    >
      <ConnectedLayerChangeOpacityInput mapId="mapid_1" />
    </div>
  </Provider>
);
