/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */
import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';

import { MapViewConnect } from '../../../src/components/MapView';
import * as mapActions from '../../../src/store/mapStore/actions';

import { store } from '../../../src/store';
import {
  baseLayer,
  baseLayerGrey,
  multiDimensionLayer,
  baseLayerOpenStreetMapNL,
  baseLayerArcGisCanvas,
  baseLayerWorldMap,
  overLayer,
} from '../../utils/exampleLayers';

import { useDefaultMapSettings } from '../../utils/defaultSettings';

import GeowebLayerSelectConnect from '../../../src/components/GeoWebLayerSelect/GeoWebLayerSelectConnect';

const useLayerSelectStyles = makeStyles({
  heightComps: { height: '75vh' },
  layerSelectDiv: { height: '25vh' },
});

const connectRedux = connect(null, {
  mapChangeDimension: mapActions.mapChangeDimension,
});

interface LayerSelectConnectProps {
  mapId: string;
  mapChangeDimension?: typeof mapActions.mapChangeDimension;
}

interface LayerSelectDimensionsConnectProps {
  mapId: string;
  mapChangeDimension?: typeof mapActions.mapChangeDimension;
}
const LayerSelectDimensionsConnect: React.FC<LayerSelectDimensionsConnectProps> = connectRedux(
  ({ mapId, mapChangeDimension }: LayerSelectConnectProps) => {
    const classes = useLayerSelectStyles();

    useDefaultMapSettings({
      mapId,
      srs: 'EPSG:4326',
      bbox: {
        left: -180,
        bottom: -90,
        right: 180,
        top: 90,
      },
      layers: [
        {
          ...multiDimensionLayer,
          id: `multiDimensionLayer-${mapId}`,
        },
      ],
      baseLayers: [{ ...baseLayerGrey, id: `baseGrey-${mapId}` }, overLayer],
    });
    React.useEffect(() => {
      // set layers
      mapChangeDimension({
        origin: 'GeoWebLayerSelectConnectDimensions.stories.tsx',
        mapId,
        dimension: {
          name: 'flight level',
          currentValue: '125',
        },
      });
    });

    const preloadedAvailableBaseLayers = [
      { ...baseLayerGrey, id: `baseGrey-${mapId}` },
      baseLayer,
      baseLayerOpenStreetMapNL,
      baseLayerArcGisCanvas,
      baseLayerWorldMap,
    ];
    return (
      <div className={classes.heightComps}>
        <Provider store={store}>
          <div className={classes.heightComps}>
            <MapViewConnect mapId={mapId} />
          </div>
          <div className={classes.layerSelectDiv}>
            <GeowebLayerSelectConnect
              mapId={mapId}
              preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
            />
          </div>
        </Provider>
      </div>
    );
  },
);

export const DemoLayerSelectWithDimensions: React.FC = () => (
  <Provider store={store}>
    <LayerSelectDimensionsConnect mapId="map-DemoGeowebLayerSelectWithDimensions" />
  </Provider>
);
