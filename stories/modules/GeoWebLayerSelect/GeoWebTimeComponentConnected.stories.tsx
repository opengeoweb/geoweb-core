/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */
import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { MapViewConnect } from '../../../src/components/MapView';
import * as mapActions from '../../../src/store/mapStore/actions';

import { store } from '../../../src/store';
import { GeoWebTimeComponentConnect } from '../../../src/components/GeoWebLayerSelect';
import { radarLayer, msgCppLayer } from '../../utils/exampleLayers';

import { useDefaultMapSettings } from '../../utils/defaultSettings';

const useLayerSelectStyles = makeStyles({
  heightComps: { height: '75vh' },
  layerSelectDiv: { height: '25vh' },
});

const connectRedux = connect(null, {
  setLayers: mapActions.setLayers,
  setBaseLayers: mapActions.setBaseLayers,
  setBbox: mapActions.setBbox,
});

interface TimeComponentDemoConnectProps {
  mapId: string;
}

const TimeComponentDemoConnect: React.FC<TimeComponentDemoConnectProps> = connectRedux(
  ({ mapId }) => {
    const classes = useLayerSelectStyles();

    useDefaultMapSettings({ mapId, layers: [radarLayer, msgCppLayer] });

    return (
      <div className={classes.heightComps}>
        <Provider store={store}>
          <div className={classes.heightComps}>
            <MapViewConnect mapId={mapId} />
          </div>
          <div className={classes.layerSelectDiv}>
            <GeoWebTimeComponentConnect mapId={mapId} />
          </div>
        </Provider>
      </div>
    );
  },
);

export const DemoTimeComponent: React.FC = () => (
  <Provider store={store}>
    <TimeComponentDemoConnect mapId="map_id_1" />
  </Provider>
);
