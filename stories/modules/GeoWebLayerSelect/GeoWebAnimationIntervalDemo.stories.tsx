/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */
import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import {
  IconButton,
  FormControl,
  InputLabel,
  Input,
  Paper,
} from '@material-ui/core';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';
import { MapViewConnect } from '../../../src/components/MapView';
import * as mapActions from '../../../src/store/mapStore/actions';

import { store } from '../../../src/store';
import { GeoWebTimeComponentConnect } from '../../../src/components/GeoWebLayerSelect';
import {
  radarLayer,
  msgCppLayer,
  overLayer,
  baseLayerGrey,
} from '../../utils/exampleLayers';

import { useDefaultMapSettings } from '../../utils/defaultSettings';

import { generateTimeList } from '../../../src/store/mapStore/map/sagas';
import { mapSelectors } from '../../../src';

const useLayerSelectStyles = makeStyles({
  heightComps: { height: '75vh' },
  layerSelectDiv: { height: '25vh' },
});

const connectDem = connect(
  (_store, props) => ({
    isAnimating: mapSelectors.isAnimating(_store, props.mapId),
  }),
  {
    startAnimation: mapActions.mapStartAnimation,
    stopAnimation: mapActions.mapStopAnimation,
  },
);

const demoWithAnimationStyle = makeStyles({
  ui: { display: 'flex' },
});

const DemoWithAnimation = connectDem(
  ({ startAnimation, stopAnimation, mapId, isAnimating, children }) => {
    useDefaultMapSettings({
      mapId,
      layers: [
        { ...radarLayer, id: `radar-${mapId}` },
        { ...msgCppLayer, id: `msg-${mapId}` },
      ],
      baseLayers: [overLayer, baseLayerGrey],
    });

    const style = demoWithAnimationStyle();
    const [animationInterval, setAnimationInterval] = React.useState(300);
    const start = moment // Remove one 5 minute step at the front
      .utc()
      .subtract(6, 'h')
      .add(5, 'm');
    const end = moment.utc();
    const timeList = generateTimeList(start, end, animationInterval);

    const onChangeAnimationInterval = (evt): void => {
      const value = parseInt(evt.currentTarget.value, 10);
      if (!Number.isNaN(value)) {
        stopAnimation({ mapId });
        setAnimationInterval(parseInt(evt.currentTarget.value, 10));
      }
    };

    return (
      <>
        <div
          style={{
            position: 'absolute',
            left: '20px',
            top: '20px',
            zIndex: 10000,
          }}
        >
          <Paper className={style.ui}>
            <FormControl>
              <InputLabel htmlFor="animationInterval">
                Animation interval
              </InputLabel>
              <Input
                id="animationInterval"
                type="number"
                onChange={onChangeAnimationInterval}
                value={animationInterval}
              />
            </FormControl>
            {isAnimating ? (
              <IconButton
                onClick={(): void => {
                  stopAnimation({ mapId });
                }}
              >
                <StopIcon />
              </IconButton>
            ) : (
              <IconButton
                onClick={(): void => {
                  startAnimation({
                    mapId,
                    timeList,
                  });
                }}
              >
                <PlayArrowIcon />
              </IconButton>
            )}
          </Paper>
        </div>
        {children}
      </>
    );
  },
);

export const DemoTimeComponentAnimationInterval: React.FC = () => {
  const classes = useLayerSelectStyles();
  const mapId = 'map-DemoTimeComponentAnimationInterval';
  return (
    <Provider store={store}>
      <DemoWithAnimation mapId={mapId} />
      <div className={classes.heightComps}>
        <div className={classes.heightComps}>
          <MapViewConnect mapId={mapId} />
        </div>
        <div className={classes.layerSelectDiv}>
          <GeoWebTimeComponentConnect mapId={mapId} />
        </div>
      </div>
    </Provider>
  );
};
