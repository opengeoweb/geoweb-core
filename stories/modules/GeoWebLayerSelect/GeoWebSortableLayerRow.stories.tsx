/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */
import * as React from 'react';
import produce from 'immer';

import {
  DeleteLayerPayload,
  SetLayerEnabledPayload,
  SetLayerNamePayload,
  SetLayerOpacityPayload,
  SetLayerStylePayload,
} from '../../../src/store/mapStore/types';
import { GeoWebSortableLayers } from '../../../src/components/GeoWebLayerSelect';
import {
  defaultReduxServices,
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
} from '../../../src/utils/defaultTestSettings';

import { handleLayerActionsCallback } from '../../utils/defaultSettings';

const layerListPre = [
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
  produce(defaultReduxLayerRadarKNMI, draft => {
    draft.id = 'otherid';
  }),
];

const layerList = layerListPre.map(layer => {
  return { ...layer, opacity: 1 };
});

export const SortableLayerRows: React.FC = () => {
  const [layers, setLayers] = React.useState(layerList);

  const onLayerChangeOpacity = (payload: SetLayerOpacityPayload): void => {
    handleLayerActionsCallback(payload);
    const index = layers.findIndex(layer => layer.id === payload.layerId);
    const newLayers = produce(layers, draft => {
      draft[index].opacity = payload.opacity;
    });
    setLayers(newLayers);
  };

  const onLayerChangeName = (payload: SetLayerNamePayload): void => {
    handleLayerActionsCallback(payload);
    const index = layers.findIndex(layer => layer.id === payload.layerId);
    const newLayers = produce(layers, draft => {
      draft[index].name = payload.name;
    });
    setLayers(newLayers);
  };

  const onLayerChangeStyle = (payload: SetLayerStylePayload): void => {
    handleLayerActionsCallback(payload);
    const index = layers.findIndex(layer => layer.id === payload.layerId);
    const newLayers = produce(layers, draft => {
      draft[index].style = payload.style;
    });
    setLayers(newLayers);
  };

  const onLayerEnable = (payload: SetLayerEnabledPayload): void => {
    handleLayerActionsCallback(payload);
    const index = layers.findIndex(layer => layer.id === payload.layerId);
    const newLayers = produce(layers, draft => {
      draft[index].enabled = payload.enabled;
    });
    setLayers(newLayers);
  };

  const onLayerDelete = (payload: DeleteLayerPayload): void => {
    handleLayerActionsCallback(payload);
    const index = layers.findIndex(layer => layer.id === payload.layerId);
    const newLayers = produce(layers, draft => {
      draft.splice(index, 1);
    });
    setLayers(newLayers);
  };

  return (
    <div style={{ width: '100%', height: '600px' }}>
      <GeoWebSortableLayers
        mapId="mapid_1"
        activeLayerId=""
        layers={layers}
        services={defaultReduxServices}
        onSortEnd={({ oldIndex, newIndex }): void => {
          // eslint-disable-next-line no-console
          console.log(`Moving ${oldIndex} to ${newIndex}`);
        }}
        onLayerChangeOpacity={onLayerChangeOpacity}
        onLayerChangeName={onLayerChangeName}
        onLayerChangeStyle={onLayerChangeStyle}
        onLayerChangeDimension={handleLayerActionsCallback}
        onLayerDelete={onLayerDelete}
        onLayerEnable={onLayerEnable}
      />
    </div>
  );
};
