/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import moment from 'moment';

import { GeoWebTimeComponent } from '../../../src/components/GeoWebLayerSelect';
import { WMLayer } from '../../../src/geoweb-webmap/index';
import { registerWMLayer } from '../../../src/store/mapStore/utils/helpers';
import { LayerType } from '../../../src/store/mapStore/types';

export const TimeComponent: React.FC = () => {
  const [timeValue, setTimeValue] = React.useState(
    moment()
      .utc()
      .toISOString(),
  );

  const wmjsLayer = new WMLayer({
    id: 'radar',
    name: 'radar',
    service: 'test',
    layerType: LayerType.mapLayer,
    dimensions: [
      {
        name: 'time',
        values: '2020-03-25T00:00:00Z/2020-03-25T15:20:00Z/PT5M',
        currentValue: '2020-03-25T15:20:00Z',
      },
    ],
  });
  registerWMLayer(wmjsLayer, 'radar');

  return (
    <div style={{ width: '100%', height: '600px' }}>
      <GeoWebTimeComponent
        layers={[
          {
            id: 'radar',
            name: 'radar',
            layerType: LayerType.mapLayer,
          },
        ]}
        activeLayerId="radar"
        dimensions={[{ name: 'time', currentValue: timeValue }]}
        onSetNewDate={(newDate: string): void => {
          setTimeValue(newDate);
        }}
        onSetActiveLayerId={(newActiveLayerId: string): void => {
          // eslint-disable-next-line no-console
          console.log(
            `setActiveLayerId Setting layer ${newActiveLayerId} as active layer`,
          );
        }}
      />
    </div>
  );
};
