/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */
import * as React from 'react';
import moment from 'moment';
import produce from 'immer';

import {
  baseLayer,
  baseLayerGrey,
  baseLayerOpenStreetMapNL,
  baseLayerArcGisCanvas,
  baseLayerWorldMap,
} from '../../utils/exampleLayers';
import {
  defaultReduxServices,
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
} from '../../../src/utils/defaultTestSettings';

import { MoveLayer } from '../../../src/store/mapStore/types';
import { handleLayerActionsCallback } from '../../utils/defaultSettings';

import GeoWebLayerSelect from '../../../src/components/GeoWebLayerSelect/GeoWebLayerSelect';

const layerListPre = [
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
  produce(defaultReduxLayerRadarKNMI, draft => {
    draft.id = 'otherid';
  }),
];

const layerList = layerListPre.map(layer => {
  return { ...layer, opacity: 1 };
});

const availableBaseLayers = [
  baseLayerGrey,
  baseLayer,
  baseLayerOpenStreetMapNL,
  baseLayerArcGisCanvas,
  baseLayerWorldMap,
];

const action = (name: string) => (data = ''): void => {
  // eslint-disable-next-line no-console
  console.log(name, data);
};

export const LayerSelect: React.FC = () => {
  const [activeLayerId, setActiveLayerId] = React.useState('');
  const [baseLayers, setBaseLayers] = React.useState([baseLayerGrey]);

  const [isAnimating, setAnimating] = React.useState(false);

  const [layers, setLayers] = React.useState(layerList);

  const onLayerChangeOpacity = (payload): void => {
    handleLayerActionsCallback(payload);
    const index = layers.findIndex(layer => layer.id === payload.layerId);
    const newLayers = produce(layers, draft => {
      draft[index].opacity = payload.opacity;
    });
    setLayers(newLayers);
  };

  const onLayerChangeName = (payload): void => {
    handleLayerActionsCallback(payload);
    const index = layers.findIndex(layer => layer.id === payload.layerId);
    const newLayers = produce(layers, draft => {
      draft[index].name = payload.name;
    });
    setLayers(newLayers);
  };

  const onLayerChangeStyle = (payload): void => {
    handleLayerActionsCallback(payload);
    const index = layers.findIndex(layer => layer.id === payload.layerId);
    const newLayers = produce(layers, draft => {
      draft[index].style = payload.style;
    });
    setLayers(newLayers);
  };

  const onLayerEnable = (payload): void => {
    handleLayerActionsCallback(payload);
    const index = layers.findIndex(layer => layer.id === payload.layerId);
    const newLayers = produce(layers, draft => {
      draft[index].enabled = payload.enabled;
    });
    setLayers(newLayers);
  };

  const onLayerDelete = (payload): void => {
    handleLayerActionsCallback(payload);
    const index = layers.findIndex(layer => layer.id === payload.layerId);
    const newLayers = produce(layers, draft => {
      draft.splice(index, 1);
    });
    setLayers(newLayers);
  };

  const onStartAnimate = (): void => {
    setAnimating(true);
    action('onStartAnimation')();
  };

  const onStopAnimate = (): void => {
    setAnimating(false);
    action('onEndAnimation')();
  };

  const onGoToBegin = (): void => {
    action('onGoToBegin')();
  };

  const onGoToEnd = (): void => {
    action('onGoToEnd')();
  };

  const onGoToNext = (): void => {
    action('onGoToNext')();
  };

  const onGoToPrevious = (): void => {
    action('onGoToPrevious')();
  };

  const onChangeBaseLayer = (_baseLayer): void => {
    for (let i = 0; i < availableBaseLayers.length; i += 1) {
      if (availableBaseLayers[i].id === _baseLayer) {
        setBaseLayers([availableBaseLayers[i]]);
      }
    }
  };

  return (
    <div style={{ width: '100%', height: '600px' }}>
      <GeoWebLayerSelect
        mapId="mapid_1"
        layers={layers}
        activeLayerId={activeLayerId}
        baseLayers={baseLayers}
        services={defaultReduxServices}
        dimensions={[
          {
            name: 'time',
            currentValue: moment()
              .utc()
              .toISOString(),
          },
        ]}
        availableBaseLayers={availableBaseLayers}
        onLayerMove={(payload): MoveLayer => {
          action('onLayerMove')(JSON.stringify(payload));
          return null;
        }}
        onSetNewDate={(newDate: string): void => {
          action('onSetNewDate')(JSON.stringify(newDate));
          return null;
        }}
        onSetActiveLayerId={(_activeLayerId: string): void => {
          action('onSetActiveLayerId')(JSON.stringify(_activeLayerId));
          setActiveLayerId(_activeLayerId);
        }}
        onLayerChangeOpacity={onLayerChangeOpacity}
        onLayerChangeName={onLayerChangeName}
        onLayerChangeStyle={onLayerChangeStyle}
        onLayerChangeDimension={handleLayerActionsCallback}
        onLayerDelete={onLayerDelete}
        onLayerEnable={onLayerEnable}
        isAnimating={isAnimating}
        onStartAnimation={onStartAnimate}
        onStopAnimation={onStopAnimate}
        onChangeBaseLayers={onChangeBaseLayer}
        onClickService={(serviceURL, layerName): void => {
          action('onClickService')(JSON.stringify({ serviceURL, layerName }));
        }}
        onGoToBegin={onGoToBegin}
        onGoToEnd={onGoToEnd}
        onGoToNext={onGoToNext}
        onGoToPrevious={onGoToPrevious}
      />
    </div>
  );
};
