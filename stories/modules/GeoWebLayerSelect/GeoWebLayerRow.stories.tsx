/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */
import * as React from 'react';

import { GeoWebLayerRow } from '../../../src/components/GeoWebLayerSelect';
import {
  defaultReduxServices,
  defaultReduxLayerRadarKNMI,
} from '../../../src/utils/defaultTestSettings';

import { handleLayerActionsCallback } from '../../utils/defaultSettings';

export const LayerRow: React.FC = () => {
  const [layer, setLayer] = React.useState({
    ...defaultReduxLayerRadarKNMI,
    opacity: 1,
  });

  const onLayerChangeOpacity = (payload): void => {
    handleLayerActionsCallback(payload);
    setLayer({ ...layer, opacity: payload.opacity });
  };

  const onLayerChangeName = (payload): void => {
    handleLayerActionsCallback(payload);
    setLayer({ ...layer, name: payload.name });
  };

  const onLayerChangeStyle = (payload): void => {
    handleLayerActionsCallback(payload);
    setLayer({ ...layer, style: payload.style });
  };

  const onLayerEnable = (payload): void => {
    handleLayerActionsCallback(payload);
    setLayer({ ...layer, enabled: payload.enabled });
  };

  const onLayerDelete = (payload): void => {
    handleLayerActionsCallback(payload);
    // eslint-disable-next-line no-alert
    alert('Delete layer!');
  };

  return (
    <div style={{ width: '100%', height: '600px' }}>
      <GeoWebLayerRow
        mapId="mapid_1"
        layer={layer}
        services={defaultReduxServices}
        onLayerChangeOpacity={onLayerChangeOpacity}
        onLayerChangeName={onLayerChangeName}
        onLayerChangeStyle={onLayerChangeStyle}
        onLayerChangeDimension={handleLayerActionsCallback}
        onLayerDelete={onLayerDelete}
        onLayerEnable={onLayerEnable}
      />
    </div>
  );
};
