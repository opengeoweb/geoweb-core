/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { Typography, Paper, Grid } from '@material-ui/core';

import { MapViewConnect } from '../../../src/components/MapView';
import { store } from '../../../src/store';

import WMSLoaderConnect from '../../../src/components/WMSLoader/WMSLoaderConnect';

import { initialBbox } from '../../utils/defaultSettings';
import * as mapActions from '../../../src/store/mapStore/actions';
import * as exampleLayers from '../../utils/exampleLayers';
import { LayerType } from '../../../src/store/mapStore/types';

const services = [
  {
    name: 'Meteo Canada',
    url: 'https://geo.weather.gc.ca/geomet/?lang=en&',
    id: 'MeteoCanada',
  },
  {
    name: 'lwe_precipitation_rate',
    url: 'http://msgcpp-ogc-realtime.knmi.nl/msgrt.cgi?',
    id: 'MSGCPP',
  },
];
interface WMSLoadProps {
  setBbox: typeof mapActions.setBbox;
  setBaseLayers: typeof mapActions.setBaseLayers;
}

const connectRedux = connect(null, {
  setLayers: mapActions.setLayers,
  setBbox: mapActions.setBbox,
  setBaseLayers: mapActions.setBaseLayers,
});

// --- example 1
const WMSLoad: React.FC = connectRedux(
  ({ setBbox, setBaseLayers }: WMSLoadProps) => {
    React.useEffect(() => {
      setBbox({
        bbox: initialBbox.bbox,
        srs: initialBbox.srs,
        mapId: 'mapid_1',
      });
      setBaseLayers({
        mapId: 'mapid_1',
        layers: [exampleLayers.baseLayerGrey, exampleLayers.overLayer],
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
      <div>
        <Provider store={store}>
          <div style={{ height: '100vh' }}>
            <MapViewConnect mapId="mapid_1" />
          </div>
          <div
            style={{
              position: 'absolute',
              left: '50px',
              top: '20px',
              zIndex: 10000,
              right: '200px',
            }}
          >
            <Paper
              style={{
                padding: '5px',
                width: '400px',
              }}
            >
              <Grid container>
                <Grid item xs={6}>
                  <Typography>Load WMS Layer:</Typography>
                  <WMSLoaderConnect mapId="mapid_1" />
                </Grid>
                <Grid item xs={6}>
                  <Typography>Load WMS BaseLayer:</Typography>
                  <WMSLoaderConnect
                    mapId="mapid_1"
                    layerType={LayerType.baseLayer}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Typography>
                    Load WMS Layer with preconfigured services:
                  </Typography>
                  <WMSLoaderConnect
                    mapId="mapid_1"
                    preloadedServices={services}
                  />
                </Grid>
              </Grid>
            </Paper>
          </div>
        </Provider>
      </div>
    );
  },
);

export const DemoWMSLoad: React.FC = () => (
  <Provider store={store}>
    <WMSLoad />
  </Provider>
);
