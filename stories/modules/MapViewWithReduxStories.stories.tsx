/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { MapView } from '../../src';

export * from './MapViewWithRedux/MapRedux.MapModuleOverDefaultMap.stories';
export * from './MapViewWithRedux/MapRedux.DoubleMap.stories';
export * from './MapViewWithRedux/MapRedux.Errors.stories';
export * from './MapViewWithRedux/MapRedux.InitialBBoxAndWithout.stories';
export * from './MapViewWithRedux/MapRedux.LayerActions.stories';
export * from './MapViewWithRedux/MapRedux.LayerChangeEnabledOpacity.stories';
export * from './MapViewWithRedux/MapRedux.SetBaseLayers.stories';
export * from './MapViewWithRedux/MapRedux.ToggleControls.stories';
export * from './MapViewWithRedux/MapRedux.FeatureLayers.stories';

export default { title: 'modules/MapView (with Redux)', component: MapView };
