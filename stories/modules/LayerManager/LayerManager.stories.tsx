/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';

import { MapViewConnect } from '../../../src/components/MapView';
import { store } from '../../../src/store';
import { useDefaultMapSettings } from '../../utils/defaultSettings';
import VerticalDimensionSelectConnect from '../../../src/components/VerticalDimensionSelect/VerticalDimensionSelectConnect';
import { LegendConnect } from '../../../src/components/Legend';
import {
  radarLayer,
  overLayer,
  baseLayerGrey,
} from '../../utils/exampleLayers';
import TimeSliderConnect from '../../../src/components/TimeSlider/TimeSliderConnect';
import LayerManager from '../../../src/components/LayerManager/LayerManager';

export default {
  title: 'modules/LayerManager',
};

interface ExampleComponentProps {
  mapId: string;
}

const ExampleComponent: React.FC<ExampleComponentProps> = ({
  mapId,
}: ExampleComponentProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [{ ...radarLayer, id: `radar-${mapId}` }],
    baseLayers: [{ ...baseLayerGrey, id: `baseGrey-${mapId}` }, overLayer],
  });
  return (
    <div style={{ height: '100vh' }}>
      <VerticalDimensionSelectConnect mapId={mapId} reverse />
      <LegendConnect mapId={mapId} />
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 1000,
          width: '100%',
        }}
      >
        <LayerManager mapId={mapId} />
        <TimeSliderConnect mapId={mapId} />
      </div>
      <MapViewConnect mapId={mapId} />
    </div>
  );
};

export const LayerManagerDemo: React.FC = () => (
  <ThemeProvider theme={GWTheme}>
    <Provider store={store}>
      <ExampleComponent mapId="mapid_1" />
    </Provider>
  </ThemeProvider>
);
