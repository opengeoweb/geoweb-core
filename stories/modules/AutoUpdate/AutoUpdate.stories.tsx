/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { Button, ButtonGroup } from '@material-ui/core';

import { AppStore } from '../../../src/types/types';
import * as mapActions from '../../../src/store/mapStore/actions';
import { MapViewConnect } from '../../../src/components/MapView';
import * as mapSelectors from '../../../src/store/mapStore/selectors';
import { store } from '../../../src/store';
import { useDefaultMapSettings } from '../../utils/defaultSettings';
import {
  radarLayer,
  overLayer,
  baseLayerGrey,
  baseLayerOpenStreetMapNL,
  harmonieRelativeHumidityPl,
} from '../../utils/exampleLayers';
import GeowebLayerSelectConnect from '../../../src/components/GeoWebLayerSelect/GeoWebLayerSelectConnect';

export default {
  title: 'modules/AutoUpdate',
};

interface RadarPresetProps {
  setLayers: typeof mapActions.setLayers;
  toggleAutoUpdate: typeof mapActions.toggleAutoUpdate;
  setActiveLayerId: typeof mapActions.setActiveLayerId;
  mapId: string;
}

const enhance = connect(
  (state: AppStore, props: RadarPresetProps) => ({
    layers: mapSelectors.getMapLayers(state, props.mapId),
  }),
  {
    setLayers: mapActions.setLayers,
    toggleAutoUpdate: mapActions.toggleAutoUpdate,
    setActiveLayerId: mapActions.setActiveLayerId,
  },
);

const RadarPresetWithAutoUpdate: React.FC<RadarPresetProps> = ({
  setLayers,
  mapId,
  toggleAutoUpdate,
  setActiveLayerId,
}: RadarPresetProps) => {
  useDefaultMapSettings({
    mapId,
    baseLayers: [
      { ...baseLayerGrey, id: `baseGrey-${mapId}` },
      {
        ...overLayer,
        id: `overLayer-1-${mapId}`,
      },
    ],
  });

  return (
    <ButtonGroup
      variant="contained"
      color="primary"
      aria-label="outlined primary button group"
    >
      <Button
        onClick={(): void => {
          setLayers({
            layers: [radarLayer],
            mapId,
          });
          toggleAutoUpdate({ mapId, shouldAutoUpdate: true });
        }}
      >
        Radar with auto-update
      </Button>
      <Button
        onClick={(): void => {
          setLayers({
            layers: [radarLayer, harmonieRelativeHumidityPl],
            mapId,
          });
          setActiveLayerId({ mapId, layerId: harmonieRelativeHumidityPl.id });
          toggleAutoUpdate({ mapId, shouldAutoUpdate: true });
        }}
      >
        Multiple layers with auto-update
      </Button>
    </ButtonGroup>
  );
};
const ConnectedRadarPresetWithAutoUpdate = enhance(RadarPresetWithAutoUpdate);

export const SetLayersWithAutoUpdate = (): React.ReactElement => {
  const mapId = 'mapid_1';
  const preloadedAvailableBaseLayers = [
    { ...baseLayerGrey, id: `baseGrey-${mapId}` },
    baseLayerOpenStreetMapNL,
  ];

  return (
    <Provider store={store}>
      <div style={{ height: '70vh' }}>
        <MapViewConnect mapId={mapId} />
      </div>
      <div style={{ height: '30vh' }}>
        <GeowebLayerSelectConnect
          mapId={mapId}
          preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
        />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '10px',
          top: '10px',
          zIndex: 10000,
        }}
      >
        <ConnectedRadarPresetWithAutoUpdate mapId={mapId} />
      </div>
    </Provider>
  );
};
