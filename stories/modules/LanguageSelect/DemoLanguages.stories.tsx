/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { makeStyles, Tooltip } from '@material-ui/core';
import RestoreFromTrashIcon from '@material-ui/icons/RestoreFromTrash';

import { IntlProvider, FormattedMessage } from 'react-intl';
import translations from '../../../src/utils/languageTranslation';
import LanguageSelect from '../../../src/components/LanguageSelect/LanguageSelect';

import DeleteButton from '../../../src/components/DeleteLayer/DeleteButton';

const useDemoLanguagesStyles = makeStyles({
  deleteDiv: {
    position: 'absolute',
    left: '20px',
    top: '60px',
  },
  restoreDiv: {
    position: 'absolute',
    left: '40px',
    top: '3px',
  },
  deleteOKDiv: {
    position: 'absolute',
    left: '20px',
    top: '60px',
  },
  languageDiv: {
    position: 'absolute',
    left: '200px',
    top: '50px',
  },
});

export const DemoLanguages = (): React.ReactElement => {
  const classes = useDemoLanguagesStyles();
  const [isDeleted, setIsDeleted] = React.useState(false);
  const [lang, setLang] = React.useState('en');

  return (
    <div>
      <div className={classes.deleteDiv}>
        <IntlProvider locale={lang} messages={translations[lang]}>
          <Tooltip
            title={
              <FormattedMessage
                id="restore"
                defaultMessage="Press to restore"
              />
            }
          >
            <RestoreFromTrashIcon
              className={classes.restoreDiv}
              fontSize="small"
              onClick={(): void => {
                setIsDeleted(false);
              }}
            />
          </Tooltip>
          <DeleteButton
            tooltipTitle={
              <FormattedMessage id="delete" defaultMessage="Press to delete" />
            }
            onClickDelete={(): void => {
              setIsDeleted(true);
            }}
          />
          {isDeleted ? (
            <div className={classes.deleteOKDiv}>
              <FormattedMessage id="deleteOK" defaultMessage="DELETED" />
            </div>
          ) : (
            <div className={classes.deleteOKDiv} />
          )}
        </IntlProvider>
      </div>
      <div className={classes.languageDiv}>
        <LanguageSelect currentLang={lang} onChangeLanguage={setLang} />
      </div>
      <h4>
        This demo demonstrates that the delete button tool-tip language
        corresponds to the language choosen.
      </h4>
    </div>
  );
};
