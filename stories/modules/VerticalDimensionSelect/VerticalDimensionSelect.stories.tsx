/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider, connect } from 'react-redux';

import * as mapActions from '../../../src/store/mapStore/actions';
import { MapViewConnect } from '../../../src/components/MapView';
import { store } from '../../../src/store';
import VerticalDimensionSelectConnect from '../../../src/components/VerticalDimensionSelect/VerticalDimensionSelectConnect';

import {
  harmonieRelativeHumidityPl,
  harmonieWindPl,
  baseLayerGrey,
  overLayer,
} from '../../utils/exampleLayers';
import { useDefaultMapSettings } from '../../utils/defaultSettings';

export default {
  title: 'modules/VerticalDimensionSelect',
};

const connectRedux = connect(null, {
  addLayer: mapActions.addLayer,
  addBaseLayer: mapActions.addBaseLayer,
  setBbox: mapActions.setBbox,
  onLayerChangeDimension: mapActions.layerChangeDimension,
});

const ElevationsExampleComponent: React.FC = connectRedux(() => {
  useDefaultMapSettings({
    mapId: 'mapid_1',
    layers: [
      { ...harmonieWindPl, id: 'layerid_wind' },
      { ...harmonieRelativeHumidityPl, id: 'layerid_humidity' },
    ],
    baseLayers: [{ ...baseLayerGrey, id: 'baseGrey' }, overLayer],
  });

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ height: '100vh', width: '100%', position: 'relative' }}>
        <VerticalDimensionSelectConnect mapId="mapid_1" reverse />
        <MapViewConnect mapId="mapid_1" />
      </div>
    </div>
  );
});

export const SyncElevationDimensionDemo: React.FC = () => (
  <Provider store={store}>
    <ElevationsExampleComponent />
  </Provider>
);
