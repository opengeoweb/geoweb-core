/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { ThemeProvider } from '@material-ui/core';
import { Provider } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { GWTheme } from '@opengeoweb/theme';
import AutoUpdateButtonConnect from '../../../../src/components/TimeSlider/TimeSliderButtons/AutoUpdateButton/AutoUpdateButtonConnect';
import { store } from '../../../../src/store';
import { MapViewConnect } from '../../../../src/components/MapView';
import { useDefaultMapSettings } from '../../../utils/defaultSettings';
import { radarLayer } from '../../../utils/exampleLayers';

interface AutoUpdateButtonConnectProps {
  mapId: string;
}

const useLayerSelectStyles = makeStyles({
  heightComps: { height: '100vh' },
});

const AutoUpdateButtonConnectDemo: React.FC<AutoUpdateButtonConnectProps> = ({
  mapId,
}: AutoUpdateButtonConnectProps) => {
  const classes = useLayerSelectStyles();
  const layers = [{ ...radarLayer, id: `radar-${mapId}` }];
  useDefaultMapSettings({ mapId, layers });

  return (
    <div>
      <ThemeProvider theme={GWTheme}>
        <Provider store={store}>
          <div className={classes.heightComps}>
            <div
              style={{
                position: 'absolute',
                left: '0px',
                top: 'calc(100% - 150px)',
                zIndex: 1000,
                height: '100%',
                width: '100%',
              }}
            >
              <AutoUpdateButtonConnect mapId={mapId} />
            </div>
            <MapViewConnect mapId={mapId} />
          </div>
        </Provider>
      </ThemeProvider>
    </div>
  );
};

export const AutoUpdateButtonDemo: React.FC = () => (
  <Provider store={store}>
    <AutoUpdateButtonConnectDemo mapId="mapid_1" />
  </Provider>
);
