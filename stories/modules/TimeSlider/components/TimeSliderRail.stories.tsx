/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import moment from 'moment';
import { ThemeProvider } from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';

import TimeSliderRail from '../../../../src/components/TimeSlider/TimeSliderRail/TimeSliderRail';

export const TimeSliderRailDemo = (): React.ReactElement => {
  const t = moment.utc();
  const simpleProps = {
    intervalEndTime: moment.utc(t).endOf('day'),
    selectedTime: moment
      .utc(t)
      .startOf('day')
      .hours(12),
    timeStep: 5,
    centerTime: moment
      .utc(t)
      .startOf('day')
      .hours(12)
      .unix(),
    secondsPerPx: 120,
  };
  const day = moment.utc(t).startOf('day');
  const propsWithAnimationTimes = {
    ...simpleProps,
    animationStartTime: moment.utc(day).hours(10),
    animationEndTime: moment.utc(day).hours(14),
    currentTime: moment
      .utc(day)
      .hours(13)
      .unix(),
    dataStartTime: moment
      .utc(day)
      .hours(2)
      .unix(),
    dataEndTime: moment
      .utc(day)
      .hours(22)
      .unix(),
  };
  return (
    <ThemeProvider theme={GWTheme}>
      <div>
        <TimeSliderRail {...simpleProps} />
        <TimeSliderRail
          {...propsWithAnimationTimes}
          selectedTime={moment.utc(day).hours(6)}
        />
        <TimeSliderRail {...propsWithAnimationTimes} />
        <TimeSliderRail
          {...propsWithAnimationTimes}
          selectedTime={moment.utc(day).hours(18)}
        />
        <TimeSliderRail
          {...propsWithAnimationTimes}
          animationStartTime={moment.utc(day).hours(7)}
          animationEndTime={moment.utc(day).hours(11)}
        />
        <TimeSliderRail
          {...propsWithAnimationTimes}
          animationStartTime={moment.utc(day).hours(14)}
          animationEndTime={moment.utc(day).hours(18)}
        />
      </div>
    </ThemeProvider>
  );
};

TimeSliderRailDemo.storyName = 'Time Slider Rail (takeSnapshot)';
