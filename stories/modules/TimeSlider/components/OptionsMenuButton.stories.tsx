/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';

import { store } from '../../../../src/store';
import { useDefaultMapSettings } from '../../../utils/defaultSettings';
import * as exampleLayers from '../../../utils/exampleLayers';
import { MapViewConnect } from '../../../../src/components/MapView';
import PlayButtonConnect from '../../../../src/components/TimeSlider/TimeSliderButtons/PlayButton/PlayButtonConnect';
import OptionsMenuButtonConnect from '../../../../src/components/TimeSlider/TimeSliderButtons/OptionsMenuButton/OptionsMenuButtonConnect';

interface OptionsMenuButtonConnectProps {
  mapId: string;
}

const OptionsMenuButtonConnectDemo: React.FC<OptionsMenuButtonConnectProps> = ({
  mapId,
}: OptionsMenuButtonConnectProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [exampleLayers.radarLayer],
    baseLayers: [exampleLayers.baseLayerGrey],
  });
  return (
    <ThemeProvider theme={GWTheme}>
      <div style={{ height: '100vh' }}>
        <Provider store={store}>
          <div style={{ height: '100vh' }}>
            <MapViewConnect mapId={mapId} />
          </div>
          <div
            style={{
              position: 'absolute',
              left: '10px',
              bottom: '135px',
              zIndex: 10000,
              right: '200px',
            }}
          >
            <OptionsMenuButtonConnect mapId={mapId} />
          </div>
          <div
            style={{
              position: 'absolute',
              left: '35px',
              bottom: '135px',
              zIndex: 10000,
              right: '200px',
            }}
          >
            <PlayButtonConnect mapId={mapId} />
          </div>
        </Provider>
      </div>
    </ThemeProvider>
  );
};

export const OptionsMenuButtonDemo: React.FC = () => (
  <Provider store={store}>
    <OptionsMenuButtonConnectDemo mapId="mapid_1" />
  </Provider>
);
