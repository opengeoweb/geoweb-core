/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';
import TimeStepButtonConnect from '../../../../src/components/TimeSlider/TimeSliderButtons/TimeStepButton/TimeStepButtonConnect';
import { store } from '../../../../src/store';
import { MapViewConnect } from '../../../../src/components/MapView';
import { useDefaultMapSettings } from '../../../utils/defaultSettings';

const useLayerSelectStyles = makeStyles({
  heightComps: { height: '100vh' },
});

interface TimeStepDemoConnectProps {
  mapId: string;
}

const TimeStepButtonConnectDemo: React.FC<TimeStepDemoConnectProps> = ({
  mapId,
}: TimeStepDemoConnectProps) => {
  const classes = useLayerSelectStyles();

  useDefaultMapSettings({ mapId });

  return (
    <ThemeProvider theme={GWTheme}>
      <div>
        <Provider store={store}>
          <div className={classes.heightComps}>
            <div
              style={{
                position: 'absolute',
                left: '0px',
                top: 'calc(100% - 150px)',
                zIndex: 1000,
                height: '100%',
                width: '100%',
              }}
            >
              <TimeStepButtonConnect mapId={mapId} />
            </div>
            <MapViewConnect mapId={mapId} />
          </div>
        </Provider>
      </div>
    </ThemeProvider>
  );
};

export const TimeStepButtonDemo: React.FC = () => (
  <Provider store={store}>
    <TimeStepButtonConnectDemo mapId="map_id_1" />
  </Provider>
);
