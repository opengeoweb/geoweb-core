/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { Provider } from 'react-redux';
import { ThemeProvider } from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';
import { MapViewConnect } from '../../../../src/components/MapView';
import TimeSliderScaleSliderConnect from '../../../../src/components/TimeSlider/TimeSliderScaleSlider/TimeSliderScaleSliderConnect';
import { store } from '../../../../src/store';

interface TimeSliderScaleDemoConnectProps {
  mapId: string;
}

const TimeSliderScaleConnectDemo: React.FC<TimeSliderScaleDemoConnectProps> = ({
  mapId,
}: TimeSliderScaleDemoConnectProps) => {
  return (
    <ThemeProvider theme={GWTheme}>
      <div>
        <Provider store={store}>
          <div>
            <div
              style={{
                position: 'absolute',
                left: '0px',
                top: 'calc(100% - 150px)',
                zIndex: 1000,
                height: '100%',
                width: '100%',
              }}
            >
              <TimeSliderScaleSliderConnect mapId={mapId} />
            </div>
            <MapViewConnect mapId={mapId} />
          </div>
        </Provider>
      </div>
    </ThemeProvider>
  );
};

export const TimeSliderScaleDemo: React.FC = () => (
  <Provider store={store}>
    <TimeSliderScaleConnectDemo mapId="map_id_1" />
  </Provider>
);
