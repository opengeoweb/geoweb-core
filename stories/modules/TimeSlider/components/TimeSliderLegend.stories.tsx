/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { GWTheme } from '@opengeoweb/theme';
import { Box, ThemeProvider } from '@material-ui/core';
import moment from 'moment';
import TimeSliderLegend from '../../../../src/components/TimeSlider/TimeSliderLegend/TimeSliderLegend';

const commonStoryProps = {
  name: 'time',
  units: 'ISO8601',
  currentValue: '2020-10-30T18:00:00.000Z',
  centerTime: moment.utc('2020-10-30T18:00:00.000Z').unix(),
};

export const TimeSliderLegendDemo = (): React.ReactElement => {
  return (
    <ThemeProvider theme={GWTheme}>
      <>
        <Box m={2}>
          <TimeSliderLegend
            {...commonStoryProps}
            secondsPerPx={1}
            scale={1}
            currentTime={moment
              .utc('2020-10-30')
              .hours(18)
              .unix()}
          />
        </Box>
        <Box m={2}>
          <TimeSliderLegend
            {...commonStoryProps}
            secondsPerPx={6}
            scale={5}
            currentTime={moment
              .utc('2020-10-30')
              .hours(19)
              .unix()}
          />
        </Box>
        <Box m={2}>
          <TimeSliderLegend
            {...commonStoryProps}
            scale={180}
            secondsPerPx={200}
            currentTime={moment
              .utc('2020-10-31')
              .hours(19)
              .unix()}
          />
        </Box>
        <Box m={2}>
          <TimeSliderLegend
            {...commonStoryProps}
            scale={10080}
            secondsPerPx={360}
            currentTime={moment
              .utc('2020-10-29')
              .hours(19)
              .unix()}
          />
        </Box>
      </>
    </ThemeProvider>
  );
};

TimeSliderLegendDemo.storyName = 'Time Slider Legend (takeSnapshot)';
