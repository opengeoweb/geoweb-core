/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@material-ui/core';
import { GWTheme } from '@opengeoweb/theme';

import { store } from '../../../src/store';
import { useDefaultMapSettings } from '../../utils/defaultSettings';
import { MapViewConnect } from '../../../src/components/MapView';
import { radarLayer } from '../../utils/exampleLayers';
import TimeSliderConnect from '../../../src/components/TimeSlider/TimeSliderConnect';
import { baseLayerGrey, overLayer } from '../../../src/utils/testLayers';

interface ExampleComponentProps {
  mapId: string;
}

const ExampleComponent: React.FC<ExampleComponentProps> = ({
  mapId,
}: ExampleComponentProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [
      {
        ...radarLayer,
        id: `radar-${mapId}`,
      },
    ],
    baseLayers: [
      {
        ...baseLayerGrey,
        id: `baseGrey-${mapId}`,
      },
      overLayer,
    ],
  });
  return (
    <div style={{ height: '100vh' }}>
      <div style={{ height: '100vh' }}>
        <MapViewConnect mapId={mapId} />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 10,
          right: '0px',
        }}
      >
        <TimeSliderConnect mapId={mapId} />
      </div>
    </div>
  );
};

export const DemoTimeSliderConnect: React.FC = () => (
  <ThemeProvider theme={GWTheme}>
    <Provider store={store}>
      <ExampleComponent mapId="mapid_1" />
    </Provider>
  </ThemeProvider>
);
