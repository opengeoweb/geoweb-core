/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

export * from './components/AutoUpdateButton.stories';
export * from './components/LoopButton.stories';
export * from './components/OptionsMenuButton.stories';
export * from './components/PlayButton.stories';
export * from './components/SpeedButton.stories';
export * from './components/TimeSliderButtons.stories';
export * from './components/TimeSliderLegend.stories';
export * from './components/TimeSliderRail.stories';
export * from './components/TimeSliderScaleSlider.stories';
export * from './components/TimeStepButton.stories';

export default { title: 'modules/timeslider/components' };
