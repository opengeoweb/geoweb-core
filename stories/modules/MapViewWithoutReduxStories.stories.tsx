/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { MapView } from '../../src';

export * from './MapViewWithoutRedux/MapStatic.stories';
export * from './MapViewWithoutRedux/MapViewError.stories';
export * from './MapViewWithoutRedux/MapViewMapCursor.stories';
export * from './MapViewWithoutRedux/MapViewPassive.stories';
export * from './MapViewWithoutRedux/MapViewRadarAnimation.stories';
export * from './MapViewWithoutRedux/MapViewRadarData.stories';
export * from './MapViewWithoutRedux/MapViewSetProjectionBBox.stories';
export * from './MapViewWithoutRedux/MapViewSetProjectionBBoxLocalState.stories';

export default { title: 'modules/MapView (without Redux)', component: MapView };
