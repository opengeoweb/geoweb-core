/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import DraggableDialog from '../../../src/components/DraggableDialog/DraggableDialog';

const useStyles = makeStyles(() => ({
  legendDialog: {
    height: '100vh',
  },
}));

export default {
  title: 'modules/DraggableDialog',
};

export const SimpleDraggableDialog = (): React.ReactElement => {
  const classes = useStyles();
  return (
    <div className={classes.legendDialog}>
      <DraggableDialog name="My Dialog" showTitle>
        <p>This is a draggable dialog with some text content.</p>
      </DraggableDialog>
    </div>
  );
};
