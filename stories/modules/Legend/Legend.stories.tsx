/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { registerWMLayer } from '../../../src';
import { Legend } from '../../../src/components/Legend';
import { WMLayer } from '../../../src/geoweb-webmap';
import {
  defaultReduxLayerRadarKNMI,
  makeGeoservicesRadarLayer,
} from '../../../src/utils/defaultTestSettings';

export const SimpleLegend = (): React.ReactElement => {
  const radarLayer = makeGeoservicesRadarLayer();
  radarLayer.legendGraphic =
    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-blue-transparent/nearest';

  registerWMLayer(radarLayer, radarLayer.id);

  return <Legend layer={radarLayer} />;
};

export const SimpleLegendWithMultiDimensions = (): React.ReactElement => {
  const storyLayer = {
    ...makeGeoservicesRadarLayer(),
    dimensions: [
      {
        name: 'flight level',
        units: 'hft',
        currentValue: '625',
        values: '25,325,625',
      },
      {
        name: 'elevation',
        units: 'meters',
        currentValue: '9000',
        values: '1000,5000,9000',
      },
      {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-04-14T14:40:00Z',
      },
    ],
  };
  const wmStoryLayer = new WMLayer(storyLayer);
  registerWMLayer(wmStoryLayer, storyLayer.id);
  wmStoryLayer.legendGraphic =
    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest';
  return <Legend layer={storyLayer} />;
};

export const SimpleLegendWithoutDimensionsOrGraphic = (): React.ReactElement => {
  const storyLayer = { ...defaultReduxLayerRadarKNMI, dimensions: [] };
  registerWMLayer(new WMLayer(storyLayer), defaultReduxLayerRadarKNMI.id);
  return <Legend layer={storyLayer} />;
};
