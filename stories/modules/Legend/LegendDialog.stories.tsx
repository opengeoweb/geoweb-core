/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { LegendDialog } from '../../../src/components/Legend';
import { LayerType } from '../../../src/store/mapStore/types';
import { registerWMLayer } from '../../../src';
import { WMLayer } from '../../../src/geoweb-webmap';

const useStyles = makeStyles(() => ({
  legendDialog: {
    height: '100vh',
  },
}));

export const LegendDialogWithoutLayers = (): React.ReactElement => {
  const classes = useStyles();
  return (
    <div className={classes.legendDialog}>
      <LegendDialog layers={[]} />
    </div>
  );
};

export const LegendDialogBigContent = (): React.ReactElement => {
  const storyLayer = {
    service: 'https://testservice',
    name: 'DUMMY_LAYER_WITH_EXTRA_EXTRA_EXTRA_LONG_NAME',
    title: 'DUMMY_LAYER_WITH_EXTRA_EXTRA_EXTRA_LONG_NAME',
    format: 'image/png',
    style: 'knmiradar/nearest',
    enabled: true,
    layerType: LayerType.mapLayer,
    id: 'dummy-layer-id-1',
    dimensions: [
      {
        name: 'time',
        units: 'ISO8601',
        currentValue:
          '2020-03-13T14:40:00Z,2020-03-13T14:40:00Z,2020-03-13T14:40:00Z,2020-03-13T14:40:00Z,2020-03-13T14:40:00Z,2020-03-13T14:40:00Z,2020-03-13T14:40:00Z,2020-03-13T14:40:00Z,2020-03-13T14:40:00Z,2020-03-13T14:40:00Z',
      },
    ],
  };
  const wmStoryLayer = new WMLayer(storyLayer);
  registerWMLayer(wmStoryLayer, storyLayer.id);
  wmStoryLayer.legendGraphic = 'https://dummyimage.com/600x400/000/fff';
  const storyLayer2 = {
    service: 'https://testservice',
    name: 'RAD_NL25_PCP_CM',
    title: 'RAD_NL25_PCP_CM',
    format: 'image/png',
    style: 'knmiradar/nearest',
    enabled: true,
    layerType: LayerType.mapLayer,
    id: 'dummy-layer-id-2',
    dimensions: [
      {
        name: 'flight level',
        units: 'hft',
        currentValue: '625',
        values: '25,325,625',
      },
      {
        name: 'elevation',
        units: 'meters',
        currentValue: '9000',
        values: '1000,5000,9000',
      },
      {
        name: 'time',
        units: 'ISO8601',
        currentValue:
          '2020-03-13T14:40:00Z,2021-04-13T14:40:00Z,2021-04-13T14:40:00Z,2021-04-13T14:40:00Z,2021-04-13T14:40:00Z,2021-04-13T14:40:00Z,2021-04-13T14:40:00Z,2021-04-13T14:40:00Z,2021-04-13T14:40:00Z,2021-04-13T14:40:00Z',
      },
    ],
  };
  const wmStoryLayer2 = new WMLayer(storyLayer2);
  registerWMLayer(wmStoryLayer2, storyLayer2.id);
  wmStoryLayer2.legendGraphic =
    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-blue-transparent/nearest&layers=RAD_NL25_PCP_CM&';

  return <LegendDialog layers={[storyLayer, storyLayer2]} />;
};
