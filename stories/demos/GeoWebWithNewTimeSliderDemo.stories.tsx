/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { GWTheme } from '@opengeoweb/theme';
import { ButtonGroup, Button, ThemeProvider } from '@material-ui/core';
import { AppStore } from '../../src/types/types';
import * as mapActions from '../../src/store/mapStore/actions';
import { MapViewConnect } from '../../src/components/MapView';
import * as mapSelectors from '../../src/store/mapStore/selectors';
import { store } from '../../src/store';
import { useDefaultMapSettings } from '../utils/defaultSettings';
import { defaultPreloadedServices } from '../../src/components/WMSLoader/services';
import { LegendConnect } from '../../src/components/Legend';
import {
  msgCppLayer,
  radarLayer,
  harmonieAirTemperature,
  harmoniePressure,
  harmoniePrecipitation,
  overLayer,
  baseLayerGrey,
  metNorwayWind1,
  metNorwayWind2,
  metNorwayWind3,
} from '../utils/exampleLayers';
import {
  baseLayerOpenStreetMapNL,
  baseLayerWorldMap,
} from '../../src/utils/testLayers';
import TimeSliderConnect from '../../src/components/TimeSlider/TimeSliderConnect';
import LayerManagerButton from '../../src/components/TimeSlider/LayerManager/LayerManager';
import MultiDimensionSelectConnect from '../../src/components/MultiDimensionSelect/MultiDimensionSelectConnect';

interface SimpleGeoWebPresetsProps {
  setLayers: typeof mapActions.setLayers;
  mapId: string;
}

const enhance = connect(
  (state: AppStore, props: SimpleGeoWebPresetsProps) => ({
    layers: mapSelectors.getMapLayers(state, props.mapId),
  }),
  {
    setLayers: mapActions.setLayers,
  },
);

const SimpleGeoWebPresets: React.FC<SimpleGeoWebPresetsProps> = ({
  setLayers,
  mapId,
}: SimpleGeoWebPresetsProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [{ ...radarLayer, id: `radar-${mapId}` }],
    baseLayers: [{ ...baseLayerGrey, id: `baseGrey-${mapId}` }, overLayer],
  });

  const presetHarmonie = {
    layers: [harmonieAirTemperature],
  };
  const presetRadar = {
    layers: [{ ...radarLayer, id: `radar-${mapId}-2` }],
  };
  const presetHarmoniePrecipAndObs = {
    layers: [
      harmoniePrecipitation,
      { ...radarLayer, id: `radar-${mapId}-3` },
      harmoniePressure,
    ],
  };

  const presetRadarMSGCPP = {
    layers: [{ ...radarLayer, id: `radar-${mapId}-4` }, msgCppLayer],
  };

  const presetWind = {
    layers: [metNorwayWind1, metNorwayWind2, metNorwayWind3],
  };

  return (
    <ButtonGroup
      variant="contained"
      color="primary"
      aria-label="outlined primary button group"
    >
      <Button
        onClick={(): void => {
          setLayers({
            layers: presetHarmonie.layers,
            mapId,
          });
        }}
      >
        {' '}
        Harmonie
      </Button>
      <Button
        onClick={(): void => {
          setLayers({
            layers: presetRadar.layers,
            mapId,
          });
        }}
      >
        Radar
      </Button>
      <Button
        onClick={(): void => {
          setLayers({
            layers: presetHarmoniePrecipAndObs.layers,
            mapId,
          });
        }}
      >
        Precip + Obs
      </Button>
      <Button
        onClick={(): void => {
          setLayers({
            layers: presetRadarMSGCPP.layers,
            mapId,
          });
        }}
      >
        Radar + MSGCPP
      </Button>
      <Button
        onClick={(): void => {
          setLayers({
            layers: presetWind.layers,
            mapId,
          });
        }}
      >
        Wind
      </Button>
    </ButtonGroup>
  );
};
const ConnectedSimpleGeoWebPresets = enhance(SimpleGeoWebPresets);

export const GeoWebWithNewTimeSliderDemo = (): React.ReactElement => {
  const mapId = 'mapid_1';
  const preloadedAvailableBaseLayers = [
    { ...baseLayerGrey, id: `baseGrey-${mapId}` },
    baseLayerOpenStreetMapNL,
    baseLayerWorldMap,
  ];
  return (
    <ThemeProvider theme={GWTheme}>
      <Provider store={store}>
        <div style={{ height: '100vh' }}>
          <MultiDimensionSelectConnect mapId={mapId} />
          <LegendConnect mapId={mapId} />
          <div
            style={{
              position: 'absolute',
              left: '0px',
              bottom: '0px',
              zIndex: 1000,
              width: '100%',
            }}
          >
            <LayerManagerButton
              mapId={mapId}
              preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
              preloadedBaseServices={defaultPreloadedServices}
              preloadedMapServices={defaultPreloadedServices}
            />
            <TimeSliderConnect mapId={mapId} />
          </div>
          <MapViewConnect mapId={mapId} />
        </div>
        <div
          style={{
            position: 'absolute',
            left: '10px',
            top: '10px',
            zIndex: 10000,
          }}
        >
          <ConnectedSimpleGeoWebPresets mapId={mapId} />
        </div>
      </Provider>
    </ThemeProvider>
  );
};
