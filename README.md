[![pipeline status](https://gitlab.com/opengeoweb/geoweb-core/badges/master/pipeline.svg)](https://gitlab.com/opengeoweb/geoweb-core/-/commits/master)
[![coverage report](https://gitlab.com/opengeoweb/geoweb-core/badges/master/coverage.svg)](https://gitlab.com/opengeoweb/geoweb-core/-/commits/master)
[![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/geoweb-core/raw/master/package.json)](https://gitlab.com/opengeoweb/geoweb-core/-/tags)

# geoweb-core

- [geoweb-core](#geoweb-core)
  - [Install environment](#install-environment)
  - [Development](#development)
    - [Application Structure](#application-structure)
    - [Storybook](#storybook)
      - [Inspecting the components with storybook](#inspecting-the-components-with-storybook)
      - [Writing a new story](#writing-a-new-story)
      - [Deploying a static version of your storybook](#deploying-a-static-version-of-your-storybook)
    - [Managing packages](#managing-packages)
      - [Removing extraneous packages](#removing-extraneous-packages)
      - [Adding a new dependency](#adding-a-new-dependency)
      - [Updating an existing dependency](#updating-an-existing-dependency)
      - [Checking for vulnerabilities](#checking-for-vulnerabilities)
    - [Tests](#tests)
    - [Image snapshot testing](#image-snapshot-testing)
    - [Testing the package in another project](#testing-the-package-in-another-project)
    - [Bundling, packaging and publishing to NPM](#bundling-packaging-and-publishing-to-npm)
    - [Setup prettier with vscode](#setup-prettier-with-vscode)
    - [Code guidelines](#code-guidelines)
      - [Naming conventions](#naming-conventions)
      - [Ignoring ESLint rules](#ignoring-eslint-rules)
    - [Documentation](#documentation)
  - [Troubleshooting](#troubleshooting)
    - [VS Code not redeploying on save](#vs-code-not-redeploying-on-save)
  - [License](#license)
  - [GeoWeb naming conventions](#geoweb-naming-conventions)

## Install environment

To run and develop geoweb-core you need npm. The easiest way to install npm is via nvm. Please visit http://nvm.sh/ and follow the instructions. When nvm is installed, please do the following command (this will install node version 12):

```
nvm install 12
```

After that you need to install its dependencies, do inside the geoweb-core folder:

```
npm ci
```

To start the storybook do:

```
npm run storybook
```

## Development

### Application Structure

The application structure presented in this boilerplate is fractal, where functionality is grouped primarily by feature rather than file type.

```
> src
|    > utils
|    > types
|    > assets
|    > store
|    |    > mapStore
|    |    |    > map
|    |    |    |    reducer.ts
|    |    |    |    action.ts
|    |    |    |    selector.ts
|    |    |    |    type.ts
|    |    |    > layer
|    |    |    |    reducer.ts
|    |    |    |    action.ts
|    |    |    |    selector.ts
|    |    |    |    type.ts
|    |    > screenStore
|    |    |    > ...
|    |    |    |    reducer.ts
|    |    |    |    action.ts
|    |    |    |    selector.ts
|    |    |    |    type.ts
|    > components
|    |    > TimeSelect
|    |    |    TimeSelect.tsx
|    |    |    TimeSelect.spec.ts
|    |    |    TimeSelect.utils.ts
|    |    |    TimeSelect.type.ts
|    |    |    index.ts
|    |    |    TimeSelectConnect.tsx
|    |    |    TimeSelectConnect.spec.tsx
|    |    |    > PlayPauseButton
|    |    |    |    PlayPauseButton.tsx
|    |    |    |    PlayPauseButton.spec.ts
|    |    |    |    PlayPauseButtonConnect.tsx
|    |    |    |    PlayPauseButtonConnect.spec.tsx
|    |    |    > TimeSlider
|    |    |    |    TimeSlider.tsx
|    |    |    |    TimeSlider.spec.ts
|    |    |    |    TimeSliderConnect.tsx
|    |    |    |    TimeSliderConnect.spec.tsx
|    |    > MapView
|    |    |    …
|    |    > LayerSelect
|    |    |    ....
```

### Storybook

#### Inspecting the components with storybook

Storybook can be used to view the components.
If you run `npm run storybook` a storybook server will start in which you can see all the components for which stories
are written.

#### Writing a new story

Stories live in the stories folder.
Here is the documentation on the syntax for adding new stories:
https://storybook.js.org/docs/basics/writing-stories/

#### Deploying a static version of your storybook

The storybook can be compiled to a static version via `npm run build-storybook`. The static contents are then placed in the folder storybook-geoweb-core.

### Managing packages

Follow the instructions below for dependency management. Please refer to [the documentation](https://docs.npmjs.com/cli-documentation/) for more information on any of the specific commands.

#### Removing extraneous packages

During development, we may end up with packages that have been installed but are not actually being used. In order to remove them, run `npm prune`.

#### Adding a new dependency

Adding a new dependency can be done following these steps:

1. `npm install <package-name>` or `npm install <package-name> --save-dev`
2. commit the updated files package.json and package-lock.json

Make sure to add the package as a dev depencency when it's only used for development purposes (like storybook or linting).

#### Updating an existing dependency

To see a list of which packages could need updating, run `npm outdated -l`.
A red package name means there’s a newer version matching the semver requirements in the package.json, so it can be updated. Yellow indicates that there’s a newer version higher than the semver requirements in the package.json (usually a new major version), so proceed with caution.

To update a single package to its latest version that matches the version required in package.json (red in the list), run `npm update <package-name>`, run `npm install` and commit the updated package-lock.json file.

To update all packages at once (that are red in the list), run `npm update`, run `npm install` and commit the updated package-lock.json file.

To update a single package to a version higher than the required version in package.json (yellow in the list):

1. check if there are any breaking changes to be aware of in the new version
2. `npm install <package-name>@<version>` or `npm install <package-name>@<version> --save-dev`
3. run `npm install`
4. commit the updated files package.json and package-lock.json
5. update the code related to breaking changes

After any version change, make sure to test if everything still works.

#### Checking for vulnerabilities

To scan the project for any known vulnerabilities in existing dependencies, you can run `npm audit`. To fix them:

1. `npm audit fix`
2. commit the updated package-lock.json
3. test if everything still works

### Tests

Tests can be run by the following command:
`npm test`
This will run all the tests specified in the stories without starting a browser.

In order to see a coverage report please use:
`npm run test:coverage`

### Image snapshot testing

Image snapshot tests verify that the visual appearance of a rendered view remains unchanged. This is specifically useful for components that are hard to unit test otherwise, such as canvas components. If a view is deliberately changed, new snapshots must be created. Up-to-date snapshots should be committed to source control.

#### Add a new snapshot test

Image snapshot tests are automatically created for all Storybook stories matching `storyNameRegex` at `stories/Storyshots.spec.ts`. The current regex filters on `takeSnapshot`. So to add a story to the snapshot tests, simply add `(takeSnapshot)` to it's storyName and run the tests. If no snapshot exists, a new one will be added.

#### Running snapshot tests and updating snapshots locally

1. You need to have [docker](https://docs.docker.com/get-docker/) installed and running.
2. Start Chromium by running: `npm run start-chromium`. (This will start a docker container with chromium, to run snapshot tests in. We need this to make sure everyone gets the same snapshot results.)
3. Run the snapshot tests: `npm run test:image-snap`. This will first create a new static storybook build and then run the tests.
4. If a snapshot test fails, you can find and inspect the differences in `stories/__image_snapshots__/__diff_output__/`.
5. To update the snapshots, run `npm run test:image-snap-update`. Snapshots are saved under `stories/__image_snapshots__/`. Make sure to commit the new snapshots.
6. Stop Chromium by running: `npm run stop-chromium`.

### Testing the package in another project

To test the package in another project without publishing it, you can follow these steps:

In `geoweb-core`:

1. `npm run package`

In `yourproject`:

1. `cd <path/to/yourproject>`
2. `npm install <absolute path/to/tar/created/by/npm pack>`

### Bundling, packaging and publishing to NPM

We use Semantic Versioning, see the [documentation](https://semver.org) for details.

1. Decide how to bump the version, is it a major, minor or patch version?
2. Create a new branch based off master and ensure there is an upstream branch BEFORE going to the next step
3. Update the version by running `npm version major|minor|patch` on the new branch
4. Merge the branch into master
5. Go to the gitlab pipeline of master and run the publish stage
6. Check for the new version on [npm](https://www.npmjs.com/package/@opengeoweb/core) and [gitlab](https://gitlab.com/opengeoweb/geoweb-core/-/tags)
7. The release tag was created and pushed automatically. Make sure to add a description of changes in the [Release notes](https://gitlab.com/opengeoweb/geoweb-core/-/releases) for the tag.

### Setup prettier with vscode

1. Download the prettier extension for vscode (https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
2. Update your settings: File->Preferences->Settings and open the settings.json as text
3. Paste the code below at the right place in settings.json

```
   "editor.defaultFormatter": "esbenp.prettier-vscode",
   "editor.formatOnSave": true,
   "[javascript]": {
     "editor.formatOnSave": true
   },
   "[javascriptreact]": {
     "editor.formatOnSave": true
   },
   "[typescript]": {
     "editor.formatOnSave": true
   },
   "[typescriptreact]": {
     "editor.formatOnSave": true
   },
```

To run it on all code do `npm run prettier`

After this, you will get a report on the console, where the files presenting formatting problems that have been fixed be highlighted.

Please note that if you always use VS Code to edit files, they will always adhere to the prettier rules (that can be found at the project root, in a file called .prettierrc).

### Code guidelines

#### Naming conventions

Regarding our file structure:

- In general, folders shall be named using camelCase
- Files and folders containing components shall be named using PascalCase
- Files with genetic names (such as 'reducer.ts', 'index.ts') shall be named using camelCase.

#### Ignoring ESLint rules

If, for some reason, ESLint needs to be ignored, a reason must be stated. For example:
`// eslint-disable-next-line`
would be considered incomplete. Instead, add the reason for ignoring the line:
`// eslint-disable-next-line no-unused-vars`

### Documentation

It is possible to automatically generate documentation from the source code using [TypeDoc](http://typedoc.org/).

In order to do so, please use `npm run docs`.
Please note that nothing will be created if the code contains errors. Otherwise, a new `docs` folder will be created under the root folder.

Documentation is also available online [here](https://opengeoweb.gitlab.io/geoweb-core/docs/).

## Troubleshooting

### VS Code not redeploying on save

When developing with VS Code, you might notice at some point that your application is not being redeployed when making changes. Please refer to the documentation for solutions:

["Visual Studio Code is unable to watch for file changes in this large workspace" (error enospc)][https://code.visualstudio.com/docs/setup/linux#\_visual-studio-code-is-unable-to-watch-for-file-changes-in-this-large-workspace-error-enospc]

## License

This project is licensed under the terms of the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license.

Every file in this project has an header that specifies the licence and copyright. It is possible to add/remove the licence header using the following commands:

- `npm run licence:add` => it adds to all possible files the licence header if the file has not an header with the same content of the LICENCE file. This action can require a manual check in the case in which the file contains a wrong header (i.g. a simple word is missed), because it just adds another header without removing the wrong one.
- `npm run licence:remove` => it removes the specific header from all files that contain an header with the same content of the LICENCE file. This action can require a manual check in the case in which the file contains a wrong header (i.g. a simple word is missed), because it just removes the correct header and not the wrong one.

## GeoWeb naming conventions

To ensure names are properly used, please refer below to the naming conventions as adopted in GeoWeb
![](./namingConventions.png)
