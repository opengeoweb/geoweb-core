export const parameters = {
  options: {
    storySort: (a, b) => a[1].id.localeCompare(b[1].id),
  },
};
